package cwi.basic.resourse;

public class CommonException extends Exception{
    private String productCode;
    private String exceptionCode;  
    private Object[] placeholderValues;
    
    public CommonException(String productCode,String exceptionCode){
        setProductCode(productCode);
        setExceptionCode(exceptionCode);
    }
    
    public CommonException(Throwable cause,String productCode,String exceptionCode){
        super(cause);
        setProductCode(productCode);
        setExceptionCode(exceptionCode);
    }
    
    public CommonException(Throwable cause,String exceptionCode){
        super(cause);
        setExceptionCode(exceptionCode);
    }
    
    public CommonException(String exceptionCode){
        setExceptionCode(exceptionCode);
    } 
    
    public CommonException(String productCode,String exceptionCode,Object[] placeholderValues){
    	this(productCode,exceptionCode);
    	setPlaceholderValues(placeholderValues);
    }
    
    public CommonException(Throwable cause,String productCode,String exceptionCode,Object[] placeholderValues){
    	this(cause,productCode,exceptionCode);
    	setPlaceholderValues(placeholderValues);
    }
    
    public CommonException(Throwable cause,String exceptionCode,Object[] placeholderValues){
    	this(cause,exceptionCode);
    	setPlaceholderValues(placeholderValues);
    }
    
    public CommonException(String exceptionCode,Object[] placeholderValues){
    	this(exceptionCode);
    	setPlaceholderValues(placeholderValues);
    } 
  

    public String getExceptionCode() {
        return exceptionCode;
    }

    public void setExceptionCode(String exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    public String getProductCode() {
        return productCode;
    }

    private void setProductCode(String productCode) {
        this.productCode = productCode;
    }

	public Object[] getPlaceholderValues() {
		return placeholderValues;
	}

	public void setPlaceholderValues(Object[] placeholderValues) {
		this.placeholderValues = placeholderValues;
	}    
}
