package cwi.basic.resourse;

import java.security.MessageDigest;

import sun.misc.BASE64Encoder;

public final class PasswordService{
	private static String ALGORITHM="SHA1";
	
	private PasswordService() {
		
	}
	
	public static synchronized String encrypt(String value)throws Exception {
		MessageDigest md = null;
		md = MessageDigest.getInstance(PasswordService.ALGORITHM);
		md.update(value.getBytes("UTF-8"));
		byte raw[] = md.digest();
		return (new BASE64Encoder()).encode(raw);
	}
	
	public synchronized static String decrypt(String key)throws Exception {
		String fis[]=new String[((key.length()/2)%2==0)?key.length()/4:(key.length()/4)+1];
		String sec[]=new String[key.length()/4];
		int count=0;
		for(int i=0;i<(((key.length()/2)%2==0)?key.length()/2:(key.length()/2)+1);i++) {
			fis[count++]=new StringBuilder().append(key.charAt(i)).append(key.charAt(++i)).toString();
		}
		count=0;
		for(int i=(((key.length()/2)%2==0)?key.length()/2:(key.length()/2)+1);i<key.length();i++) {
			sec[count++]=new StringBuilder().append(key.charAt(i)).append(key.charAt(++i)).toString();
		}
		key="";
		for(int i=0;i<fis.length;i++) {
			key+=(char)Integer.parseInt(fis[i],16);
			if(i<sec.length)
				key+=(char)Integer.parseInt(sec[i],16);
		}
		return key;
	}
}
