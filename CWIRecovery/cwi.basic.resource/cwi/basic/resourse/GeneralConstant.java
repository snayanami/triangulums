package cwi.basic.resourse;

public class GeneralConstant {
	/*
	 *	Event Delimiter in tree node  
	 */
	public static final String FIELD_DELIMITER=":";
	
	/*
	 * 	Common system date format
	 */
	public static final String DATE_FORMAT="dd/MM/yyyy";
	
	/*
	 * 	Common temp directory for file operations
	 */
	public static final String COMMON_TEMP_DOCUMENT_FOLDER="userImages";
	
	/*
	 * 	file split size when upload files to DB 
	 */
	public static final int FILE_SPLIT_SIZE=3000;
	
	/*
	 * Date format as String 
	 */
	public static final String DATE_FORMAT_STR="dd-MM-yyyy";
}
