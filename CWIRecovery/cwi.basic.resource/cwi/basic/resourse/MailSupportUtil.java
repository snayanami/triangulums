package cwi.basic.resourse;

import java.io.InputStream;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.MimeMessage;

import org.springframework.core.io.InputStreamResource;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

public class MailSupportUtil {
	private String systemMailAddress;
	private MailSender mailSender;

	public String getSystemMailAddress() {
		return systemMailAddress;
	}

	public void setSystemMailAddress(String systemMailAddress) {
		this.systemMailAddress = systemMailAddress;
	}

	public MailSender getMailSender() {
		return mailSender;
	}

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void createEmail(UserSessionConfig userConfig, final String to, final String subject, final String messageText ) throws BasicException {
		createEmail(userConfig, getSystemMailAddress(), to, null, null, subject, messageText, null);
	}
	
	public void createEmail(UserSessionConfig userConfig, final String to, final String subject, final String messageText, Map<String, InputStream> attachments) throws BasicException {
		createEmail(userConfig, getSystemMailAddress(), to, null, null, subject, messageText, attachments);
	}
	
	public void createEmail(UserSessionConfig userConfig, final String from, final String to, final String subject, final String messageText) throws BasicException {
		createEmail(userConfig, from, to,null, from, subject, messageText, null);
	}
	
	public void createEmail(UserSessionConfig userConfig, final String from, final String to, final String subject, final String messageText, Map<String, InputStream> attachments) throws BasicException {
		createEmail(userConfig, from, to,null, from, subject, messageText, attachments);
	}
	
	public void createEmail(UserSessionConfig userConfig, final String to, final String[] bcc, final String subject, final String messageText) throws BasicException {
		createEmail(userConfig, getSystemMailAddress(), to, bcc, null, subject, messageText, null);
	}
	
	public void createEmail(UserSessionConfig userConfig, final String to, final String[] bcc, final String subject, final String messageText, Map<String, InputStream> attachments) throws BasicException {
		createEmail(userConfig, getSystemMailAddress(), to, bcc, null, subject, messageText, attachments);
	}
	
	public void createEmail(UserSessionConfig userConfig, final String from, final String to, final String[] bcc, final String replyTo, final String subject, final String messageText, Map<String, InputStream> attachments) throws BasicException {
		try {
			// Validation
			if(to==null || !isValidateEmailAddress(to))
				throw new BasicDataAccessException("Invalid Email address.");
			
			if(replyTo!=null && !isValidateEmailAddress(replyTo))
				throw new BasicDataAccessException("Invalid Email address.");
			
			// Create New E-mail Message
			MimeMessage message = ((JavaMailSenderImpl) getMailSender()).createMimeMessage();
			// Create Spring E-mail Message Helper
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			// Set Recipient
			helper.setTo(to);
			// Set E-mail Message text
			helper.setText(messageText, true);
			// Set BCC (optional)
			if (bcc != null && bcc.length > 0)
				helper.setBcc(bcc);
			// Set Subject
			helper.setSubject(subject);
			// Set From E-mail address (default system email address)
			helper.setFrom((from!=null)?from:getSystemMailAddress());
			// Set Attachments (optional)
			if (attachments != null) {
				for (String attachmentName : attachments.keySet()) {
					helper.addAttachment(attachmentName, new InputStreamResource(attachments.get(attachmentName)));
				}
			}
			// Set ReplyTo (Optional)
			if (replyTo != null && !"".equals(replyTo))
				helper.setReplyTo(replyTo);
			
			// Finally Send message
			((JavaMailSenderImpl) getMailSender()).send(message);
		} catch (Exception e) {
			// Throw Error if error occurred during sending E-Mail message
			throw new BasicDataAccessException(e.getLocalizedMessage());
		}
	}
	
	public static boolean isValidateEmailAddress(String emailAddress) {
		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher m = p.matcher(emailAddress);
		return m.matches();
	}
}