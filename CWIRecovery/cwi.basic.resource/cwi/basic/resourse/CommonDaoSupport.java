package cwi.basic.resourse;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cwi.system.domain.BranchDayStatus;

public class CommonDaoSupport extends HibernateDaoSupport {
	protected String getCreatedTime() {
		SimpleDateFormat stf = new SimpleDateFormat( "hh:mm:ss a" );
		return stf.format(new Date());
	}
	
	protected double roundValue(double value){
		if(value == Double.NaN)
			value=0;
		DecimalFormat dfmt=new DecimalFormat();
		dfmt.setMaximumFractionDigits(2);
		dfmt.setMinimumFractionDigits(2);
		dfmt.setGroupingUsed(false);
		String formattedValue=dfmt.format(value);
		if(formattedValue.contains(".")) {
			String [] data=formattedValue.split("\\.");
			if(data[1].length()>2)
				data[1]=data[1].substring(0, 2);
			return Double.valueOf(data[0]+"."+data[1]);
		}
		return Double.valueOf(dfmt.format(value));
	}
	
	protected double roundUp(double value) {
		Number number=value;
		DecimalFormat decimalFormat=new DecimalFormat();
		decimalFormat.setMinimumFractionDigits(3);
		decimalFormat.setMaximumFractionDigits(12);
		decimalFormat.setGroupingUsed(false);
		if(decimalFormat.format(value).contains(".")) {
			String fraction=decimalFormat.format(value).split("\\.")[1];
			int franctionAmount=Integer.parseInt(fraction.substring(0,2));
			DecimalFormat fractionFormat = new DecimalFormat("00");
			value = Double.valueOf(number.longValue()+"."+fractionFormat.format(franctionAmount));
			return ((fraction.endsWith("0"))?value:value+0.01);
		}
		return value;
	}
	
	public Integer getYear(Date date){
		Calendar calendar= Calendar.getInstance();
		calendar.setTime(date);
		int year = Integer.valueOf(calendar.get(Calendar.YEAR));
		//str=str.substring(2);
		return year;
	}
	
	public String getYear2Digit(Date date){
		Calendar calendar= Calendar.getInstance();
		calendar.setTime(date);
		String yearString = Integer.toString(Integer.valueOf(calendar.get(Calendar.YEAR)));
		yearString=yearString.substring(2);
		return yearString;
	}
	
	public Integer getMonth(Date date){
		Calendar calendar= Calendar.getInstance();
		calendar.setTime(date);
		int month = Integer.valueOf(calendar.get(Calendar.MONTH));
		//str=str.substring(3);
		return month;
	}
	
	public Integer getDay(Date date){
		Calendar calendar= Calendar.getInstance();
		calendar.setTime(date);
		int day = Integer.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		//str=str.substring(2);
		return day;
	}
	
	public String getParameterValueByParameterCode(UserSessionConfig userSessionConfig, String code){
		Criteria criteria = getSession().createCriteria(Parameter.class);
		criteria.add(Restrictions.eq("parameterCode", code));
		Parameter parameter = (Parameter)criteria.uniqueResult();
		
		Criteria paraValCriteria1 = getSession().createCriteria(ParameterValue.class);
		paraValCriteria1.add(Restrictions.eq("parameterId", parameter.getParameterId()));
		paraValCriteria1.add(Restrictions.ge("effectiveDate", userSessionConfig.getLoginDate()));
		paraValCriteria1.setProjection(Projections.max("sequence"));
		Number maxSeq = (Number)paraValCriteria1.uniqueResult();
		int maxSeqNo = maxSeq!=null?maxSeq.intValue():0;
		
		Criteria paraValCriteria2 = getSession().createCriteria(ParameterValue.class);
		paraValCriteria2.add(Restrictions.eq("parameterId", parameter.getParameterId()));
		paraValCriteria2.add(Restrictions.ge("effectiveDate", userSessionConfig.getLoginDate()));
		paraValCriteria2.add(Restrictions.ge("sequence", maxSeqNo));
		ParameterValue parameterValue = (ParameterValue)paraValCriteria2.uniqueResult();
		
		if(parameterValue!=null)
			return parameterValue.getValue();
		else
			return parameter.getDefaultValue();
	}
	
	public BranchDayStatus  getBranchDayStatusByBranchId(UserSessionConfig userSessionConfig, int branchId){
		Criteria criteria = getSession().createCriteria(BranchDayStatus.class);
		criteria.add(Restrictions.eq("branchId", branchId));
		BranchDayStatus branchDayStatus = (BranchDayStatus)criteria.uniqueResult();
		return branchDayStatus;
	}
	
}
