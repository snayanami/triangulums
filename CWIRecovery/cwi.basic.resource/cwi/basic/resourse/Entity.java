package cwi.basic.resourse;

import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


@javax.persistence.Entity
@Table(name="ENTITY")
public class Entity {
	private int entityId;
    private String entityName;
    private List<EntityAttribute> attributes;

    @Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ENTID")
    public int getEntityId() {
        return entityId;
    }
    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

	@Column(name="ENTNAME")
    public String getEntityName() {
        return entityName;
    }
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

	@OneToMany(fetch=FetchType.EAGER,targetEntity=EntityAttribute.class,mappedBy="entityId")
	//@OrderBy(clause="ATRSEQ")
    public List<EntityAttribute> getAttributes() {
		//Collections.sort(attributes);
        return attributes;
    }
    public void setAttributes(List<EntityAttribute> attributes) {
        this.attributes = attributes;
    }
}
