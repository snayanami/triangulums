package cwi.basic.resourse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class SearchEntity implements Serializable {
    private int entityId;
    private String entityName;
    private String sortOrder;
    
    Collection attributes = new ArrayList();
    
    public SearchEntity(){
        
    }
    public SearchEntity(int entityId,String sortOrder,Collection attributes){
        this.entityId = entityId;
        this.sortOrder = sortOrder;
        this.attributes = attributes;
    }
    public SearchEntity(int entityId,String sortOrder){
        this.entityId = entityId;
        this.sortOrder = sortOrder;        
    }
    public SearchEntity(int entityId){
        this.entityId = entityId;
    }      
  
    public Collection getAttributes() {
        return attributes;
    }
    public void setAttributes(Collection attributes) {
        this.attributes = attributes;
    }
    public String getEntityName() {
        return entityName;
    }
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
    public String getSortOrder() {
        return sortOrder;
    }
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
    public int getEntityId() {
        return entityId;
    }
    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }   
}
