package cwi.basic.resourse;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class StrutsFormValidateUtil {
	/**
	 * 
	 * @param request
	 * @param actionMessages
	 * @param resources
	 * @param userLocale
	 * @param property
	 * @return JSONArray - Containing JSONObject with element of message and message description from user locale 
	 *  
	 */
    public static JSONArray getMessages(HttpServletRequest request,ActionMessages actionMessages,MessageResources resources,Locale userLocale,String property){
        StringBuilder messageText = new StringBuilder();        
        String localProperty = null;        
        Iterator reports = null;        
        ActionMessage report = null;
        
    	JSONObject message 	= null;
        JSONArray messages	= new JSONArray();        
        
        if (property==null){//Messages for all properties
        	Iterator properties = actionMessages.properties();
        	while(properties.hasNext()){//Iterate for all properties
        		localProperty = (String)properties.next(); 
        		reports = actionMessages.get(localProperty);
        		while (reports.hasNext()) {//Iterate for all messages of property 
                	report = (ActionMessage) reports.next();
                	messageText.append(getMessageText(report, resources, userLocale));//Get text message
        		}
        		message = new JSONObject();
        		try{
        			message.put(localProperty,messageText);
        			messageText = new StringBuilder();
        		}catch(JSONException jex){}
        		messages.put(message);
        	}
        }else{//Messages for given properties
        	reports = actionMessages.get(property);
        	while (reports.hasNext()) {
            	report = (ActionMessage) reports.next();
            	messageText.append(getMessageText(report, resources, userLocale));//Get text message
        	}
    		message = new JSONObject();
    		try{
    			message.put(property,messageText);
    		}catch(JSONException jex){}
    		messages.put(message);
        }
        return messages;
        
    }
    public static JSONObject getErrorMessage(BasicException ex,MessageResources messageResources,Locale userLocale){
    	JSONObject errorObject = new JSONObject();
    	try{
    		errorObject.put("error",messageResources.getMessage(userLocale,ex.getExceptionCode())==null?ex.getExceptionCode():messageResources.getMessage(userLocale,ex.getExceptionCode()));
    	}catch(JSONException jex){}
    	
    	return errorObject;
    }    
    
    public static String getAJAXErrorMessage(BasicException ex,MessageResources messageResources,Locale userLocale){
    	JSONObject message = new JSONObject();
    	try{
    		message.put("error",messageResources.getMessage(userLocale,ex.getExceptionCode())==null?ex.getExceptionCode():messageResources.getMessage(userLocale,ex.getExceptionCode()));
        	message.put("id", "");
        	message.put("description", "");    		
    	}catch(JSONException jex){
    		
    	}    	
    	return message.toString();
    }
    public static String getAJAXReferenceData(int id,String description){
    	JSONObject message	= new JSONObject();
        try{
        	message.put("error","");
        	message.put("id", id);
        	message.put("description", description);        	
        }catch (JSONException e) {
        	
		}
        return message.toString(); 
    }
    
 
    public static JSONObject getMessageCreateSuccess(MessageResources messageResources){
    	JSONObject messageObject = new JSONObject();
    	try{
    		messageObject.put("createSuccess",messageResources.getMessage("msg.createsuccess"));
    	}catch(JSONException jex){}
    	
    	return messageObject;    	
    }
    
    public static JSONObject getMessageUpdateSuccess(MessageResources messageResources){
    	JSONObject messageObject = new JSONObject();
    	try{
    		messageObject.put("updateSuccess",messageResources.getMessage("msg.updatesuccess"));
    	}catch(JSONException jex){}
    	
    	return messageObject;    	
    }
    
    public static JSONObject getMessageDeleteSuccess(MessageResources messageResources){
    	JSONObject messageObject = new JSONObject();
    	try{
    		messageObject.put("deleteSuccess",messageResources.getMessage("msg.deletesuccess"));
    	}catch(JSONException jex){}
    	
    	return messageObject;    	
    }
    
    public static JSONObject getMessageAuthorizeSuccess(MessageResources messageResources){
    	JSONObject messageObject = new JSONObject();
    	try{
    		messageObject.put("authorizeSuccess",messageResources.getMessage("msg.authorized"));
    	}catch(JSONException jex){}
    	
    	return messageObject;    	
    }
    
    public static JSONObject getMessageApproveSuccess(MessageResources messageResources){
    	JSONObject messageObject = new JSONObject();
    	try{
    		messageObject.put("approveSuccess",messageResources.getMessage("msg.approvesuccess"));
    	}catch(JSONException jex){}
    	
    	return messageObject;    	
    } 
    
    public static JSONObject getMessageRejectSuccess(MessageResources messageResources){
    	JSONObject messageObject = new JSONObject();
    	try{
    		messageObject.put("rejectSuccess",messageResources.getMessage("msg.rejectsuccess"));
    	}catch(JSONException jex){}
    	
    	return messageObject;    	
    } 
    
    public static JSONObject getMessageSendSuccess(MessageResources messageResources){
    	JSONObject messageObject = new JSONObject();
    	try{
    		messageObject.put("sendSuccess",messageResources.getMessage("msg.sendsuccess"));
    	}catch(JSONException jex){}
    	
    	return messageObject;    	
    } 
    
    public static JSONObject getMessageClosedSuccess(MessageResources messageResources){
    	JSONObject messageObject = new JSONObject();
    	try{
    		messageObject.put("closedSuccess",messageResources.getMessage("msg.closedtsuccess"));
    	}catch(JSONException jex){}
    	
    	return messageObject;    	
    }
    
    public static JSONObject getMessageNotFound(MessageResources messageResources){
    	JSONObject messageObject = new JSONObject();
    	try{
    		messageObject.put("notFound",messageResources.getMessage("errors.notfound"));
    	}catch(JSONException jex){}
    	
    	return messageObject;    	
    }     
    
    private static String getMessageText(ActionMessage message,MessageResources resources,Locale userLocale){
    	String messageText=null;
    	if (message.isResource()) {
    		if (message.getValues()==null){
    			messageText = resources.getMessage(userLocale, message.getKey()); //Message            		
            }else{
            	messageText = resources.getMessage(userLocale, message.getKey(),message.getValues()); //Message with values            		
            }                
    	}else{
    		messageText = message.getKey(); //Message is not found in the resource file
        }            	
        return messageText;    	
    }  
    
    public static Date parseDate(String date)throws BasicException {
		try {
			SimpleDateFormat simpdate=new SimpleDateFormat(GeneralConstant.DATE_FORMAT);
			return simpdate.parse(date);
		} catch (Exception e) {
			throw new BasicException("","errors.date");
		}
	}
	
	public static String parseString(Date date)throws BasicException  {
		try {
			SimpleDateFormat simpdate=new SimpleDateFormat(GeneralConstant.DATE_FORMAT);
			return simpdate.format(date);
		} catch (Exception e) {
			throw new BasicException("","errors.date");
		}
	}
	
	public static String parseStringDate(Date date)throws BasicException  {
		try {
			SimpleDateFormat simpdate=new SimpleDateFormat(GeneralConstant.DATE_FORMAT_STR);
			return simpdate.format(date);
		} catch (Exception e) {
			throw new BasicException("","errors.date");
		}
	}
	
	public static boolean compareDate(Date minDate,Date maxDate)throws Exception {
		return minDate.compareTo(maxDate)<=0;
	}
	
	public static boolean compareWithSystemDate(HttpServletRequest request,Date date)throws Exception {
		return compareDate(new Date(), date);
	}
	
	public static JSONObject getErrorMessage(String field,BasicException ex,MessageResources messageResources) {
		JSONObject object=new JSONObject();
		try {
			return object.put(field, messageResources.getMessage(ex.getExceptionCode(),field));
		} catch (Exception e) {}
		return object;
	}
	
}
