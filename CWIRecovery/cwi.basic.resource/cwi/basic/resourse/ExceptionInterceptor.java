package cwi.basic.resourse;

import org.springframework.aop.ThrowsAdvice;

public class ExceptionInterceptor implements ThrowsAdvice {
	public void afterThrowing(Exception ex) throws Throwable {
		Throwable t = ex.getCause();
		if (ex instanceof BasicDataAccessException){ //Manually thrown exception from DAO/BL
			BasicDataAccessException cde = (BasicDataAccessException)ex;
            if(cde.getErrorCode()==null){                
                cde.setErrorCode("Undefined.");
                throw new BasicException("",cde.getErrorCode());
            }else{
                throw new BasicException("",cde.getErrorCode());
            }     
        }   
	}
}
