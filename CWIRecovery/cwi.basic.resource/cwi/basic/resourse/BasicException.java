package cwi.basic.resourse;

public class BasicException extends CommonException {
    public BasicException(String productCode,String exceptionCode){
        super(productCode,exceptionCode);
    }
    public BasicException(String productCode,String exceptionCode,Object[] placeholderValues){
        super(productCode,exceptionCode,placeholderValues);
    }    
    public BasicException(String exceptionCode){
        super(exceptionCode);
    }
}
