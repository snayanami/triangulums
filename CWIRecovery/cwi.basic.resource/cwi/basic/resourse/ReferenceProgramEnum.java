package cwi.basic.resourse;


public class ReferenceProgramEnum {
	public static final int STAKEHOLDER_TYPE = 1;
	public static final int ACCESS_GROUP = 2;
	public static final int TITLE = 3;
	public static final int DA_TYPE = 4;
	public static final int GOLD_TYPE = 5;
	public static final int ARTICLE_TYPE = 6;
	public static final int MOVEMENT_DEFINITION = 7;
}
