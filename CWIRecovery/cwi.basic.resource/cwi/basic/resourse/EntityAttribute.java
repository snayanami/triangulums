package cwi.basic.resourse;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


@Entity
@Table(name="ENTITY_ATTR")
public class EntityAttribute {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ENTATTRID")
    private int entityAttributeId;

	@Column(name="ENTID")
    private int entityId;

	@Column(name="ATTRNAME")
    private String attributeName;

	@Column(name="DESCR")
    private String attributeDescription;

	@Column(name="RESKEY")
    private String resourcesKey;

	@Column(name="ISDISPLAY")
    private int isDisplay;

	@Column(name="ATRSEQ")
    private int displaySequence;

	@Column(name="ENTTYPE")
    private String type;

	@Column(name="ENTITYTYPE")
	private int entityType;

	@Column(name="ISASSOCIATION")
	private int isAssociation;

    /**
     *
     * @hibernate.id
     *   generator-class = "increment"
     *   column = "ENTATTRID"
     */
    public int getEntityAttributeId() {
        return entityAttributeId;
    }
    public void setEntityAttributeId(int entityAttributeId) {
        this.entityAttributeId = entityAttributeId;
    }

    /**
     * @hibernate.property
     *  column = "ENTID"
     */
    public int getEntityId() {
        return entityId;
    }
    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }
    /**
     *
     * @hibernate.property
     *  column = "ATTRNAME"
     */
    public String getAttributeName() {
        return attributeName;
    }
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    /**
     *
     * @hibernate.property
     *  column = "DESCR"
     */
    public String getAttributeDescription() {
        return attributeDescription;
    }
    public void setAttributeDescription(String attributeDescription) {
        this.attributeDescription = attributeDescription;
    }
    /**
     *
     * @hibernate.property
     *  column = "ISDISPLAY"
     */
    public int getIsDisplay() {
        return isDisplay;
    }
    public void setIsDisplay(int isDisplay) {
        this.isDisplay = isDisplay;
    }
    /**
     *
     * @hibernate.property
     *  column = "RESKEY"
     */
    public String getResourcesKey() {
        return resourcesKey;
    }
    public void setResourcesKey(String resourcesKey) {
        this.resourcesKey = resourcesKey;
    }
    /**
     *
     * @hibernate.property
     *  column = "ATRSEQ"
     */
    public int getDisplaySequence() {
        return displaySequence;
    }
    public void setDisplaySequence(int displaySequence) {
        this.displaySequence = displaySequence;
    }
    /**
     *
     * @hibernate.property
     *  column = "ENTTYPE"
     */
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @hibernate.property
     *  column = "ENTITYTYPE"
     */
    public int getEntityType() {
		return entityType;
	}
	public void setEntityType(int entityType) {
		this.entityType = entityType;
	}

	public int getIsAssociation() {
		return isAssociation;
	}
	public void setIsAssociation(int isAssociation) {
		this.isAssociation = isAssociation;
	}
	
	public int compareTo(Object object) {
		EntityAttribute entityAttribute = (EntityAttribute) object;
	    return Integer.valueOf(displaySequence).compareTo(entityAttribute.getDisplaySequence());
	}
}
