package cwi.basic.resourse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;

import cwi.admin.spring.SearchBD;



public class CommonBrowserAction extends DispatchAction {
	
	private SearchBD searchBD;	
	
    public SearchBD getSearchBD() {
		return searchBD;
	}
    public void setSearchBD(SearchBD searchBD) {
		this.searchBD = searchBD;
	}
    public void createBrowser(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	if (!SessionUtil.isValidSession(request)){
    		response.getWriter().println("<center><font color=\"red\">Your Session is Expired , Please Login Again !</font></center>");
        }else{
	    	String tblId=request.getParameter("tableId");
	    	MessageResources screenLables=getResources(request,"lable");
	    	int tableId=0;
	    	if(tblId!=null || tblId!="")
	    		tableId=Integer.parseInt(tblId);
	    	
	    	if (tableId!=0) {
	    		Entity entity = getSearchBD().getEntityById(tableId);
	    		Collection attributes = entity.getAttributes();
	    		for(Object o:attributes) {
	    			EntityAttribute entityAttribute=(EntityAttribute)o;
	    			if(entityAttribute.getIsDisplay()==0)
	    				attributes.remove(entityAttribute);
	    		}
	    		String fieldNames[]=new String[attributes.size()];
	    		String fieldDataType[]=new String[attributes.size()];
	    		String attributeNames[]=new String[attributes.size()];
	    		int position=0;
	    		for(Object o:attributes) {
	    			EntityAttribute entityAttribute=(EntityAttribute)o;
	    			//fieldNames[position]=screenLables.getMessage("screen."+entityAttribute.getResourcesKey());
	    			fieldNames[position]=entityAttribute.getAttributeDescription();
	    			fieldDataType[position]=entityAttribute.getType();
	    			attributeNames[position]=entityAttribute.getAttributeName();
	    			position++;
	    		}
	    		String tableName=entity.getEntityName();
	    		writeJSP(form, request, response,tableName, fieldNames, fieldDataType, attributeNames);
	    	}else {
	    		response.getWriter().println("<center><font color=\"red\">Error : Entity Id Not Found!</font></center>");
	    	}
        }
    }
    
    public ActionForward getSearchData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	JSONArray jsonArray = null; 
    	int tableId=0;
    	boolean checkCompany=false;
    	String[] conditions=new String[0];
    	String[] oparators=new String[0];
    	String[] values=new String[0];
    	if(request.getParameter("tableId")!=null && request.getParameter("tableId")!="") 
    		tableId=Integer.parseInt(request.getParameter("tableId"));
    	if(request.getParameter("conditions")!=null && request.getParameter("conditions")!="")
    		conditions=request.getParameter("conditions").split("<next>");
    	if(request.getParameter("oparators")!=null && request.getParameter("conditions")!="")
    		oparators=request.getParameter("oparators").split("<next>");
    	if(request.getParameter("values")!=null && request.getParameter("conditions")!="")
    		values=request.getParameter("values").split("<next>");
    	if(request.getParameter("checkComapny")!=null && request.getParameter("checkComapny").equals("1"))
    		checkCompany=true;
    	
    	SearchEntity searchEntity = new SearchEntity(Integer.valueOf(tableId));
    	
    	if(conditions.length==oparators.length && oparators.length==values.length){
    		for(int count=0;count<conditions.length;count++) {
    			if(!(conditions[count].equals("") && oparators[count].equals(""))) {
    				searchEntity.getAttributes().add(new SearchAttribute(conditions[count],oparators[count],values[count]));
    			}
    		}
    	}
    	if(checkCompany) {
    		try {
    		UserSessionConfig session = (UserSessionConfig) request.getSession().getAttribute("LOGIN_KEY");
    		searchEntity.getAttributes().add(new SearchAttribute("companyId","=",String.valueOf(session.getCompanyId())));
    		}catch(Exception e) {
    			response.getWriter().write("{}");
    			return null;
    		}
    	}
    	
    	SearchResult result = getSearchBD().getSearchData(searchEntity);
    	jsonArray=new JSONArray();
    	JSONArray innerArray=new JSONArray();
    	for(int out=0;out<result.getDataMap().size();out++) {
    		Object[] recordData=result.getDataMap().get(out).values().toArray();
    		innerArray=new JSONArray();
    		for(int in=1;in<recordData.length;in++) {
    			Object data=recordData[in];
    			innerArray.put(data);
    		}
    		if(recordData.length>0)
    			innerArray.put(recordData[0]);
    		jsonArray.put(innerArray);
    	}
    	response.getWriter().write(jsonArray.toString());
    	return null;
    }
    
    private void writeJSP(ActionForm form,HttpServletRequest request,HttpServletResponse response,String tableName,String[] fieldNames,String[] FieldDataTypes,String[] attributeNames) {
    	try {
			PrintWriter out = response.getWriter();
			out.println("<html>");
			
			out.println("<head>");
			out.println("<title>"+tableName+"</title>");
			out.println("<link href=\"css/common/common.css\" rel=\"stylesheet\" type=\"text/css\"></link>");
			out.println("<link href=\"css/common/system/aw.css\" rel=\"stylesheet\" type=\"text/css\"></link>");
			out.println("<script src=\"js/aw.js\"></script>");
			out.println("<script src=\"js/ajax.js\"></script>");
			out.println("<script src=\"js/Grid.js\"></script>");
			out.println("<script src=\"js/SkyBrowser.js\"></script>");
			
			out.println("<style>");
			out.println("BODY {");
			//out.println("MARGIN: 0px; FONT-SIZE: 12px; FONT-FAMILY: Trebuchet MS ;BACKGROUND-COLOR: #ece9d8");
			out.println("MARGIN: 0px; FONT-SIZE: 12px; FONT-FAMILY: Trebuchet MS ;BACKGROUND-COLOR: #eeeeee");
			out.println("}");
			out.println(".GridColumn{FONT-SIZE: 10px; FONT-FAMILY: Trebuchet MS;WIDTH:150px;}");
			out.println(".GridColumnItem{FONT-SIZE: 10px; FONT-FAMILY: Trebuchet MS;WIDTH:100px;}");
			out.println("INPUT{FONT-SIZE: 10px; FONT-FAMILY: Trebuchet MS;}");
			out.println("#myGrid {height: 200px; width: 570px;}");
			out.println("#myGrid .aw-row-selector {text-align: center}");
			out.println("#myGrid .aw-column-0 {width: 100px;}");
			out.println("#myGrid .aw-column-1 {width: 200px;}");
			out.println("#myGrid .aw-column-2 {text-align: left;}");
			out.println("#myGrid .aw-grid-cell {border-right: 1px solid threedlightshadow;}");
			out.println("#myGrid .aw-grid-row {border-bottom: 1px solid threedlightshadow;}");
			out.println("table.CommonBrowserTable {");
			out.println("    border-top: #91a7b4 1px solid;");
			out.println("    BORDER-RIGHT: #91a7b4 1px solid;");
			out.println("    BORDER-LEFT: #91a7b4 1px solid;");
			out.println("	 BORDER-BOTTOM: #91a7b4 1px solid;");
			//out.println("    background-color:#f9f9ef;");
			out.println("    background-color:#eeeeee;");
			out.println("}");
			out.println("</style>");
			
			out.println("<script language=JavaScript>");
			
			String fields="[";
			int count=0;
			for(String s:fieldNames) {
				fields+="'"+s+"'";
				if(count++<fieldNames.length-1)
					fields+=",";
			}
			fields+="]";
			out.println("var fieldNames="+fields+";");
			
			out.println("function getFieldNames(){");
			out.println("	return fieldNames;");
			out.println("}");
			
			String fieldsDataTypes="[";
			count=0;
			for(String s:FieldDataTypes) {
				fieldsDataTypes+="'"+s+"'";
				if(count++<FieldDataTypes.length-1)
					fieldsDataTypes+=",";
			}
			fieldsDataTypes+="]";
			out.println("var fieldDataTypes="+fieldsDataTypes+";");
			
			out.println("function getDataTypes(){");
			out.println("	return fieldDataTypes;");
			out.println("}");
			
			String fieldAttributeNames="[";
			count=0;
			for(String s:attributeNames) {
				fieldAttributeNames+="'"+s+"'";
				if(count++<attributeNames.length-1)
					fieldAttributeNames+=",";
			}
			fieldAttributeNames+="]";
			out.println("var fieldAttributeNames="+fieldAttributeNames+";");
			
			out.println("function getAttributeNames(){");
			out.println("	return fieldAttributeNames;");
			out.println("}");
			
			out.println("var tableId='"+request.getParameter("tableId")+"';");
			out.println("var IdElementId='"+request.getParameter("IdElementId")+"';");
			out.println("var CodeElementId='"+request.getParameter("CodeElementId")+"';");
			out.println("var CodeFieldNo='"+request.getParameter("CodeFieldNo")+"';");
			out.println("var DescElementId='"+request.getParameter("DescElementId")+"';");
			out.println("var DescFieldNo='"+request.getParameter("DescFieldNo")+"';");
			out.println("var additionalParams='"+request.getParameter("additionalParams")+"';");
			out.println("var additionalOparators='"+request.getParameter("additionalOparators")+"';");
			out.println("var additionalParamValues='"+request.getParameter("additionalParamValues")+"';");
			out.println("var checkCompany='"+request.getParameter("checkCompany")+"';");
			out.println("var checkRecordStatus='"+request.getParameter("checkRecordStatus")+"';");
			
			out.println("var IdElementValue='';");
			out.println("var CodeElementValue='';");
			out.println("var DescElementValue='';");
			
			out.println("function executeFunction(){");
			if(request.getParameter("Function")!=null && (!request.getParameter("Function").equals("")) && (!request.getParameter("Function").equals("undefined"))) {
				out.println("		setTimeout(\"window.opener."+request.getParameter("Function")+"\",10);");
			}
			out.println("}");
			
			out.println("function setValues(){");
			out.println("	try{");
			out.println("		window.opener.document.getElementById(IdElementId).value=IdElementValue;");
			out.println("	}catch(er1){	alert('Cannot Find HTML Element '+IdElementId); }");
			out.println("	try{");
			out.println("		window.opener.document.getElementById(CodeElementId).value=CodeElementValue;");
			out.println("	}catch(er1){	alert('Cannot Find HTML Element '+CodeElementId); }");
			out.println("	try{");
			out.println("		window.opener.document.getElementById(DescElementId).value=DescElementValue;");
			out.println("	}catch(er1){	alert('Cannot Find HTML Element '+DescElementId); }");
			out.println("}");
			
			out.println("function getSearchData(){");
			out.println("	if(validateOnSubmit()){");
			out.println("		obj.loading();");
			out.println("		try{");
			out.println("			var conditions=document.getElementById('searchField').value;");
			out.println("			var oparators=document.getElementById('oparator').value;");
			out.println("			var values=document.getElementById('searchValue').value;");
			
			out.println("			if(conditions=='select' && oparators=='select'){");
			out.println("				conditions=oparators=values='';");
			out.println("			}");

			out.println("			if(additionalParams!='undefined' && additionalParams!='null' && additionalParams!=''){");
			out.println("				var con=additionalParams.split('<next>');");
			out.println("				if(con.length>0 && conditions!='')");
			out.println("					conditions+=\"<next>\"");
			out.println("				for (var i=0;i<con.length;i++){");
			out.println("					conditions+=con[i];");
			out.println("					if(i<con.length-1 && conditions!='')");
			out.println("						conditions+=\"<next>\"");
			out.println("				}");
			out.println("			}");
			out.println("			if(additionalParamValues!='undefined' && additionalParamValues!='null' && additionalParamValues!=''){");
			out.println("				var con=additionalParamValues.split('<next>');");
			out.println("				if(con.length>0 && values!='')");
			out.println("					values+=\"<next>\"");
			out.println("				if(con.length>0 && oparators!='')");
			out.println("					oparators+=\"<next>\"");
			out.println("				for (var i=0;i<con.length;i++){");
			out.println("					values+=con[i];");
			out.println("					if(additionalOparators=='undefined' || additionalOparators=='null' || additionalOparators=='')");
			out.println("						oparators+=\"=\"");
			out.println("					if(i<con.length-1 && values!=''){");
			out.println("						values+=\"<next>\"");
			out.println("						if(additionalOparators=='undefined' || additionalOparators=='null' || additionalOparators=='')");
			out.println("							oparators+=\"<next>\"");
			out.println("					}");
			out.println("				}");
			out.println("			}");
			
			out.println("			if(additionalOparators!='undefined' && additionalOparators!='null' && additionalOparators!=''){");
			out.println("				var con=additionalOparators.split('<next>');");
			out.println("				if(con.length>0 && values!='')");
			out.println("					if(oparators!='')oparators+=\"<next>\"");
			out.println("				for (var i=0;i<con.length;i++){");
			out.println("					oparators+=con[i];");
			out.println("					if(i<con.length-1 && oparators!='')");
			out.println("						oparators+=\"<next>\"");
			out.println("				}");
			out.println("			}");
			
			out.println("			if(checkRecordStatus!='undefined' && checkRecordStatus!='null' && checkRecordStatus!=''){");
			out.println("				if(checkRecordStatus=='1'){");
			out.println("					conditions+=\"<next>recordStatus\";");
			out.println("					oparators+=\"<next>=\";");
			out.println("					values+=\"<next>1\"");
			out.println("				}");
			out.println("			}");
			out.println("			var uri=\"CommonBrowserAction.do\";");
			out.println("			var data=\"dispatch=getSearchData&tableId=\"+tableId+\"&conditions=\"+conditions+\"&oparators=\"+oparators+\"&values=\"+values+\"&checkComapny=\"+checkCompany;");
			out.println("			var ajax=new ajaxObject(uri);");
			out.println("			ajax.callback = function(responseText, responseStatus, responseXML) {");
			out.println("				if (responseStatus==200) {");
			out.println("					var responseData=eval('('+responseText+')');");
			out.println("					obj.setRowCount(responseData.length);");
			out.println("					obj.setCellText(responseData);");
			out.println("					obj.refresh();");
			out.println("					obj.loadingCompleted();");
			out.println("				}");
			out.println("			}");
			out.println("			ajax.update(data,'POST');");
			out.println("		}catch(er){	");
			out.println("			obj.loadingCompleted();");
			out.println("		}");
			out.println("	}");
			out.println("}");
			
			out.println("function setData(row){");
			out.println("	document.getElementById('Check').disabled=false;");
			out.println("	IdElementValue=obj.getCellText(getFieldNames().length,row);");
			out.println("   CodeElementValue=obj.getCellText(CodeFieldNo,row);");
			out.println("   DescElementValue=obj.getCellText(DescFieldNo,row);");
			out.println("}");
			
			out.println("</script>");
			
			out.println("</head>");
			
			out.println("<body onload=\"getSearchData();\">");
			out.println("<center>");
			out.println("<table cellpadding=\"0\" width=\"590px\" cellspacing=\"0\"  >");
			out.println("<tbody>");
			out.println("<tr>");
			out.println("<td align=\"left\" >");
			
			out.println("<fieldset style=\"width:1px\">");
			out.println("<legend>Search by Column</legend>");
			out.println("<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" >");
			out.println("	<tbody>");
			out.println("		<tr>");
			out.println("			<td>");
			out.println("				<table class=\"CommonBrowserTable\" width=\"570\" height=\"35\">");
			out.println("		        	<tbody>");
			out.println("			        	<tr>");
			out.println("				           	<td>Search By</td>");
			out.println("				            <td>");				
			out.println("			    	            <select name=\"searchField\" class=\"GridColumn\" id=\"searchField\" >");
			out.println("			        	        	<option value=\"select\" selected=\"selected\">-Select-</option> ");
			for(int pos=0;pos<fieldNames.length;pos++) {
				out.println("		                    		<option value=\""+attributeNames[pos]+"\" >"+fieldNames[pos]+"</option>");
			}
			out.println("				                </select>");
			out.println("			            	</td>");
			out.println("			            	<td>");
			out.println("			                	<select name=\"oparator\" class=\"searchTypeCom\" id=\"oparator\" >");
			out.println("				                 	<option value=\"select\" selected=\"selected\">-Select-</option>");
			out.println("									<option value=\"=\" >=</option>");
			out.println("									<option value=\"<>\" >&#60;&#62;</option>");
			out.println("									<option value=\"<=\" >&#60;=</option>");
			out.println("									<option value=\">=\" >&#62;=</option>");
			out.println("									<option value=\"like\" >Like</option>");
			out.println("				                </select>");
			out.println("			            	</td>");
			out.println("			            	<td align=\"left\" >");
			out.println("			                	<input type=\"text\" name=\"searchValue\" size=\"30\" class=\"GridColumn\" onfocus=\"document.getElementById('divSearchValue').innerHTML=''\" onkeyup=\"validate()\" onblur=\"validate()\" id=\"searchValue\" /><br/><div id=\"divSearchValue\" class=\"validate\"></div>");
			out.println("			            	</td>");
			out.println("			            	<td align=\"left\" >");
			out.println("				            	<input type=\"button\" class=\"buttonNormal\" id=\"Search\" value=\"Search\" onclick=\"getSearchData()\" >	");			
			out.println("							</td>");
			out.println("			        	</tr>");
			out.println("					</tbody>");
			out.println("				</table>");
			out.println("			</td>");
			out.println("		</tr>");
			out.println("		<tr>");
			out.println("			<td height=\"10px\">");
			out.println("			</td>");
			out.println("		</tr>");
			out.println("		<tr>");
			out.println("			<td>");
			
			out.println("				<script language=JavaScript>");
			out.println("				var obj=createBrowser(fieldNames);");
			out.println("				document.write(obj);");
			out.println("				</script>");
			
			out.println("			</td>");
			out.println("		</tr>");
			out.println("		<tr>");
			out.println("			<td>");
			out.println("				<table  class=\"CommonBrowserTable\"  width=\"570\" height=\"35\">");
			out.println("			    	<tbody>");
			out.println("					    <tr>");
			out.println("			        		<td align=\"Right\" >");
			out.println("								<input type=\"button\" class=\"buttonNormal\" id=\"Check\" value=\"Ok\" disabled=\"true\" onclick=\"setDataAndClose();\" />");
			out.println("		        	    	</td>");
			out.println("		            		<td align=\"left\" >");
			out.println("								<input type=\"button\" class=\"buttonNormal\" id=\"Cancel\" value=\"Cancel\" onclick=\"isClose=true;closeWindow();\" />");			
			out.println("			            	</td>");
			out.println("			        	</tr>");
			out.println("					</tbody>");
			out.println("				</table>");
			out.println("			</td>");
			out.println("		</tr>");
			out.println("	</tbody>");
			out.println("</table>");
			out.println("</fieldset>");
			
			out.println("</td>");
			out.println("</tr>");
			out.println("</tbody>");
			out.println("</table>");
			out.println("</center>");
			out.println("</body>");
			
			out.println("</html>");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}