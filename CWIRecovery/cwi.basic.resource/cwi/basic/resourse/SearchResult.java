package cwi.basic.resourse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class SearchResult implements Serializable {
    private ArrayList<LinkedHashMap> dataMap = new ArrayList<LinkedHashMap>();
    private String [] headings;
    private int count;
    
    public SearchResult(){
        
    }
    public SearchResult(ArrayList<LinkedHashMap> dataMap,String [] headings,int count){
        this.dataMap = dataMap;
        this.headings = headings;
        this.count = count;
    }
    
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
    public ArrayList<LinkedHashMap> getDataMap() {
        return dataMap;
    }
    public void setDataMap(ArrayList<LinkedHashMap> dataMap) {
        this.dataMap = dataMap;
    }
    public String[] getHeadings() {
        return headings;
    }
    public void setHeadings(String[] headings) {
        this.headings = headings;
    }
}
