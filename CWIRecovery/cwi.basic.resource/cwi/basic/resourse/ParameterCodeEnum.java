package cwi.basic.resourse;

public enum ParameterCodeEnum {
	P01("P01","Financial Year Begining Month"),
	P02("P02","Interest Rate (Per Year)"),
	P03("P03","Renew Period"),
	P04("P04","Maximum Amount per Ticket"),
	P05("P05","Minimum Capital Amount Required"),
	P06("P06","Reminder Letter 1"),
	P07("P07","Reminder Letter 2"),
	P08("P08","Notice of Termination"),
	P09("P09","Termination Letter"),
	P10("P10","Reminder Printing Charge");
	
	
	private String code;
	String description;
	
	private ParameterCodeEnum(String code,String description){
		this.code = code;
		this.description = description;
	}
	
	public String getCode() {
		return this.code;
	}
	
	public String getDescription() {
		return this.description;
	}
}

/*
 * P01 - Financial Year Begining Month
 * 0 - January
 * 1 - February
 * 2 - March
 * 3 - April
 * 4 - May
 * 5 - June
 * 6 - July
 * 7 - August
 * 8 - September
 * 9 - October
 * 10 - November
 * 11 - December
 */
