package cwi.basic.resourse;

import javax.servlet.http.HttpServletRequest;


public class SessionUtil {
	
	public static boolean isValidSession(HttpServletRequest request){
		if (request.getSession(false) == null ||(request.getSession().getAttribute( "LOGIN_KEY" ) == null)){
			return false;
		} else{
			return true;
		}      
	}
	
    public static  UserSessionConfig getUserSession(HttpServletRequest request){
    	UserSessionConfig session = null;        
        session =( UserSessionConfig) request.getSession().getAttribute("LOGIN_KEY");
        System.out.println("<HTTP Session ID> " + request.getSession().getId());
        return session;
    }
    
    public static  UserSessionConfig getUserSessionInfo(HttpServletRequest request){
    	UserSessionConfig session = null;        
        session =( UserSessionConfig) request.getSession().getAttribute("LOGIN_KEY");
        System.out.println("<HTTP Session ID> " + request.getSession().getId());
        return session;
    }
}
