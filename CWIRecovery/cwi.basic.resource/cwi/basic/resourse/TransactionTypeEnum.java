package cwi.basic.resourse;

public enum TransactionTypeEnum {
	
	PAYMENT("PAY","Payment"),
	RECEIPT("RCP","Receipt"),
	REMINDER("REM","Reminder Printing"),
	ARTICLE_RELEASE("ARL","Article Release"),
	AUCTION("AUC","Auction");
	
	private String code;
	String description;
	
	private TransactionTypeEnum(String code,String description){
		this.code = code;
		this.description = description;
	}
	
	public String getCode() {
		return this.code;
	}
	
	public String getDescription() {
		return this.description;
	}

}
