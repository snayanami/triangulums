package cwi.basic.resourse;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import cwi.admin.domain.SuperCommon;


@Entity
@Table(name="PARA_VAL")
public class ParameterValue extends SuperCommon {
	private int parameterValueId;
	private int parameterId;
	private String value;
	private int sequence;
	private Date effectiveDate;
	
	public ParameterValue(){}
	public ParameterValue(int parameterValueId){
		this.parameterValueId = parameterValueId;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="PARA_VAL_ID")
	public int getParameterValueId() {
		return parameterValueId;
	}
	public void setParameterValueId(int parameterValueId) {
		this.parameterValueId = parameterValueId;
	}
	
	@Column(name="PARA_ID")
	public int getParameterId() {
		return parameterId;
	}
	public void setParameterId(int parameterId) {
		this.parameterId = parameterId;
	}
	
	@Column(name="VALUE")
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	@Column(name="SEQ")
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	
	@Column(name="EFF_DATE")
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
		
}
