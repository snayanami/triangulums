package cwi.basic.resourse;

public enum StakeholderTypeEnum {
	CLIENT("CLI","Client"),
	OFFICER("OFF","Officer");
	
	
	private String code;
	String description;
	
	private StakeholderTypeEnum(String code,String description){
		this.code = code;
		this.description = description;
	}
	
	public String getCode() {
		return this.code;
	}
	
	public String getDescription() {
		return this.description;
	}
}
