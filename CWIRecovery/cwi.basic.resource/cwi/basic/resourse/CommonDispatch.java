package cwi.basic.resourse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.actions.DispatchAction;

public class CommonDispatch extends DispatchAction{

	public ActionForward defaultPageLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		if (!SessionUtil.isValidSession(request)) {
			return mapping.findForward("sessionError");
		}
		DynaActionForm frm = (DynaActionForm) form;            	
	    frm.initialize(mapping);
        frm.set("action","add");
        String page = request.getParameter("page");
		return mapping.findForward(page);
	}
	
	public ActionForward loadPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		if (!SessionUtil.isValidSession(request)) {
			return mapping.findForward("sessionError");
		}
		String action = request.getParameter("action");
		DynaActionForm frm = (DynaActionForm) form;
		frm.initialize(mapping);
        frm.set("action",action);
        String page = request.getParameter("page");
		return mapping.findForward("tab");
	}
	
	public ActionForward loadTabPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		if (!SessionUtil.isValidSession(request)) {
			return mapping.findForward("sessionError");
		}
		String action = request.getParameter("action");
		DynaActionForm frm = (DynaActionForm) form;
		frm.initialize(mapping);
        frm.set("action",action);
        String page = request.getParameter("page");
		return mapping.findForward(page);
	}
	
	public ActionForward loadBrowserWindow(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("method", ""+request.getParameter("method")+"");
		request.setAttribute("headers", request.getParameter("headers"));
		request.setAttribute("bean", ""+request.getParameter("bean")+"");
		request.setAttribute("elements", ""+request.getParameter("elements")+"");
		request.setAttribute("param1", request.getParameter("param1"));
		return mapping.findForward("browser");
	}
}