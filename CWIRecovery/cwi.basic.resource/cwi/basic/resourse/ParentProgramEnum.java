package cwi.basic.resourse;

public enum ParentProgramEnum {
	REF(1,"Reference"),
	PAWN(2,"Recovery"),
	GL(3,"General Ledger"),
	INFO(4,"Information"),
	ADMIN(5,"Administration"),
	CASHIER(6,"Cashier"),
	DASHBOARD(7,"Dashboard"),
	MAINTENANCE(8,"Maintenance");
	
	private int id;
	private String name;
	
	private ParentProgramEnum(int id,String name){
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
}
