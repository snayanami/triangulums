package cwi.basic.resourse;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PARAMETER")
public class Parameter {
	private int parameterId;
	private String parameterCode;
	private String description;
	private String defaultValue;
	private int inputComponentNo;
	
	public Parameter(){}
	public Parameter(int parameterId){
		this.parameterId = parameterId;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="PARA_ID")
	public int getParameterId() {
		return parameterId;
	}
	public void setParameterId(int parameterId) {
		this.parameterId = parameterId;
	}
	
	@Column(name="PARA_CODE")
	public String getParameterCode() {
		return parameterCode;
	}
	public void setParameterCode(String parameterCode) {
		this.parameterCode = parameterCode;
	}
	
	@Column(name="DEF_VALUE")
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	@Column(name="DESCR")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="INPUT_COM_NO")
	public int getInputComponentNo() {
		return inputComponentNo;
	}
	public void setInputComponentNo(int inputComponentNo) {
		this.inputComponentNo = inputComponentNo;
	}
	
}
