package cwi.basic.resourse;

import java.io.Serializable;

public class SearchAttribute implements Serializable {
    
    private String attributeName;
    private String oparator;
    private String type;
    private String value;
    
    public SearchAttribute(){
        
    }
    
    public SearchAttribute(String attributeName,String oparator,String value){
        this.attributeName = attributeName;
        this.oparator = oparator;
        this.value = value;
    }
    
    public String getAttributeName() {
        return attributeName;
    }
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
    public String getOparator() {
        return oparator;
    }
    public void setOparator(String oparator) {
        this.oparator = oparator;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}
