package cwi.basic.resourse;

import java.io.File;
import java.util.Date;

import com.oreilly.servlet.multipart.FileRenamePolicy;

public class ImageUploadRenameUtil implements FileRenamePolicy {
	String seesionId="";
		
	public ImageUploadRenameUtil() {}
	
	public ImageUploadRenameUtil(String sessionId){
		this.seesionId=sessionId;
	}
	
	public File rename(File file) {
		String parentDir = file.getParent();
		String fname = file.getName();
		String fpath=file.getAbsolutePath();
		int index=0;
		if(fpath.contains("/"))
			index=fpath.lastIndexOf("/");
		else if(fpath.contains("\\"))
			index=fpath.lastIndexOf("\\");
		parentDir=fpath.substring(0,index+1);
		String fileExt = "";
		int i = -1;
		if ((i = fname.lastIndexOf(".")) != -1) {
			fileExt = fname.substring(i);
			fname = fname.substring(0, i);
		}
		fname = (""+( new Date( ).getTime( ) / 1000));
		//fname = parentDir + seesionId + fname + fileExt;
		File temp = new File(parentDir + seesionId + fileExt);
		return temp;
	}
}