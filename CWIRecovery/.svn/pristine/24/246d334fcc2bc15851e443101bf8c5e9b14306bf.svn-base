package pawning.admin.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import pawning.admin.dao.InterestRateConfigurationDAO;
import pawning.admin.domain.InterestRateConfigDetail;
import pawning.admin.domain.InterestRateConfiguration;
import pawning.admin.domain.ProductConfiguration;
import pawning.admin.dto.InterestRateConfigurationDTO;
import pawning.basic.resourse.BasicDataAccessException;
import pawning.basic.resourse.CommonDaoSupport;
import pawning.basic.resourse.UserSessionConfig;

public class InterestRateConfigurationDAOImpl extends CommonDaoSupport implements InterestRateConfigurationDAO {

	@Override
	public void createInterestRateConfig(UserSessionConfig userConfig,InterestRateConfiguration interestRateConfiguration,List<InterestRateConfigDetail> list) {
		Query efffectiveQuery = getSession().createQuery("SELECT count(irc.interestRateConfigurationId) FROM InterestRateConfiguration irc WHERE irc.effectiveDate=:effectiveDate AND irc.productConfigId=:productConfigId");
		efffectiveQuery.setDate("effectiveDate", interestRateConfiguration.getEffectiveDate());
		efffectiveQuery.setInteger("productConfigId", interestRateConfiguration.getProductConfigId());
		Number count = (Number) efffectiveQuery.uniqueResult();
		if(count.intValue()!=0){
			throw new BasicDataAccessException("Record already exist.");
		}else{
			interestRateConfiguration.setCreatedTime(getCreatedTime());
			interestRateConfiguration.setLastUpdatedDate(userConfig.getLoginDate());
			interestRateConfiguration.setUserId(userConfig.getOfficerId());
			
			getHibernateTemplate().save(interestRateConfiguration);
			
			for (InterestRateConfigDetail interestRateConfigDetail : list) {
				interestRateConfigDetail.setInterestRateConfigId(interestRateConfiguration.getInterestRateConfigurationId());
				getHibernateTemplate().save(interestRateConfigDetail);
			}
		}
	}

	@Override
	public List<InterestRateConfigurationDTO> getInterestRateConfigByProductConfigId(UserSessionConfig userConfig, int productConfigId,Date effectiveDate) {
		List<InterestRateConfigurationDTO> list = new ArrayList<InterestRateConfigurationDTO>();
		ProductConfiguration productConfiguration = (ProductConfiguration) getSession().get(ProductConfiguration.class, productConfigId);
		String interestRateConfigString = "SELECT irc FROM InterestRateConfiguration irc WHERE irc.productConfigId=:productConfigId";
		if(effectiveDate!=null)
			interestRateConfigString+=" AND irc.effectiveDate>:effectiveDate";
		Query interestRateConfigQuery = getSession().createQuery(interestRateConfigString);
		interestRateConfigQuery.setInteger("productConfigId", productConfigId);
		if(effectiveDate!=null)
			interestRateConfigQuery.setDate("effectiveDate", effectiveDate);
		List<InterestRateConfiguration> configurations = interestRateConfigQuery.list();
		for (InterestRateConfiguration interestRateConfiguration : configurations) {
			InterestRateConfigurationDTO configurationDTO = new InterestRateConfigurationDTO();
			configurationDTO.setEffectiveDate(interestRateConfiguration.getEffectiveDate());
			configurationDTO.setInterestRateConfigId(interestRateConfiguration.getInterestRateConfigurationId());
			configurationDTO.setIsAnnually(interestRateConfiguration.getIsAnnually()==1?"Yes":"No");
			configurationDTO.setIsPeriodically(interestRateConfiguration.getIsPeriod()==1?"Yes":"No");
			configurationDTO.setProductFrequency(productConfiguration.getFrequency().equalsIgnoreCase("D")?"Daily":"Monthly");
			configurationDTO.setProductPeriod(productConfiguration.getPeriod());
			configurationDTO.setProductEffDate(productConfiguration.getEffectiveDate());
			configurationDTO.setIntCalculateMethod(interestRateConfiguration.getIsAnnually()==1?"Anually":"Monthly");
			configurationDTO.setIntEffectBasis(interestRateConfiguration.getIsPeriod()==1?"Period Base":"Amount Base");
			list.add(configurationDTO);
		}
			
		return list;
	}
	
	public InterestRateConfiguration getInterestRateConfigurationByConfigId(UserSessionConfig userConfig, int configId){
		InterestRateConfiguration configuration = new InterestRateConfiguration();
		configuration = (InterestRateConfiguration) getHibernateTemplate().get(InterestRateConfiguration.class, configId);
		return configuration;
	}
	
	public List<InterestRateConfigDetail> getInterestRateConfigDeatilByInterestConfigId(UserSessionConfig userConfig, int interestConfigId) {
		List<InterestRateConfigDetail> list = new ArrayList<InterestRateConfigDetail>();
		Query interestRateDetailQuery = getSession().createQuery("SELECT ircd FROM InterestRateConfigDetail ircd WHERE ircd.interestRateConfigId=:interestRateConfigId");
		interestRateDetailQuery.setInteger("interestRateConfigId", interestConfigId);
		list = interestRateDetailQuery.list();
		return list;
	}

	@Override
	public void removeInterestConfig(UserSessionConfig userConfig,int interestConfigId) {
		InterestRateConfiguration configuration = getInterestRateConfigurationByConfigId(userConfig, interestConfigId);
		List<InterestRateConfigDetail> configDetails = getInterestRateConfigDeatilByInterestConfigId(userConfig, interestConfigId);
		getHibernateTemplate().delete(configuration);
		getHibernateTemplate().deleteAll(configDetails);
	}
	
	@Override
	public InterestRateConfigurationDTO getEffectiveProductRate(UserSessionConfig userConfig, int productConfigId) {
		InterestRateConfigurationDTO dto = new InterestRateConfigurationDTO();
		ProductConfiguration productConfiguration = (ProductConfiguration) getSession().get(ProductConfiguration.class, productConfigId);
		
		String sql = "SELECT i FROM InterestRateConfiguration i WHERE i.interestRateConfigurationId = (SELECT MAX(irc.interestRateConfigurationId) FROM InterestRateConfiguration irc WHERE irc.productConfigId=:productConfigId AND irc.effectiveDate<:effectiveDate)";
		Query query = getSession().createQuery(sql);
		query.setInteger("productConfigId", productConfigId);
		query.setDate("effectiveDate", userConfig.getLoginDate());
		
		InterestRateConfiguration configuration = (InterestRateConfiguration) query.uniqueResult();
		
		sql = "SELECT d FROM InterestRateConfigDetail d WHERE d.interestRateConfigDetailId = (SELECT MAX(det.interestRateConfigDetailId) FROM InterestRateConfigDetail det WHERE det.interestRateConfigId=:interestRateConfigId)";
		query = getSession().createQuery(sql);
		query.setInteger("interestRateConfigId", configuration.getInterestRateConfigurationId());
		
		InterestRateConfigDetail detail = (InterestRateConfigDetail) query.uniqueResult();
		
		dto.setEffectiveRate(detail.getInterestRate());
		dto.setProductFrequency(productConfiguration.getFrequency());
		dto.setProductPeriod(productConfiguration.getPeriod());
		dto.setProductIntRateConfigId(configuration.getInterestRateConfigurationId());
		return dto;
	} 

}
