package pawning.admin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;

import pawning.admin.domain.RefProgram;
import pawning.admin.domain.RefProgramData;
import pawning.admin.spring.RefProgramBD;
import pawning.admin.spring.RefProgramDataBD;
import pawning.basic.resourse.BasicException;
import pawning.basic.resourse.CommonDispatch;
import pawning.basic.resourse.SessionUtil;
import pawning.basic.resourse.StrutsFormValidateUtil;



public class RefProgramDataAction extends CommonDispatch {

	private RefProgramDataBD refProgramDataBD;
	private RefProgramBD refProgramBD;
	
	public RefProgramDataBD getRefProgramDataBD() {
		return refProgramDataBD;
	}
	public void setRefProgramDataBD(RefProgramDataBD refProgramDataBD) {
		this.refProgramDataBD = refProgramDataBD;
	}
	
	public RefProgramBD getRefProgramBD() {
		return refProgramBD;
	}
	public void setRefProgramBD(RefProgramBD refProgramBD) {
		this.refProgramBD = refProgramBD;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	RefProgram refProgram = null;
        	int refProgramId = 0;
    		if(request.getParameter("refProgramId")!=null && !request.getParameter("refProgramId").equals(""))
    			refProgramId=Integer.parseInt(request.getParameter("refProgramId"));
        	String code = request.getParameter("code");
    		String description = request.getParameter("description");
    		try {
    			refProgram = getRefProgramBD().getReferenceProgramById(SessionUtil.getUserSession(request), refProgramId);
			} catch (BasicException e) {
				response.getWriter().write(StrutsFormValidateUtil.getMessageNotFound(messageResources).toString());
			}
    		try {
    			RefProgramData refProgramData = new RefProgramData();
    			//refProgramData.setRefProgram(refProgram);
    			refProgramData.setRefPrgId(refProgramId);
    			refProgramData.setRefProgDataCode(code);
    			refProgramData.setRefProgDataDescription(description);
    			
    			getRefProgramDataBD().createRefProgramData(SessionUtil.getUserSession(request), refProgramData);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int refProgramDataId = 0;
    		if(request.getParameter("refProgramDataId")!=null && !request.getParameter("refProgramDataId").equals(""))
    			refProgramDataId=Integer.parseInt(request.getParameter("refProgramDataId"));
        	String code = request.getParameter("code");
    		String description = request.getParameter("description");
    		try {
    			RefProgramData refProgramData = getRefProgramDataBD().getRefProgramDataById(SessionUtil.getUserSession(request), refProgramDataId);

    			refProgramData.setRefProgDataCode(code);
    			refProgramData.setRefProgDataDescription(description);
    			
    			getRefProgramDataBD().updateRefProgramData(SessionUtil.getUserSession(request), refProgramData);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageUpdateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
        MessageResources messageResources = getResources(request,"message");
		
        int refProgramDataId = 0;
		if(request.getParameter("refProgramDataId")!=null && !request.getParameter("refProgramDataId").equals(""))
			refProgramDataId=Integer.parseInt(request.getParameter("refProgramDataId"));
		try {
			getRefProgramDataBD().deleteRefProgramData(SessionUtil.getUserSession(request), refProgramDataId);
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
			return null;
		}
		response.getWriter().write(StrutsFormValidateUtil.getMessageDeleteSuccess(messageResources).toString());
		return null;
	}
	
	public ActionForward getAllRefProgramData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		int refProgramId = 0;
		if(request.getParameter("refProgramId")!=null && !request.getParameter("refProgramId").equals(""))
			refProgramId=Integer.parseInt(request.getParameter("refProgramId"));
		try {
			List<RefProgramData> list = getRefProgramDataBD().getAllRefProgramDataByRefProgramId(SessionUtil.getUserSession(request), refProgramId);
			JSONArray mainArray = new JSONArray();
			for(RefProgramData refProgramData : list){
				JSONArray array = new JSONArray();
				array.put(refProgramData.getRefProgDataCode());
				array.put(refProgramData.getRefProgDataDescription());
				array.put(refProgramData.getRefProgDataId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	public ActionForward getAllRefProgramDataForView(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<RefProgramData> list = getRefProgramDataBD().getAllRefProgramData(SessionUtil.getUserSession(request));
			JSONArray mainArray = new JSONArray();
			for(RefProgramData refProgramData : list){
				JSONArray array = new JSONArray();
				RefProgram refProgram = getRefProgramBD().getReferenceProgramById(SessionUtil.getUserSession(request), refProgramData.getRefPrgId());
				array.put(refProgram.getRefProgDescription());
				array.put(refProgramData.getRefProgDataCode());
				array.put(refProgramData.getRefProgDataDescription());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
}