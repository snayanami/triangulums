package pawning.pawn.dao.hibernate;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import pawning.admin.domain.Branch;
import pawning.admin.domain.RefProgramData;
import pawning.basic.resourse.CommonDaoSupport;
import pawning.basic.resourse.LedgerAccountCodeEnum;
import pawning.basic.resourse.PawningStatusEnum;
import pawning.basic.resourse.TransactionTypeEnum;
import pawning.basic.resourse.UserSessionConfig;
import pawning.generalledger.domain.Receipt;
import pawning.generalledger.domain.ReceiptComponent;
import pawning.pawn.dao.AuctionDAO;
import pawning.pawn.domain.AuctionDetails;
import pawning.pawn.domain.DueDetails;
import pawning.pawn.domain.PawnTicketDetails;
import pawning.pawn.domain.PawnTicketDetailsHistory;
import pawning.pawn.domain.PawningTicket;
import pawning.pawn.dto.PawnDetailsDTO;

public class AuctionDAOImpl extends CommonDaoSupport implements AuctionDAO {
	
	public List<PawnDetailsDTO> getArticleDetailsByPawnTicketId(UserSessionConfig userSessionConfig, int pawningTicketId){
		List<PawnDetailsDTO> returnList = new ArrayList<PawnDetailsDTO>();
		Criteria criteria = getSession().createCriteria(PawnTicketDetails.class);
		criteria.add(Restrictions.eq("pawningTicketId", pawningTicketId));
		List<PawnTicketDetails> list = criteria.list();
		for (PawnTicketDetails ticketDetails : list) {
			PawnDetailsDTO detailsDTO = new PawnDetailsDTO();
			
			RefProgramData articleType = (RefProgramData)getHibernateTemplate().get(RefProgramData.class, ticketDetails.getArticleTypeId());
			RefProgramData goldType = (RefProgramData)getHibernateTemplate().get(RefProgramData.class, ticketDetails.getGoldTypeId());
			
			List<Integer> articleSequenceList = new ArrayList<Integer>();
			articleSequenceList.add(ticketDetails.getSequence());
			
			double rate = getArticlePropotionateRatio(userSessionConfig, pawningTicketId, articleSequenceList)/100;
			
			Criteria dueCriteria = getSession().createCriteria(DueDetails.class);
			dueCriteria.add(Restrictions.eq("pawningTicketId", pawningTicketId));
			DueDetails dueDetails = (DueDetails)dueCriteria.uniqueResult();
			
			double totalOutstanding = 0;
			totalOutstanding = ((dueDetails.getTotalCapitalAmount()-dueDetails.getSettledCapitalAmount()) + (dueDetails.getTotalInterestAmount()-dueDetails.getSettledWaivedInterestAmount()) + (dueDetails.getTotalOtherChargesAmount()-dueDetails.getSettledOtherChargesAmount()))*rate;
			
			detailsDTO.setArticleType(articleType.getRefProgDataDescription());
			detailsDTO.setGoldType(goldType.getRefProgDataDescription());
			detailsDTO.setNetWeight(ticketDetails.getNetWeight());
			detailsDTO.setGrossWeight(ticketDetails.getGrossWeight());
			detailsDTO.setAssesedValue(ticketDetails.getAssessedValue());
			detailsDTO.setArticleTypeId(articleType.getRefProgDataId());
			detailsDTO.setSequence(ticketDetails.getSequence());
			detailsDTO.setTotalOutstandingPerArticle(totalOutstanding);
			detailsDTO.setPawningTicketDetailsId(ticketDetails.getPawnTicketDetailsId());
			returnList.add(detailsDTO);
		}
		return returnList;
	}
	
	public boolean getIsLastArticle(UserSessionConfig userSessionConfig, int pawningTicketId){
		Query query = getSession().createQuery("SELECT COUNT(ptd.sequence) FROM PawnTicketDetails ptd WHERE ptd.pawningTicketId=:pawningTicketId");
		query.setInteger("pawningTicketId", pawningTicketId);
		Number number = (Number)query.uniqueResult();
		int count = number!=null?number.intValue():0;
		if(count==1)
			return true;
		else
			return false;
	}
	
	public String createAuction(UserSessionConfig userSessionConfig, AuctionDetails auctionDetails){
		PawningTicket pawningTicket = (PawningTicket)getHibernateTemplate().load(PawningTicket.class, auctionDetails.getPawningTicketId());
		PawnTicketDetails pawnTicketDetails = (PawnTicketDetails)getHibernateTemplate().get(PawnTicketDetails.class, auctionDetails.getPawningTicketDetailsId());
		boolean isLast = getIsLastArticle(userSessionConfig, auctionDetails.getPawningTicketId());
		List<Integer> articleSequenceList = new ArrayList<Integer>();
		articleSequenceList.add(pawnTicketDetails.getSequence());
		double rate = getArticlePropotionateRatio(userSessionConfig, pawningTicket.getPawningTicketId(), articleSequenceList)/100;
		double settlingAmount = auctionDetails.getTotalOutstanding()-auctionDetails.getAuctionCharges();
		
		Criteria dueCriteria = getSession().createCriteria(DueDetails.class);
		dueCriteria.add(Restrictions.eq("pawningTicketId", pawningTicket.getPawningTicketId()));
		DueDetails dueDetails = (DueDetails)dueCriteria.uniqueResult();
		
		double settingCapitalAmount = (dueDetails.getTotalCapitalAmount()-dueDetails.getSettledCapitalAmount())*rate;
		double settingInterestAmount = (dueDetails.getTotalInterestAmount()-dueDetails.getSettledWaivedInterestAmount())*rate;
		double settingOtherChargesAmount = (dueDetails.getTotalOtherChargesAmount()-dueDetails.getSettledOtherChargesAmount())*rate;
		
		//*Create Auction Receipt*//
		Receipt receipt = new Receipt();
		receipt.setPawningTicketId(pawningTicket.getPawningTicketId());
		receipt.setBranchId(pawningTicket.getBranchId());
		receipt.setReceiptAmount(auctionDetails.getSalePrice());
		receipt.setSettledAmount(auctionDetails.getSalePrice());
		receipt.setReceiptDate(userSessionConfig.getLoginDate());
		receipt.setDescription(pawningTicket.getPawningTicketNo()+" : Auction Receipt");
		if(isLast)
			receipt.setIsClosureReceipt(1);
		else
			receipt.setIsClosureReceipt(0);
		receipt.setClientId(pawningTicket.getClientId());
		receipt.setIsAuctionReceipt(1);
		
		receipt.setCreatedTime(getCreatedTime());
		receipt.setLastUpdatedDate(userSessionConfig.getLoginDate());
		receipt.setUserId(userSessionConfig.getOfficerId());
		
		getHibernateTemplate().save(receipt);
		
		StringBuilder receiptNo = new StringBuilder();
        DecimalFormat decimalFormat = new DecimalFormat("000000");
        Branch branch = (Branch)getHibernateTemplate().get(Branch.class, receipt.getBranchId());
		String year = getYear2Digit(userSessionConfig.getLoginDate());
		receiptNo.append(branch.getBranchCode());
		receiptNo.append("R"+year);
		receiptNo.append(decimalFormat.format(receipt.getReceiptId()));
		
		receipt.setReceiptNo(receiptNo.toString());
		getHibernateTemplate().saveOrUpdate(receipt);
		
		//*Create Auction Receipt Component*//
		ReceiptComponent receiptComponent = new ReceiptComponent();
		receiptComponent.setReceiptId(receipt.getReceiptId());
		receiptComponent.setPawningTicketId(receipt.getPawningTicketId());
		receiptComponent.setTotalSettledCapital(settingCapitalAmount);
		receiptComponent.setTotalSettledInterest(settingInterestAmount);
		receiptComponent.setTotalSettledOtherCharges(settingOtherChargesAmount);
		receiptComponent.setCreatedTime(getCreatedTime());
		receiptComponent.setLastUpdatedDate(userSessionConfig.getLoginDate());
		receiptComponent.setUserId(userSessionConfig.getOfficerId());
		
		getHibernateTemplate().save(receiptComponent);
		
		//*Update Due Details*//
		dueDetails.setSettledCapitalAmount(dueDetails.getSettledCapitalAmount()+settingCapitalAmount);
		dueDetails.setSettledWaivedInterestAmount(dueDetails.getSettledWaivedInterestAmount()+settingInterestAmount);
		dueDetails.setSettledOtherChargesAmount(dueDetails.getSettledOtherChargesAmount()+settingOtherChargesAmount);
		
		getHibernateTemplate().update(dueDetails);
		
		//*Save Auction Details*//
		auctionDetails.setReceiptId(receipt.getReceiptId());
		auctionDetails.setCreatedTime(getCreatedTime());
		auctionDetails.setLastUpdatedDate(userSessionConfig.getLoginDate());
		auctionDetails.setUserId(userSessionConfig.getOfficerId());
		
		getHibernateTemplate().save(auctionDetails);
		
		//*Create Pawning Ticket Details History*//
		PawnTicketDetailsHistory ticketDetailsHistory = new PawnTicketDetailsHistory();
		
		ticketDetailsHistory.setPawnTicketDetailsId(pawnTicketDetails.getPawnTicketDetailsId());
		ticketDetailsHistory.setPawningTicketId(pawnTicketDetails.getPawningTicketId());
		ticketDetailsHistory.setArticleTypeId(pawnTicketDetails.getArticleTypeId());
		ticketDetailsHistory.setGoldTypeId(pawnTicketDetails.getGoldTypeId());
		ticketDetailsHistory.setNetWeight(pawnTicketDetails.getNetWeight());
		ticketDetailsHistory.setGrossWeight(pawnTicketDetails.getGrossWeight());
		ticketDetailsHistory.setAssessedValue(pawnTicketDetails.getAssessedValue());
		ticketDetailsHistory.setSequence(pawnTicketDetails.getSequence());
		ticketDetailsHistory.setAuctionDetailsId(auctionDetails.getAuctionDetailsId());
		ticketDetailsHistory.setCreatedTime(getCreatedTime());
		ticketDetailsHistory.setLastUpdatedDate(userSessionConfig.getLoginDate());
		ticketDetailsHistory.setUserId(userSessionConfig.getOfficerId());
		
		getHibernateTemplate().save(ticketDetailsHistory);
		
		//*If Last Article, Update Pawning Ticket Status as Closed*//
		if(isLast){
			pawningTicket.setStatus(PawningStatusEnum.CLOSED.getCode());
		}
		
		//*Update Pawning Ticket*//
		pawningTicket.setArrearsCapitalAmount(pawningTicket.getArrearsCapitalAmount()-settingCapitalAmount);
		getHibernateTemplate().update(pawningTicket);
		
		//*Delete Auctioned Pawning Ticket Detail*//
		getHibernateTemplate().delete(pawnTicketDetails);
		
		//*Start - Pass GL Entries*//
		passGlEntries(userSessionConfig, pawningTicket, LedgerAccountCodeEnum.CASH_IN_HAND.getCode(), "Dr", TransactionTypeEnum.AUCTION.getCode(), pawningTicket.getPawningTicketNo()+" : Auction : "+receipt.getReceiptNo(), receipt.getReceiptAmount());
		passGlEntries(userSessionConfig, pawningTicket, LedgerAccountCodeEnum.PAWN_BROKING_CONTROL.getCode(), "Cr", TransactionTypeEnum.AUCTION.getCode(), pawningTicket.getPawningTicketNo()+" : Auction : "+receipt.getReceiptNo(), receiptComponent.getTotalSettledCapital());
		passGlEntries(userSessionConfig, pawningTicket, LedgerAccountCodeEnum.PAWN_INT.getCode(), "Cr", TransactionTypeEnum.AUCTION.getCode(), pawningTicket.getPawningTicketNo()+" : Auction : "+receipt.getReceiptNo(), receiptComponent.getTotalSettledInterest());
		passGlEntries(userSessionConfig, pawningTicket, LedgerAccountCodeEnum.OTHER_INCOME.getCode(), "Cr", TransactionTypeEnum.AUCTION.getCode(), pawningTicket.getPawningTicketNo()+" : Auction : "+receipt.getReceiptNo(), (receiptComponent.getTotalSettledOtherCharges()+auctionDetails.getAuctionCharges()));
		if(auctionDetails.getProfitLossAmount()>0){
			passGlEntries(userSessionConfig, pawningTicket, LedgerAccountCodeEnum.PROFIT_ON_SALE.getCode(), "Cr", TransactionTypeEnum.AUCTION.getCode(), pawningTicket.getPawningTicketNo()+" : Auction : "+receipt.getReceiptNo(), auctionDetails.getProfitLossAmount());
		}else if(auctionDetails.getProfitLossAmount()<0){
			passGlEntries(userSessionConfig, pawningTicket, LedgerAccountCodeEnum.LOSS_ON_SALE.getCode(), "Dr", TransactionTypeEnum.AUCTION.getCode(), pawningTicket.getPawningTicketNo()+" : Auction : "+receipt.getReceiptNo(), Math.abs(auctionDetails.getProfitLossAmount()));
		}
		//*End - Pass GL Entries*//
			
		return receiptNo.toString();
	}
}
