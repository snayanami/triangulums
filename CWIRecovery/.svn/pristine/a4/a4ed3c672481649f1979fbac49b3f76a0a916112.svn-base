package pawning.admin.dao.hibernate;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import pawning.admin.dao.StakeholderDAO;
import pawning.admin.domain.RefProgram;
import pawning.admin.domain.RefProgramData;
import pawning.admin.domain.Stakeholder;
import pawning.admin.domain.StakeholderTypeMapping;
import pawning.basic.resourse.BasicDataAccessException;
import pawning.basic.resourse.ReferenceProgramEnum;
import pawning.basic.resourse.UserSessionConfig;


public class StakeholderDAOImpl extends HibernateDaoSupport implements StakeholderDAO{
	
	public List<RefProgramData> getAllTitles(UserSessionConfig userSessionConfig){
		Criteria criteria = getSession().createCriteria(RefProgramData.class);
		criteria.add(Restrictions.eq("refPrgId", ReferenceProgramEnum.TITLE));
		List<RefProgramData> titleList = criteria.list();
		return titleList;
	}
	
	public void createStakeholder(UserSessionConfig userSessionConfig, Stakeholder stakeholder, int stakeholderTypeId){
		Criteria criteria = getSession().createCriteria(Stakeholder.class);
		criteria.add(Restrictions.eq("idBrNo", stakeholder.getIdBrNo()));
		criteria.setProjection(Projections.rowCount());          
        int count = ((Integer) criteria.uniqueResult()).intValue();
        if (count !=0)
            throw new BasicDataAccessException("Stakeholder already exist with given NIC.");
        
        StringBuilder stakeholderCode = new StringBuilder();
        DecimalFormat decimalFormat = new DecimalFormat("00000");
        //stakeholderCode.append(stakeholder.getCorpIndividual());
         
        getHibernateTemplate().save(stakeholder);
        
        stakeholderCode.append(decimalFormat.format(stakeholder.getStakeholderId()));
        stakeholder.setStakeholderCode(stakeholderCode.toString());
        
        getHibernateTemplate().saveOrUpdate(stakeholder);
        
        StakeholderTypeMapping stakeholderTypeMapping = new StakeholderTypeMapping();
        stakeholderTypeMapping.setStakeholderId(stakeholder.getStakeholderId());
        stakeholderTypeMapping.setReferenceProgramDataId(stakeholderTypeId);
        
        getHibernateTemplate().save(stakeholderTypeMapping);
	}
	
	public void updateStakeholder(UserSessionConfig userSessionConfig, Stakeholder stakeholder) {
		Stakeholder oldStakeholder = (Stakeholder)getHibernateTemplate().get(Stakeholder.class, stakeholder.getStakeholderId());
		
		Query query = getSession().createQuery("SELECT COUNT(pt.pawningTicketId) FROM PawningTicket pt WHERE pt.clientId=:clientId");
		query.setInteger("clientId", stakeholder.getStakeholderId());
		Number num = (Number)query.uniqueResult();
		int count = num!=null?num.intValue():0;
		if(count>0 && !oldStakeholder.getIdBrNo().equalsIgnoreCase(stakeholder.getIdBrNo())){
			throw new BasicDataAccessException("Cannot change the NIC No. Client has existing Pawn Facilities.");
		}else{
			getHibernateTemplate().evict(oldStakeholder);
		}
		
		getHibernateTemplate().update(stakeholder);
	}
	
	public Stakeholder getStakeholderById(UserSessionConfig userSessionConfig, int stakeholderId) {
		Criteria criteria = getSession().createCriteria(Stakeholder.class);
		criteria.add(Restrictions.eq("stakeholderId", stakeholderId));
		Stakeholder stakeholder = (Stakeholder)criteria.uniqueResult();
		return stakeholder;
	}
	
	public List<Stakeholder> getAllStakeholder(UserSessionConfig userSessionConfig){
		Criteria criteria = getSession().createCriteria(Stakeholder.class);
		List<Stakeholder> returnList = criteria.list();
		return returnList;
	}

	public void addStakeholder(Stakeholder stakeholder) {
		getHibernateTemplate().save(stakeholder);
	}

	public void deleteStakeholder(Stakeholder stakeholder) {
		getHibernateTemplate().delete(stakeholder);
	}

	public Collection<Stakeholder> getAllStakeholder() {
		Criteria criteria = getSession().createCriteria(Stakeholder.class);
		if(criteria.list()!=null){
			Collection<Stakeholder> collection = criteria.list();
			return collection;
		}else{
			throw new BasicDataAccessException("No recordds found.");
		}
	}

	

	public void modifyStakeholder(Stakeholder stakeholder) {
		getHibernateTemplate().update(stakeholder);
	}
}