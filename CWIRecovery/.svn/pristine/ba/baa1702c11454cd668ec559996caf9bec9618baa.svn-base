<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE beans PUBLIC "-//SPRING//DTD BEAN//EN"
"http://www.springframework.org/dtd/spring-beans.dtd">
<beans>

	<bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
		<property name="driverClassName" value="com.mysql.jdbc.Driver"/>
		<property name="url" value="jdbc:mysql://localhost:3306/pawning"/>
		<property name="username" value="root"/>
		<property name="password" value="abc@123"/>				
	</bean>
	
	<bean id="hibernateProps" class="org.springframework.beans.factory.config.PropertiesFactoryBean">
		<property name="properties">
			<props>
				<prop key="hibernate.dialect">org.hibernate.dialect.MySQLDialect</prop>
				<prop key="hibernate.cache.use_second_level_cache">true</prop>
				<prop key="hibernate.cache.use_query_cache">true</prop>
				<prop key="hibernate.cache.provider_class">org.hibernate.cache.EhCacheProvider</prop>
				<prop key="hibernate.connection.release_mode">after_transaction</prop>
				<prop key="hibernate.show_sql">true</prop>
				<prop key="hibernate.connection.charSet">utf8</prop>
				<prop key="hibernate.connection.useUnicode">true</prop>
				<prop key="hibernate.format_sql">true</prop>
				<prop key="hibernate.use_sql_comments">true</prop>
				<prop key="hibernate.generate_statistics">true</prop>
				<prop key="hibernate.jdbc.batch_size">20</prop>
				<prop key="hibernate.transaction.factory_class">org.hibernate.transaction.JDBCTransactionFactory</prop>
			</props>
		</property>
	</bean>
	
	<bean id="mySessionFactory" class="org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean">
		<property name="dataSource" ref="dataSource"/>			
		<property name="hibernateProperties" ref="hibernateProps"/>	
		<property name="annotatedClasses">
			<list>
				<value>pawning.admin.domain.RefProgram</value>
				<value>pawning.admin.domain.RefProgramData</value>
				<value>pawning.admin.domain.Stakeholder</value>
				<value>pawning.admin.domain.Officer</value>
				<value>pawning.admin.domain.StakeholderTypeMapping</value>
				<value>pawning.admin.domain.Branch</value>
				<value>pawning.admin.domain.BranchOfficer</value>
				<value>pawning.admin.domain.SystemProgram</value>
				<value>pawning.admin.domain.ProgramEvent</value>
				<value>pawning.admin.domain.GroupProgram</value>
				<value>pawning.admin.domain.GroupProgramEvent</value>
				<value>pawning.basic.resourse.Entity</value>
				<value>pawning.basic.resourse.EntityAttribute</value>
				<value>pawning.admin.domain.SuperCommon</value>
				<value>pawning.system.domain.BranchDayStatus</value>
				<value>pawning.admin.domain.Holiday</value>
				<value>pawning.generalledger.domain.LedgerAccount</value>
				<value>pawning.generalledger.domain.GlEntry</value>
				<value>pawning.basic.resourse.Parameter</value>
				<value>pawning.basic.resourse.ParameterValue</value>
				<value>pawning.pawn.domain.GoldValue</value>
				<value>pawning.pawn.domain.PawnTicketDetails</value>
				<value>pawning.pawn.domain.PawningTicket</value>
				<value>pawning.generalledger.domain.PaymentVoucher</value>
				<value>pawning.pawn.domain.DueDetails</value>
				<value>pawning.generalledger.domain.Receipt</value>
				<value>pawning.generalledger.domain.ReceiptComponent</value>
				<value>pawning.admin.domain.BlackListClientHistory</value>
				<value>pawning.pawn.domain.Reminder</value>
				<value>pawning.pawn.domain.PawnTicketDetailsHistory</value>
				<value>pawning.pawn.domain.ArticleReleaseHistory</value>
				<value>pawning.admin.domain.Event</value>
				<value>pawning.pawn.domain.AuctionDetails</value>
				<value>pawning.admin.domain.SafeType</value>
				<value>pawning.admin.domain.PawningStores</value>
				<value>pawning.admin.domain.SafeLockers</value>
				<value>pawning.admin.domain.ProductConfiguration</value>
				<value>pawning.teller.domain.TellerGroups</value>
				<value>pawning.teller.domain.TellerLimits</value>
				<value>pawning.admin.domain.InterestRateConfiguration</value>
				<value>pawning.admin.domain.InterestRateConfigDetail</value>
				<value>pawning.pawn.domain.ArticleSafeDetails</value>
				<value>pawning.teller.domain.BranchVault</value>
				<value>pawning.teller.domain.Vault</value>
				<value>pawning.teller.domain.BranchTill</value>
				<value>pawning.teller.domain.Till</value>
				<value>pawning.maintenance.domain.Note</value>
			</list>
		</property>	
		<property name="annotatedPackages">
			<list>
       			<value></value>
       		</list>
		</property>
	</bean>
	
	<bean id="exceptionInterceptor" class="pawning.basic.resourse.ExceptionInterceptor"/>
	
	<bean id="hibernateInterceptor" class="org.springframework.orm.hibernate3.HibernateInterceptor">
		<property name="sessionFactory" ref="mySessionFactory" />
	</bean>
	
	<bean id="transactionAttributeSource"
		class="org.springframework.transaction.interceptor.NameMatchTransactionAttributeSource">
		<property name="properties">
			<props>
				<prop key="get*">PROPAGATION_SUPPORTS,readOnly</prop>
				<prop key="create*">PROPAGATION_REQUIRED</prop>
				<prop key="confirm*">PROPAGATION_REQUIRED</prop>
				<prop key="update*">PROPAGATION_REQUIRED</prop>
				<prop key="delete*">PROPAGATION_REQUIRED</prop>
				<prop key="remove*">PROPAGATION_REQUIRED</prop>
				<prop key="approve*">PROPAGATION_REQUIRED</prop>
			</props>
		</property>
	</bean>
	
	
	<bean id="transactionManager" class="org.springframework.orm.hibernate3.HibernateTransactionManager">
		<property name="sessionFactory" ref="mySessionFactory" />
	</bean>
	
	<bean id="baseTransactionProxy" class="org.springframework.transaction.interceptor.TransactionProxyFactoryBean"
		abstract="true" lazy-init="true">

		<property name="transactionManager" ref="transactionManager" />

		<property name="transactionAttributeSource" ref="transactionAttributeSource" />
		
		<property name="preInterceptors">
   			<list>
				<ref bean="exceptionInterceptor"/>
   			</list>
   		</property>
		
		<property name="postInterceptors">
			<list>
				<ref bean="hibernateInterceptor" />
			</list>
		</property>
		<!--Force to use CGLIB proxy instead of JDK dynamic proxy for better performance-->
		<property name="proxyTargetClass" value="true" />
	</bean>
	
</beans>
