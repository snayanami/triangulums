package pawning.admin.action;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.hibernate.mapping.Array;
import org.json.JSONArray;
import org.json.JSONObject;

import pawning.admin.domain.InterestRateConfigDetail;
import pawning.admin.domain.InterestRateConfiguration;
import pawning.admin.domain.ProductConfiguration;
import pawning.admin.dto.InterestRateConfigurationDTO;
import pawning.admin.spring.InterestRateConfigurationBD;
import pawning.admin.spring.ProductConfigurationBD;
import pawning.basic.resourse.BasicException;
import pawning.basic.resourse.CommonDispatch;
import pawning.basic.resourse.SessionUtil;
import pawning.basic.resourse.StrutsFormValidateUtil;

public class InterestRateConfigurationAction extends CommonDispatch {
	
	static DecimalFormat points2decimalFormat = new DecimalFormat();
	static {
		points2decimalFormat.setMinimumFractionDigits(2);
		points2decimalFormat.setMaximumFractionDigits(2);
		points2decimalFormat.setGroupingSize(3);
	}
	
	private InterestRateConfigurationBD interestRateConfigurationBD;
	private ProductConfigurationBD productConfigurationBD;

	public InterestRateConfigurationBD getInterestRateConfigurationBD() {
		return interestRateConfigurationBD;
	}

	public void setInterestRateConfigurationBD(InterestRateConfigurationBD interestRateConfigurationBD) {
		this.interestRateConfigurationBD = interestRateConfigurationBD;
	}
	public ProductConfigurationBD getProductConfigurationBD() {
		return productConfigurationBD;
	}

	public void setProductConfigurationBD(
			ProductConfigurationBD productConfigurationBD) {
		this.productConfigurationBD = productConfigurationBD;
	}

	public ActionForward getProductConfig(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		int productConfigurationId = 0;
		if(request.getParameter("productConfigurationId")!=null && !request.getParameter("productConfigurationId").equals(""))
			productConfigurationId=Integer.parseInt(request.getParameter("productConfigurationId"));
		try {
			ProductConfiguration productConfiguration = getProductConfigurationBD().getProductById(SessionUtil.getUserSession(request),productConfigurationId);
			JSONObject jsonObject = new JSONObject();
			if(productConfiguration!=null){
				jsonObject.put("period", productConfiguration.getPeriod());
				jsonObject.put("frequency", productConfiguration.getFrequency());
				jsonObject.put("productConfigurationId", productConfiguration.getProductConfigurationId());
				jsonObject.put("productEffDate", productConfiguration.getEffectiveDate());
			}
			
			response.getWriter().write(jsonObject.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        }
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int productConfigurationId = 0;
        	int isPeriod =0;
        	int isAnnually=0;
        	Date effDate = null;
        	String dataString = "";
        	try{
        		List<InterestRateConfigDetail> list = new ArrayList<InterestRateConfigDetail>();
        		if(request.getParameter("productConfigurationId")!=null && !request.getParameter("productConfigurationId").equals(""))
        			productConfigurationId=Integer.parseInt(request.getParameter("productConfigurationId"));
        		if(request.getParameter("isPeriod")!=null && !request.getParameter("isPeriod").equals(""))
        			isPeriod=Integer.parseInt(request.getParameter("isPeriod"));
        		if(request.getParameter("isAnnually")!=null && !request.getParameter("isAnnually").equals(""))
        			isAnnually=Integer.parseInt(request.getParameter("isAnnually"));
        		if(request.getParameter("effDate")!=null && !request.getParameter("effDate").equals(""))
        			effDate=StrutsFormValidateUtil.parseDate(request.getParameter("effDate"));
        		if(request.getParameter("dataString")!=null && !request.getParameter("dataString").equals(""))
        			dataString=request.getParameter("dataString").toString();
        		
        		InterestRateConfiguration interestRateConfiguration = new InterestRateConfiguration();
        		interestRateConfiguration.setProductConfigId(productConfigurationId);
        		interestRateConfiguration.setEffectiveDate(effDate);
        		interestRateConfiguration.setIsAnnually(isAnnually);
        		interestRateConfiguration.setIsPeriod(isPeriod);
        		if (!dataString.equals("")) {
        			String[] rows  = dataString.split("<row>");
        			for (String row : rows) {
        				String[] data = row.split("<:>");
        				InterestRateConfigDetail interestRateConfigDetail = new InterestRateConfigDetail();
        				interestRateConfigDetail.setFromValue(Double.parseDouble(data[0]));
        				interestRateConfigDetail.setToValue(Double.parseDouble(data[1]));
        				interestRateConfigDetail.setIsForEntirePeriod(data[2].toString().equals("Yes")?1:0);
        				interestRateConfigDetail.setInterestRate(Double.parseDouble(data[3]));
        				list.add(interestRateConfigDetail);
        			}
        		}
        		
        		getInterestRateConfigurationBD().createInterestRateConfig(SessionUtil.getUserSession(request),interestRateConfiguration,list);
        	}catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
        	response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
    	}
		return null;
	}
	
	
	public ActionForward getInterestRateConfig(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		
		try {
			int productConfigurationId= 0;
			if(request.getParameter("productConfigurationId")!=null && !request.getParameter("productConfigurationId").equals(""))
    			productConfigurationId=Integer.parseInt(request.getParameter("productConfigurationId"));
    		
			List<InterestRateConfigurationDTO> list = getInterestRateConfigurationBD().getInterestRateConfigByProductConfigId(SessionUtil.getUserSession(request),productConfigurationId,SessionUtil.getUserSession(request).getLoginDate());
			JSONArray mainArray = new JSONArray();
			for (InterestRateConfigurationDTO interestRateConfigurationDTO : list) {
				JSONArray array = new JSONArray();
				/*0*/array.put(interestRateConfigurationDTO.getProductEffDate());
				/*1*/array.put(interestRateConfigurationDTO.getProductPeriod());
				/*2*/array.put(interestRateConfigurationDTO.getProductFrequency());
				/*3*/array.put(interestRateConfigurationDTO.getIntEffectBasis());
				/*4*/array.put(interestRateConfigurationDTO.getIntCalculateMethod());
				/*5*//*0*/array.put(interestRateConfigurationDTO.getEffectiveDate());
				/*6*/array.put(interestRateConfigurationDTO.getInterestRateConfigId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		}catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
		return null;
	}
	
	public ActionForward getInterestRateConfigDetail(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			int interestConfigurationId= 0;
			if(request.getParameter("intRateConfig")!=null && !request.getParameter("intRateConfig").equals(""))
				interestConfigurationId=Integer.parseInt(request.getParameter("intRateConfig"));
    		InterestRateConfiguration configuration = getInterestRateConfigurationBD().getInterestRateConfigurationByConfigId(SessionUtil.getUserSession(request), interestConfigurationId);
    		if(configuration!=null){
    			List<InterestRateConfigDetail> list = getInterestRateConfigurationBD().getInterestRateConfigDeatilByInterestConfigId(SessionUtil.getUserSession(request),interestConfigurationId);
    			JSONArray mainArray = new JSONArray();
    			for (InterestRateConfigDetail detail : list) {
    				JSONArray array = new JSONArray();
    				/*0*/array.put(configuration.getIsPeriod()==1?detail.getFromValue():points2decimalFormat.format(detail.getFromValue()));
    				/*1*/array.put(configuration.getIsPeriod()==1?detail.getToValue():points2decimalFormat.format(detail.getToValue()));
    				/*2*/array.put(detail.getIsForEntirePeriod()==1?"Yes":"No");
    				/*3*/array.put(detail.getInterestRate());
    				mainArray.put(array);
    			}
    			response.getWriter().write(mainArray.toString());
    		}else{
    			return null;
    		}
			
		}catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
		return null;
	}
	
	public ActionForward getInterestRateConfigView(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		
		try {
			int productConfigurationId= 0;
			if(request.getParameter("productConfigurationId")!=null && !request.getParameter("productConfigurationId").equals(""))
    			productConfigurationId=Integer.parseInt(request.getParameter("productConfigurationId"));
    		
			List<InterestRateConfigurationDTO> list = getInterestRateConfigurationBD().getInterestRateConfigByProductConfigId(SessionUtil.getUserSession(request),productConfigurationId,null);
			JSONArray mainArray = new JSONArray();
			for (InterestRateConfigurationDTO interestRateConfigurationDTO : list) {
				JSONArray array = new JSONArray();
				/*0*/array.put(interestRateConfigurationDTO.getProductEffDate());
				/*1*/array.put(interestRateConfigurationDTO.getProductPeriod());
				/*2*/array.put(interestRateConfigurationDTO.getProductFrequency());
				/*3*/array.put(interestRateConfigurationDTO.getIntEffectBasis());
				/*4*/array.put(interestRateConfigurationDTO.getIntCalculateMethod());
				/*5*//*0*/array.put(interestRateConfigurationDTO.getEffectiveDate());
				/*6*/array.put(interestRateConfigurationDTO.getInterestRateConfigId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		}catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
		return null;
	}
	
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		 MessageResources messageResources = getResources(request,"message");
		 
		 	int interestConfigurationId= 0;
			if(request.getParameter("intRateConfigId")!=null && !request.getParameter("intRateConfigId").equals(""))
				interestConfigurationId=Integer.parseInt(request.getParameter("intRateConfigId"));
			try {
				getInterestRateConfigurationBD().removeInterestConfig(SessionUtil.getUserSession(request), interestConfigurationId);
			}catch (BasicException e) {
				response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
				return null;
			}
			response.getWriter().write(StrutsFormValidateUtil.getMessageDeleteSuccess(messageResources).toString());
			return null;
	}
}
