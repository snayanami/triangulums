package pawning.generalledger.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;

import pawning.basic.resourse.BasicException;
import pawning.basic.resourse.CommonDispatch;
import pawning.basic.resourse.SessionUtil;
import pawning.basic.resourse.StrutsFormValidateUtil;
import pawning.generalledger.domain.LedgerAccount;
import pawning.generalledger.spring.LedgerAccountBD;


public class LedgerAccountAction extends CommonDispatch {
	private LedgerAccountBD ledgerAccountBD;
	public LedgerAccountBD getLedgerAccountBD() {
		return ledgerAccountBD;
	}
	public void setLedgerAccountBD(LedgerAccountBD ledgerAccountBD) {
		this.ledgerAccountBD = ledgerAccountBD;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int mainCategory = 0;
    		if(request.getParameter("mainCategory")!=null && !request.getParameter("mainCategory").equals(""))
    			mainCategory=Integer.parseInt(request.getParameter("mainCategory"));
    		int isSystem = 0;
    		if(request.getParameter("isSystem")!=null && !request.getParameter("isSystem").equals(""))
    			isSystem=Integer.parseInt(request.getParameter("isSystem"));
    		int parentCodeId = 0;
    		if(request.getParameter("parentCodeId")!=null && !request.getParameter("parentCodeId").equals(""))
    			parentCodeId=Integer.parseInt(request.getParameter("parentCodeId"));
        	String standardType = request.getParameter("standardType");
    		String ledAccCode = request.getParameter("ledAccCode");
    		String description = request.getParameter("description");
    		//String trnType = request.getParameter("trnType");
    		try {
    			LedgerAccount ledgerAccount = new LedgerAccount();
    			
    			ledgerAccount.setMainLedgerCategoryCode(mainCategory);
    			ledgerAccount.setIsSystem(isSystem);
    			ledgerAccount.setParentCode(parentCodeId);
    			ledgerAccount.setStandardType(standardType);
    			ledgerAccount.setLedgerAccountCode(ledAccCode);
    			ledgerAccount.setDescription(description);
    			//ledgerAccount.setTransactionType(trnType);
    			
    			getLedgerAccountBD().createLedgerAccount(SessionUtil.getUserSession(request), ledgerAccount);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int ledgerAccountId = 0;
    		if(request.getParameter("ledgerAccountId")!=null && !request.getParameter("ledgerAccountId").equals(""))
    			ledgerAccountId=Integer.parseInt(request.getParameter("ledgerAccountId"));
    		int mainCategory = 0;
    		if(request.getParameter("mainCategory")!=null && !request.getParameter("mainCategory").equals(""))
    			mainCategory=Integer.parseInt(request.getParameter("mainCategory"));
    		int isSystem = 0;
    		if(request.getParameter("isSystem")!=null && !request.getParameter("isSystem").equals(""))
    			isSystem=Integer.parseInt(request.getParameter("isSystem"));
    		int parentCodeId = 0;
    		if(request.getParameter("parentCodeId")!=null && !request.getParameter("parentCodeId").equals(""))
    			parentCodeId=Integer.parseInt(request.getParameter("parentCodeId"));
        	String standardType = request.getParameter("standardType");
    		String ledAccCode = request.getParameter("ledAccCode");
    		String description = request.getParameter("description");
    		//String trnType = request.getParameter("trnType");
    		
    		try {
    			LedgerAccount ledgerAccount = getLedgerAccountBD().geLedgerAccountById(SessionUtil.getUserSession(request), ledgerAccountId);

    			ledgerAccount.setMainLedgerCategoryCode(mainCategory);
    			ledgerAccount.setIsSystem(isSystem);
    			ledgerAccount.setParentCode(parentCodeId);
    			ledgerAccount.setStandardType(standardType);
    			ledgerAccount.setLedgerAccountCode(ledAccCode);
    			ledgerAccount.setDescription(description);
    			//ledgerAccount.setTransactionType(trnType);
    			
    			getLedgerAccountBD().updateLedgerAccount(SessionUtil.getUserSession(request), ledgerAccount);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageUpdateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
        MessageResources messageResources = getResources(request,"message");
		
        int ledgerAccountId = 0;
		if(request.getParameter("ledgerAccountId")!=null && !request.getParameter("ledgerAccountId").equals(""))
			ledgerAccountId=Integer.parseInt(request.getParameter("ledgerAccountId"));
		try {
			getLedgerAccountBD().deleteLedgerAccount(SessionUtil.getUserSession(request), ledgerAccountId);
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
			return null;
		}
		response.getWriter().write(StrutsFormValidateUtil.getMessageDeleteSuccess(messageResources).toString());
		return null;
	}
	
	public ActionForward getAllLedgerAccount(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<LedgerAccount> list = getLedgerAccountBD().getAllLedgerAccount(SessionUtil.getUserSession(request));
			JSONArray mainArray = new JSONArray();
			for(LedgerAccount ledgerAccount : list){
				JSONArray array = new JSONArray();
				LedgerAccount pareneCode = getLedgerAccountBD().geLedgerAccountById(SessionUtil.getUserSession(request), ledgerAccount.getParentCode());
				/*00*/array.put(ledgerAccount.getMainLedgerCategoryCode()==1?"Asset":ledgerAccount.getMainLedgerCategoryCode()==2?"Liability":ledgerAccount.getMainLedgerCategoryCode()==3?"Income":ledgerAccount.getMainLedgerCategoryCode()==4?"Expenditure":"");
				/*01*/array.put(ledgerAccount.getStandardType());
				/*02*/array.put(ledgerAccount.getLedgerAccountCode());
				/*03*/array.put(ledgerAccount.getDescription());
				/*04*/array.put(ledgerAccount.getLedgerAccountId());
				/*05*/array.put(ledgerAccount.getIsSystem());
				/*06*/array.put(ledgerAccount.getParentCode());
				/*07*/array.put(pareneCode!=null?pareneCode.getLedgerAccountCode():"");
				/*08*/array.put(pareneCode!=null?pareneCode.getDescription():"");
				/*09*///array.put(ledgerAccount.getTransactionType());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	public ActionForward getAllLedgerAccountForView(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<LedgerAccount> list = getLedgerAccountBD().getAllLedgerAccount(SessionUtil.getUserSession(request));
			JSONArray mainArray = new JSONArray();
			for(LedgerAccount ledgerAccount : list){
				JSONArray array = new JSONArray();
				/*00*/array.put(ledgerAccount.getMainLedgerCategoryCode()==1?"Asset":ledgerAccount.getMainLedgerCategoryCode()==2?"Liability":ledgerAccount.getMainLedgerCategoryCode()==3?"Income":ledgerAccount.getMainLedgerCategoryCode()==4?"Expenditure":"");
				/*01*/array.put(ledgerAccount.getStandardType());
				/*02*/array.put(ledgerAccount.getLedgerAccountCode());
				/*03*/array.put(ledgerAccount.getDescription());
				/*04*/array.put(ledgerAccount.getLedgerAccountId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
}
