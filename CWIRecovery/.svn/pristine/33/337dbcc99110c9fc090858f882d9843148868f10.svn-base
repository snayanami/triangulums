package pawning.generalledger.dao.hibernate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pawning.admin.domain.Officer;
import pawning.admin.domain.Stakeholder;
import pawning.basic.resourse.BasicDataAccessException;
import pawning.basic.resourse.CommonDaoSupport;
import pawning.basic.resourse.LedgerAccountCodeEnum;
import pawning.basic.resourse.PawningStatusEnum;
import pawning.basic.resourse.TransactionTypeEnum;
import pawning.basic.resourse.UserSessionConfig;
import pawning.generalledger.dao.PaymentDAO;
import pawning.generalledger.domain.PaymentVoucher;
import pawning.pawn.domain.DueDetails;
import pawning.pawn.domain.PawningTicket;
import pawning.pawn.dto.PawnDetailsDTO;

public class PaymentDAOImpl extends CommonDaoSupport implements PaymentDAO {
	
	public List<PawnDetailsDTO> getPaymentGridData(UserSessionConfig userSessionConfig, int clientId, int pawningTicketId){
		List<PawnDetailsDTO> returnList = new ArrayList<PawnDetailsDTO>();
		Criteria criteria = getSession().createCriteria(PawningTicket.class);
		criteria.add(Restrictions.eq("branchId", userSessionConfig.getBranchId()));
		criteria.add(Restrictions.eq("status", "S"));
		if(clientId>0)
			criteria.add(Restrictions.eq("clientId", clientId));
		if(pawningTicketId>0)
			criteria.add(Restrictions.eq("pawningTicketId", pawningTicketId));
		List<PawningTicket> list = criteria.list();
		for (PawningTicket pawningTicket : list) {
			PawnDetailsDTO pawnDetailsDTO = new PawnDetailsDTO();
			Stakeholder stakeholder = (Stakeholder)getHibernateTemplate().get(Stakeholder.class, pawningTicket.getClientId());
			Officer officer = (Officer)getHibernateTemplate().get(Officer.class, pawningTicket.getApproveRejectUserId());
			
			pawnDetailsDTO.setPawnTicketNo(pawningTicket.getPawningTicketNo());
			pawnDetailsDTO.setDescription(pawningTicket.getDescription());
			pawnDetailsDTO.setTotalAssessedValue(pawningTicket.getTotalAssessedValue());
			pawnDetailsDTO.setPawningAdvance(pawningTicket.getCapitalAmount());
			pawnDetailsDTO.setClientName(stakeholder.getTitle().getRefProgDataCode()+" "+stakeholder.getFullName());
			pawnDetailsDTO.setPawnTicketId(pawningTicket.getPawningTicketId());
			pawnDetailsDTO.setApprovedOfficerName(officer.getUserName());
			pawnDetailsDTO.setApprovedDate(pawningTicket.getApproveRejectDate());
			
			returnList.add(pawnDetailsDTO);
		}
		return returnList;
	}
	
	public void createPayment(UserSessionConfig userSessionConfig, int pawningTicketId){
		PawningTicket pawningTicket = (PawningTicket)getHibernateTemplate().load(PawningTicket.class, pawningTicketId);
		
		if(pawningTicket.getStatus().equalsIgnoreCase(PawningStatusEnum.ACTIVATED.getCode())){
			throw new BasicDataAccessException("Ticket already activated.");
		}
		
		//*Update Pawning Ticket*//
		Calendar redempDate = Calendar.getInstance();
		redempDate.setTime(userSessionConfig.getLoginDate());
		redempDate.add(Calendar.YEAR, pawningTicket.getPeriod());
		
		pawningTicket.setStatus(PawningStatusEnum.ACTIVATED.getCode());
		pawningTicket.setRedemptionDate(redempDate.getTime());
		pawningTicket.setInitialRedemptionDate(redempDate.getTime());
		
		getHibernateTemplate().update(pawningTicket);
		
		//*Create Voucher*//
		PaymentVoucher paymentVoucher = new PaymentVoucher();
		paymentVoucher.setPawningTicketId(pawningTicketId);
		paymentVoucher.setAmount(pawningTicket.getCapitalAmount());
		paymentVoucher.setBranchId(pawningTicket.getBranchId());
		paymentVoucher.setCreatedTime(getCreatedTime());
		paymentVoucher.setLastUpdatedDate(userSessionConfig.getLoginDate());
		paymentVoucher.setUserId(userSessionConfig.getOfficerId());
		
		getHibernateTemplate().save(paymentVoucher);
		
		//*Create Due Details*//
		double intAmount = 0;
		intAmount = (pawningTicket.getCapitalAmount()*pawningTicket.getInterestRate()/100)/365;
		
		DueDetails dueDetails = new DueDetails();
		dueDetails.setClientId(pawningTicket.getClientId());
		dueDetails.setPawningTicketId(pawningTicketId);
		dueDetails.setBranchId(pawningTicket.getBranchId());
		dueDetails.setTotalCapitalAmount(pawningTicket.getCapitalAmount());
		dueDetails.setTotalInterestAmount(intAmount);	// Interest Amount at the point of Activation (Per Today)
		dueDetails.setTotalOtherChargesAmount(0);
		dueDetails.setSettledCapitalAmount(0);
		dueDetails.setSettledWaivedInterestAmount(0);
		dueDetails.setSettledOtherChargesAmount(0);
		dueDetails.setCreatedTime(getCreatedTime());
		dueDetails.setLastUpdatedDate(userSessionConfig.getLoginDate());
		dueDetails.setUserId(userSessionConfig.getOfficerId());
		
		getHibernateTemplate().save(dueDetails);
		
		//*Start - Pass GL Entries*//
		passGlEntries(userSessionConfig, pawningTicket, LedgerAccountCodeEnum.PAWN_BROKING_CONTROL.getCode(), "Dr", TransactionTypeEnum.PAYMENT.getCode(), pawningTicket.getPawningTicketNo()+" : Payment", pawningTicket.getCapitalAmount());
		passGlEntries(userSessionConfig, pawningTicket, LedgerAccountCodeEnum.CASH_IN_HAND.getCode(), "Cr", TransactionTypeEnum.PAYMENT.getCode(), pawningTicket.getPawningTicketNo()+" : Payment", pawningTicket.getCapitalAmount());
		//*End - Pass GL Entries*//
	}
	
}
