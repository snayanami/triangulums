package pawning.teller.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import pawning.admin.domain.Officer;
import pawning.basic.resourse.CommonDaoSupport;
import pawning.basic.resourse.UserSessionConfig;
import pawning.basic.resourse.VaultTillStatusEnum;
import pawning.teller.dao.BranchPositionDAO;
import pawning.teller.domain.Till;
import pawning.teller.domain.Vault;
import pawning.teller.dto.TellerInformationDTO;
import pawning.teller.dto.VaultTillDTO;

public class BranchPositionDAOImpl extends CommonDaoSupport implements BranchPositionDAO {

	@Override
	public List<TellerInformationDTO> getTellerInformation(UserSessionConfig userConfig, int branchId) {
		List<TellerInformationDTO> informationDTO = new ArrayList<TellerInformationDTO>();
		
		Officer officer = new Officer();
		TellerInformationDTO mainTellerInformationDTO = new TellerInformationDTO();
		String stakeholderName = "";
		int mainTeller = getVaultMainTellerId(userConfig,branchId);
		//Main Teller
		if(mainTeller>0){
			officer = (Officer)getHibernateTemplate().get(Officer.class, mainTeller);
			Query stakeholderQuery=getSession().createQuery("SELECT stk.initials as initials," +
					" stk.lastName as lastName FROM Stakeholder stk WHERE stk.stakeholderId=:stakeholderId")
					.setInteger("stakeholderId", officer.getStakeholderId());
			Object[] obj = (Object[]) stakeholderQuery.uniqueResult();
			stakeholderName= obj[0]!=null?(obj[0].toString().concat(" ")):"".concat(obj[1]!=null?obj[1].toString():"");
			mainTellerInformationDTO.setIsTeller(0);
			mainTellerInformationDTO.setStakeholderId(officer.getStakeholderId());
			mainTellerInformationDTO.setStakeholderName(stakeholderName);
			mainTellerInformationDTO.setUserId(mainTeller);
			mainTellerInformationDTO.setUserName(officer.getUserName());
			informationDTO.add(mainTellerInformationDTO);
		}
		//Other Tellers
		List<Integer> list = getTellerIdList(userConfig,branchId);
		for (Integer id : list) {
			TellerInformationDTO tellerInfoDTO = new TellerInformationDTO();
			officer = (Officer)getHibernateTemplate().get(Officer.class, id);
			Query stakeholderQuery=getSession().createQuery("SELECT stk.initials as initials," +
					" stk.lastName as lastName FROM Stakeholder stk WHERE stk.stakeholderId=:stakeholderId")
					.setInteger("stakeholderId", officer.getStakeholderId());
			Object[] obj = (Object[]) stakeholderQuery.uniqueResult();
			stakeholderName= obj[0]!=null?(obj[0].toString().concat(" ")):"".concat(obj[1]!=null?obj[1].toString():"");
			tellerInfoDTO.setIsTeller(1);
			tellerInfoDTO.setStakeholderId(officer.getStakeholderId());
			tellerInfoDTO.setStakeholderName(stakeholderName);
			tellerInfoDTO.setUserId(id);
			tellerInfoDTO.setUserName(officer.getUserName());
			informationDTO.add(tellerInfoDTO);
		}
		return informationDTO;
	}

	@Override
	public List<VaultTillDTO> getCashPosition(UserSessionConfig userSessionConfig, int userId, int isTeller,int branchId) {
		List<VaultTillDTO> list = new ArrayList<VaultTillDTO>();
		List<Till> tillList = new ArrayList<Till>();
		List<Vault> vaultList = new ArrayList<Vault>();
		VaultTillDTO vaultTillDTO = new VaultTillDTO();
		if(isTeller==0){
			vaultList = getVaultBalanceMainTellerId(userSessionConfig, userId, branchId);
			for (Vault vault : vaultList) {
				vaultTillDTO = new VaultTillDTO();
				vaultTillDTO.setBalance(vault.getVaultBalance());
				list.add(vaultTillDTO);
			}
		}else{
			tillList = getTillBalanceByTellerId(userSessionConfig, userId, branchId);
			for (Till till : tillList) {
				vaultTillDTO = new VaultTillDTO();
				vaultTillDTO.setBalance(till.getTillBalance());
				list.add(vaultTillDTO);
			}
		}
		return list;
	}

	@Override
	public List<VaultTillDTO> getBranchCashPosition(UserSessionConfig userSessionConfig, int branchId) {
		List<VaultTillDTO> list = new ArrayList<VaultTillDTO>();
			double balanceAmount = 0;
			//vault balance
			Query vQuery = getSession().createQuery("SELECT vlt FROM Vault vlt WHERE vlt.vaultId = (SELECT MAX(vt.vaultId) FROM Vault vt WHERE vt.branchId=:branchId AND vt.vaultDate=:systemDate)");
			vQuery.setInteger("branchId", branchId);
			vQuery.setDate("systemDate", userSessionConfig.getLoginDate());
			Vault vault = (Vault)vQuery.uniqueResult();
			if(vault!=null){
				balanceAmount += vault.getVaultBalance();
			}
				
			//till balance
			Query tQuery = getSession().createQuery("SELECT tl FROM Till tl WHERE tl.tillId = (SELECT MAX(t.tillId) FROM Till t WHERE t.branchId=:branchId AND t.tillDate=:systemDate)");
			tQuery.setInteger("branchId", branchId);
			tQuery.setDate("systemDate", userSessionConfig.getLoginDate());
			Till till = (Till)tQuery.uniqueResult();
			if(till!=null){
				balanceAmount +=till.getTillBalance();
			}
			VaultTillDTO vaultTillDTO = new VaultTillDTO();
			vaultTillDTO.setBalance(balanceAmount);
			list.add(vaultTillDTO);
		
		return list;
	}

	public int getVaultMainTellerId(UserSessionConfig userConfig, int branchId){
		int vaultMainTellerId = 0;
		Query query = getSession().createQuery("SELECT bv.openUsedId FROM BranchVault bv WHERE bv.branchVaultDate=:systemDate AND bv.branchId=:branchId AND bv.status=:status");
		query.setInteger("branchId", branchId);
		query.setDate("systemDate", userConfig.getLoginDate());
		query.setString("status", VaultTillStatusEnum.OPEN.getCode());
		List<Integer> list = query.list();
		if(list!=null && !list.isEmpty())
			vaultMainTellerId = list.get(0);
		return vaultMainTellerId;
	}
	
	public List<Integer> getTellerIdList(UserSessionConfig userConfig, int branchId){
		List<Integer> tellerList = new ArrayList<Integer>();
		Query query = getSession().createQuery("SELECT DISTINCT(bt.openUserId) FROM BranchTill bt WHERE bt.branchTillDate=:systemDate AND bt.branchId=:branchId AND bt.status=:status");
		query.setInteger("branchId", branchId);
		query.setDate("systemDate", userConfig.getLoginDate());
		query.setString("status", VaultTillStatusEnum.OPEN.getCode());
		tellerList = query.list();
		return tellerList;
	}
	
	//For branch id and teller id
		public List<Till> getTillBalanceByTellerId(UserSessionConfig userConfig, int tellerId, int branchId){
			List<Till> returnList = new ArrayList<Till>();
			
			Query tQuery = getSession().createQuery("SELECT tl FROM Till tl WHERE tl.tillId = (SELECT MAX(t.tillId) FROM Till t WHERE t.branchId=:branchId AND t.tillDate=:systemDate AND t.tellerId=:tellerId)");
			tQuery.setInteger("branchId", branchId);
			tQuery.setDate("systemDate", userConfig.getLoginDate());
			tQuery.setInteger("tellerId", tellerId);
			Till till = (Till)tQuery.uniqueResult();
			returnList.add(till);
			return returnList;
		}
		
		public List<Vault> getVaultBalanceMainTellerId(UserSessionConfig userConfig, int mainTellerId, int branchId){
			List<Vault> returnList = new ArrayList<Vault>();
			Query vQuery = getSession().createQuery("SELECT vlt FROM Vault vlt WHERE vlt.vaultId = (SELECT MAX(vt.vaultId) FROM Vault vt WHERE vt.branchId=:branchId AND vt.vaultDate=:systemDate AND vt.mainTellerId=:mainTellerId)");
			vQuery.setInteger("branchId",branchId);
			vQuery.setDate("systemDate", userConfig.getLoginDate());
			vQuery.setInteger("mainTellerId", mainTellerId);
			Vault vault = (Vault)vQuery.uniqueResult();
			returnList.add(vault);
			return returnList;
		}
}
