package pawning.pawn.action;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;
import org.json.JSONObject;

import pawning.basic.resourse.BasicException;
import pawning.basic.resourse.CommonDispatch;
import pawning.basic.resourse.SessionUtil;
import pawning.basic.resourse.StrutsFormValidateUtil;
import pawning.pawn.domain.AuctionDetails;
import pawning.pawn.dto.PawnDetailsDTO;
import pawning.pawn.spring.AuctionBD;

public class AuctionAction extends CommonDispatch {
	static DecimalFormat points2decimalFormat = new DecimalFormat();
	static {
		points2decimalFormat.setMinimumFractionDigits(2);
		points2decimalFormat.setMaximumFractionDigits(2);
		points2decimalFormat.setGroupingSize(3);
	}
	
	private AuctionBD auctionBD;
	public AuctionBD getAuctionBD() {
		return auctionBD;
	}
	public void setAuctionBD(AuctionBD auctionBD) {
		this.auctionBD = auctionBD;
	}
	
	public ActionForward getArticleDetails(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		int pawningTicketId = 0;
		if(request.getParameter("pawningTicketId")!=null && !request.getParameter("pawningTicketId").equals(""))
			pawningTicketId=Integer.parseInt(request.getParameter("pawningTicketId"));
		try {
			List<PawnDetailsDTO> list = getAuctionBD().getArticleDetailsByPawnTicketId(SessionUtil.getUserSession(request), pawningTicketId);
			JSONArray mainArray = new JSONArray();
			for(PawnDetailsDTO detailsDTO : list){
				JSONArray array = new JSONArray();
		   /*0*/array.put(detailsDTO.getArticleType());
		   /*1*/array.put(detailsDTO.getGoldType());
		   /*2*/array.put(points2decimalFormat.format(detailsDTO.getNetWeight()));
		   /*3*/array.put(points2decimalFormat.format(detailsDTO.getGrossWeight()));
		   /*4*/array.put(points2decimalFormat.format(detailsDTO.getAssesedValue()));
		   /*5*/array.put(points2decimalFormat.format(detailsDTO.getTotalOutstandingPerArticle()));
		   /*6*/array.put(detailsDTO.getArticleTypeId());
		   /*7*/array.put(detailsDTO.getSequence());
		   /*8*/array.put(detailsDTO.getPawningTicketDetailsId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	String receiptNo = "";
        	
        	int pawningTicketId = 0;
        	int pawningTicketDetailsId = 0;
        	double totalOutstanding = 0;
        	double profitLossAmount = 0;
        	double salePrice = 0;
        	double auctionCharges = 0;
        	Date auctionDate = null;
        	
        	if(request.getParameter("pawningTicketId")!=null && !request.getParameter("pawningTicketId").equals(""))
        		pawningTicketId=Integer.parseInt(request.getParameter("pawningTicketId"));
        	if(request.getParameter("pawningTicketDetailsId")!=null && !request.getParameter("pawningTicketDetailsId").equals(""))
        		pawningTicketDetailsId=Integer.parseInt(request.getParameter("pawningTicketDetailsId"));
        	if(request.getParameter("totalOutstanding")!=null && !request.getParameter("totalOutstanding").equals(""))
        		totalOutstanding=Double.parseDouble(request.getParameter("totalOutstanding"));
        	if(request.getParameter("profitLossAmount")!=null && !request.getParameter("profitLossAmount").equals(""))
        		profitLossAmount=Double.parseDouble(request.getParameter("profitLossAmount"));
        	if(request.getParameter("salePrice")!=null && !request.getParameter("salePrice").equals(""))
        		salePrice=Double.parseDouble(request.getParameter("salePrice"));
        	if(request.getParameter("auctionCharges")!=null && !request.getParameter("auctionCharges").equals(""))
        		auctionCharges=Double.parseDouble(request.getParameter("auctionCharges"));
        	if(request.getParameter("auctionDate")!=null && !request.getParameter("auctionDate").equals(""))
        		auctionDate=StrutsFormValidateUtil.parseDate(request.getParameter("auctionDate"));
        	
        	try {
        		AuctionDetails auctionDetails = new AuctionDetails();
            	auctionDetails.setPawningTicketId(pawningTicketId);
            	auctionDetails.setPawningTicketDetailsId(pawningTicketDetailsId);
            	auctionDetails.setBuyerName(request.getParameter("buyerName"));
            	auctionDetails.setBuyerAddress(request.getParameter("buyerAddress"));
            	auctionDetails.setBuyerNic(request.getParameter("buyerNic"));
            	auctionDetails.setBuyerContactNo(request.getParameter("contactNo"));
            	auctionDetails.setRemark(request.getParameter("remark"));
            	auctionDetails.setSalePrice(salePrice);
            	auctionDetails.setProfitLossAmount(profitLossAmount);
            	auctionDetails.setTotalOutstanding(totalOutstanding);
            	auctionDetails.setAuctionCharges(auctionCharges);
            	auctionDetails.setAuctionDate(auctionDate);
            	
            	receiptNo = getAuctionBD().createAuction(SessionUtil.getUserSession(request), auctionDetails);
        	}catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
        	JSONObject messageObject = new JSONObject();
			messageObject.put("createSuccess",messageResources.getMessage("msg.createsuccess"));
    		messageObject.put("receiptNo",receiptNo);
    		response.getWriter().write(messageObject.toString());
        }
        return null;
	}
}
