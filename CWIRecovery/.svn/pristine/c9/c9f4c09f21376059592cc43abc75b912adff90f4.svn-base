package pawning.teller.action;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.write.WritableCellFormat;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import pawning.basic.resourse.BasicException;
import pawning.basic.resourse.CommonDispatch;
import pawning.basic.resourse.SessionUtil;
import pawning.basic.resourse.StrutsFormValidateUtil;
import pawning.basic.resourse.UserSessionConfig;
import pawning.teller.dto.VaultPositionDTO;
import pawning.teller.spring.VaultPositionBD;

public class VaultPositionAction extends CommonDispatch {
	private ByteArrayOutputStream pdfStream;
	private static DecimalFormat decimalFormat=new DecimalFormat();
	static{
		decimalFormat.setMaximumFractionDigits(2);
		decimalFormat.setMinimumFractionDigits(2);
	}
	private VaultPositionBD vaultPositionBD;

	public VaultPositionBD getVaultPositionBD() {
		return vaultPositionBD;
	}

	public void setVaultPositionBD(VaultPositionBD vaultPositionBD) {
		this.vaultPositionBD = vaultPositionBD;
	}
	
	public ActionForward getVaultPosition(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        }
		 MessageResources messageResources = getResources(request,"message"); 
		try {
			List<VaultPositionDTO> vaultList=getVaultPositionBD().getVaultPositionByBranch(SessionUtil.getUserSession(request));
			JSONArray mainArray=new JSONArray();
			if(!vaultList.isEmpty()){
				for(VaultPositionDTO vaultPositionDTO:vaultList){
					JSONArray subarray=new JSONArray();
					subarray.put(vaultPositionDTO.getBranchCode());
					subarray.put(vaultPositionDTO.getBranchDescription());
					subarray.put(vaultPositionDTO.getVaultStatus());
					subarray.put(decimalFormat.format(vaultPositionDTO.getBalance()));
					
					mainArray.put(subarray);
				}
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException ex) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(ex, messageResources, getLocale(request)).toString());
			return null;	
		}
		return null;
	}
	
	public ActionForward printEXCEL(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		UserSessionConfig  sessionConfig = SessionUtil.getUserSessionInfo(request);
		
		String description  = sessionConfig.getLoginUserName();
    	String addressLine1 ="";	
        String addressLine2 = "";	
        String addressLine3 = "";	
        String addressLine4 = "";
		
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );        
		String dateString = sdf.format(sessionConfig.getLoginDate());
        SimpleDateFormat stf = new SimpleDateFormat( "hh:mm:ss a" );
        String timeString = stf.format(new Date().getTime());
		
		WritableCellFormat headerCellFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));
		WritableCellFormat companyAddressFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
		WritableCellFormat dataFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
		WritableCellFormat companyHeaderFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 12));
		WritableCellFormat decimalValueCellFormat = new WritableCellFormat(new NumberFormat("#,##0.00"));
		WritableCellFormat stringCellFormatt = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));
		
		headerCellFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));
        headerCellFormat.setBackground(Colour.GREY_25_PERCENT);
        headerCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        
        companyAddressFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
        companyAddressFormat.setAlignment(Alignment.CENTRE);
        
        dataFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
        dataFormat.setAlignment(Alignment.LEFT);
        
        companyHeaderFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 12));
        companyHeaderFormat.setAlignment(Alignment.CENTRE);
        
        decimalValueCellFormat = new WritableCellFormat(new NumberFormat("#,##0.00"));
        decimalValueCellFormat.setFont(new WritableFont(WritableFont.ARIAL, 10));
        decimalValueCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        decimalValueCellFormat.setAlignment(Alignment.RIGHT);
        
        stringCellFormatt = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));
        stringCellFormatt.setAlignment(Alignment.LEFT);
        stringCellFormatt.setBorder(Border.ALL, BorderLineStyle.THIN);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(outputStream);

        WorkbookSettings settings = new WorkbookSettings();
        settings.setInitialFileSize(1);

        WritableWorkbook workBook = Workbook.createWorkbook(outputStream, settings);
        WritableSheet sheet = workBook.createSheet("Sheet1", 0);

        int headerWidth = 5;
        int sheetNumber = 1;

        sheet.addCell(new Label(1, 1, description, companyHeaderFormat));//colmn,row
        sheet.mergeCells(1, 1, headerWidth, 1);

        sheet.addCell(new Label(1, 2, addressLine1, companyAddressFormat));
        sheet.mergeCells(1, 2, headerWidth, 2);
        
        sheet.addCell(new Label(1, 3, addressLine2, companyAddressFormat));
        sheet.mergeCells(1, 3, headerWidth, 3);
        
        sheet.addCell(new Label(1, 4, addressLine3, companyAddressFormat));
        sheet.mergeCells(1, 4, headerWidth, 4);
        
        sheet.addCell(new Label(1, 4, addressLine4, companyAddressFormat));
        sheet.mergeCells(1, 4, headerWidth, 4);
        
        sheet.addCell(new Label(1, 5, "", companyAddressFormat));
     
        sheet.addCell(new Label(1, 6, "Date :"+dateString, dataFormat));
        sheet.mergeCells(1, 6, headerWidth, 6);
  
        sheet.addCell(new Label(1, 7, "Time :"+timeString, dataFormat));
        sheet.mergeCells(1, 7, headerWidth, 7);
        
        sheet.addCell(new Label(1, 8, "", dataFormat));
        
        int line = 9;
        headerCellFormat.setAlignment(Alignment.CENTRE);
        
        sheet.addCell(new Label(1, line, "Branch Code", headerCellFormat));
        sheet.setColumnView(1, 15);

        sheet.addCell(new Label(2, line, "Description", headerCellFormat));
        sheet.setColumnView(2, 25);

        sheet.addCell(new Label(3, line, "Vault Status", headerCellFormat));
        sheet.setColumnView(3, 15);

        sheet.addCell(new Label(4, line, "Currency Type", headerCellFormat));
        sheet.setColumnView(4, 15);
        
        //headerCellFormat.setAlignment(Alignment.RIGHT);
        
        sheet.addCell(new Label(5, line, "Balance", headerCellFormat));
        sheet.setColumnView(5, 30);
       
        List<VaultPositionDTO> vaultList=getVaultPositionBD().getVaultPositionByBranch(SessionUtil.getUserSession(request));
        
        line=9;
        if(!vaultList.isEmpty()){
	        for(VaultPositionDTO positionDTO:vaultList){
	        	line++;
	            if (line > 65000) {
	                sheetNumber++;
	                sheet = workBook.createSheet("Sheet" + sheetNumber, 0);
	                line = 1;
	            }
	            
        	 sheet.addCell(new Label(1, line, positionDTO.getBranchCode(), stringCellFormatt));
        	 sheet.addCell(new Label(2, line, positionDTO.getBranchDescription(), stringCellFormatt));
        	 sheet.addCell(new Label(3, line, positionDTO.getVaultStatus(), stringCellFormatt));
        	 sheet.addCell(new Label(4, line, positionDTO.getCurrencyType(), stringCellFormatt));
        	 sheet.addCell(new jxl.write.Number(5, line, positionDTO.getBalance(), decimalValueCellFormat));
	        }
        }
		workBook.write();
        workBook.close();

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "inline; filename=Branch_Position.xls");
        response.setContentLength(outputStream.size());
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");

        ServletOutputStream stream = response.getOutputStream();
        outputStream.writeTo(stream);
        stream.flush();
		return null;
	}
}
