package pawning.pawn.action;

import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;

import pawning.admin.domain.SafeType;
import pawning.basic.resourse.BasicException;
import pawning.basic.resourse.CommonDispatch;
import pawning.basic.resourse.SessionUtil;
import pawning.basic.resourse.StrutsFormValidateUtil;
import pawning.pawn.domain.ArticleSafeDetails;
import pawning.pawn.domain.PawningTicket;
import pawning.pawn.dto.PawnDetailsDTO;
import pawning.pawn.spring.ArticleMovementDetailsBD;
import pawning.pawn.spring.PawningTicketBD;

public class ArticleAcceptanceAction extends CommonDispatch {
	static DecimalFormat points2decimalFormat = new DecimalFormat();
	static {
		points2decimalFormat.setMinimumFractionDigits(2);
		points2decimalFormat.setMaximumFractionDigits(2);
		points2decimalFormat.setGroupingSize(3);
	}
	
	private ArticleMovementDetailsBD articleMovementDetailsBD;
	public ArticleMovementDetailsBD getArticleMovementDetailsBD() {
		return articleMovementDetailsBD;
	}
	public void setArticleMovementDetailsBD(ArticleMovementDetailsBD articleMovementDetailsBD) {
		this.articleMovementDetailsBD = articleMovementDetailsBD;
	}
	
	private PawningTicketBD pawningTicketBD;
	public PawningTicketBD getPawningTicketBD() {
		return pawningTicketBD;
	}
	public void setPawningTicketBD(PawningTicketBD pawningTicketBD) {
		this.pawningTicketBD = pawningTicketBD;
	}
	
	public ActionForward loadTabPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		if (!SessionUtil.isValidSession(request)) {
			return mapping.findForward("sessionError");
		}
		String systemDate = "";
		String loginOfficer = "";
		try{
			systemDate = StrutsFormValidateUtil.parseString(SessionUtil.getUserSession(request).getLoginDate());
			loginOfficer = SessionUtil.getUserSession(request).getUserName();
		}catch (Exception e) {
			System.out.println(e.toString());
		}
		request.setAttribute("systemDate", systemDate);
		request.setAttribute("loginOfficer", loginOfficer);
		
		String action = request.getParameter("action");
		DynaActionForm frm = (DynaActionForm) form;
		frm.initialize(mapping);
        frm.set("action",action);
        String page = request.getParameter("page");
		return mapping.findForward(page);
	}
	
	public ActionForward getApprovedPawningTickets(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<PawningTicket> list = getArticleMovementDetailsBD().getApprovedPawningTicketsByBranch(SessionUtil.getUserSession(request), SessionUtil.getUserSession(request).getBranchId());
			JSONArray mainArray = new JSONArray();
			for(PawningTicket pawningTicket : list){
				JSONArray array = new JSONArray();
				array.put(pawningTicket.getPawningTicketNo());
				array.put(pawningTicket.getNoOfArticles());
				array.put(pawningTicket.getStatus());
				array.put(pawningTicket.getPawningTicketId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	public ActionForward getArticleDetails(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		int pawningTicketId = 0;
		if(request.getParameter("pawningTicketId")!=null && !request.getParameter("pawningTicketId").equals(""))
			pawningTicketId=Integer.parseInt(request.getParameter("pawningTicketId"));
		try {
			List<PawnDetailsDTO> list = getPawningTicketBD().getArticleDetailsByPawnTicketId(SessionUtil.getUserSession(request), pawningTicketId);
			JSONArray mainArray = new JSONArray();
			for(PawnDetailsDTO pawnDetailsDTO : list){
				JSONArray array = new JSONArray();
				array.put(pawnDetailsDTO.getArticleType());
				array.put(pawnDetailsDTO.getGoldType());
				array.put(points2decimalFormat.format(pawnDetailsDTO.getNetWeight()));
				array.put(points2decimalFormat.format(pawnDetailsDTO.getAssesedValue()));
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
        
        int pawningTicketId = 0;
		if(request.getParameter("pawningTicketId")!=null && !request.getParameter("pawningTicketId").equals(""))
			pawningTicketId=Integer.parseInt(request.getParameter("pawningTicketId"));
		int storeId = 0;
		if(request.getParameter("storeId")!=null && !request.getParameter("storeId").equals(""))
			storeId=Integer.parseInt(request.getParameter("storeId"));
		int safeTypeId = 0;
		if(request.getParameter("safeTypeId")!=null && !request.getParameter("safeTypeId").equals(""))
			safeTypeId=Integer.parseInt(request.getParameter("safeTypeId"));
		int lockerId = 0;
		if(request.getParameter("lockerId")!=null && !request.getParameter("lockerId").equals(""))
			lockerId=Integer.parseInt(request.getParameter("lockerId"));
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
    		try {
    			ArticleSafeDetails articleSafeDetails = new ArticleSafeDetails();
    			
    			articleSafeDetails.setPawningTicket(getPawningTicketBD().getPawningTicketById(SessionUtil.getUserSession(request), pawningTicketId));
    			articleSafeDetails.setToStoreId(storeId);
    			articleSafeDetails.setToSafeTypeId(safeTypeId);
    			articleSafeDetails.setToLockerId(lockerId);
    			
    			getArticleMovementDetailsBD().createArticleAcceptance(SessionUtil.getUserSession(request), articleSafeDetails);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
        }
		return null;
	}
	
}
