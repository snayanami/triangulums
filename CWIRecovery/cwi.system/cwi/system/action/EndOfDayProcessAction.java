package cwi.system.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;
import org.json.JSONObject;

import cwi.admin.domain.Branch;
import cwi.admin.domain.Officer;
import cwi.admin.spring.BranchBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;
import cwi.system.domain.BranchDayStatus;
import cwi.system.dto.EndOfProcessDTO;
import cwi.system.spring.EndOfDayProcessBD;

public class EndOfDayProcessAction extends CommonDispatch {
	private EndOfDayProcessBD endOfDayProcessBD;
	private BranchBD branchBD;
	public EndOfDayProcessBD getEndOfDayProcessBD() {
		return endOfDayProcessBD;
	}
	public void setEndOfDayProcessBD(EndOfDayProcessBD endOfDayProcessBD) {
		this.endOfDayProcessBD = endOfDayProcessBD;
	}
	public BranchBD getBranchBD() {
		return branchBD;
	}
	public void setBranchBD(BranchBD branchBD) {
		this.branchBD = branchBD;
	}
	
	public ActionForward loadTabPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		if (!SessionUtil.isValidSession(request)) {
			return mapping.findForward("sessionError");
		}
		String processStartDate = "";
		String processEndDate = "";
		try{
			processStartDate = StrutsFormValidateUtil.parseString(SessionUtil.getUserSession(request).getLoginDate());
			processEndDate = StrutsFormValidateUtil.parseString(getEndOfDayProcessBD().getProcessEndDate(SessionUtil.getUserSession(request)));
		}catch (Exception e) {
			System.out.println(e.toString());
		}
		
		request.setAttribute("processStartDate", processStartDate);
		request.setAttribute("processEndDate", processEndDate);
		
		String action = request.getParameter("action");
		DynaActionForm frm = (DynaActionForm) form;
		frm.initialize(mapping);
        frm.set("action",action);
        String page = request.getParameter("page");
		return mapping.findForward(page);
	}
	
	public ActionForward processEOD(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ActionMessages validateForm =form.validate(mapping,request);
		MessageResources messageResources = getResources(request,"message");        
		
		if(!validateForm.isEmpty()){
			response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
		}else{
			try{
				
				List<Integer> branchIds=new ArrayList<Integer>();
				if(!request.getParameter("branchIdArray").equals("")){
					String brnIds[]=request.getParameter("branchIdArray").split("<>");
					for(String id:brnIds){
						branchIds.add(Integer.parseInt(id));
					}
				}else if(request.getParameter("branchIdArray").equals("")){
					JSONObject errorObj=new JSONObject();
					errorObj.put("error","Please select record(s)");
					response.getWriter().write(errorObj.toString());
					return null;
				}
				Date startDate = null;
				if(request.getParameter("startDate")!=null && !request.getParameter("startDate").equals(""))
					startDate = StrutsFormValidateUtil.parseDate(request.getParameter("startDate"));
				
				Date endDate = null;
				if(request.getParameter("endDate")!=null && !request.getParameter("endDate").equals(""))
					endDate = StrutsFormValidateUtil.parseDate(request.getParameter("endDate"));
				
				getEndOfDayProcessBD().processEndOfDay(SessionUtil.getUserSession(request), branchIds, startDate, endDate);
				response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
			} catch (BasicException e) {
				response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e, messageResources, getLocale(request)).toString());
				return null;
			}
		}
		return null;
	}
	
	public ActionForward getAllBranch(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<EndOfProcessDTO> list = getEndOfDayProcessBD().getDataForEODProcess(SessionUtil.getUserSession(request));
			JSONArray mainArray = new JSONArray();
			for(EndOfProcessDTO endOfProcessDTO : list){
				JSONArray array = new JSONArray();
				array.put(endOfProcessDTO.getBranchCode());
				array.put(endOfProcessDTO.getBranchName());
				array.put(StrutsFormValidateUtil.parseString(endOfProcessDTO.getBranchDate()));
				array.put("");
				array.put(endOfProcessDTO.getBranchId());
				array.put(endOfProcessDTO.getBranchStatus());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
}
