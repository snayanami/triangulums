package cwi.system.action;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;

import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.Parameter;
import cwi.basic.resourse.ParameterValue;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;
import cwi.system.spring.ParameterBD;


public class ParameterAction extends CommonDispatch {
	private ParameterBD parameterBD;
	public ParameterBD getParameterBD() {
		return parameterBD;
	}
	public void setParameterBD(ParameterBD parameterBD) {
		this.parameterBD = parameterBD;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int paramId = 0;
    		if(request.getParameter("paramId")!=null && !request.getParameter("paramId").equals(""))
    			paramId=Integer.parseInt(request.getParameter("paramId"));
    		
        	Date effDate = StrutsFormValidateUtil.parseDate(request.getParameter("effDate"));
    		String paramValue = request.getParameter("paramValue");
    		try {
    			ParameterValue parameterValue = new ParameterValue();
    			parameterValue.setParameterId(paramId);
    			parameterValue.setValue(paramValue);
    			parameterValue.setEffectiveDate(effDate);
    			
    			getParameterBD().createParameterValue(SessionUtil.getUserSession(request), parameterValue);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward getAllParameter(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<Parameter> list = getParameterBD().getAllParameter(SessionUtil.getUserSession(request));
			JSONArray mainArray = new JSONArray();
			for(Parameter parameter : list){
				JSONArray array = new JSONArray();
				array.put(parameter.getParameterCode());
				array.put(parameter.getDescription());
				array.put(parameter.getDefaultValue());
				array.put(parameter.getParameterId());
				array.put(parameter.getInputComponentNo());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	public ActionForward getAllParameterValueByParameter(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		int parameterId = 0;
		if(request.getParameter("parameterId")!=null && !request.getParameter("parameterId").equals(""))
			parameterId=Integer.parseInt(request.getParameter("parameterId"));
		try {
			List<ParameterValue> list = getParameterBD().getAllParameterValueByParameterId(SessionUtil.getUserSession(request), parameterId);
			JSONArray mainArray = new JSONArray();
			for(ParameterValue parameterValue : list){
				JSONArray array = new JSONArray();
				array.put(parameterValue.getSequence());
				array.put(StrutsFormValidateUtil.parseString(parameterValue.getEffectiveDate()));
				array.put(parameterValue.getValue());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
}
