package cwi.system.spring;

import java.util.Date;
import java.util.List;

import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;
import cwi.system.dto.EndOfProcessDTO;


public interface EndOfDayProcessBD {
	public void updateBranchDayStatus(UserSessionConfig userSessionConfig, List<Integer> branchIdList) throws BasicException;
//	public void processEOD(UserSessionConfig userSessionConfig, List<Integer> branchIdList) throws BasicException;
	public List<EndOfProcessDTO> getDataForEODProcess(UserSessionConfig userSessionConfig) throws BasicException;
	public Date getProcessEndDate(UserSessionConfig userSessionConfig) throws BasicException;
	
	
	public void processEndOfDay(UserSessionConfig userSessionConfig, List<Integer> branchIdList, Date startDate, Date endDate) throws BasicException;
}
