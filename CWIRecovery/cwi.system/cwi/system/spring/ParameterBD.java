package cwi.system.spring;

import java.util.List;

import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.Parameter;
import cwi.basic.resourse.ParameterValue;
import cwi.basic.resourse.UserSessionConfig;


public interface ParameterBD {
	public void createParameterValue(UserSessionConfig userSessionConfig, ParameterValue parameterValue)throws BasicException;
	public List<Parameter> getAllParameter(UserSessionConfig userSessionConfig)throws BasicException;
	public List<ParameterValue> getAllParameterValueByParameterId(UserSessionConfig userSessionConfig,int parameterId)throws BasicException;
}
