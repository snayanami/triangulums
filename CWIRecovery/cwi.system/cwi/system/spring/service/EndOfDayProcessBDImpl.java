package cwi.system.spring.service;

import java.util.Date;
import java.util.List;

import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;
import cwi.system.dao.EndOfDayProcessDAO;
import cwi.system.dto.EndOfProcessDTO;
import cwi.system.spring.EndOfDayProcessBD;


public class EndOfDayProcessBDImpl implements EndOfDayProcessBD {
	private EndOfDayProcessDAO endOfDayProcessDAO;
	public EndOfDayProcessDAO getEndOfDayProcessDAO() {
		return endOfDayProcessDAO;
	}
	public void setEndOfDayProcessDAO(EndOfDayProcessDAO endOfDayProcessDAO) {
		this.endOfDayProcessDAO = endOfDayProcessDAO;
	}
	
//	public void processEOD(UserSessionConfig userSessionConfig, List<Integer> branchIdList) throws BasicException {
//		getEndOfDayProcessDAO().processEOD(userSessionConfig, branchIdList);
//	}
	
	public void updateBranchDayStatus(UserSessionConfig userSessionConfig, List<Integer> branchIdList) throws BasicException {
		getEndOfDayProcessDAO().updateBranchDayStatus(userSessionConfig, branchIdList);
	}
	
	public List<EndOfProcessDTO> getDataForEODProcess(UserSessionConfig userSessionConfig) throws BasicException {
		return getEndOfDayProcessDAO().getDataForEODProcess(userSessionConfig);
	}
	
	public Date getProcessEndDate(UserSessionConfig userSessionConfig) throws BasicException {
		return getEndOfDayProcessDAO().getProcessEndDate(userSessionConfig);
	}
	
	
	public void processEndOfDay(UserSessionConfig userSessionConfig, List<Integer> branchIdList, Date startDate, Date endDate) throws BasicException {
		getEndOfDayProcessDAO().processEndOfDay(userSessionConfig, branchIdList, startDate, endDate);
	}
}
