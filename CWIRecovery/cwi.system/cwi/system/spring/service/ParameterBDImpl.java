package cwi.system.spring.service;

import java.util.List;

import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.Parameter;
import cwi.basic.resourse.ParameterValue;
import cwi.basic.resourse.UserSessionConfig;
import cwi.system.dao.ParameterDAO;
import cwi.system.spring.ParameterBD;


public class ParameterBDImpl implements ParameterBD {
	private ParameterDAO parameterDAO;
	public ParameterDAO getParameterDAO() {
		return parameterDAO;
	}
	public void setParameterDAO(ParameterDAO parameterDAO) {
		this.parameterDAO = parameterDAO;
	}

	public List<Parameter> getAllParameter(UserSessionConfig userSessionConfig) throws BasicException {
		return getParameterDAO().getAllParameter(userSessionConfig);
	}
	
	public List<ParameterValue> getAllParameterValueByParameterId(UserSessionConfig userSessionConfig, int parameterId) throws BasicException {
		return getParameterDAO().getAllParameterValueByParameterId(userSessionConfig, parameterId);
	}
	
	public void createParameterValue(UserSessionConfig userSessionConfig, ParameterValue parameterValue) throws BasicException {
		getParameterDAO().createParameterValue(userSessionConfig, parameterValue);
	}
	
}
