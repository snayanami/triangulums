package cwi.system.dao.hibernate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import cwi.admin.domain.Branch;
import cwi.admin.domain.Holiday;
import cwi.basic.resourse.CommonDaoSupport;
import cwi.basic.resourse.ParameterCodeEnum;
import cwi.basic.resourse.StrutsFormValidateUtil;
import cwi.basic.resourse.UserSessionConfig;
import cwi.system.dao.EndOfDayProcessDAO;
import cwi.system.domain.BranchDayStatus;
import cwi.system.dto.EndOfProcessDTO;


public class EndOfDayProcessDAOImpl extends CommonDaoSupport implements EndOfDayProcessDAO {
	
	public void processEOD(UserSessionConfig userSessionConfig, List<Integer> branchIdList){
		
		//*Start - Day End Process*//
		processReminder(userSessionConfig, branchIdList);
		//*End - Day End Process*//
		
		
		//*Start - Mid Database Backup*//
		String DateStr = "";
		try {
			DateStr = StrutsFormValidateUtil.parseStringDate(userSessionConfig.getLoginDate());
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		String backupName = "E:/PawningBackup/"+DateStr+"_BEFORE_DAY_BEGIN.sql";
		boolean dbBackupOk = tbBackup("pawn", "root", "root", backupName);
		//*End - Mid Database Backup*//
		
		
		//*Start - Date Transition*//
		updateBranchDayStatus(userSessionConfig, branchIdList);
		//*End - Date Transition*//
		
		
		//*Start - Day Begin Process*//
		processInterestCalculation(userSessionConfig, branchIdList);
		//*End - Day Begin Process*//
	}
	
	public void updateBranchDayStatus(UserSessionConfig userSessionConfig, List<Integer> branchIdList){
		for (Integer branchId : branchIdList) {
			Criteria criteria = getSession().createCriteria(BranchDayStatus.class);
			criteria.add(Restrictions.eq("branchId", branchId));
			BranchDayStatus branchDayStatus = (BranchDayStatus)criteria.uniqueResult();
			
			Calendar calendar= Calendar.getInstance();
			calendar.setTime(branchDayStatus.getBranchDate());
			calendar.add(Calendar.DATE, 1);
			
			branchDayStatus.setBranchDate(calendar.getTime());
			
			userSessionConfig.setLoginDate(calendar.getTime());
			//branchDayStatus.setIsActive(1);
			getHibernateTemplate().update(branchDayStatus);
			
		}
	}
	
	public List<EndOfProcessDTO> getDataForEODProcess(UserSessionConfig userSessionConfig){
		List<EndOfProcessDTO> returnList = new ArrayList<EndOfProcessDTO>();
		Criteria criteria = getSession().createCriteria(Branch.class);
		List<Branch> list = criteria.list();
		for (Branch branch : list) {
			EndOfProcessDTO endOfProcessDTO = new EndOfProcessDTO();
			BranchDayStatus branchDayStatus = getBranchDayStatusByBranchId(userSessionConfig, branch.getBranchId());
			
			endOfProcessDTO.setBranchId(branch.getBranchId());
			endOfProcessDTO.setBranchCode(branch.getBranchCode());
			endOfProcessDTO.setBranchName(branch.getBranchName());
			endOfProcessDTO.setBranchDate(branchDayStatus.getBranchDate());
			endOfProcessDTO.setBranchStatus(branchDayStatus.getIsActive()==1?"Active":"Inactive");
			
			returnList.add(endOfProcessDTO);
		}
		
		return returnList;
	}
	
	public void processReminder(UserSessionConfig userSessionConfig, List<Integer> branchIdList){
//		for (Integer branchId : branchIdList) {
//			Criteria pawnTicketCriteria = getSession().createCriteria(PawningTicket.class);
//			pawnTicketCriteria.add(Restrictions.eq("branchId", branchId));
//			pawnTicketCriteria.add(Restrictions.eq("status", PawningStatusEnum.ACTIVATED.getCode()));
//			List<PawningTicket> pawnTicketList = pawnTicketCriteria.list();
//			for (PawningTicket pawningTicket : pawnTicketList) {
//				if(pawningTicket.getRedemptionDate().compareTo(userSessionConfig.getLoginDate())<=0){
//					Calendar redempDate = Calendar.getInstance();
//					redempDate.setTime(pawningTicket.getRedemptionDate());
//					
//					List<Integer> remParamList = new ArrayList<Integer>();
//					remParamList.add(Integer.parseInt(getParameterValueByParameterCode(userSessionConfig, ParameterCodeEnum.P06.getCode())));
//					remParamList.add(Integer.parseInt(getParameterValueByParameterCode(userSessionConfig, ParameterCodeEnum.P07.getCode())));
//					remParamList.add(Integer.parseInt(getParameterValueByParameterCode(userSessionConfig, ParameterCodeEnum.P08.getCode())));
//					remParamList.add(Integer.parseInt(getParameterValueByParameterCode(userSessionConfig, ParameterCodeEnum.P09.getCode())));
//					
//					int count = 0;
//					for (Integer remParam : remParamList) {
//						redempDate.add(Calendar.DATE, remParam);
//						count ++;
//						if(redempDate.getTime().compareTo(userSessionConfig.getLoginDate())==0){
//							Reminder reminder = new Reminder();
//							
//							reminder.setReminderNo(count);
//							reminder.setPawningTicketId(pawningTicket.getPawningTicketId());
//							reminder.setGeneratedDate(userSessionConfig.getLoginDate());
//							reminder.setBranchId(pawningTicket.getBranchId());
//							
//							reminder.setCreatedTime(getCreatedTime());
//							reminder.setUserId(userSessionConfig.getOfficerId());
//							reminder.setLastUpdatedDate(userSessionConfig.getLoginDate());
//							
//							getHibernateTemplate().save(reminder);
//						}else{
//							redempDate.setTime(pawningTicket.getRedemptionDate());
//						}
//					}
//				}
//			}
//		}
	}
	
	public boolean tbBackup(String dbName, String dbUserName, String dbPassword, String path) {

		String executeCmd = "mysqldump -u" + dbUserName + " -p" + dbPassword +" " + dbName + " -r " + path;

		Process runtimeProcess;
		
		boolean isSuccess = true;
		try {
			runtimeProcess = Runtime.getRuntime().exec(executeCmd);
			int processComplete = runtimeProcess.waitFor();

			if (processComplete == 0) {
				System.out.println("Backup created successfully");
				isSuccess = true;
			} else {
				System.out.println("Could not create the backup");
				isSuccess = false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return isSuccess;
	}
	
	public void processInterestCalculation(UserSessionConfig userSessionConfig, List<Integer> branchIdList){
//		for (Integer branchId : branchIdList) {
//			Criteria pawnTicketCriteria = getSession().createCriteria(PawningTicket.class);
//			pawnTicketCriteria.add(Restrictions.eq("branchId", branchId));
//			pawnTicketCriteria.add(Restrictions.eq("status", PawningStatusEnum.ACTIVATED.getCode()));
//			List<PawningTicket> pawnTicketList = pawnTicketCriteria.list();
//			for (PawningTicket pawningTicket : pawnTicketList) {
//				double intAmount = 0;
//				intAmount = (pawningTicket.getArrearsCapitalAmount()*pawningTicket.getInterestRate()/100)/365;
//				
//				Criteria dueCriteria = getSession().createCriteria(DueDetails.class);
//				dueCriteria.add(Restrictions.eq("pawningTicketId", pawningTicket.getPawningTicketId()));
//				DueDetails dueDetails = (DueDetails)dueCriteria.uniqueResult();
//				
//				dueDetails.setTotalInterestAmount(dueDetails.getTotalInterestAmount()+intAmount);
//				
//				getHibernateTemplate().update(dueDetails);
//			}
//		}
	}
	
	public Date getProcessEndDate(UserSessionConfig userSessionConfig){
		Date systemDate = userSessionConfig.getLoginDate();
		
		Calendar endDate = Calendar.getInstance();
		endDate.setTime(systemDate);
		endDate.add(Calendar.DATE, 1);
		
		Criteria holidayCriteria = getSession().createCriteria(Holiday.class);
		holidayCriteria.add(Restrictions.gt("holiday", systemDate));
		holidayCriteria.setProjection(Projections.property("holiday"));
		holidayCriteria.addOrder(Order.asc("holiday"));
		List<Date> holidayList = holidayCriteria.list();
		for (Date day : holidayList) {
			Calendar holiday = Calendar.getInstance();
			holiday.setTime(day);
			
			if(endDate.getTime().compareTo(holiday.getTime())==0){
				endDate.add(Calendar.DATE, 1);
			}else{
				break;
			}
		}
		return endDate.getTime();
	}
	
	public void processEndOfDay(UserSessionConfig userSessionConfig, List<Integer> branchIdList, Date startDate, Date endDate){
		int noOfDays = 0;
		
		Calendar firstDate = Calendar.getInstance();
		firstDate.setTime(startDate);
		
		Calendar lastDate = Calendar.getInstance();
		lastDate.setTime(endDate);
		
		long dateDifference = lastDate.getTimeInMillis() - firstDate.getTimeInMillis();
		noOfDays = new Long(dateDifference / 86400000).intValue();
		
		for (int i = 0; i < noOfDays; i++) {
			processEOD(userSessionConfig, branchIdList);
		}
	}
	
}
