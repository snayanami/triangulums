package cwi.system.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import cwi.basic.resourse.CommonDaoSupport;
import cwi.basic.resourse.Parameter;
import cwi.basic.resourse.ParameterValue;
import cwi.basic.resourse.UserSessionConfig;
import cwi.system.dao.ParameterDAO;


public class ParameterDAOImpl extends CommonDaoSupport implements ParameterDAO {
	
	public void createParameterValue(UserSessionConfig userSessionConfig, ParameterValue parameterValue){
		Criteria paraValCriteria1 = getSession().createCriteria(ParameterValue.class);
		paraValCriteria1.add(Restrictions.eq("parameterId", parameterValue.getParameterId()));
		paraValCriteria1.setProjection(Projections.max("sequence"));
		Number maxSeq = (Number)paraValCriteria1.uniqueResult();
		int maxSeqNo = maxSeq!=null?maxSeq.intValue():0;
		
		parameterValue.setSequence(maxSeqNo+1);
		parameterValue.setUserId(userSessionConfig.getOfficerId());
		parameterValue.setLastUpdatedDate(userSessionConfig.getLoginDate());
		parameterValue.setCreatedTime(getCreatedTime());
		
		getHibernateTemplate().save(parameterValue);
	}
	
	public List<Parameter> getAllParameter(UserSessionConfig userSessionConfig){
		Criteria criteria = getSession().createCriteria(Parameter.class);
		List<Parameter> returnList = criteria.list();
		return returnList;
	}
	
	public List<ParameterValue> getAllParameterValueByParameterId(UserSessionConfig userSessionConfig,int parameterId){
		Criteria criteria = getSession().createCriteria(ParameterValue.class);
		criteria.add(Restrictions.eq("parameterId", parameterId));
		List<ParameterValue> returnList = criteria.list();
		return returnList;
	}
}
