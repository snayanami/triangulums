package cwi.system.dao;

import java.util.Date;
import java.util.List;

import cwi.basic.resourse.UserSessionConfig;
import cwi.system.dto.EndOfProcessDTO;


public interface EndOfDayProcessDAO {
	public void updateBranchDayStatus(UserSessionConfig userSessionConfig, List<Integer> branchIdList);
//	public void processEOD(UserSessionConfig userSessionConfig, List<Integer> branchIdList);
	public List<EndOfProcessDTO> getDataForEODProcess(UserSessionConfig userSessionConfig);
	public Date getProcessEndDate(UserSessionConfig userSessionConfig);
	
	public void processEndOfDay(UserSessionConfig userSessionConfig, List<Integer> branchIdList, Date startDate, Date endDate);
}
