package cwi.system.dao;

import java.util.List;

import cwi.basic.resourse.Parameter;
import cwi.basic.resourse.ParameterValue;
import cwi.basic.resourse.UserSessionConfig;


public interface ParameterDAO {
	public void createParameterValue(UserSessionConfig userSessionConfig, ParameterValue parameterValue);
	public List<Parameter> getAllParameter(UserSessionConfig userSessionConfig);
	public List<ParameterValue> getAllParameterValueByParameterId(UserSessionConfig userSessionConfig,int parameterId);
}
