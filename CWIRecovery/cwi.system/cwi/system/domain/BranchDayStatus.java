package cwi.system.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BRN_DAY_STATUS")
public class BranchDayStatus {
	private int branchDayStatusId;
	private int branchId;
	private int isActive;
	private Date branchDate;
	
	public BranchDayStatus(){}
	public BranchDayStatus(int branchDayStatusId){
		this.branchDayStatusId = branchDayStatusId;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="BRN_DAY_STATUS_ID")
	public int getBranchDayStatusId() {
		return branchDayStatusId;
	}
	public void setBranchDayStatusId(int branchDayStatusId) {
		this.branchDayStatusId = branchDayStatusId;
	}
	
	@Column(name="BRN_ID")
	public int getBranchId() {
		return branchId;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	
	@Column(name="IS_ACTIVE")
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	
	@Column(name="BRN_DATE")
	public Date getBranchDate() {
		return branchDate;
	}
	public void setBranchDate(Date branchDate) {
		this.branchDate = branchDate;
	}
}
