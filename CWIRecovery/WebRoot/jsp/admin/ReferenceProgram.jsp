<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>Reference Program</title>
    
	<script type="text/javascript" src="js/ajax.js"></script>
	<script type="text/javascript" src="js/aw.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	
	<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
	<link rel="stylesheet" href="css/common/common.css" type="text/css" />
	
	<script type="text/javascript">
		function clearScreen(id){
			$(id).innerHTML = "";              	
		}
		
		function upperCase(id){
			$(id).value = $(id).value.toUpperCase();	
		}
		
		function $(name){return document.getElementById(name);}
		
		function createBrowser(gridName,columns,cellFormat){
			var obj = new AW.UI.Grid;
			obj.setId(gridName);
			var str = new AW.Formats.String;
			var num = new AW.Formats.Number;
			obj.setCellText([]);
			obj.setCellFormat(cellFormat);
			obj.setHeaderText(columns);
			obj.setSelectorVisible(true);
			obj.setRowCount(0);
			obj.setColumnCount(columns.length);
			obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
			obj.setSelectorWidth(20);
			obj.setHeaderHeight(20);
			obj.setSelectionMode("single-row");
			obj.onSelectedRowsChanged= function(row){};
			obj.loadingCompleted= function() {
			    if (obj._maskLoading) { 
			       obj._maskLoading.style.display = 'none'; 
			    } 
			}
			obj.loading = function() { 
			    if (! obj._maskLoading) { 
			        var maskLoading = document.createElement('div'); 
			        document.body.appendChild(maskLoading); 
			        maskLoading.className = 'aw-grid-loading'; 
			        var gridEl = obj.element(); 
			        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
			        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
			        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
			        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
			        obj._maskLoading = maskLoading; 
			   	}
		        obj.clearCellModel(); 
			    obj.clearRowModel(); 
			    obj.clearScrollModel(); 
			    obj.clearSelectionModel(); 
			    obj.clearSortModel(); 
			    obj.refresh(); 
		    	obj._maskLoading.style.display = 'block'; 
			}
			return obj;
		}
		
		function setGridData(grid,data){
			grid.setCellText(data);
			grid.setRowCount(data.length);
			grid.refresh();
			grid.setSelectedRows([]);
		}
	
		function create(){
			if(!(confirm("Confirm to create a record?"))){
				return;														
			}
			var url="dispatch=create&rand="+Math.random()*19851227 + getCreateDate();
			//alert(url);
			var ajax=new ajaxObject("refProgram.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	//alert(eval('(' + responseText + ')'));
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	//alert(myData);
	            	if(myData['createSuccess']){
	            		clearAll();
	            		alert(myData['createSuccess']);
	            	}else if(myData['error']){
	            		alert(myData['error']);
	            	}else{
	            		//alert('awa');
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function update(){
			if(!checkedRecordSelected()){
				alert('Please select a record.');
				return false;
			}
			if(!(confirm("Confirm to update a record?"))){
				return;														
			}
			var url="dispatch=update&rand="+Math.random()*19851227 + getChangeData();
			//alert(url);
			var ajax=new ajaxObject("refProgram.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(myData['updateSuccess']){
	            		clearAll();
	            		getGridData();
	            		alert(myData['updateSuccess']);
	            	}else if(myData['error']){
	            		alert(myData['error']);
	            	}else{
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function remove(){
			if(!checkedRecordSelected()){
				alert('Please select a record.');
				return false;
			}
			if(!(confirm("Confirm to delete a record?"))){
				return;														
			}
			var refProgramId = $('refProgramId').value;
			var url="dispatch=delete&refProgramId="+refProgramId+"&rand="+Math.random()*19851227;
			//alert(url);
			var ajax=new ajaxObject("refProgram.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(['deleteSuccess']){
	            		clearFilling();
	            		getGridData();
	            		alert(myData['deleteSuccess']);
	            	}else{
	            		//displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function getCreateDate(){
			var code = $('code').value;
			var description = $('description').value;

			return "&code="+code+"&description="+description;
		}
		
		function getChangeData(){
			var refProgramId = $('refProgramId').value;
			
			return "&refProgramId="+refProgramId + getCreateDate();
		}
		
		/*Get Grid Data*/
        var dataArray=new Array();
		function getGridData(){
			grid.loading();
			var data="dispatch=getAllRefProgram&rand="+Math.random()*19851227;
			var ajax=new ajaxObject('refProgram.do');
				ajax.callback=function(responseText, responseStatus, responseXML) {
             		if (responseStatus == 200) {
                  		myArray = eval("("+responseText+")");							
             			setGridData(grid,myArray);
             			grid.loadingCompleted();
						dataArray = myArray;	
       	         	}
				}
			ajax.update(data,'POST');
		}
		
		function displayErrors(myData){
			for(i = 0; i<myData.length;i++){
				if(myData[i]["code"]){
					$("codeDiv").innerHTML = myData[i]["code"];
				}if(myData[i]["description"]){
					$("descriptionDiv").innerHTML = myData[i]["description"];
				}
			}
		}
		
		function clearAll(){
			clearFilling();
			clearDivision();
		}
		
		function clearFilling(){
			$('code').value = '';
			$('description').value = '';
			try{
				grid.setSelectedRows([]);	
			}catch(e){
				//alert(e);
			}
			
		}
		
		function clearDivision(){
			$("codeDiv").innerHTML = '';
			$("descriptionDiv").innerHTML = '';
		}
		
		function checkedRecordSelected(){
			var refProgramId = $('refProgramId').value ;
    		if(refProgramId ==null || refProgramId ==''){
    			return false;
    		}else
    			return true;
		}
		
		function callFunction(){
			if($('page').value!='a'){
				getGridData();	
			}
		}
	
	</script>
	
	<style>
		.browserBorder {
		    border-top: #91a7b4 1px solid;
		    border-right: #91a7b4 1px solid;
		    border-left: #91a7b4 1px solid;
		    border-bottom: #91a7b4 1px solid;
		    background-color:#eeeeee;
		}
		
		#refPrg {height: 300px;width:100%;}
		#refPrg .aw-row-selector {text-align: center}
		#refPrg .aw-column-0 {width: 200px;}
		#refPrg .aw-column-1 {width: 500px;}
		#refPrg .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#refPrg .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
		
		#refPrgView {height: 400px;width:100%;}
		#refPrgView .aw-row-selector {text-align: center}
		#refPrgView .aw-column-0 {width: 200px;}
		#refPrgView .aw-column-1 {width: 500px;}
		#refPrgView .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#refPrgView .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
	</style>
	
  </head>
  
  <body onload="callFunction()">
  	<table width="100%">
  		<tr>
  			<td>
  				<logic:equal name="refProgram" property="action" value="add">
  					<input id="page" type="hidden" value="a" name="page">
  					<table width="100%" align="center" border="0" class="">
  						<tr>
			    			<td>
			    				<table align="center" width="100%" class="browserBorder">
			    					<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.referenceprogramcode" />
			  							</td>
			  							<td>
			  								<input type="text" id="code" name="code" maxlength="3" size="5" onfocus="clearScreen('codeDiv')" onblur="upperCase('code')" onkeyup="upperCase('code')"/>
			  								<font color="red">*</font><br><div id="codeDiv" class="validate"></div>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.description" />
			  							</td>
			  							<td>
			  								<input type="text" id="description" name="description" maxlength="60" size="85"  onfocus="clearScreen('descriptionDiv')"/>
			  								<font color="red">*</font><br><div id="descriptionDiv" class="validate"></div>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			    				</table>
			    			</td>
			    		</tr>
  						<tr>
			    			<td>
			    				<table align="center" width="100%" class="browserBorder">
			    					<tr>
							  			<td></td>
							  			<td align="right">
							  				<input type="button" value="Submit" onclick="create()" id="Save" class="buttonNormal"/>
							  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
							  			</td>
							  		</tr>
			    				</table>
			    			</td>
			    		</tr>
  					</table>
  				</logic:equal>	
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<logic:equal name="refProgram" property="action" value="modify">
  					<input id="page" type="hidden" value="m" name="page">
  					<input id="refProgramId" type="hidden" name="refProgramId">
  					<table width="100%" align="center"  class="browserBorder">
  						<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" align="center">
			  								<script type="text/javascript">
												var Columns = ["<bean:message bundle='lable' key='lable.referenceprogramcode' />","<bean:message bundle='lable' key='lable.description' />"];
												var str=new AW.Formats.String;
												var num=new AW.Formats.Number;
												var grid=createBrowser('refPrg',Columns,[str,str]);
												document.write(grid);
												grid.onSelectedRowsChanged = function(row){
													$('code').value = this.getCellText(0,row);
													$('description').value = this.getCellText(1,row);
													$('refProgramId').value = this.getCellText(2,row);
					                           	};
											</script>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.referenceprogramcode" />
			  							</td>
			  							<td>
			  								<input type="text" id="code" name="code" readonly="readonly" size="5"/>
			  								<br><div id="codeDiv" class="validate"></div>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="2px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.description" />
			  							</td>
			  							<td>
			  								<input type="text" id="description" name="description" size="85" maxlength="100" onfocus="clearScreen('descriptionDiv')"/>
			  								<font color="red">*</font><br><div id="descriptionDiv" class="validate"></div>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			    				</table>
			    			</td>
			    		</tr>
  						<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
							  			<td></td>
							  			<td align="right">
							  				<input type="button" value="Submit" onclick="update()" id="Check" class="buttonNormal"/>
							  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal" />
							  			</td>
							  		</tr>
			    				</table>
			    			</td>
			    		</tr>
  					</table>
  				</logic:equal>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<logic:equal name="refProgram" property="action" value="delete">
  					<input id="page" type="hidden" value="d" name="page">
  					<input id="refProgramId" type="hidden" name="refProgramId">
  					<table width="100%" align="center" class="browserBorder">
  						<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" align="center">
			  								<script type="text/javascript">
												var Columns = ["<bean:message bundle='lable' key='lable.referenceprogramcode' />","<bean:message bundle='lable' key='lable.description' />"];
												var str=new AW.Formats.String;
												var num=new AW.Formats.Number;
												var grid=createBrowser('refPrg',Columns,[str,str,str,num]);
												document.write(grid);
												grid.onSelectedRowsChanged = function(row){
													$('code').value = this.getCellText(0,row);
													$('description').value = this.getCellText(1,row);
													$('refProgramId').value = this.getCellText(2,row);
													//$('').value = this.getCellText(3,row);
					                           	};
											</script>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.referenceprogramcode" />
			  							</td>
			  							<td>
			  								<input type="text" id="code" name="code" readonly="readonly" size="5"/>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="2px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.description" />
			  							</td>
			  							<td>
			  								<input type="text" id="description" name="description" readonly="readonly" size="85"/>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			    				</table>
			    			</td>
			    		</tr>
  						<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
							  			<td></td>
							  			<td align="right">
							  				<input type="button" value="Submit" onclick="remove()" id="Check" class="buttonNormal" />
							  				<input type="button" value="Clear" onclick="clearFilling()" id="Clear" class="buttonNormal" />
							  			</td>
							  		</tr>
			    				</table>
			    			</td>
			    		</tr>
  					</table>
  				</logic:equal>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<logic:equal name="refProgram" property="action" value="view">
  					<input id="page" type="hidden" value="v" name="page">
  					<table width="100%" align="center" class="browserBorder">
  						<tr>
  							<td colspan="2" height="5px"></td>
  						</tr>
  						<tr>
  							<td colspan="2" align="center">
  								<script type="text/javascript">
									var Columns = ["<bean:message bundle='lable' key='lable.referenceprogramcode' />","<bean:message bundle='lable' key='lable.description' />"];
									var str=new AW.Formats.String;
									var num=new AW.Formats.Number;
									var grid=createBrowser('refPrgView',Columns,[str,str,str,num]);
									document.write(grid);
								</script>
  							</td>
  						</tr>
  						<tr>
  							<td colspan="2" height="5px"></td>
  						</tr>
  					</table>
  				</logic:equal>
  			</td>
  		</tr>
  	</table>
  </body>
</html>
