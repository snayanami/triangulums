<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
	<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
	<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
  <head>
    <title>Stakeholder</title>
    
	<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
	<link rel="stylesheet" href="css/common/calendar-system.css" type="text/css" />
	<link rel="stylesheet" href="css/common/calenderCSS.css" type="text/css" />
	<link rel="stylesheet" href="css/common/common.css" type="text/css" />
	
	<script type="text/javascript" src="js/GridBrowser.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/aw.js"></script>
	<script type="text/javascript" src="js/ajax.js"></script>
	
	<script type="text/javascript" src="js/SkyBrowser.js"></script>
	
	<script type="text/javascript" src="js/skyCalendar.js"></script>
	
	<script type="text/javascript">
		function unformatNumberWithoutDotStr(num) { return num.replace(/([^A-z])/g,''); }
		function unformatNumberWithoutDot(num) { return num.replace(/([^0-9])/g,''); }
		
		function clearScreen(id){
			$(id).innerHTML = "";              	
		}
		
		function upperCase(id){
			$(id).value = $(id).value.toUpperCase();	
		}
		
		function addStakeholder(){
			var initial = document.getElementById('initials').value;
			var fullName = document.getElementById('fullName').value;
			var lastName = document.getElementById('lastName').value;
			var url="dispatch=addStakeholder&initial="+initial+"&fullName="+fullName+"&lastName="+lastName+"&rand="+Math.random()*9999;
			var ajax=new ajaxObject("stakeholder.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	         		myData=eval('(' + responseText + ')');
	            	if(myData["error"]){
		            		alert(myData["error"]);
		            }else if(myData["success"]){
		            	alert(myData["success"]);
		            	clearData();
		            }          
	            }
			}
			ajax.update(url,'POST');
		}
		function editStakeholder(opr){
			var stakeholderId = document.getElementById('stakeholderId').value;
			if(stakeholderId==""){
				alert("Please select a record.");
				return;
			}
			var initial = document.getElementById('initials').value;
			var fullName = document.getElementById('fullName').value;
			var lastName = document.getElementById('lastName').value;
			if(opr=='delete')
				var url="dispatch=deleteStakeholder&stakeholderId="+stakeholderId+"&initial="+initial+"&fullName="+fullName+"&lastName="+lastName+"&rand="+Math.random()*9999;
			else
				var url="dispatch=modifyStakeholder&stakeholderId="+stakeholderId+"&initial="+initial+"&fullName="+fullName+"&lastName="+lastName+"&rand="+Math.random()*9999;
			var ajax=new ajaxObject("stakeholder.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	         		myData=eval('(' + responseText + ')');
	            	if(myData["error"]){
		            		alert(myData["error"]);
		            }else if(myData["success"]){
		            	alert(myData["success"]);
		            	clearDataOnEdit();
		            	getAllStakeholder();
		            }          
	            }
			}
			ajax.update(url,'POST');
		}
		function getAllStakeholder(){
			var url="dispatch=getAllStakeholder&&rand="+Math.random()*9999;
			var ajax=new ajaxObject("stakeholder.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	         		myData=eval('(' + responseText + ')');
	            	if(myData["error"]){
		            		alert(myData["error"]);
		            }else{
		            	setGridData(obj,myData,myData.length);
		            }          
	            }
			}
			ajax.update(url,'POST');
		}
		function clearData(){
			document.getElementById('initials').value = "";
			document.getElementById('fullName').value = "";
			document.getElementById('lastName').value = "";
		}
		function clearDataOnEdit(){
			document.getElementById('stakeholderId').value = "";
			document.getElementById('initials').value = "";
			document.getElementById('fullName').value = "";
			document.getElementById('lastName').value = "";
		}
		
		function $(name){return document.getElementById(name);}
		
		function createBrowser(gridName,columns,cellFormat){
			var obj = new AW.UI.Grid;
			obj.setId(gridName);
			var str = new AW.Formats.String;
			var num = new AW.Formats.Number;
			obj.setCellText([]);
			obj.setCellFormat(cellFormat);
			obj.setHeaderText(columns);
			obj.setSelectorVisible(true);
			obj.setRowCount(0);
			obj.setColumnCount(columns.length);
			obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
			obj.setSelectorWidth(20);
			obj.setHeaderHeight(20);
			obj.setSelectionMode("single-row");
			obj.onSelectedRowsChanged= function(row){};
			obj.loadingCompleted= function() {
			    if (obj._maskLoading) { 
			       obj._maskLoading.style.display = 'none'; 
			    } 
			}
			obj.loading = function() { 
			    if (! obj._maskLoading) { 
			        var maskLoading = document.createElement('div'); 
			        document.body.appendChild(maskLoading); 
			        maskLoading.className = 'aw-grid-loading'; 
			        var gridEl = obj.element(); 
			        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
			        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
			        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
			        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
			        obj._maskLoading = maskLoading; 
			   	}
		        obj.clearCellModel(); 
			    obj.clearRowModel(); 
			    obj.clearScrollModel(); 
			    obj.clearSelectionModel(); 
			    obj.clearSortModel(); 
			    obj.refresh(); 
		    	obj._maskLoading.style.display = 'block'; 
			}
			return obj;
		}
		
		function setGridData(grid,data){
			grid.setCellText(data);
			grid.setRowCount(data.length);
			grid.refresh();
			grid.setSelectedRows([]);
		}
		
		function displayErrors(myData){
			if(getCheckedValueIndCor()=='I'){
				for(i = 0; i<myData.length;i++){
					if(myData[i]["stakeholderTypeId"]){
						$("stakeholderTypeDiv").innerHTML = myData[i]["stakeholderTypeId"];
					}if(myData[i]["titleId"]){
						$("titleDiv").innerHTML = myData[i]["titleId"];
					}if(myData[i]["initials"]){
						$("initialsDiv").innerHTML = myData[i]["initials"];
					}if(myData[i]["lastName"]){
						$("lastNameDiv").innerHTML = myData[i]["lastName"];
					}if(myData[i]["fullName"]){
						$("fullNameDiv").innerHTML = myData[i]["fullName"];
					}if(myData[i]["civilStatus"]){
						$("civilStatusDiv").innerHTML = myData[i]["civilStatus"];
					}if(myData[i]["gender"]){
						$("genderDiv").innerHTML = myData[i]["gender"];
					}if(myData[i]["nicNo"]){
						$("nicNoDiv").innerHTML = myData[i]["nicNo"];
					}if(myData[i]["dob"]){
						$("dobDiv").innerHTML = myData[i]["dob"];
					}if(myData[i]["tpNo"]){
						$("tpNoDiv").innerHTML = myData[i]["tpNo"];
					}if(myData[i]["email"]){
						$("emailDiv").innerHTML = myData[i]["email"];
					}if(myData[i]["address"]){
						$("addressDiv").innerHTML = myData[i]["address"];
					}if(myData[i]["stakeholderId"]){
						$("stakeholderDiv").innerHTML = myData[i]["stakeholderId"];
					}
				}	
			}else if(getCheckedValueIndCor()=='C'){
				for(i = 0; i<myData.length;i++){
					if(myData[i]["stakeholderTypeIdC"]){
						$("stakeholderTypeDivC").innerHTML = myData[i]["stakeholderTypeIdC"];
					}if(myData[i]["companyName"]){
						$("companyNameDiv").innerHTML = myData[i]["companyName"];
					}if(myData[i]["brNo"]){
						$("brNoDiv").innerHTML = myData[i]["brNo"];
					}if(myData[i]["dor"]){
						$("dorDiv").innerHTML = myData[i]["dor"];
					}if(myData[i]["contactName"]){
						$("contactNameDiv").innerHTML = myData[i]["contactName"];
					}if(myData[i]["contactDesignation"]){
						$("contactDesignationDiv").innerHTML = myData[i]["contactDesignation"];
					}if(myData[i]["contactTpNo"]){
						$("contactTpNoDiv").innerHTML = myData[i]["contactTpNo"];
					}if(myData[i]["tpNoC"]){
						$("tpNoCDiv").innerHTML = myData[i]["tpNoC"];
					}if(myData[i]["emailC"]){
						$("emailCDiv").innerHTML = myData[i]["emailC"];
					}if(myData[i]["addressC"]){
						$("addressCDiv").innerHTML = myData[i]["addressC"];
					}if(myData[i]["stakeholderId"]){
						$("stakeholderDiv").innerHTML = myData[i]["stakeholderId"];
					}
				}
			}
		}
		
		function getCheckedValueIndCor(){
			if($('individual').checked){
				return $('individual').value;
			}else if($('coorperate').checked){
				return $('coorperate').value;
			}else
				return false;
		}
		
		function onClickRadioIndCor(){
			if(getCheckedValueIndCor()=='I'){
				$('ind').style.display = "" ;
				$('cor').style.display = "none" ;
			}else if(getCheckedValueIndCor()=='C'){
				$('ind').style.display = "none" ;
				$('cor').style.display = "" ;
			}
		}
		
		function clearAll(){
			clearFilling();
			clearDivision();
		}
		
		function clearFilling(){
			if(getCheckedValueIndCor()=='I'){
				try{
					$('stakeholderTypeId').value 			= '';
					$('stakeholderType').value 				= '';
					$('stakeholderTypeDescription').value	= '';
				}catch(e){}
				try{
					$('stakeholderId').value 			= '';
					$('stakeholderCode').value 				= '';
					$('stakeholderName').value	= '';
				}catch(e){}
				
				$('titleId').value 						= '';
				$('title').value 						= '';
				$('titleComBo').value 					= '';
				$('initials').value 					= '';
				$('lastName').value 					= '';
				$('fullName').value 					= '';
				$('civilStatus').value 					= '';
				$('gender').value 						= '';
				$('nicNo').value 						= '';
				$('dob').value 							= '';
				$('tpNo').value 						= '';
				$('mobileNo').value 					= '';
				$('faxNo').value 						= '';
				$('email').value 						= '';
				$('address').value 						= '';
			}else if(getCheckedValueIndCor()=='C'){
				try{
					$('stakeholderId').value 			= '';
					$('stakeholderCode').value 				= '';
					$('stakeholderName').value	= '';
				}catch(e){}
				try{
					$('stakeholderTypeIdC').value 			= '';
					$('stakeholderTypeC').value 			= '';
					$('stakeholderTypeDescriptionC').value	= '';
				}catch(e){}
				$('companyName').value 					= '';
				$('brNo').value 						= '';
				$('dor').value 							= '';
				$('contactName').value 					= '';
				$('contactDesignation').value 			= '';
				$('contactTpNo').value 					= '';
				$('tpNoC').value 						= '';
				$('mobileNoC').value 					= '';
				$('faxNoC').value 						= '';
				$('emailC').value 						= '';
				$('addressC').value 					= '';
			}
		}
		
		function clearDivision(){
			if(getCheckedValueIndCor()=='I'){
				try{
					$("stakeholderTypeDiv").innerHTML 	= '';	
				}catch(e){}
				try{
					$("stakeholderDiv").innerHTML 	= '';	
				}catch(e){}
				$("titleDiv").innerHTML 			= '';
				$("initialsDiv").innerHTML 			= '';
				$("lastNameDiv").innerHTML 			= '';
				$("fullNameDiv").innerHTML 			= '';
				$("civilStatusDiv").innerHTML 		= '';
				$("genderDiv").innerHTML 			= '';
				$("nicNoDiv").innerHTML 			= '';
				$("dobDiv").innerHTML 				= '';
				$("tpNoDiv").innerHTML 				= '';
				$("mobileNoDiv").innerHTML 			= '';
				$("faxNoDiv").innerHTML 			= '';
				$("emailDiv").innerHTML 			= '';
				$("addressDiv").innerHTML 			= '';
			}else if(getCheckedValueIndCor()=='C'){
				try{
					$("stakeholderTypeDivC").innerHTML 	= '';
				}catch(e){}
				try{
					$("stakeholderDiv").innerHTML 	= '';	
				}catch(e){}
				$("companyNameDiv").innerHTML 		= '';
				$("brNoDiv").innerHTML 				= '';
				$("dorDiv").innerHTML 				= '';
				$("contactNameDiv").innerHTML		= '';
				$("contactDesignationDiv").innerHTML= '';
				$("contactTpNoDiv").innerHTML 		= '';
				$("tpNoCDiv").innerHTML 			= '';
				$("mobileNoCDiv").innerHTML 		= '';
				$("faxNoCDiv").innerHTML 			= '';
				$("emailCDiv").innerHTML 			= '';
				$("addressCDiv").innerHTML 			= '';
			}
		}
		
		function getCreateDate(){
			
			if(getCheckedValueIndCor()=='I'){
				var indOrCor = getCheckedValueIndCor();
				//alert("I - "+indOrCor);
				var stakeholderTypeId = 0;
				if($('pg').value=='a'){
					stakeholderTypeId = $('stakeholderTypeId').value;	
				}
				var titleId = $('titleId').value;
				var initials = $('initials').value;
				var lastName = $('lastName').value;
				var fullName = $('fullName').value;
				var civilStatus = $('civilStatus').value;
				var gender = $('gender').value;
				var nicNo = $('nicNo').value;
				var dob = $('dob').value;
				var tpNo = $('tpNo').value;
				var mobileNo = $('mobileNo').value;
				var faxNo = $('faxNo').value;
				var email = $('email').value;
				var address = $('address').value;
				var imageData =  canvas.toDataURL();
				
				return "&stakeholderTypeId="+stakeholderTypeId+"&titleId="+titleId+"&initials="+initials+"&lastName="+lastName
						+"&fullName="+fullName+"&civilStatus="+civilStatus+"&gender="+gender+"&nicNo="+nicNo+"&dob="+dob
						+"&tpNo="+tpNo+"&mobileNo="+mobileNo+"&faxNo="+faxNo+"&email="+email+"&address="+address+"&indOrCor="+indOrCor+"&profileImage="+encodeURIComponent(imageData);
				
			}else if(getCheckedValueIndCor()=='C'){
				var indOrCor = getCheckedValueIndCor();
				//alert("C - "+indOrCor);
				var stakeholderTypeIdC = 0;
				if($('pg').value=='a'){
					stakeholderTypeIdC = $('stakeholderTypeIdC').value;
				}
				var companyName = $('companyName').value;
				var brNo = $('brNo').value;
				var dor = $('dor').value;
				var contactName = $('contactName').value;
				var contactDesignation = $('contactDesignation').value;
				var contactTpNo = $('contactTpNo').value;
				var tpNoC = $('tpNoC').value;
				var mobileNoC = $('mobileNoC').value;
				var faxNoC = $('faxNoC').value;
				var emailC = $('emailC').value;
				var addressC = $('addressC').value;
				
				return "&stakeholderTypeIdC="+stakeholderTypeIdC+"&companyName="+companyName+"&brNo="+brNo+"&dor="+dor+"&contactName="+contactName
						+"&contactDesignation="+contactDesignation+"&contactTpNo="+contactTpNo+"&tpNoC="+tpNoC+"&mobileNoC="+mobileNoC+"&faxNoC="+faxNoC
						+"&emailC="+emailC+"&addressC="+addressC+"&indOrCor="+indOrCor;
			}
		}
		
		function getChangeData(){
			var stakeholderId = $('stakeholderId').value;
			
			return "&stakeholderId="+stakeholderId + getCreateDate();
		}
		
		function create(){
			var indCor = getCheckedValueIndCor();
			if(!(confirm("Confirm to create a record?"))){
				return;														
			}
			var url="dispatch=create&rand="+Math.random()*19851227 + getCreateDate();
			//alert(url);
			if(indCor=='I')
				var ajax=new ajaxObject("stakeholderInd.do");
			else
				var ajax=new ajaxObject("stakeholderCor.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	//alert(eval('(' + responseText + ')'));
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	//alert(myData);
	            	if(myData['createSuccess']){
	            		clearAll();
	            		alert(myData['createSuccess']);
	            	}else if(myData['error']){
	            		alert(myData['error']);
	            	}else{
	            		//alert('awa');
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function update(){
			var indCor = getCheckedValueIndCor();
			if(!(confirm("Confirm to update a record?"))){
				return;														
			}
			var url="dispatch=update&rand="+Math.random()*19851227 + getChangeData();
			//alert(url);
			if(indCor=='I')
				var ajax=new ajaxObject("stakeholderIndUpdate.do");
			else
				var ajax=new ajaxObject("stakeholderCorUpdate.do");
			//var ajax=new ajaxObject("stakeholder.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	//alert(eval('(' + responseText + ')'));
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	//alert(myData);
	            	if(myData['updateSuccess']){
	            		clearAll();
	            		alert(myData['updateSuccess']);
	            	}else if(myData['error']){
	            		alert(myData['error']);
	            	}else{
	            		//alert('awa');
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function getStakeholderDetails(){
			var stakeholderId = $('stakeholderId').value;
	    	if(stakeholderId!=''){
		    	var data="dispatch=getStakeholderDetails&stakeholderId="+stakeholderId+"&rand="+Math.random()*9999;				
				url = "stakeholder.do";
				var mySearchRequest = new ajaxObject(url);
				mySearchRequest.callback = function(responseText, responseStatus, responseXML) {
					if (responseStatus==200) {							
						var myObject =  eval('(' + responseText + ')');			       	
						if(myObject['error']){
							alert(myObject['error']);
						}else{
							if(myObject['indOrCor']=='I'){
								$('titleComBo').value = myObject['titleId']+"^"+myObject['titleCode'];
								$('titleId').value = myObject['titleId'];
								$('initials').value = myObject['initials'];
								$('lastName').value = myObject['lastName'];
								$('fullName').value = myObject['fullName'];
								if(myObject['civilStatus']=='S'){
									$('civilStatus').selectedIndex=1;
								}else if(myObject['civilStatus']=='M'){
									$('civilStatus').selectedIndex=2;
								}else if(myObject['civilStatus']=='D'){
									$('civilStatus').selectedIndex=3;
								}
								if(myObject['gender']=='M'){
									$('gender').selectedIndex=1;
								}else if(myObject['gender']=='F'){
									$('gender').selectedIndex=2;
								}
								$('nicNo').value = myObject['nicNo'];
								$('dob').value = myObject['dob'];
								$('tpNo').value = myObject['tpNo'];
								$('mobileNo').value = myObject['mobileNo'];
								$('faxNo').value = myObject['faxNo'];
								$('email').value = myObject['email'];
								$('address').value = myObject['address'];
								
								$('img_saved').src = myObject['profileImage']=="0"?"":"data:image/jpeg;base64,"+myObject['profileImage'];
								var canvas1 = document.getElementById('canvasID');
								var context1 = canvas1.getContext('2d');
								var image = $('img_saved');
								context1.drawImage(image, 0, 0);
								
							}else if(myObject['indOrCor']=='C'){
								$('companyName').value = myObject['companyName'];
								$('brNo').value = myObject['brNo'];
								$('dor').value = myObject['dor'];
								$('contactName').value = myObject['contactName'];
								$('contactDesignation').value = myObject['contactDesignation'];
								$('contactTpNo').value = myObject['contactTpNo'];
								$('tpNoC').value = myObject['tpNoC'];
								$('mobileNoC').value = myObject['mobileNoC'];
								$('faxNoC').value = myObject['faxNoC'];
								$('emailC').value = myObject['emailC'];
								$('addressC').value = myObject['addressC'];
							}
						}
					}else {
			    	    // 
				    }
				}
				mySearchRequest.update(data,'POST');
			}
	    }
	    
	    /*Get Grid Data*/
        var dataArray=new Array();
		function getGridData(){
			var data="dispatch=getAllStakeholder&rand="+Math.random()*19851227;
			var ajax=new ajaxObject('stakeholder.do');
				ajax.callback=function(responseText, responseStatus, responseXML) {
             		if (responseStatus == 200) {
                  		myArray = eval("("+responseText+")");							
             			setGridData(grid,myArray);
						dataArray = myArray;	
       	         	}
				}
			ajax.update(data,'POST');
		}
	    
	    function callFunction(){
	    	if($('pg').value=='v'){
	    		getGridData();
	    	}
	    }
	    
	    function validateDOB(){
	    	if(!checkdate($("dob"))&& !($("dob").value=='')){
				$('dobDiv').innerHTML = "Date of Birth is Invalid ";
				$("dob").value='';
				return false;
			}
	    }
	    
	    var actualNic='';
	    function validateNic(){
	    	var nic=$('nicNo').value;
	    	if(nic=='')
                return;
          	if(nic.length==10 && (nic.charAt(9)=='V' || nic.charAt(9)=='X')){
          		var dates=parseInt(unformatNumberWithoutDot(nic.substring(2,5))*1);
            	var gender=(dates<501)?"M":"F";
                dates=(dates<501)?dates:dates-500;
                var dateofbirth=new Date();
                var year=parseInt(nic.substring(0,2));
                dateofbirth.setYear(year);
                dateofbirth.setMonth(0);
                dateofbirth.setDate((year%4!=0 && dates>59)?dates-1:dates);
                $('gender').value=gender;
                actualNic=(dateofbirth.getDate()>9?dateofbirth.getDate():'0'+dateofbirth.getDate())+'/'+((dateofbirth.getMonth()+1)>9?dateofbirth.getMonth()+1:'0'+(dateofbirth.getMonth()+1))+'/'+dateofbirth.getFullYear();
          	}else{
          		alert('NIC Number is invalid!');
          		return false;
          	}
	    }
	    
	    function validateNICwithDOB(){
			var entered=$('dob').value;
			if( $('nicNo').value!='' && entered!=actualNic){
				alert('Date Of Birth mismatch with NIC Number.');
			}
		}
		
	</script>
	
	<style>
		
		.browserBorder {
		    border-top: #91a7b4 1px solid;
		    border-right: #91a7b4 1px solid;
		    border-left: #91a7b4 1px solid;
		    border-bottom: #91a7b4 1px solid;
		    background-color:#eeeeee;
		}
		
		#stk {height: 420px;width:100%;}
		#stk .aw-row-selector {text-align: center}
		#stk .aw-column-0 {width: 100px;}
		#stk .aw-column-1 {width: 250px;}
		#stk .aw-column-2 {width: 100px;}
		#stk .aw-column-3 {width: 100px;}
		#stk .aw-column-4 {width: 100px;}
		#stk .aw-column-5 {width: 100px;}
		#stk .aw-column-6 {width: 100px;}
		#stk .aw-column-7 {width: 100px;}
		#stk .aw-column-8 {width: 100px;}
		#stk .aw-column-9 {width: 100px;}
		#stk .aw-column-10 {width: 150px;}
		#stk .aw-column-11 {width: 350px;}
		#stk .aw-column-12 {width: 100px;}
		#stk .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#stk .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
	</style>
	
  </head>
  
  <body onload="callFunction()" style="">
  	<table width="100%">
    	<tr>
			<td>
				<logic:equal name="stakeholder" property="action" value="add">
					<input type="hidden" id="pg" name="pg" value="a">
			    	<table width="100%">
			    		<tr style="display: ;">
			    			<td>
			    				<table width="100%" align="center" class="browserBorder">
			    					<tr>
						    			<td align="center">
						    				<input type="radio" name="indCor" id="individual" value="I" onclick="onClickRadioIndCor();clearAll();" onchange="onClickRadioIndCor();clearAll()" checked="checked">Individual
						    				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="indCor" id="coorperate" value="C" onclick="onClickRadioIndCor();clearAll()" onchange="onClickRadioIndCor();clearAll()">Corporate
						    			</td>
						    		</tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<table width="100%" align="center" class="browserBorder">
			    					<tr>
						    			<td>
						    				<div id="ind" style="overflow: auto;">
						    					<table width="100%">
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.stakeholdertype" />
						    							</td>
						    							<td colspan="3">
							  								<input type="hidden" id="stakeholderTypeId" name="stakeholderTypeId" value=""/>
							  								<input type="text" name="stakeholderType" id="stakeholderType" maxlength="3" size="5"
																onfocus="$('stakeholderTypeDiv').innerHTML=''" onKeyUp=""
																onblur="" readonly="readonly"/>
																<input type="button" name="btnStakeholderType" id="btnStakeholderType" value="..." class="dot3buttonNormal"
																onclick="showSkyBrowser(2,'stakeholderTypeId','stakeholderType',0,'stakeholderTypeDescription',1,0,0,'refPrgId','1');$('stakeholderTypeDiv').innerHTML='';"/>
							  								<input name="stakeholderTypeDescription" type="text" id="stakeholderTypeDescription" maxlength="60" size="75" readonly="readonly" tabindex="-1"/><font color="red">* </font>
							  								<br><div id="stakeholderTypeDiv" class="validate"></div>
							  							</td>
						    						</tr>
						    						<tr>
						    							<td colspan="4" height="3px"></td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.title" />
						    							</td>
						    							<td>
						    								<input type="hidden" id="titleId">
						    								<input type="hidden" id="title">
						    								<select id="titleComBo" name="titleComBo" onfocus="clearScreen('titleDiv')" onchange="" onblur="var tileArray = $('titleComBo').value.split('^');
						    																					$('titleId').value=tileArray[0]; 
						    																					$('title').value=tileArray[1];" style="width: 100px;">
						    									<option value="">-- Select --</option>
						    									<c:forEach items="${titleList}" var="title" >
						    										<option value='<c:out value="${title.refProgDataId}"/><c:out value="^"/><c:out value="${title.refProgDataCode}"/>'><c:out value="${title.refProgDataDescription}"/></option>
						    									</c:forEach>
						    								</select><font color="red">* </font><br><div id="titleDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.initials" />
						    							</td>
						    							<td>
						    								<input type="text" id="initials" name="initials" onfocus="clearScreen('initialsDiv')" size="22" maxlength="100" onkeyup="upperCase('initials');" onblur="upperCase('initials');this.value=unformatNumberWithoutDotStr(this.value);(this.value=='NaN')?this.value='':this.value=this.value;">
						    								<font color="red">* </font><br><div id="initialsDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td colspan="4" height="3px"></td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.lastname" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="lastName" name="lastName" onfocus="clearScreen('lastNameDiv')" size="81" maxlength="100">
						    								<font color="red">* </font><br><div id="lastNameDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td colspan="4" height="3px"></td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.fulname" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="fullName" name="fullName" onfocus="clearScreen('fullNameDiv')" maxlength="100" size="81">
						    								<font color="red">* </font><br><div id="fullNameDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td colspan="4" height="3px"></td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.civilstatus" />
						    							</td>
						    							<td>
						    								<select id="civilStatus" name="civilStatus" onfocus="clearScreen('civilStatusDiv')">
						    									<option value="">-- Select --</option>
						    									<option value="S">Single</option>
						    									<option value="M">Married</option>
						    									<option value="D">Divorced</option>
						    								</select><font color="red">* </font><br><div id="civilStatusDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.gender" />
						    							</td>
						    							<td>
						    								<select id="gender" name="gender" onfocus="clearScreen('genderDiv')">
						    									<option value="">-- Select --</option>
						    									<option value="M">Male</option>
						    									<option value="F">Female</option>
						    								</select><font color="red">* </font><br><div id="genderDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td colspan="4" height="3px"></td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.nicno" />
						    							</td>
						    							<td>
						    								<input type="text" id="nicNo" name="nicNo" onfocus="clearScreen('nicNoDiv')" maxlength="10" size="15" onblur="validateNic();" onkeyup="upperCase('nicNo')">
						    								<font color="red">* </font><br><div id="nicNoDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.dateofbirth" />
						    							</td>
						    							<td>
															<input type="text" name="dob" id="dob" size="15" maxlength="10" value=""
																onfocus="clearScreen('dobDiv')"
																onkeypress="DateFormat(this,event);" 
																onkeyup="DateFormat(this,event);" 
																onblur="DateFormat(this,event);validateDOB();validateNICwithDOB();" /><input type="button" name="dobBtn" id="dobBtn" class="dot3buttonNormal" value="..." onclick="return showCalendar('dob', function(){validateDOB();validateNICwithDOB();clearScreen('dobDiv')});"/> 																							
																<font color="red">* </font><br /><div id="dobDiv" class="validate"></div>
														</td>
						    						</tr>
						    						<tr>
						    							<td colspan="4" height="3px"></td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.tpno" />
						    							</td>
						    							<td>
						    								<input type="text" id="tpNo" name="tpNo" onfocus="clearScreen('tpNoDiv')" size="15" maxlength="10" onblur="this.value=unformatNumberWithoutDot(this.value);(this.value=='NaN')?this.value='':this.value=this.value;">
						    								<font color="red">* </font><br><div id="tpNoDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.mobileno" />
						    							</td>
						    							<td>
						    								<input type="text" id="mobileNo" name="mobileNo" onfocus="clearScreen('mobileNoDiv')" size="15" maxlength="10" onblur="this.value=unformatNumberWithoutDot(this.value);(this.value=='NaN')?this.value='':this.value=this.value;">
						    								<br><div id="mobileNoDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td colspan="4" height="3px"></td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.faxno" />
						    							</td>
						    							<td>
						    								<input type="text" id="faxNo" name="faxNo" onfocus="clearScreen('faxNoDiv')" size="15" maxlength="10" onblur="this.value=unformatNumberWithoutDot(this.value);(this.value=='NaN')?this.value='':this.value=this.value;">
						    								<br><div id="faxNoDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.email" />
						    							</td>
						    							<td>
						    								<input type="text" id="email" name="email" onfocus="clearScreen('emailDiv')" size="22" maxlength="50"><br><div id="emailDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td colspan="4" height="3px"></td>
						    						</tr>
						    						<tr>
						    							<td align="right" valign="top">
						    								<bean:message bundle="lable" key="lable.address" />
						    							</td>
						    							<td colspan="3">
						    								<textarea rows="2" cols="95" id="address" name="address" onfocus="clearScreen('addressDiv')" /></textarea>
						    								<font color="red">* </font><br><div id="addressDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td colspan="4" height="3px"></td>
						    						</tr>
							  						<tr>
							  							<td align="right">
							  								Profile Image
							  							</td>
							  							<td>
							  								<video id="videoID" autoplay style="border: 1px solid black;" width="240" height="240"></video>
							  							</td>
							  							<td>
							  								<canvas id="canvasID" style="border: 1px solid black;" width="240" height="240"></canvas>
							  								<script type="text/javascript">
																var video = document.getElementById('videoID');
																var canvas = document.getElementById('canvasID');
																var context = canvas.getContext('2d');
																window.URL = window.URL || window.webkitURL;
																navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
																navigator.getUserMedia({
																	 video : true
																	 }, function(stream) {
																	 video.src = window.URL.createObjectURL(stream);
																	 }, function(e) { 
																		 ///console.log(An error happened:, e)
																	 });
														
														
																function capture() {
																	 context.drawImage(video, 0, 30, canvas.width, 180);
																};
																
														 	</script>
							  							</td>
							  							<td>
							  								<input type="button" value="Take photo" onclick="capture()" style="width: 200px; height: 30px;"/>
							  							</td>
							  						</tr>
							  						<tr>
							  							<td colspan="4" height="3px"></td>
							  						</tr>
						    					</table>
						    				</div>
						    				<div id="cor" style="display: none;">
						    					<table width="100%">
						    						<tr>
							  							<td colspan="4" height="1px"></td>
							  						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.stakeholdertype" />
						    							</td>
						    							<td colspan="3">
							  								<input type="hidden" id="stakeholderTypeIdC" name="stakeholderTypeIdC" value=""/>
							  								<input type="text" name=" " id="stakeholderTypeC" maxlength="3" size="5"
																onfocus="$('stakeholderTypeDivC').innerHTML=''" onKeyUp=""
																onblur="" readonly="readonly"/>
																<input type="button" name="btnStakeholderTypeC" id="btnStakeholderTypeC" value="..." class="dot3buttonNormal"
																onclick="showSkyBrowser(2,'stakeholderTypeIdC','stakeholderTypeC',0,'stakeholderTypeDescriptionC',1,0,0,'refPrgId','1');$('stakeholderTypeDivC').innerHTML='';"/>
							  								<input name="stakeholderTypeDescriptionC" type="text" id="stakeholderTypeDescriptionC" maxlength="60" size="67" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
							  								<br><div id="stakeholderTypeDivC" class="validate"></div>
							  							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.companyname" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="companyName" name="companyName" onfocus="clearScreen('companyNameDiv')" size="80" maxlength="100">
						    								<font color="red">* </font><br /><div id="companyNameDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.brno" />
						    							</td>
						    							<td>
						    								<input type="text" id="brNo" name="brNo" onfocus="clearScreen('brNoDiv')">
						    								<font color="red">* </font><br /><div id="brNoDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.dateofregistration" />
						    							</td>
						    							<td>
															<input type="text" name="dor" id="dor" size="15" maxlength="10" value=""
																onfocus="clearScreen('dorDiv')"
																onkeypress="" 
																onkeyup="" 
																onblur="" /><input type="button" name="dorBtn" id="dorBtn" class="dot3buttonNormal" value="..." onclick="return showCalendar('dor', function(){});"/> 																							
																<font color="red">* </font><br /><div id="dorDiv" class="validate"></div>
														</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.contactpersonname" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="contactName" name="contactName" onfocus="clearScreen('contactNameDiv')" size="80" maxlength="100">
						    								<font color="red">* </font><br /><div id="contactNameDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.contactpersondesignation" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="contactDesignation" name="contactDesignation" onfocus="clearScreen('contactDesignationDiv')" size="55" maxlength="100">
						    								<font color="red">* </font><br /><div id="contactDesignationDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.contactpersontpno" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="contactTpNo" name="contactTpNo" onfocus="clearScreen('contactTpNoDiv')">
						    								<font color="red">* </font><br /><div id="contactTpNoDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.tpno" />
						    							</td>
						    							<td>
						    								<input type="text" id="tpNoC" name="tpNoC" onfocus="clearScreen('tpNoCDiv')">
						    								<font color="red">* </font><br /><div id="tpNoCDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.mobileno" />
						    							</td>
						    							<td>
						    								<input type="text" id="mobileNoC" name="mobileNoC" onfocus="clearScreen('mobileNoCDiv')">
						    								<br /><div id="mobileNoCDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.faxno" />
						    							</td>
						    							<td>
						    								<input type="text" id="faxNoC" name="faxNoC" onfocus="clearScreen('faxNoCDiv')">
						    								<br /><div id="faxNoCDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.email" />
						    							</td>
						    							<td>
						    								<input type="text" id="emailC" name="emailC" onfocus="clearScreen('emailCDiv')" maxlength="50">
						    								<br /><div id="emailCDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right" valign="top">
						    								<bean:message bundle="lable" key="lable.address" />
						    							</td>
						    							<td colspan="3">
						    								<textarea rows="2" cols="95" id="addressC" name="addressC" onfocus="clearScreen('addressCDiv')" /></textarea>
						    								<font color="red">* </font><br /><div id="addressCDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
							  							<td colspan="4" height="1px"></td>
							  						</tr>
						    					</table>
						    				</div>
						    			</td>
						    		</tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<table width="100%" align="center" class="browserBorder">
			    					<tr>
						    			<td>
						    				
						    			</td>
						    			<td align="right">
						    				<input type="button" id="Save" name="Save" value="Submit" onclick="create()" class="buttonNormal">
						    				<input type="button" id="Clear" name="Clear" value="Clear" onclick="clearAll()" class="buttonNormal">
						    			</td>
						    		</tr>			
			    				</table>
			    			</td>
			    		</tr>
			    	</table>
			    </logic:equal>
			</td>    	
    	</tr>
 		<tr>
 			<td>
 				<logic:equal name="stakeholder" property="action" value="delete">
 					<input type="hidden" id="pg" name="pg" value="d">
			    	No Delete Option
			    </logic:equal>
 			</td>
 		</tr>
 		<tr>
 			<td>
 				<logic:equal name="stakeholder" property="action" value="modify">
 					<input type="hidden" id="pg" name="pg" value="m">
 					<img id="img_saved" class="" src="" style="display: none"/>
			    	<table width="100%" align="center">
			    		<tr style="display: none;">
			    			<td>
			    				<table width="100%" align="center" class="browserBorder">
			    					<tr>
						    			<td align="center">
						    				<input type="radio" name="indCor" id="individual" value="I" onclick="onClickRadioIndCor();clearAll()" onchange="onClickRadioIndCor();clearAll()" checked="checked">Individual
						    				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="indCor" id="coorperate" value="C" onclick="onClickRadioIndCor();clearAll()" onchange="onClickRadioIndCor();clearAll()">Cooperate
						    			</td>
						    		</tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<table width="100%" align="center" class="browserBorder">
									<tr>
			    						<td height="5px"></td>
			    					</tr>
									<tr>
			   							<td align="right">
			   								<bean:message bundle="lable" key="lable.stakeholder" />
			   							</td>
			   							<td>
			  								<input type="hidden" id="stakeholderId" name="stakeholderId" value=""/>
			  								<input type="text" name="stakeholderCode" id="stakeholderCode" maxlength="6" size="10"
												onfocus="$('stakeholderDiv').innerHTML=''" onKeyUp=""
												onblur="" readonly="readonly"/>
												<input type="button" name="btnStakeholder" id="btnStakeholder" value="..." class="dot3buttonNormal"
												onclick="showSkyBrowser(3,'stakeholderId','stakeholderCode',0,'stakeholderName',1,0,0,'corpIndividual',($('individual').checked?'I':'C'),'getStakeholderDetails()');clearDivision();"/>
			  								<input name="stakeholderName" type="text" id="stakeholderName" size="75" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
			  								<br><div id="stakeholderDiv" class="validate"></div>
			  							</td>
			   						</tr>	
			   						<tr>
			    						<td height="5px"></td>
			    					</tr>		    					
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<table width="100%" align="center" class="browserBorder">
			    					<tr>
						    			<td>
						    				<div id="ind" style="overflow: auto;">
						    					<table width="100%">
						    						<tr>
							    						<td height="5px"></td>
							    					</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.title" />
						    							</td>
						    							<td>
						    								<input type="hidden" id="titleId">
						    								<input type="hidden" id="title">
						    								<select id="titleComBo" name="titleComBo" onfocus="clearScreen('titleDiv')" onblur="var tileArray = $('titleComBo').value.split('^');
						    																					$('titleId').value=tileArray[0]; 
						    																					$('title').value=tileArray[1];" style="width: 100px;">
						    									<option value="">-- Select --</option>
						    									<c:forEach items="${titleList}" var="title" >
						    										<option value='<c:out value="${title.refProgDataId}"/><c:out value="^"/><c:out value="${title.refProgDataCode}"/>'><c:out value="${title.refProgDataDescription}"/></option>
						    									</c:forEach>
						    								</select><font color="red">* </font><br /><div id="titleDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.initials" />
						    							</td>
						    							<td>
						    								<input type="text" id="initials" name="initials" maxlength="100" onfocus="clearScreen('initialsDiv')" onkeyup="upperCase('initials');" onblur="upperCase('initials');this.value=unformatNumberWithoutDotStr(this.value);(this.value=='NaN')?this.value='':this.value=this.value;">
						    								<font color="red">* </font><br /><div id="initialsDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
							    						<td height="3px"></td>
							    					</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.lastname" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="lastName" name="lastName" size="87" onfocus="clearScreen('lastNameDiv')" maxlength="100">
						    								<font color="red">* </font><br /><div id="lastNameDiv" class="validate"></div>
						    							</td>	
						    						</tr>
						    						<tr>
							    						<td height="3px"></td>
							    					</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.fulname" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="fullName" name="fullName" size="87" onfocus="clearScreen('fullNameDiv')" maxlength="100">
						    								<font color="red">* </font><br /><div id="fullNameDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
							    						<td height="3px"></td>
							    					</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.civilstatus" />
						    							</td>
						    							<td>
						    								<select id="civilStatus" name="civilStatus" onfocus="clearScreen('civilStatusDiv')">
						    									<option value="">-- Select --</option>
						    									<option value="S">Single</option>
						    									<option value="M">Married</option>
						    									<option value="D">Divorced</option>
						    								</select>
						    								<font color="red">* </font><br /><div id="civilStatusDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.gender" />
						    							</td>
						    							<td>
						    								<select id="gender" name="gender" onfocus="clearScreen('genderDiv')">
						    									<option value="">-- Select --</option>
						    									<option value="M">Male</option>
						    									<option value="F">Female</option>
						    								</select>
						    								<font color="red">* </font><br /><div id="genderDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
							    						<td height="3px"></td>
							    					</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.nicno" />
						    							</td>
						    							<td>
						    								<input type="text" id="nicNo" name="nicNo" onfocus="clearScreen('nicNoDiv')"  onblur="validateNic();" onkeyup="upperCase('nicNo')" maxlength="10">
						    								<font color="red">* </font><br /><div id="nicNoDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.dateofbirth" />
						    							</td>
						    							<td>
															<input type="text" name="dob" id="dob" size="10" maxlength="10" value=""
																onfocus="clearScreen('dobDiv')"
																onkeypress="DateFormat(this,event);" 
																onkeyup="DateFormat(this,event);" 
																onblur="DateFormat(this,event);validateDOB();validateNICwithDOB();" /><input type="button" name="dobBtn" id="dobBtn" class="dot3buttonNormal" value="..." onclick="return showCalendar('dob', function(){validateDOB();validateNICwithDOB();clearScreen('dobDiv');});"/> 																							
																<font color="red">* </font><br /><div id="dobDiv" class="validate"></div>
														</td>
						    						</tr>
						    						<tr>
							    						<td height="3px"></td>
							    					</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.tpno" />
						    							</td>
						    							<td>
						    								<input type="text" id="tpNo" name="tpNo" maxlength="10" onfocus="clearScreen('tpNoDiv')" onblur="this.value=unformatNumberWithoutDot(this.value);(this.value=='NaN')?this.value='':this.value=this.value;">
						    								<font color="red">* </font><br /><div id="tpNoDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.mobileno" />
						    							</td>
						    							<td>
						    								<input type="text" id="mobileNo" name="mobileNo" maxlength="10" onblur="this.value=unformatNumberWithoutDot(this.value);(this.value=='NaN')?this.value='':this.value=this.value;">
						    								<br /><div id="mobileNoDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
							    						<td height="3px"></td>
							    					</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.faxno" />
						    							</td>
						    							<td>
						    								<input type="text" id="faxNo" name="faxNo" maxlength="10" onblur="this.value=unformatNumberWithoutDot(this.value);(this.value=='NaN')?this.value='':this.value=this.value;">
						    								<br /><div id="faxNoDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.email" />
						    							</td>
						    							<td>
						    								<input type="text" id="email" name="email">
						    								<br /><div id="emailDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
							    						<td height="3px"></td>
							    					</tr>
						    						<tr>
						    							<td align="right" valign="top">
						    								<bean:message bundle="lable" key="lable.address" />
						    							</td>
						    							<td colspan="3">
						    								<textarea rows="2" cols="102" id="address" name="address"  onfocus="clearScreen('addressDiv')" /></textarea>
						    								<font color="red">* </font><br /><div id="addressDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
							    						<td height="3px"></td>
							    					</tr>
						    						<tr>
							  							<td align="right">
							  								Profile Image
							  							</td>
							  							<td>
							  								<video id="videoID" autoplay style="border: 1px solid black;" width="240" height="240"></video>
							  							</td>
							  							<td>
							  								<canvas id="canvasID" style="border: 1px solid black;" width="240" height="240"></canvas>
							  								<script type="text/javascript">
																var video = document.getElementById('videoID');
																var canvas = document.getElementById('canvasID');
																var context = canvas.getContext('2d');
																window.URL = window.URL || window.webkitURL;
																navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
																navigator.getUserMedia({
																	 video : true
																	 }, function(stream) {
																	 video.src = window.URL.createObjectURL(stream);
																	 }, function(e) { 
																		 //console.log(An error happened:, e)
																	 });
																
																img_saved
														
																function capture() {
																	 context.drawImage(video, 0, 30, canvas.width, 180);
																};
																
														 	</script>
							  							</td>
							  							<td>
							  								<input type="button" value="Take photo" onclick="capture()" style="width: 200px; height: 30px;"/>
							  							</td>
							  						</tr>
							  						<tr>
							    						<td height="5px"></td>
							    					</tr>
						    					</table>
						    				</div>
						    				<div id="cor" style="display: none;">
						    					<table width="100%">
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.companyname" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="companyName" name="companyName" size="81">
						    								<font color="red">* </font><br /><div id="companyNameDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.brno" />
						    							</td>
						    							<td>
						    								<input type="text" id="brNo" name="brNo">
						    								<font color="red">* </font><br /><div id="brNoDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.dateofregistration" />
						    							</td>
						    							<td>
															<input type="text" name="dor" id="dor" size="10" maxlength="10" value=""
																onfocus="clearScreen('dorDiv')"
																onkeypress="" 
																onkeyup="" 
																onblur="" /><input type="button" name="dorBtn" id="dorBtn" class="dot3buttonNormal" value="..." onclick="return showCalendar('dor', function(){});"/> 																							
																<font color="red">* </font><br /><div id="dorDiv" class="validate"></div>
														</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.contactpersonname" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="contactName" name="contactName" size="81">
						    								<font color="red">* </font><br /><div id="contactNameDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.contactpersondesignation" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="contactDesignation" name="contactDesignation" size="60">
						    								<font color="red">* </font><br /><div id="contactDesignationDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.contactpersontpno" />
						    							</td>
						    							<td colspan="3">
						    								<input type="text" id="contactTpNo" name="contactTpNo">
						    								<font color="red">* </font><br /><div id="contactTpNoDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.tpno" />
						    							</td>
						    							<td>
						    								<input type="text" id="tpNoC" name="tpNoC">
						    								<font color="red">* </font><br /><div id="tpNoCDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.mobileno" />
						    							</td>
						    							<td>
						    								<input type="text" id="mobileNoC" name="mobileNoC">
						    								<br /><div id="mobileNoCDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.faxno" />
						    							</td>
						    							<td>
						    								<input type="text" id="faxNoC" name="faxNoC">
						    								<br /><div id="faxNoCDiv" class="validate"></div>
						    							</td>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.email" />
						    							</td>
						    							<td>
						    								<input type="text" id="emailC" name="emailC">
						    								<br /><div id="emailCDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    						<tr>
						    							<td align="right">
						    								<bean:message bundle="lable" key="lable.address" />
						    							</td>
						    							<td colspan="3">
						    								<textarea rows="3" cols="95" id="addressC" name="addressC" /></textarea>
						    								<font color="red">* </font><br /><div id="addressCDiv" class="validate"></div>
						    							</td>
						    						</tr>
						    					</table>
						    				</div>
						    			</td>
						    		</tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<table width="100%" align="right" class="browserBorder">
			    					<tr>
						    			<td>
						    				
						    			</td>
						    			<td align="right">
						    				<input type="button" id="Check" name="submit" value="Submit" onclick="update()" class="buttonNormal">
						    				<input type="button" id="Clear" name="clear" value="Clear" onclick="clearAll()" class="buttonNormal">
						    			</td>
						    		</tr>
			    				</table>
			    			</td>
			    		</tr>
			    	</table>
			    </logic:equal>
 			</td>
 		</tr>
 		<tr>
 			<td>
 				<logic:equal name="stakeholder" property="action" value="view">
 					<input type="hidden" id="pg" name="pg" value="v">
			    	<table width="100%" align="center">
  						<tr>
  							<td colspan="2" align="center">
  								<script type="text/javascript">
									var Columns = ["Stakeholder Code","Name","Initials","Gender","Civil Status","NIC No","Date of Birth","Phone No","Mobile No","Fax No","E-Mail Address","Address","Is Member?"];
									var str=new AW.Formats.String;
									var num=new AW.Formats.Number;
									var grid=createBrowser('stk',Columns,[str,str,str,str,str,str,str,str,str]);
									document.write(grid);
								</script>
  							</td>
  						</tr>
  					</table>
			    </logic:equal>
 			</td>
 		</tr>  
    </table>
    <script>
  		
  	</script>
  </body>
</html>