<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>Holiday</title>
    
    <script type="text/javascript" src="js/ajax.js"></script>
	<script type="text/javascript" src="js/aw.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	
	<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
	
	<!-- Annual Calendar CSS -->
	<link rel="stylesheet" href="css/annualCalCSS/zpcal.css"></link>
	<link rel="stylesheet" href="css/annualCalCSS/template.css"></link>
	<link rel="stylesheet" href="css/annualCalCSS/system.css"></link>
	<link rel="stylesheet" href="css/annualCalCSS/small.css"></link>

	<!-- Annual Calendar JS -->
	<script type="text/javascript" src="js/annualCalJS/zapatec.js"></script>
	<script type="text/javascript" src="js/annualCalJS/zpdict.js"></script>
	<script type="text/javascript" src="js/annualCalJS/zpdate-en.js"></script>
	<script type="text/javascript" src="js/annualCalJS/zpdate.js"></script>
	<script type="text/javascript" src="js/annualCalJS/calendar.js"></script>
	<script type="text/javascript" src="js/annualCalJS/effects.js"></script>
	
	<script type="text/javascript">
		var cal;
		
		var sysDate="<f:formatDate value="${sessionScope.LOGIN_KEY.loginDate}" pattern="dd/MM/yyyy"/>";
		
		function getSystemDate(){
			var dt = '<f:formatDate value="${sessionScope.LOGIN_KEY.loginDate}" pattern="dd/MM/yyyy" />';
			//alert(dt);
			return '<f:formatDate value="${sessionScope.LOGIN_KEY.loginDate}" pattern="dd/MM/yyyy" />';
		}
		
		function clearScreen(id){
			$(id).innerHTML = "";              	
		}
		
		function upperCase(id){
			$(id).value = $(id).value.toUpperCase();	
		}
		
		function trim(sString){
			while (sString.substring(0,1) == ' ') {
				sString = sString.substring(1, sString.length);
			}
			while (sString.substring(sString.length-1, sString.length) == ' '){
				sString = sString.substring(0,sString.length-1);
			}
			return sString;
		}
		
		function $(name){return document.getElementById(name);}
		
		function createBrowser(gridName,columns,cellFormat){
			var obj = new AW.UI.Grid;
			obj.setId(gridName);
			var str = new AW.Formats.String;
			var num = new AW.Formats.Number;
			obj.setCellText([]);
			obj.setCellFormat(cellFormat);
			obj.setHeaderText(columns);
			obj.setSelectorVisible(true);
			obj.setRowCount(0);
			obj.setColumnCount(columns.length);
			obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
			obj.setSelectorWidth(20);
			obj.setHeaderHeight(20);
			obj.setSelectionMode("single-row");
			obj.onSelectedRowsChanged= function(row){};
			obj.loadingCompleted= function() {
			    if (obj._maskLoading) { 
			       obj._maskLoading.style.display = 'none'; 
			    } 
			}
			obj.loading = function() { 
			    if (! obj._maskLoading) { 
			        var maskLoading = document.createElement('div'); 
			        document.body.appendChild(maskLoading); 
			        maskLoading.className = 'aw-grid-loading'; 
			        var gridEl = obj.element(); 
			        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
			        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
			        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
			        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
			        obj._maskLoading = maskLoading; 
			   	}
		        obj.clearCellModel(); 
			    obj.clearRowModel(); 
			    obj.clearScrollModel(); 
			    obj.clearSelectionModel(); 
			    obj.clearSortModel(); 
			    obj.refresh(); 
		    	obj._maskLoading.style.display = 'block'; 
			}
			return obj;
		}
		
		function setGridData(grid,data){
			grid.setCellText(data);
			grid.setRowCount(data.length);
			grid.refresh();
			grid.setSelectedRows([]);
		}
		
		function getCreateData(){
			var res = $('reason').value;	    		
	    	
	    	var strDate='';
	    	var dates = cal.submitFlatDates();
	    	if(dates.length==0){
	    		alert("Please select dates.");
	    		return false;
	    	}else{
	    		for(var i in dates){
	    			var ddd= dates[i].getDate()+"/"+(Number(dates[i].getMonth())+1)+"/"+dates[i].getFullYear();
	    			strDate+=ddd+'<sri>';
	    		}
	    	}
	    	return 	"&reason="+res+ "&sysDate="+sysDate+ "&strDate="+strDate;
    	}
    	
    	function getChangeData(){
			var holidayId = $('holidayId').value ;
			var holiday = $('holiday').value ;
			var reason = $('reason').value ;
			
			return "&holidayId="+holidayId + "&holiday="+holiday+"&reason="+reason;
		}
    	
    	function create(){
			if(!(confirm("Confirm to create a record?"))){
				return;														
			}
			var url="dispatch=create&rand="+Math.random()*19851227 + getCreateData();
			var ajax=new ajaxObject("holiday.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	//alert(eval('(' + responseText + ')'));
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(myData['createSuccess']){
	            		clearAll();
	            		alert(myData['createSuccess']);
	            	}else if(myData['error']){
	            		alert(myData['error']);
	            	}else{
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function update(){
			if(!checkedRecordSelected()){
				alert('Please select a record.');
				return false;
			}
			if(!(confirm("Confirm to update a record?"))){
				return;														
			}
			var url="dispatch=update&rand="+Math.random()*19851227 + getChangeData();
			var ajax=new ajaxObject("holiday.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(myData['updateSuccess']){
	            		clearAll();
	            		getGridData();
	            		alert(myData['updateSuccess']);
	            	}else if(myData['error']){
	            		alert(myData['error']);
	            	}else{
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function remove(){
			if(!checkedRecordSelected()){
				alert('Please select a record.');
				return false;
			}
			if(!(confirm("Confirm to delete a record?"))){
				return;														
			}
			var holidayId = $('holidayId').value ;
			var url="dispatch=delete&holidayId="+holidayId+"&rand="+Math.random()*19851227;
			var ajax=new ajaxObject("holiday.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(['deleteSuccess']){
	            		clearFilling();
	            		getGridData();
	            		alert(myData['deleteSuccess']);
	            	}else{
	            		
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function displayErrors(myData){
			for(i = 0; i<myData.length;i++){
				if(myData[i]["reason"]){
					$("divreason").innerHTML = myData[i]["reason"];
				}
			}
		}
		
		function checkedRecordSelected(){
			var holidayId = $('holidayId').value ;
    		if(holidayId ==null || holidayId ==''){
    			return false;
    		}else
    			return true;
		}
		
		function clearFilling(){
        	$('reason').value 	= "";
        	
        	try{
        		$('holiday').value 	= "";
        		grid.setSelectedRows([]);
        	}catch(e){}
        }	  
        
       	function clearDivision(){
        	$('divreason').innerHTML = ""; 
        }
        	       
        function clearAll(){
        	clearDivision();
            clearFilling();
            try{
            	cal_clear_days();	
            }catch(e){} 	
        }
        
        /*Start Clear selected Dates*/
        function init_globals() {
			var i
				Zapatec.Langs[cal.config.langId][cal.langStr]["DAY_FIRST"] = "Select <b>%s</b> days between "
				+ Zapatec.Date.print(dateStart, "<i>%a %b %d %Y</i>") + " and "
				+ Zapatec.Date.print(dateEnd, "<i>%a %b %d %Y</i>") + "."
			MA=[]
	
			cal.reinit()
		}
	
		// Clear the selected multiple days
		function cal_clear_days() {
			init_globals()
			cal.config.multiple=[]
			selectedWeekDays = [];
			// Will refressh the view of the calendar
			// Set reinit call date to today
			cal.config.date=new Date()
			cal.reinit()
		}
	
		/*
		-Set the allowed start and end dates
		-These variables are used for DATE only compares, so zero out time
		*/
		var selectedWeekDays = [];//not to duplicate selected week days
		var dateStart=new Date() // Today
		dateStart.setHours(0,0,0,0)
		var dateEnd=new Date() // Today
		dateEnd.setDate(dateStart.getDate()+30);	// Allow 30 days
		dateEnd.setHours(0,0,0,0)
	        
        /*End Clear selected Dates*/  
		
		function loadCalendar(){
        	try{
        		cal = new Zapatec.Calendar({
				flat         : "flatcalMultiDays", // ID of the parent element
				flatCallback : flatCallback,          // our callback function
				numberMonths   :     6,
				controlMonth   :     1,
				monthsInRow    :     3,
				lang             : "en",
				weekNumbers       : false,
				range             : [2000.01, 2999.12],
				multiple   	   : MA,
				dateStatusFunc :    dateInRange, //the function to call
				titleHtml     : function(strTitle, month, year) {
					return "This Month is " +  Zapatec.Calendar.i18n(month, "mn")
				},
				theme         : "system",
				themeSize         : "small"
				});
        	}catch(e){}
		}
		
		function getSystemDateAsDate(){
			var systemDate="<f:formatDate value="${sessionScope.LOGIN_KEY.loginDate}" pattern="dd/MM/yyyy"/>";
			var temp=systemDate.split('/');
			var m,d,y;
			var strTemp=SystemDateFormat.split("/");
			for (var s = 0; s < strTemp.length; s++) {
				switch (strTemp[s]){
				    case 'MM':
				        m=s;
				    	break
				    case 'dd':
				        d=s;
				    	break
				    case 'yyyy':
				        y=s;
				    	break
				}
			}
			var sysD=new Date(temp[y], temp[m]- 1 , temp[d]);
			return sysD;
		}
		
		/*Get Grid Data*/
        var dataArray=new Array();
		function getGridData(){
			grid.loading();
			var data="dispatch=getAllHoliday&rand="+Math.random()*19851227;
			var ajax=new ajaxObject('holiday.do');
				ajax.callback=function(responseText, responseStatus, responseXML) {
             		if (responseStatus == 200) {
                  		myArray = eval("("+responseText+")");							
             			setGridData(grid,myArray);
             			grid.loadingCompleted();
						dataArray = myArray;	
       	         	}
				}
			ajax.update(data,'POST');
		}
		
		function callFunction(){
			if($('pg').value!='a'){
				getGridData();	
			}
		}
			
	</script>
	
	<style>
		input.buttonNormal#Save {
		  background-image: url('images/icon/save.gif');
		  padding-right:20px
		}
		
		input.buttonNormal#Check {
		  background-image: url('images/icon/check.gif');
		  padding-right:20px
		}
		
		input.buttonNormal#Clear {
		  background-image: url('images/icon/clear.gif');
		  padding-right:20px
		}
		
		input.buttonNormal {
		  font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;
		  font-size:13px;
		  color: #000000;
		  background-color: #e4e4f5;
		  background-image: url('images/icon/normal.gif');
		  border: medium none;
		  cursor: hand;
		  width:90px;
		  height:22px;
		}
		
		.tableTdStyle{
			FONT-SIZE: 12px;
			FONT-FAMILY: Trebuchet MS;
		}
			
		.browserBorder {
		    border-top: #91a7b4 1px solid;
		    border-right: #91a7b4 1px solid;
		    border-left: #91a7b4 1px solid;
		    border-bottom: #91a7b4 1px solid;
		    background-color:#eeeeee;
		}
		
		div.validate{
			background-position:top;
			background-color:#eeeeee;
			padding: 0px 0px 0px 0px; 
			color: red;
			position: absolute ;
			font-family: Trebuchet MS;
			font-size:11px;
		}
		
		.background{
			background-color:#eeeeee;
			background-position:bottom;
			/*background-image:url(images/header/sri_04.gif);*/
			background-repeat:repeat-x;
		}
		
		BODY {
			MARGIN: 0px;
			FONT-SIZE: 12px;
			FONT-FAMILY: Trebuchet MS ;
			
		}
		
		TD {
			FONT-SIZE: 13px;
			FONT-FAMILY: Trebuchet MS;
		}
		
		TH {
			FONT-SIZE: 12px;
			FONT-FAMILY: Trebuchet MS;
		}
		
		INPUT{
			FONT-SIZE: 13px;
			FONT-FAMILY: Trebuchet MS;
		}
		
		TEXTAREA{
			FONT-SIZE: 12px;
			FONT-FAMILY: Trebuchet MS;
		}
		
		SELECT{
			FONT-SIZE: 13px; 
			FONT-FAMILY: Trebuchet MS;
		}
		
		legend{
			color:#0066CC;
		}
		
		#hol {height: 350px;width:100%;}
		#hol .aw-row-selector {text-align: center}
		#hol .aw-column-0 {width: 150px;}
		#hol .aw-column-1 {width: 300px;}
		#hol .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#hol .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
		
		#holidayView {height: 450px;width:100%;}
		#holidayView .aw-row-selector {text-align: center}
		#holidayView .aw-column-0 {width: 150px;}
		#holidayView .aw-column-1 {width: 300px;}
		#holidayView .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#holidayView .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
		
		/*Disable Dates*/			
   		/* for start/end dates */
       	.edges {
          	border : 1px solid;
          	border-color: #adaa9c #fff #fff #adaa9c;
          	background-color: #fffbee;
       	}

       	/* for dates between start and end dates */
       	.between {
          	background-color: #dccdb9;
       	}

       	.calendar tbody .disabled {
			text-decoration: line-through; color:#000
		}
		
	</style>
	
	
	
  </head>
  
  <body onload="callFunction();loadCalendar();" class="background">
    <logic:equal name="holiday" property="action" value="add">
    	<input id="pg" type="hidden" value="a" name="pg">
    	<table align="center" width="100%" class="">
    		<tr>
    			<td>
    				<table class="">
						<!--<tr>
							<td height="5px"></td>
						</tr>-->
						<tr>
							<td align="center">
								<table width="100%" align="center" class="" cellpadding="0" cellspacing="0">
									<tr>
										<td align="center">
											<!-- ********** Start New Annual Calendar ********** -->
												<div style="float: left; margin-left: 1em; margin-bottom: 1em;" id="flatcalMultiDays"></div>
												
												<script type="text/javascript">
													function dateChanged(calendar) {
														var preview = document.getElementById("preview");
														if (preview) {
															preview.innerHTML = Zapatec.Date.print(calendar.currentDate, '%a, %b %d, %Y');
														}
													}
												</script>
												
												<script type="text/javascript">
													// the default multiple dates selected, first time the calendar is instantiated
													var MA = [];
												
													function flatCallback(cal) {
														// here we'll write the output; this is only for example.  You
														// will normally fill an input field or something with the dates.
														var el = document.getElementById("sel7");
														// reset initial content.
														el.value = "";
												
														// Reset the "MA", in case one triggers the calendar again.
														// CAREFUL!  You don't want to do "MA = [];".  We need to modify
														// the value of the current array, instead of creating a new one.
														// Zapatec.Calendar.setup is called only once! :-)  So be careful.
														MA.length = 0;
												
														// walk the calendar's multiple dates selection hash
														for (var i in cal.config.multiple) {
															var currentDate = cal.config.multiple[i];
															// sometimes the date is not actually selected, that's why we need to check.
															if (currentDate) {
																// OK, selected.  Fill an input field.  Or something.  Just for example,
																// we will display all selected dates in the element having the id "output".
																el.value += Zapatec.Date.print(currentDate, "%B %d %Y\n");
												
																// and push it in the "MA", in case one triggers the calendar again.
																MA[MA.length] = currentDate;
															}
														}
													}
												</script>
												
												<script type="text/javascript">
									
													function compareDatesOnly(date1, date2) {
														var year1 = date1.getYear();
														var year2 = date2.getYear();
														var month1 = date1.getMonth();
														var month2 = date2.getMonth();
														var day1 = date1.getDate();
														var day2 = date2.getDate();
														if (year1 > year2) {
															return -1;
														}
														if (year2 > year1) {
															return 1;
														}
														//years are equal
														if (month1 > month2) {
															return -1;
														}
														if (month2 > month1) {
															return 1;
														}
														//years and months are equal
														if (day1 > day2) {
															return -1;
														}
														if (day2 > day1) {
															return 1;
														}
														//days are equal
														return 0;
													}
								
													function dateInRange(date) {
														//disable days prior to today
														//var today = new Date();
														var today = getSystemDateAsDate();
														var compareToday = compareDatesOnly(date, today);
														if (compareToday >= 0) {
															return(true);
														}
														//all other days are enabled
														return false;
														return(ret);
													}
													
												</script>
																
												<!-- <br> -->
												<textarea name="date7" id="sel7" rows='10' cols='40' style="display: none;"></textarea>			
											<!-- ********** End New Annual Calendar ********** -->
										</td>
									</tr>
									<tr>
										<td class="tableTdStyle">
											<table class="" width="100%" border="0">
												<tr>
													<td align="right" width="" class="tableTdStyle">
														<bean:message bundle="lable" key="lable.reason" />
													</td>
													<td width="" class="tableTdStyle">
														<input type="text" name="reason" id="reason" maxlength="60" size="90" onfocus="clearScreen('divreason')" />
														<font color="red">* </font><br /><div id="divreason" class="validate"></div>
													</td>
												</tr>
												<tr>
													<td colspan="2">
														<table align="center" width="100%" class="browserBorder">
									    					<tr>
													  			<td align="right">
													  				<input type="button" value="Submit" onclick="create();" id="Save" class="buttonNormal"/>
													  				<input type="button" value="Clear" onclick="clearAll();" id="Clear" class="buttonNormal"/>
													  			</td>
													  		</tr>
									    				</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
    			</td>
    		</tr>
    	</table>
    </logic:equal>
    <logic:equal name="holiday" property="action" value="modify">
    	<input id="pg" type="hidden" value="m" name="pg">
    	<input id="holidayId" type="hidden" name="holidayId">
    	<table align="center" width="100%" class="browserBorder">
    		<tr>
    			<td>
    				<table align="center" width="100%" class="browserBorder">
    					<tr>
    						<td colspan="2">
    							<script type="text/javascript">
									var Columns = ["Holiday","Reason"];
									var str=new AW.Formats.String;
									var num=new AW.Formats.Number;
									var grid=createBrowser('hol',Columns,[str,str]);
									document.write(grid);
									grid.onSelectedRowsChanged = function(row){
										$('holiday').value = this.getCellText(0,row);
										$('reason').value = this.getCellText(1,row);
										$('holidayId').value = this.getCellText(2,row);
		                           	};
								</script>
    						</td>
    					</tr>
    					<tr>
    						<td align="right" class="tableTdStyle">
    							Holiday
    						</td>
    						<td class="tableTdStyle">
    							<input type="text" id="holiday" name="holiday" size="15" maxlength="10" readonly="readonly">
    						</td>
    					</tr>
    					<tr>
    						<td align="right" class="tableTdStyle">
    							Reason
    						</td>
    						<td class="tableTdStyle">
    							<input type="text" id="reason" name="reason" size="75" maxlength="60">
    							<font color="red">*</font><br><div id="divreason"></div>
    						</td>
    					</tr>
    				</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    				<table align="center" width="100%" class="browserBorder">
    					<tr>
				  			<td class="tableTdStyle"></td>
				  			<td align="right" class="tableTdStyle">
				  				<input type="button" value="Submit" onclick="update()" id="Check" class="buttonNormal"/>
				  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
				  			</td>
				  		</tr>
    				</table>
    			</td>
    		</tr>
    	</table>
    </logic:equal>
    <logic:equal name="holiday" property="action" value="delete">
    	<input id="pg" type="hidden" value="d" name="pg">
    	<input id="holidayId" type="hidden" name="holidayId">
    	<table align="center" width="100%" class="browserBorder">
    		<tr>
    			<td>
    				<table align="center" width="100%" class="browserBorder">
    					<tr>
    						<td colspan="2">
    							<script type="text/javascript">
									var Columns = ["Holiday","Reason"];
									var str=new AW.Formats.String;
									var num=new AW.Formats.Number;
									var grid=createBrowser('hol',Columns,[str,str]);
									document.write(grid);
									grid.onSelectedRowsChanged = function(row){
										$('holiday').value = this.getCellText(0,row);
										$('reason').value = this.getCellText(1,row);
										$('holidayId').value = this.getCellText(2,row);
		                           	};
								</script>
    						</td>
    					</tr>
    					<tr>
    						<td align="right" class="tableTdStyle">
    							Holiday
    						</td>
    						<td class="tableTdStyle">
    							<input type="text" id="holiday" name="holiday" size="15" maxlength="10" readonly="readonly">
    						</td>
    					</tr>
    					<tr>
    						<td align="right" class="tableTdStyle">
    							Reason
    						</td>
    						<td class="tableTdStyle">
    							<input type="text" id="reason" name="reason" size="75" maxlength="60" readonly="readonly">
    						</td>
    					</tr>
    				</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    				<table align="center" width="100%" class="browserBorder">
    					<tr>
				  			<td class="tableTdStyle"></td>
				  			<td align="right" class="tableTdStyle">
				  				<input type="button" value="Submit" onclick="remove()" id="Check" class="buttonNormal"/>
				  				<input type="button" value="Clear" onclick="clearFilling()" id="Clear" class="buttonNormal"/>
				  			</td>
				  		</tr>
    				</table>
    			</td>
    		</tr>
    	</table>
    </logic:equal>
    <logic:equal name="holiday" property="action" value="view">
    	<input id="pg" type="hidden" value="v" name="pg">
    	<table align="center" width="100%" class="browserBorder">
    		<tr>
    			<td>
    				<script type="text/javascript">
						var Columns = ["Holiday","Reason"];
						var str=new AW.Formats.String;
						var num=new AW.Formats.Number;
						var grid=createBrowser('holidayView',Columns,[str,str]);
						document.write(grid);
						grid.onSelectedRowsChanged = function(row){
							
                        };
					</script>
    			</td>
    		</tr>
    	</table>
    </logic:equal>
  </body>
</html>
