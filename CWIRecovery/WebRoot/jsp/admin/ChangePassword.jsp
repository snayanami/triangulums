<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>Change Password</title>
    
	<link rel="stylesheet" href="css/common/common.css" type="text/css" />
	<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
	
	<script type="text/javascript" src="js/GridBrowser.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/aw.js"></script>
	<script type="text/javascript" src="js/ajax.js"></script>
	<script type="text/javascript" src="js/SkyBrowser.js"></script>

	<script type="text/javascript">	
		function cry(v){var str="";for(var i=0;i<v.length;i++){str+=v.charCodeAt(i++).toString(16);}for(var i=1;i<v.length;i++){str+=v.charCodeAt(i++).toString(16);}return str;}
		
		function hideTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="none";
		}
		
		function visibleTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="";
		}
	
		function clearScreen(id){$(id).innerHTML = "";}
		
		function $(name){return document.getElementById(name);}
		
		function validateCurrentPassword(){
			var currentPwrd = $('curPerd').value;
			var newCurrentPwrd = $('currentPassword').value;
			if(newCurrentPwrd!='' && currentPwrd!=newCurrentPwrd){
				$("currentPasswordDiv").innerHTML = 'Current Password mismatch.';
				$('currentPassword').value = '';
				return false;
			}
		}
		
		function changePassword(){
			if(!(confirm("Confirm to update the record?"))){
				return;														
			}
			
			var userId = $('userId').value;
			var currentPassword = escape(cry($('currentPassword').value));
			var newPassword = escape(cry($('newPassword').value));
			var confirmPassword = escape(cry($('confirmPassword').value));
			
			if(newPassword!=confirmPassword){
				$("confirmPasswordDiv").innerHTML = 'New Password & Confirm Password must be equal.';
				return false;
			}
			
			var url="dispatch=changePassword&officerId="+userId+"&newPassword="+newPassword+"&confirmPassword="+confirmPassword+"&currentPassword="+currentPassword+"&rand="+Math.random()*19851227;
			var ajax=new ajaxObject("changePassword.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(myData['updateSuccess']){
	            		clearAll();
	            		alert(myData['updateSuccess']);
	            	}else if(myData['error']){
		            	alert(myData['error']);
	            	}else{
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function displayErrors(myData){
			for(i = 0; i<myData.length;i++){
				if(myData[i]["newPassword"]){
					$("newPasswordDiv").innerHTML = myData[i]["newPassword"];
				}if(myData[i]["confirmPassword"]){
					$("confirmPasswordDiv").innerHTML = myData[i]["confirmPassword"];
				}if(myData[i]["currentPassword"]){
					$("currentPasswordDiv").innerHTML = myData[i]["currentPassword"];
				}
			}
		}
		
		function clearAll(){
			$('currentPassword').value = '';
			$('newPassword').value = '';
			$('confirmPassword').value = '';
			
			$('currentPasswordDiv').innerHTML = '';
			$('newPasswordDiv').innerHTML = '';
			$('confirmPasswordDiv').innerHTML = '';
		}
	</script>
	
	<style>
		.browserBorder {
		    border-top: #91a7b4 1px solid;
		    border-right: #91a7b4 1px solid;
		    border-left: #91a7b4 1px solid;
		    border-bottom: #91a7b4 1px solid;
		    background-color:#eeeeee;
		}
	</style>
	
  </head>
  
  <body onload="hideTabFrame()" onunload="visibleTabFrame()">
    <table width="100%" align="center">
    	<tr>
    		<td>
    			<table align="center" width="100%" class="browserBorder">
    				<tr>
    					<td colspan="2" height="10px"></td>
    				</tr>
    				<tr>
    					<td align="right">
    						User Name
    					</td>
    					<td>
    						<input type="hidden" id="curPerd" name="curPerd" value="<c:out value="${currentPassword}"/>">
    						<input type="hidden" id="userId" name="userId" value="<c:out value="${userId}"/>">
    						<input type="text" id="userName" name="userName" size="" readonly="readonly" value="<c:out value="${userName}"/>">
    					</td>
    				</tr>
    				<tr>
    					<td colspan="2" height="5px"></td>
    				</tr>
    				<tr>
    					<td align="right">
    						Current Password
    					</td>
    					<td>
    						<input type="password" id="currentPassword" name="currentPassword" size="" maxlength="" onblur="" onfocus="clearScreen('currentPasswordDiv')">
    						<font color="red">*</font><br><div id="currentPasswordDiv" class="validate"></div>
    					</td>
    				</tr>
    				<tr>
    					<td colspan="2" height="5px"></td>
    				</tr>
    				<tr>
    					<td align="right">
    						New Password
    					</td>
    					<td>
    						<input type="password" id="newPassword" name="newPassword" size="" maxlength="" onfocus="clearScreen('newPasswordDiv')">
    						<font color="red">*</font><br><div id="newPasswordDiv" class="validate"></div>
    					</td>
    				</tr>
    				<tr>
    					<td colspan="2" height="5px"></td>
    				</tr>
    				<tr>
    					<td align="right">
    						Confirm Password
    					</td>
    					<td>
    						<input type="password" id="confirmPassword" name="confirmPassword" size="" maxlength="" onfocus="clearScreen('confirmPasswordDiv')">
    						<font color="red">*</font><br><div id="confirmPasswordDiv" class="validate"></div>
    					</td>
    				</tr>
    				<tr>
    					<td colspan="2" height="10px"></td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	<tr>
			<td>
				<table align="center" width="100%" class="browserBorder">
					<tr>
			  			<td align="right">
			  				<input type="button" value="Submit" onclick="changePassword()" id="Check" class="buttonNormal"/>
			  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
			  			</td>
  					</tr>
				</table>
			</td>
		</tr>
    </table>
  </body>
</html>
