<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  	<head>
    	<title>BranchOfficer</title>
    	
  		<link rel="stylesheet" href="css/common/common.css" type="text/css" />
		<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
		
		<script type="text/javascript" src="js/GridBrowser.js"></script>
		<script type="text/javascript" src="js/common.js"></script>
		<script type="text/javascript" src="js/aw.js"></script>
		<script type="text/javascript" src="js/ajax.js"></script>
		
		<script type="text/javascript" src="js/SkyBrowser.js"></script>
		
		<script type="text/javascript">
			function clearScreen(id){
				$(id).innerHTML = "";              	
			}
			
			function upperCase(id){
				$(id).value = $(id).value.toUpperCase();	
			}
		
			function $(name){return document.getElementById(name);}
		
			function createBrowser(gridName,columns,cellFormat){
				var obj = new AW.UI.Grid;
				obj.setId(gridName);
				var str = new AW.Formats.String;
				var num = new AW.Formats.Number;
				obj.setCellText([]);
				obj.setCellFormat(cellFormat);
				obj.setHeaderText(columns);
				obj.setSelectorVisible(true);
				obj.setRowCount(0);
				obj.setColumnCount(columns.length);
				obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
				obj.setSelectorWidth(20);
				obj.setHeaderHeight(20);
				obj.setSelectionMode("single-row");
				obj.onSelectedRowsChanged= function(row){};
				obj.loadingCompleted= function() {
				    if (obj._maskLoading) { 
				       obj._maskLoading.style.display = 'none'; 
				    } 
				}
				obj.loading = function() { 
				    if (! obj._maskLoading) { 
				        var maskLoading = document.createElement('div'); 
				        document.body.appendChild(maskLoading); 
				        maskLoading.className = 'aw-grid-loading'; 
				        var gridEl = obj.element(); 
				        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
				        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
				        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
				        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
				        obj._maskLoading = maskLoading; 
				   	}
			        obj.clearCellModel(); 
				    obj.clearRowModel(); 
				    obj.clearScrollModel(); 
				    obj.clearSelectionModel(); 
				    obj.clearSortModel(); 
				    obj.refresh(); 
			    	obj._maskLoading.style.display = 'block'; 
				}
				return obj;
			}
			
			function setGridData(grid,data){
				grid.setCellText(data);
				grid.setRowCount(data.length);
				grid.refresh();
				grid.setSelectedRows([]);
			}
		
			function create(){
				if(!(confirm("Confirm to create a record?"))){
					return;														
				}
				var url="dispatch=create&rand="+Math.random()*19851227 + getCreateDate();
				//alert(url);
				var ajax=new ajaxObject("branchOfficer.do");
				ajax.callback=function(responseText, responseStatus, responseXML) {
		         	//alert(eval('(' + responseText + ')'));
		         	if (responseStatus == 200) {
		            	myData=eval('(' + responseText + ')');
		            	//alert(myData);
		            	if(myData['createSuccess']){
		            		clearAll();
		            		alert(myData['createSuccess']);
		            	}else if(myData['error']){
		            		alert(myData['error']);
		            	}else{
		            		//alert('awa');
		            		displayErrors(myData);
		            	}
		            }	   
				}
				ajax.update(url,'POST');
			}
			
			function update(){
				if(!checkedRecordSelected()){
					alert('Please select a record.');
					return false;
				}
				if(!(confirm("Confirm to update a record?"))){
					return;														
				}
				var url="dispatch=update&rand="+Math.random()*19851227 + getChangeData();
				//alert(url);
				var ajax=new ajaxObject("branchOfficer.do");
				ajax.callback=function(responseText, responseStatus, responseXML) {
		         	if (responseStatus == 200) {
		            	myData=eval('(' + responseText + ')');
		            	if(myData['updateSuccess']){
		            		clearAll();
		            		//getGridData();
		            		alert(myData['updateSuccess']);
		            	}else if(myData['error']){
		            		alert(myData['error']);
		            	}else{
		            		displayErrors(myData);
		            	}
		            }	   
				}
				ajax.update(url,'POST');
			}
			
			function remove(){
				if(!checkedRecordSelected()){
					alert('Please select a record.');
					return false;
				}
				if(!(confirm("Confirm to delete a record?"))){
					return;														
				}
				var branchOfficerId = $('branchOfficerId').value;
				var url="dispatch=delete&branchOfficerId="+branchOfficerId+"&rand="+Math.random()*19851227;
				//alert(url);
				var ajax=new ajaxObject("branchOfficer.do");
				ajax.callback=function(responseText, responseStatus, responseXML) {
		         	if (responseStatus == 200) {
		            	myData=eval('(' + responseText + ')');
		            	if(myData['deleteSuccess']){
		            		clearFilling();
		            		//getGridData();
		            		alert(myData['deleteSuccess']);
		            	}else if(myData['error']){
		            		alert(myData['error']);
		            	}else{
		            		//displayErrors(myData);
		            	}
		            }	   
				}
				ajax.update(url,'POST');
			}
			
			function getCreateDate(){
				var officerId = $('officerId').value;
				var branchId = $('branchId').value;
				var accessGroupId = $('accessGroupId').value;
				if($('isDefault').checked){
					var isDefault = 1;
				}else{
					var isDefault = 0;
				}
	
				return "&officerId="+officerId+"&branchId="+branchId+"&accessGroupId="+accessGroupId+"&isDefault="+isDefault;
			}
			
			function getChangeData(){
				var branchOfficerId = $('branchOfficerId').value;
				
				return "&branchOfficerId="+branchOfficerId + getCreateDate();
			}
			
			function displayErrors(myData){
				for(i = 0; i<myData.length;i++){
					if(myData[i]["officerId"]){
						$("officerDiv").innerHTML = myData[i]["officerId"];
					}if(myData[i]["branchId"]){
						$("branchDiv").innerHTML = myData[i]["branchId"];
					}if(myData[i]["accessGroupId"]){
						$("accessGroupDiv").innerHTML = myData[i]["accessGroupId"];
					}
				}
			}
			
			function clearAll(){
				clearFilling();
				clearDivision();
			}
			
			function clearFilling(){
				$('officerId').value = '';
				$('userName').value = '';
				$('description').value = '';
				$('branchId').value = '';
				$('branchCode').value = '';
				$('branchName').value = '';
				$('accessGroupId').value = '';
				$('accessGroup').value = '';
				$('accessGroupDescription').value = '';
				$('isDefault').checked = true;
				try{
					grid.setSelectedRows([]);
					setGridData(grid,[]);
				}catch(e){
					//alert(e);
				}
			}
			
			function clearDivision(){
				$("officerDiv").innerHTML = '';
				$("branchDiv").innerHTML = '';
				$("accessGroupDiv").innerHTML = '';
			}
			
			function checkedRecordSelected(){
				var branchOfficerId = $('branchOfficerId').value ;
	    		if(branchOfficerId ==null || branchOfficerId ==''){
	    			return false;
	    		}else
	    			return true;
			}
			
			/*Get Grid Data*/
	        var dataArray=new Array();
			function getGridDataForCreate(){
				var officerId=$('officerId').value;
				if(officerId!=''){
					grid.loading();
					var data="dispatch=getAllBranchOfficerByOfficerId&officerId="+officerId+"&rand="+Math.random()*19851227;
					var ajax=new ajaxObject('branchOfficer.do');
						ajax.callback=function(responseText, responseStatus, responseXML) {
		             		if (responseStatus == 200) {
		                  		myArray = eval("("+responseText+")");							
		             			setGridData(grid,myArray);
		             			grid.loadingCompleted();
								dataArray = myArray;	
		       	         	}
						}
					ajax.update(data,'POST');					
				}
			}
			
			function callFunction(){
				if($('page').value!='a'){
					//getGridData();	
				}
			}
		</script>
		
		<style type="text/css">
			.browserBorder {
			    border-top: #91a7b4 1px solid;
			    border-right: #91a7b4 1px solid;
			    border-left: #91a7b4 1px solid;
			    border-bottom: #91a7b4 1px solid;
			    background-color:#eeeeee;
			}
		
			#gridCreate {height: 230px;width:100%;}
			#gridCreate .aw-row-selector {text-align: center}
			#gridCreate .aw-column-0 {width: 300px;}
			#gridCreate .aw-column-1 {width: 300px;}
			#gridCreate .aw-column-2 {width: 100px;}
			#gridCreate .aw-grid-cell {border-right: 1px solid threedlightshadow;}
			#gridCreate .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
			
			#gridView {height: 370px;width:100%;}
			#gridView .aw-row-selector {text-align: center}
			#gridView .aw-column-0 {width: 300px;}
			#gridView .aw-column-1 {width: 300px;}
			#gridView .aw-column-2 {width: 100px;}
			#gridView .aw-grid-cell {border-right: 1px solid threedlightshadow;}
			#gridView .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
		</style>
		
	</head>

  	<body>
    	<table width="100%">
    	<tr>
			<td>
				<logic:equal name="branchOfficer" property="action" value="add">
			    	<input type="hidden" id="pg" value="a">
			    	<table align="center" width="100%" class="browserBorder">
			    		<tr>
			    			<td>
			    				<table align="center" width="100%" class="browserBorder">
			    					<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.officer" />
						    			</td>
						    			<td>
			  								<input type="hidden" id="officerId" name="officerId" value=""/>
			  								<input type="text" name="userName" id="userName" size="15"
												onfocus="$('officerDiv').innerHTML=''" onKeyUp=""
												onblur="" readonly="readonly"/>
												<input type="button" name="btnOfficer" id="btnOfficer" value="..." class="dot3buttonNormal"
												onclick="showSkyBrowser(5,'officerId','userName',0,'description',1,0,0,'','','getGridDataForCreate()');$('officerDiv').innerHTML='';"/>
			  								<input name="description" type="hidden" id="description" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
			  								<br><div id="officerDiv" class="validate"></div>
			  							</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="7px"></td>
			  						</tr>
						    		<tr>
						    			<td colspan="2">
						    				<script type="text/javascript">
												var Columns = ["<bean:message bundle='lable' key='lable.branch' />","Access Group","<bean:message bundle='lable' key='lable.isdefault' />"];
												var str=new AW.Formats.String;
												var num=new AW.Formats.Number;
												var grid=createBrowser('gridCreate',Columns,[str,str,str]);
												document.write(grid);
												grid.onSelectedRowsChanged = function(row){
													//$('code').value = this.getCellText(0,row);
													//$('description').value = this.getCellText(1,row);
													//$('refProgramId').value = this.getCellText(2,row);
					                           	};
											</script>
						    			</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.branch" />
						    			</td>
						    			<td>
			  								<input type="hidden" id="branchId" name="branchId" value=""/>
			  								<input type="text" name="branchCode" id="branchCode" maxlength="3" size="5"
												onfocus="$('branchDiv').innerHTML=''" onKeyUp="upperCase('branchCode')"
												onblur="upperCase('branchCode')" readonly="readonly"/>
												<input type="button" name="btnBranch" id="btnBranch" value="..." class="dot3buttonNormal"
												onclick="showSkyBrowser(4,'branchId','branchCode',0,'branchName',1,0,0,'','');$('branchDiv').innerHTML='';"/>
			  								<input name="branchName" type="text" id="branchName" maxlength="60" size="80" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
			  								<br><div id="branchDiv" class="validate"></div>
			  							</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.accessgroup" />
						    			</td>
						    			<td>
			  								<input type="hidden" id="accessGroupId" name="accessGroupId" value=""/>
			  								<input type="text" name="accessGroup" id="accessGroup" maxlength="3" size="5"
												onfocus="$('accessGroupDiv').innerHTML=''" onKeyUp="upperCase('accessGroup')"
												onblur="upperCase('accessGroup')" readonly="readonly"/>
												<input type="button" name="btnAccessGroup" id="btnAccessGroup" value="..." class="dot3buttonNormal"
												onclick="showSkyBrowser(2,'accessGroupId','accessGroup',0,'accessGroupDescription',1,0,0,'refPrgId','2');$('accessGroupDiv').innerHTML='';"/>
			  								<input name="accessGroupDescription" type="text" id="accessGroupDescription" maxlength="60" size="80" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
			  								<br><div id="accessGroupDiv" class="validate"></div>
			  							</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.isdefault" />
						    			</td>
						    			<td>
						    				<input type="checkbox" id="isDefault" name="isDefault" checked="checked">
						    			</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="8px"></td>
			  						</tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<table align="center" width="100%" class="browserBorder">
			    					<tr>
							  			<td></td>
							  			<td align="right">
							  				<input type="button" value="Submit" onclick="create()" id="Save" class="buttonNormal"/>
							  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
							  			</td>
							  		</tr>
			    				</table>
			    			</td>
			    		</tr>
			    	</table>
			    </logic:equal>
			</td>
		</tr>
		<tr>
			<td>
				<logic:equal name="branchOfficer" property="action" value="delete">
			    	<input type="hidden" id="pg" value="d">
			    	<input type="hidden" id="branchOfficerId" name="branchOfficerId">
 					<table width="100%" class="browserBorder" align="center">
			    		<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.officer" />
						    			</td>
						    			<td>
			  								<input type="hidden" id="officerId" name="officerId" value=""/>
			  								<input type="text" name="userName" id="userName" size="15"
												onfocus="" onKeyUp=""
												onblur="" readonly="readonly" />
												<input type="button" name="btnOfficer" id="btnOfficer" value="..." class="dot3buttonNormal"
												onclick="showSkyBrowser(5,'officerId','userName',0,'description',1,0,0,'','','getGridDataForCreate()');"/>
			  								<input name="description" type="hidden" id="description" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
			  							</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td colspan="2">
						    				<script type="text/javascript">
												var Columns = ["<bean:message bundle='lable' key='lable.branch' />","<bean:message bundle='lable' key='lable.accessgroup' />","<bean:message bundle='lable' key='lable.isdefault' />"];
												var str=new AW.Formats.String;
												var num=new AW.Formats.Number;
												var grid=createBrowser('gridCreate',Columns,[str,str,str]);
												document.write(grid);
												grid.onSelectedRowsChanged = function(row){
													$('branchId').value = this.getCellText(3,row);
													$('branchCode').value = this.getCellText(4,row);
													$('branchName').value = this.getCellText(0,row);
													
													$('accessGroupId').value = this.getCellText(5,row);
													$('accessGroup').value = this.getCellText(6,row);
													$('accessGroupDescription').value = this.getCellText(1,row);
													
													$('branchOfficerId').value = this.getCellText(7,row);
													
													var isDef = this.getCellText(2,row);
													if(isDef=='Yes'){
														$('isDefault').checked=true;
													}else{
														$('isDefault').checked=false;
													}
					                           	};
											</script>
						    			</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.branch" />
						    			</td>
						    			<td>
			  								<input type="hidden" id="branchId" name="branchId" value=""/>
			  								<input type="text" name="branchCode" id="branchCode" maxlength="3" size="5"
												onfocus="" onKeyUp=""
												onblur="" readonly="readonly" />
												<input type="button" name="btnBranch" id="btnBranch" value="..." class="dot3buttonNormal" disabled="disabled"
												onclick="showSkyBrowser(4,'branchId','branchCode',0,'branchName',1,0,0,'','');" style="display: none;"/>
			  								<input name="branchName" type="text" id="branchName" maxlength="60" size="85" readonly="readonly" tabindex="-1"/>
			  							</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.accessgroup" />
						    			</td>
						    			<td>
			  								<input type="hidden" id="accessGroupId" name="accessGroupId" value=""/>
			  								<input type="text" name="accessGroup" id="accessGroup" maxlength="3" size="5"
												onfocus="" onKeyUp=""
												onblur="" readonly="readonly" />
												<input type="button" name="btnAccessGroup" id="btnAccessGroup" value="..." class="dot3buttonNormal" disabled="disabled"
												onclick="showSkyBrowser(2,'accessGroupId','accessGroup',0,'accessGroupDescription',1,0,0,'refPrgId','2');" style="display: none;"/>
			  								<input name="accessGroupDescription" type="text" id="accessGroupDescription" maxlength="60" size="85" readonly="readonly" tabindex="-1"/>
			  							</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.isdefault" />
						    			</td>
						    			<td>
						    				<input type="checkbox" id="isDefault" name="isDefault" checked="checked" disabled="disabled">
						    			</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="10px"></td>
			  						</tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
							  			<td></td>
							  			<td align="right">
							  				<input type="button" value="Submit" onclick="remove()" id="Check" class="buttonNormal"/>
							  				<input type="button" value="Clear" onclick="clearFilling()" id="Clear" class="buttonNormal" />
							  			</td>
							  		</tr>
			    				</table>
			    			</td>
			    		</tr>
			    	</table>
			    </logic:equal>
			</td>
 		</tr>
 		<tr>
			<td>
 				<logic:equal name="branchOfficer" property="action" value="modify">
 					<input type="hidden" id="pg" value="m">
 					<input type="hidden" id="branchOfficerId" name="branchOfficerId">
 					<table width="100%" class="browserBorder">
			    		<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.officer" />
						    			</td>
						    			<td>
			  								<input type="hidden" id="officerId" name="officerId" value=""/>
			  								<input type="text" name="userName" id="userName" maxlength="3" size="15"
												onfocus="$('officerDiv').innerHTML=''" onKeyUp=""
												onblur="" readonly="readonly"/>
												<input type="button" name="btnOfficer" id="btnOfficer" value="..." class="dot3buttonNormal"
												onclick="showSkyBrowser(5,'officerId','userName',0,'description',1,0,0,'','','getGridDataForCreate()');$('officerDiv').innerHTML='';"/>
			  								<input name="description" type="hidden" id="description" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
			  								<br><div id="officerDiv" class="validate"></div>
			  							</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td colspan="2">
						    				<script type="text/javascript">
												var Columns = ["<bean:message bundle='lable' key='lable.branch' />","<bean:message bundle='lable' key='lable.accessgroup' />","<bean:message bundle='lable' key='lable.isdefault' />"];
												var str=new AW.Formats.String;
												var num=new AW.Formats.Number;
												var grid=createBrowser('gridCreate',Columns,[str,str,str]);
												document.write(grid);
												grid.onSelectedRowsChanged = function(row){
													$('branchId').value = this.getCellText(3,row);
													$('branchCode').value = this.getCellText(4,row);
													$('branchName').value = this.getCellText(0,row);
													
													$('accessGroupId').value = this.getCellText(5,row);
													$('accessGroup').value = this.getCellText(6,row);
													$('accessGroupDescription').value = this.getCellText(1,row);
													
													$('branchOfficerId').value = this.getCellText(7,row);
													
													var isDef = this.getCellText(2,row);
													if(isDef=='Yes'){
														$('isDefault').checked=true;
													}else{
														$('isDefault').checked=false;
													}
					                           	};
											</script>
						    			</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.branch" />
						    			</td>
						    			<td>
			  								<input type="hidden" id="branchId" name="branchId" value=""/>
			  								<input type="text" name="branchCode" id="branchCode" maxlength="3" size="5"
												onfocus="$('branchDiv').innerHTML=''" onKeyUp=""
												onblur="" readonly="readonly"/>
												<input type="button" name="btnBranch" id="btnBranch" value="..." class=""
												onclick="showSkyBrowser(4,'branchId','branchCode',0,'branchName',1,0,0,'','');$('branchDiv').innerHTML='';" style="display: none;"/>
			  								<input name="branchName" type="text" id="branchName" maxlength="60" size="81" readonly="readonly" tabindex="-1"/>
			  								<br><div id="branchDiv" class="validate"></div>
			  							</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.accessgroup" />
						    			</td>
						    			<td>
			  								<input type="hidden" id="accessGroupId" name="accessGroupId" value=""/>
			  								<input type="text" name="accessGroup" id="accessGroup" maxlength="3" size="5"
												onfocus="$('accessGroupDiv').innerHTML=''" onKeyUp=""
												onblur="" readonly="readonly"/>
												<input type="button" name="btnAccessGroup" id="btnAccessGroup" value="..." class="dot3buttonNormal"
												onclick="showSkyBrowser(2,'accessGroupId','accessGroup',0,'accessGroupDescription',1,0,0,'refPrgId','2');$('accessGroupDiv').innerHTML='';"/>
			  								<input name="accessGroupDescription" type="text" id="accessGroupDescription" maxlength="60" size="76" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
			  								<br><div id="accessGroupDiv" class="validate"></div>
			  							</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
						    		<tr>
						    			<td align="right">
						    				<bean:message bundle="lable" key="lable.isdefault" />
						    			</td>
						    			<td>
						    				<input type="checkbox" id="isDefault" name="isDefault" checked="checked">
						    			</td>
						    		</tr>
						    		<tr>
			  							<td colspan="2" height="8px"></td>
			  						</tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
							  			<td></td>
							  			<td align="right">
							  				<input type="button" value="Submit" onclick="update()" id="Check" class="buttonNormal" />
							  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal" />
							  			</td>
							  		</tr>
			    				</table>
			    			</td>
			    		</tr>
			    	</table>
 				</logic:equal>
 			</td>
 		</tr>
 		<tr>
			<td>
 				<logic:equal name="branchOfficer" property="action" value="view">
 					<input type="hidden" id="pg" value="v">
 					<table width="100%" class="browserBorder">
			    		<tr>
  							<td colspan="2" height="5px"></td>
  						</tr>
			    		<tr>
			    			<td align="right">
			    				<bean:message bundle="lable" key="lable.officer" />
			    			</td>
			    			<td>
  								<input type="hidden" id="officerId" name="officerId" value=""/>
  								<input type="text" name="userName" id="userName" size="15"
									onfocus="" onKeyUp=""
									onblur="" readonly="readonly" />
									<input type="button" name="btnOfficer" id="btnOfficer" value="..." class="dot3buttonNormal"
												onclick="showSkyBrowser(5,'officerId','userName',0,'description',1,0,0,'','','getGridDataForCreate()');"/>
  								<input name="description" type="hidden" id="description" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
  							</td>
			    		</tr>
			    		<tr>
  							<td colspan="2" height="5px"></td>
  						</tr>
			    		<tr>
			    			<td colspan="2">
			    				<script type="text/javascript">
									var Columns = ["<bean:message bundle='lable' key='lable.branch' />","<bean:message bundle='lable' key='lable.accessgroup' />","<bean:message bundle='lable' key='lable.isdefault' />"];
									var str=new AW.Formats.String;
									var num=new AW.Formats.Number;
									var grid=createBrowser('gridView',Columns,[str,str,str]);
									document.write(grid);
								</script>
			    			</td>
			    		</tr>
			    		<tr>
  							<td colspan="2" height="5px"></td>
  						</tr>
			    	</table>
 				</logic:equal>
 			</td>
 		</tr>
  	</body>
</html>