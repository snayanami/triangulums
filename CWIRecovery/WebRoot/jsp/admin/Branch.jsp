<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  	<head>
   		<title>Branch</title>

  		<link rel="stylesheet" href="css/common/common.css" type="text/css" />
		<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
		
		<script type="text/javascript" src="js/GridBrowser.js"></script>
		<script type="text/javascript" src="js/common.js"></script>
		<script type="text/javascript" src="js/aw.js"></script>
		<script type="text/javascript" src="js/ajax.js"></script>
		
		<script type="text/javascript">
			function unformatNumberWithoutDot(num) { return num.replace(/([^0-9])/g,''); }
			
			function clearScreen(id){
				$(id).innerHTML = "";              	
			}
			
			function upperCase(id){
				$(id).value = $(id).value.toUpperCase();	
			}
		
			function $(name){return document.getElementById(name);}
		
			function createBrowser(gridName,columns,cellFormat){
				var obj = new AW.UI.Grid;
				obj.setId(gridName);
				var str = new AW.Formats.String;
				var num = new AW.Formats.Number;
				obj.setCellText([]);
				obj.setCellFormat(cellFormat);
				obj.setHeaderText(columns);
				obj.setSelectorVisible(true);
				obj.setRowCount(0);
				obj.setColumnCount(columns.length);
				obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
				obj.setSelectorWidth(20);
				obj.setHeaderHeight(20);
				obj.setSelectionMode("single-row");
				obj.onSelectedRowsChanged= function(row){};
				obj.loadingCompleted= function() {
				    if (obj._maskLoading) { 
				       obj._maskLoading.style.display = 'none'; 
				    } 
				}
				obj.loading = function() { 
				    if (! obj._maskLoading) { 
				        var maskLoading = document.createElement('div'); 
				        document.body.appendChild(maskLoading); 
				        maskLoading.className = 'aw-grid-loading'; 
				        var gridEl = obj.element(); 
				        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
				        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
				        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
				        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
				        obj._maskLoading = maskLoading; 
				   	}
			        obj.clearCellModel(); 
				    obj.clearRowModel(); 
				    obj.clearScrollModel(); 
				    obj.clearSelectionModel(); 
				    obj.clearSortModel(); 
				    obj.refresh(); 
			    	obj._maskLoading.style.display = 'block'; 
				}
				return obj;
			}
			
			function setGridData(grid,data){
				grid.setCellText(data);
				grid.setRowCount(data.length);
				grid.refresh();
				grid.setSelectedRows([]);
			}
			
			function create(){
				if(!(confirm("Confirm to create a record?"))){
					return;														
				}
				var url="dispatch=create&rand="+Math.random()*19851227 + getCreateDate();
				//alert(url);
				var ajax=new ajaxObject("branch.do");
				ajax.callback=function(responseText, responseStatus, responseXML) {
		         	//alert(eval('(' + responseText + ')'));
		         	if (responseStatus == 200) {
		            	myData=eval('(' + responseText + ')');
		            	//alert(myData);
		            	if(myData['createSuccess']){
		            		clearAll();
		            		alert(myData['createSuccess']);
		            	}else if(myData['error']){
		            		alert(myData['error']);
		            	}else{
		            		//alert('awa');
		            		displayErrors(myData);
		            	}
		            }	   
				}
				ajax.update(url,'POST');
			}
		
			function update(){
				if(!checkedRecordSelected()){
					alert('Please select a record.');
					return false;
				}
				if(!(confirm("Confirm to update a record?"))){
					return;														
				}
				var url="dispatch=update&rand="+Math.random()*19851227 + getChangeData();
				//alert(url);
				var ajax=new ajaxObject("branch.do");
				ajax.callback=function(responseText, responseStatus, responseXML) {
		         	if (responseStatus == 200) {
		            	myData=eval('(' + responseText + ')');
		            	if(myData['updateSuccess']){
		            		clearAll();
		            		getGridData();
		            		alert(myData['updateSuccess']);
		            	}else if(myData['error']){
		            		alert(myData['error']);
		            	}else{
		            		displayErrors(myData);
		            	}
		            }	   
				}
				ajax.update(url,'POST');
			}
			
			function remove(){
				if(!checkedRecordSelected()){
					alert('Please select a record.');
					return false;
				}
				if(!(confirm("Confirm to delete a record?"))){
					return;														
				}
				var branchId = $('branchId').value;
				var url="dispatch=delete&branchId="+branchId+"&rand="+Math.random()*19851227;
				//alert(url);
				var ajax=new ajaxObject("branch.do");
				ajax.callback=function(responseText, responseStatus, responseXML) {
		         	if (responseStatus == 200) {
		            	myData=eval('(' + responseText + ')');
		            	if(myData['deleteSuccess']){
		            		clearFilling();
		            		getGridData();
		            		alert(myData['deleteSuccess']);
		            	}else if(myData['error']){
		            		alert(myData['error']);
		            	}else{
		            		
		            	}
		            }	   
				}
				ajax.update(url,'POST');
			}
			
			function getCreateDate(){
				var branchCode = $('branchCode').value;
				var branchName = $('branchName').value;
				var address = $('address').value;
				var contactNo = $('contactNo').value;
				
				return "&branchCode="+branchCode+"&branchName="+branchName+"&address="+address+"&contactNo="+contactNo;
			}
			
			function getChangeData(){
				var branchId = $('branchId').value;
				
				return "&branchId="+branchId + getCreateDate();
			}
			
			/*Get Grid Data*/
	        var dataArray=new Array();
			function getGridData(){
				var data="dispatch=getAllBranch&rand="+Math.random()*19851227;
				var ajax=new ajaxObject('branch.do');
					ajax.callback=function(responseText, responseStatus, responseXML) {
	             		if (responseStatus == 200) {
	                  		myArray = eval("("+responseText+")");							
	             			setGridData(grid,myArray);
							dataArray = myArray;	
	       	         	}
					}
				ajax.update(data,'POST');
			}
			
			function displayErrors(myData){
				for(i = 0; i<myData.length;i++){
					if(myData[i]["branchCode"]){
						$("branchCodeDiv").innerHTML = myData[i]["branchCode"];
					}if(myData[i]["branchName"]){
						$("branchNameDiv").innerHTML = myData[i]["branchName"];
					}if(myData[i]["address"]){
						$("addressDiv").innerHTML = myData[i]["address"];
					}if(myData[i]["contactNo"]){
						$("contactNoDiv").innerHTML = myData[i]["contactNo"];
					}
				}
			}
			
			function clearAll(){
				clearFilling();
				clearDivision();
			}
			
			function clearFilling(){
				$('branchCode').value = '';
				$('branchName').value = '';
				$('address').value = '';
				$('contactNo').value = '';
				try{
					grid.setSelectedRows([]);	
				}catch(e){
					//alert(e);
				}
			}
			
			function clearDivision(){
				$("branchCodeDiv").innerHTML = '';
				$("branchNameDiv").innerHTML = '';
				$("addressDiv").innerHTML = '';
				$("contactNoDiv").innerHTML = '';
			}
			
			function checkedRecordSelected(){
				var branchId = $('branchId').value ;
	    		if(branchId ==null || branchId ==''){
	    			return false;
	    		}else
	    			return true;
			}
			
			function callFunction(){
				if($('pg').value!='a'){
					getGridData();	
				}
			}
		</script>
		
		<style type="text/css">
			.browserBorder {
			    border-top: #91a7b4 1px solid;
			    border-right: #91a7b4 1px solid;
			    border-left: #91a7b4 1px solid;
			    border-bottom: #91a7b4 1px solid;
			    background-color:#eeeeee;
			}
		
			#branch {height: 120px;width:100%;}
			#branch .aw-row-selector {text-align: center}
			#branch .aw-column-0 {width: 80px;}
			#branch .aw-column-1 {width: 200px;}
			#branch .aw-column-2 {width: 320px;}
			#branch .aw-column-3 {width: 100px;}
			#branch .aw-grid-cell {border-right: 1px solid threedlightshadow;}
			#branch .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
			
			#branchView {height: 350px;width:100%;}
			#branchView .aw-row-selector {text-align: center}
			#branchView .aw-column-0 {width: 80px;}
			#branchView .aw-column-1 {width: 200px;}
			#branchView .aw-column-2 {width: 320px;}
			#branchView .aw-column-3 {width: 100px;}
			#branchView .aw-grid-cell {border-right: 1px solid threedlightshadow;}
			#branchView .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
		</style>
	</head>
  
  	<body onload="callFunction()">
    	<table width="100%">
	    	<tr>
				<td>
					<logic:equal name="branch" property="action" value="add">
						<input id="pg" type="hidden" value="a" name="pg">
				    	<table width="100%" class="" align="center">
				    		<tr>
				    			<td>
				    				<table width="100%" class="browserBorder" align="center">
				    					<tr>
				 							<td colspan="2" height="5px"></td>
				 						</tr>
							    		<tr>
											<td align="right">
												<bean:message bundle="lable" key="lable.branchcode" />
											</td>
											<td>
												<input type="text" id="branchCode" name="branchCode" size="5" maxlength="3" onfocus="clearScreen('branchCodeDiv')" onkeydown="upperCase('branchCode')" onblur="upperCase('branchCode')"/>
												<font color="red">*</font><br><div id="branchCodeDiv" class="validate"></div>
											</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
							    		<tr>
							    			<td align="right"><bean:message bundle="lable" key="lable.branchname" /></td>
							    			<td>
							    				<input type="text" id="branchName" name="branchName" size="86" maxlength="50" onfocus="clearScreen('branchNameDiv')"/>
							    				<font color="red">*</font><br><div id="branchNameDiv" class="validate"></div>
							    			</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
							    		<tr>
							    			<td align="right"><bean:message bundle="lable" key="lable.address" /></td>
							    			<td>
							    				<textarea id="address" name="address" cols="100" rows="3" onfocus="clearScreen('addressDiv')"></textarea>
							    				<font color="red">*</font><br><div id="addressDiv" class="validate"></div>
							    			</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
							    		<tr>
							    			<td align="right"><bean:message bundle="lable" key="lable.contactno" /></td>
							    			<td>
							    				<input type="text" id="contactNo" name="contactNo" onfocus="clearScreen('contactNoDiv')" size="25" maxlength="10" onblur="this.value=unformatNumberWithoutDot(this.value);(this.value=='NaN')?this.value='':this.value=this.value;"/>
							    				<font color="red">*</font><br><div id="contactNoDiv" class="validate"></div>
							    			</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="10px"></td>
				  						</tr>
				    				</table>
				    			</td>
				    		</tr>
				    		<tr>
				    			<td>
				    				<table width="100%" class="browserBorder" align="center">
				    					<tr>
							    			<td></td>
							    			<td align="right">
							    				<input type="button" id="Save" class="buttonNormal" value="Submit" onclick="create()"/>
							    				<input type="button" id="Clear" class="buttonNormal" value="Clear" onclick="clearAll()"/>
							    			</td>
							    		</tr>
				    				</table>
				    			</td>
				    		</tr>
				    	</table>
				    </logic:equal>
				</td>
			</tr>
			<tr>
				<td>
					<logic:equal name="branch" property="action" value="delete">
				    	<input id="pg" type="hidden" value="d" name="pg">
  						<input id="branchId" type="hidden" name="branchId">
  						<table width="100%" align="center" class="browserBorder">
	  						<tr>
	  							<td>
	  								<table width="100%" align="center" class="browserBorder">
	  									<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
				  						<tr>
				  							<td colspan="2" align="center">
				  								<script type="text/javascript">
													var Columns = ["<bean:message bundle='lable' key='lable.branchcode' />","<bean:message bundle='lable' key='lable.branchname' />","<bean:message bundle='lable' key='lable.address' />","<bean:message bundle='lable' key='lable.contactno' />"];
													var str=new AW.Formats.String;
													var num=new AW.Formats.Number;
													var grid=createBrowser('branch',Columns,[str,str,str,str]);
													document.write(grid);
													grid.onSelectedRowsChanged = function(row){
														$('branchCode').value = this.getCellText(0,row);
														$('branchName').value = this.getCellText(1,row);
														$('address').value = this.getCellText(2,row);
														$('contactNo').value = this.getCellText(3,row);
														$('branchId').value = this.getCellText(4,row);
						                           	};
												</script>
				  							</td>
				  						</tr>
				  						<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
				  						<tr>
											<td align="right"><bean:message bundle="lable" key="lable.branchcode" /></td>
											<td>
												<input type="text" id="branchCode" name="branchCode" readonly="readonly" size="5"/>
											</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
							    		<tr>
							    			<td align="right"><bean:message bundle="lable" key="lable.branchname" /></td>
							    			<td>
							    				<input type="text" id="branchName" name="branchName" readonly="readonly" size="86"/>
							    			</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
							    		<tr>
							    			<td align="right"><bean:message bundle="lable" key="lable.address" /></td>
							    			<td>
							    				<textarea id="address" name="address" cols="100" rows="3" readonly="readonly"></textarea>
							    			</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
							    		<tr>
							    			<td align="right"><bean:message bundle="lable" key="lable.contactno" /></td>
							    			<td>
							    				<input type="text" id="contactNo" name="contactNo" size="25" readonly="readonly"/>
							    			</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
	  								</table>
	  							</td>
	  						</tr>
	  						<tr>
	  							<td>
	  								<table width="100%" align="center" class="browserBorder">
	  									<tr>
							    			<td></td>
							    			<td align="right">
							    				<input type="button" id="Check" class="buttonNormal" value="Submit" onclick="remove()"/>
							    				<input type="button" id="Clear" class="buttonNormal" value="Clear" onclick="clearFilling()"/>
							    			</td>
							    		</tr>
	  								</table>
	  							</td>
	  						</tr>
				    		
	  					</table>
				    </logic:equal>
				</td>
			</tr>
			<tr>
				<td>
					<logic:equal name="branch" property="action" value="modify">
				    	<input id="pg" type="hidden" value="m" name="pg">
  						<input id="branchId" type="hidden" name="branchId">
  						<table width="100%" align="center" class="browserBorder">
	  						<tr>
	  							<td>
	  								<table width="100%" align="center" class="browserBorder">
	  									<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
				  						<tr>
				  							<td colspan="2" align="center">
				  								<script type="text/javascript">
													var Columns = ["<bean:message bundle='lable' key='lable.branchcode' />","<bean:message bundle='lable' key='lable.branchname' />","<bean:message bundle='lable' key='lable.address' />","<bean:message bundle='lable' key='lable.contactno' />"];
													var str=new AW.Formats.String;
													var num=new AW.Formats.Number;
													var grid=createBrowser('branch',Columns,[str,str,str,str]);
													document.write(grid);
													grid.onSelectedRowsChanged = function(row){
														$('branchCode').value = this.getCellText(0,row);
														$('branchName').value = this.getCellText(1,row);
														$('address').value = this.getCellText(2,row);
														$('contactNo').value = this.getCellText(3,row);
														$('branchId').value = this.getCellText(4,row);
						                           	};
												</script>
				  							</td>
				  						</tr>
				  						<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
				  						<tr>
											<td align="right"><bean:message bundle="lable" key="lable.branchcode" /></td>
											<td>
												<input type="text" id="branchCode" name="branchCode" size="5" readonly="readonly"/>
												<br><div id="branchCodeDiv" class="validate"></div>
											</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
							    		<tr>
							    			<td align="right"><bean:message bundle="lable" key="lable.branchname" /></td>
							    			<td>
							    				<input type="text" id="branchName" name="branchName" size="86" maxlength="50" onfocus="clearScreen('branchNameDiv')"/>
							    				<font color="red">*</font><br><div id="branchNameDiv" class="validate"></div>
							    			</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
							    		<tr>
							    			<td align="right"><bean:message bundle="lable" key="lable.address" /></td>
							    			<td>
							    				<textarea id="address" name="address" cols="100" rows="3" onfocus="clearScreen('addressDiv')"></textarea>
							    				<font color="red">*</font><br><div id="addressDiv" class="validate"></div>
							    			</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
							    		<tr>
							    			<td align="right"><bean:message bundle="lable" key="lable.contactno" /></td>
							    			<td>
							    				<input type="text" id="contactNo" name="contactNo" onfocus="clearScreen('contactNoDiv')" size="25" maxlength="10" onblur="this.value=unformatNumberWithoutDot(this.value);(this.value=='NaN')?this.value='':this.value=this.value;"/>
							    				<font color="red">*</font><br><div id="contactNoDiv" class="validate"></div>
							    			</td>
							    		</tr>
							    		<tr>
				  							<td colspan="2" height="5px"></td>
				  						</tr>
	  								</table>
	  							</td>
	  						</tr>
	  						<tr>
	  							<td>
	  								<table width="100%" align="center" class="browserBorder">
	  									<tr>
							    			<td></td>
							    			<td align="right"> 
							    				<input type="button" id="Check" class="buttonNormal" value="Submit" onclick="update()"/>
							    				<input type="button" id="Clear" class="buttonNormal" value="Clear" onclick="clearAll()"/>
							    			</td>
							    		</tr>
	  								</table>
	  							</td>
	  						</tr>
	  					</table>
				    </logic:equal>
				</td>
			</tr>
			<tr>
				<td>
					<logic:equal name="branch" property="action" value="view">
				    	<input id="pg" type="hidden" value="v" name="pg">
				    	<table width="100%" align="center" class="browserBorder">
	  						<tr>
	  							<td colspan="2" height="5px"></td>
	  						</tr>
	  						<tr>
	  							<td colspan="2" align="center">
	  								<script type="text/javascript">
										var Columns = ["<bean:message bundle='lable' key='lable.branchcode' />","<bean:message bundle='lable' key='lable.branchname' />","<bean:message bundle='lable' key='lable.address' />","<bean:message bundle='lable' key='lable.contactno' />"];
										var str=new AW.Formats.String;
										var num=new AW.Formats.Number;
										var grid=createBrowser('branchView',Columns,[str,str,str,str]);
										document.write(grid);
									</script>
	  							</td>
	  						</tr>
	  						<tr>
	  							<td colspan="2" height="5px"></td>
	  						</tr>
	  					</table>
				    </logic:equal>
				</td>
			</tr>
		</table>
  	</body>
</html>
