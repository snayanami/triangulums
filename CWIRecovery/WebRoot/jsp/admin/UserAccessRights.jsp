<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>User Access Rights</title>
    
    <script type="text/javascript" src="js/ajax.js"></script>
	<script type="text/javascript" src="js/aw.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/SkyBrowser.js"></script>
	
	<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
	<link rel="stylesheet" href="css/common/common.css" type="text/css" />
	
	<script type="text/javascript">
		function hideTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="none";
			//window.parent.document.getElementById('bodyFrame').setAttribute('height','1500');
		}
		
		function visibleTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="";
		}
		
		function clearScreen(id){$(id).innerHTML = "";}
		function upperCase(id){$(id).value = $(id).value.toUpperCase();}
		function $(name){return document.getElementById(name);}
		
		function createBrowser(gridName,columns,cellFormat, checkBoxColomn, checkBoxValueColomn){
			var obj = new AW.UI.Grid;
			obj.setId(gridName);
			var str = new AW.Formats.String;
			var num = new AW.Formats.Number;
			obj.setCellText([]);
			obj.setCellFormat(cellFormat);
			obj.setHeaderText(columns);
			obj.setSelectorVisible(true);
			obj.setRowCount(0);
			obj.setColumnCount(columns.length);
			obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
			obj.setSelectorWidth(30);
			obj.setHeaderHeight(20);
			obj.setSelectionMode("single-row");
			obj.onSelectedRowsChanged= function(row){};
			obj.loadingCompleted= function() {
			    if (obj._maskLoading) { 
			       obj._maskLoading.style.display = 'none'; 
			    } 
			}
			obj.loading = function() { 
			    if (! obj._maskLoading) { 
			        var maskLoading = document.createElement('div'); 
			        document.body.appendChild(maskLoading); 
			        maskLoading.className = 'aw-grid-loading'; 
			        var gridEl = obj.element(); 
			        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
			        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
			        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
			        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
			        obj._maskLoading = maskLoading; 
			   	}
		        obj.clearCellModel(); 
			    obj.clearRowModel(); 
			    obj.clearScrollModel(); 
			    obj.clearSelectionModel(); 
			    obj.clearSortModel(); 
			    obj.refresh(); 
		    	obj._maskLoading.style.display = 'block'; 
			}
			if(checkBoxColomn!=undefined){
				var chk=new AW.Templates.Checkbox;//create check box
				obj.setCellTemplate(chk, checkBoxColomn); // add check box
			    
			    obj.setCellValue(function(col,row){return this.getCellText(checkBoxValueColomn, row)}, checkBoxColomn);
			    obj.refreshCheckBox=function(){
			    	for(var i=0;i<obj.getRowCount();i++)
			    		obj.setCellValue(this.getCellText(checkBoxValueColomn, i), checkBoxColomn, i);
			    }
			    obj.getCheckBoxValue=function(col,row) {
			    	return obj.getCellValue(col,row);
			    }
			}
			return obj;
		}
		
		function setGridData(grid,data){
			grid.setCellText(data);
			grid.setRowCount(data.length);
			grid.refreshCheckBox();
			grid.refresh();
			grid.setSelectedRows([]);
		}
		
		/*Get Grid Data*/
        var dataArray=new Array();
		function getSystemPrograms(){
			grid.loading();
			var data="dispatch=getSystemPrograms&rand="+Math.random()*19851227;
			var ajax=new ajaxObject('userAccessRights.do');
				ajax.callback=function(responseText, responseStatus, responseXML) {
             		if (responseStatus == 200) {
                  		myArray = eval("("+responseText+")");							
             			setGridData(grid,myArray);
             			grid.loadingCompleted();
						dataArray = myArray;	
       	         	}
				}
			ajax.update(data,'POST');
		}
		
		/*Get Group System Programs*/
        var dataArray=new Array();
		function getGroupSystemPrograms(){
			var accessGroupId = $('accessGroupId').value;
			grid.loading();
			var data="dispatch=getGroupSystemPrograms&accessGroupId="+accessGroupId+"&rand="+Math.random()*19851227;
			var ajax=new ajaxObject('userAccessRights.do');
				ajax.callback=function(responseText, responseStatus, responseXML) {
             		if (responseStatus == 200) {
                  		myArray = eval("("+responseText+")");							
             			setGridData(grid,myArray);
             			/*for(var i=0;i<grid.getRowCount();i++){
							grid.setCellValue(grid.getCellValue(7,i),3,i);
						}*/
						grid.refresh();
             			grid.loadingCompleted();
						dataArray = myArray;	
       	         	}
				}
			ajax.update(data,'POST');
		}
		
		
		function getCreateData(){
			var accessGroupId = $('accessGroupId').value;
			var dataString="";
			for(var i=0;i < grid.getRowCount();i++){
				if(grid.getCellValue(3,i)==true){
					dataString+=grid.getCellText(4,i) + "<col>" + grid.getCellText(5,i) + "<col>" + grid.getCellText(6,i)+"<row>";
				}
			}
			
			return "&dataString="+dataString+"&accessGroupId="+accessGroupId;
		}
		
		function create(){
			if(!(confirm("Confirm to create a record?"))){
				return;														
			}
			
			var url="dispatch=create&rand="+Math.random()*19851227 + getCreateData();
			var ajax=new ajaxObject("userAccessRights.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(myData['createSuccess']){
	            		clearAll();
	            		alert(myData['createSuccess']);
	            	}else if(myData['error']){
	            		alert(myData['error']);
	            	}else{
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
			//alert(url);
		}
		
		function displayErrors(myData){
			for(i = 0; i<myData.length;i++){
				if(myData[i]["accessGroupId"]){
					$("accessGroupDiv").innerHTML = myData[i]["accessGroupId"];
				}
			}
		}
		
		function clearAll(){
			$('accessGroupId').value = '';
			$('accessGroup').value = '';
			$('accessGroupDescription').value = '';
			$('accessGroupDiv').innerHTML = '';
			for(var i=0;i<grid.getRowCount();i++){
				grid.setCellValue(false,3,i);
			}
			grid.setSelectedRows([]);
			grid.refresh();
		}
	</script>
	
	<style>
		.browserBorder {
		    border-top: #91a7b4 1px solid;
		    border-right: #91a7b4 1px solid;
		    border-left: #91a7b4 1px solid;
		    border-bottom: #91a7b4 1px solid;
		    background-color:#eeeeee;
		}
		
		#obj {height: 380px;width:100%;}
		#obj .aw-row-selector {text-align: center}
		#obj .aw-column-0 {width: 150px;}
		#obj .aw-column-1 {width: 200px;}
		#obj .aw-column-2 {width: 120px;}
		#obj .aw-column-3 {width: 100px;text-align: center;}
		#obj .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#obj .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
		
	</style>

  </head>
  
  <body onload="hideTabFrame();getSystemPrograms()" onunload="visibleTabFrame();">
    <table width="100%" align="center">
    	<tr>
    		<td>
    			
    			<table width="100%" align="center">
			    	<tr>
			    		<td height="5px"></td>
			    	</tr>
			    	<tr>
			    		<td>
			    			<table width="100%" align="center" class="browserBorder">
			    				<tr>
						    		<td colspan="2" height="3px"></td>
						    	</tr>
			    				<tr>
			    					<td align="right">
					    				<bean:message bundle="lable" key="lable.accessgroup" />
					    			</td>
					    			<td>
										<input type="hidden" id="accessGroupId" name="accessGroupId" value=""/>
										<input type="text" name="accessGroup" id="accessGroup" maxlength="3" size="5"
											onfocus="$('accessGroupDiv').innerHTML=''" onKeyUp="upperCase('accessGroup')"
											onblur="upperCase('accessGroup')" readonly="readonly"/>
										<input type="button" name="btnAccessGroup" id="btnAccessGroup" value="..." class="dot3buttonNormal"
											onclick="showSkyBrowser(2,'accessGroupId','accessGroup',0,'accessGroupDescription',1,0,0,'refPrgId','2','getGroupSystemPrograms()');$('accessGroupDiv').innerHTML='';"/>
										<input name="accessGroupDescription" type="text" id="accessGroupDescription" maxlength="60" size="60" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
										<br><div id="accessGroupDiv" class="validate"></div>
									</td>
			    				</tr>
			    				<tr>
						    		<td colspan="2" height="3px"></td>
						    	</tr>
			    			</table>
			    		</td>
		    		</tr>
		    		<tr>
		    			<td>
		    				<table width="100%" align="center" class="browserBorder">
		    					<tr>
		    						<td colspan="2" align="center">
					    				<script type="text/javascript">
											var Columns = ["Module","Program","Event","Select"];
											var str=new AW.Formats.String;
											var num=new AW.Formats.Number;
											var grid=createBrowser('obj',Columns,[str,str,str],3,7);
											/*var check=new AW.Templates.Checkbox;
											grid.setCellTemplate(check, 3);*/
											document.write(grid);
											
											grid.onSelectedRowsChanged = function(row){
												
				                           	};
										</script>
					    			</td>
		    					</tr>
		    				</table>
		    			</td>
		    		</tr>
		    		<tr>
    					<td>
    						<table width="100%" align="center" class="browserBorder">
    							<tr>
						  			<td></td>
						  			<td align="right">
						  				<input type="button" value="Submit" onclick="create()" id="Check" class="buttonNormal"/>
						  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
						  			</td>
						  		</tr>
    						</table>
    					</td>
    				</tr>
			    </table>
    			
    		</td>
    	</tr>
    </table>
  </body>
</html>
