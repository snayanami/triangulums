<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>My JSP 'BlackListClient.jsp' starting page</title>
    
	<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
	<link rel="stylesheet" href="css/common/calendar-system.css" type="text/css" />
	<link rel="stylesheet" href="css/common/calenderCSS.css" type="text/css" />
	<link rel="stylesheet" href="css/common/common.css" type="text/css" />
	
	<script type="text/javascript" src="js/GridBrowser.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/aw.js"></script>
	<script type="text/javascript" src="js/ajax.js"></script>
	
	<script type="text/javascript" src="js/SkyBrowser.js"></script>
	<script type="text/javascript" src="js/skyCalendar.js"></script>
	
	<script type="text/javascript">
		function hideTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="none";
		}
		
		function visibleTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="";
		}
	
		function clearScreen(id){$(id).innerHTML = "";}
		
		
		function $(name){return document.getElementById(name);}
		
		function createBrowser(gridName,columns,cellFormat){
			var obj = new AW.UI.Grid;
			obj.setId(gridName);
			var str = new AW.Formats.String;
			var num = new AW.Formats.Number;
			obj.setCellText([]);
			obj.setCellFormat(cellFormat);
			obj.setHeaderText(columns);
			obj.setSelectorVisible(true);
			obj.setRowCount(0);
			obj.setColumnCount(columns.length);
			obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
			obj.setSelectorWidth(20);
			obj.setHeaderHeight(20);
			obj.setSelectionMode("single-row");
			obj.onSelectedRowsChanged= function(row){};
			obj.loadingCompleted= function() {
			    if (obj._maskLoading) { 
			       obj._maskLoading.style.display = 'none'; 
			    } 
			}
			obj.loading = function() { 
			    if (! obj._maskLoading) { 
			        var maskLoading = document.createElement('div'); 
			        document.body.appendChild(maskLoading); 
			        maskLoading.className = 'aw-grid-loading'; 
			        var gridEl = obj.element(); 
			        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
			        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
			        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
			        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
			        obj._maskLoading = maskLoading; 
			   	}
		        obj.clearCellModel(); 
			    obj.clearRowModel(); 
			    obj.clearScrollModel(); 
			    obj.clearSelectionModel(); 
			    obj.clearSortModel(); 
			    obj.refresh(); 
		    	obj._maskLoading.style.display = 'block'; 
			}
			return obj;
		}
		
		function setGridData(grid,data){
			grid.setCellText(data);
			grid.setRowCount(data.length);
			grid.refresh();
			grid.setSelectedRows([]);
		}
		
		function getIsBlackListed(){
			var clientId = $('stakeholderId').value;
	    	if(clientId!=''){
		    	var data="dispatch=getIsBlackListed&clientId="+clientId+"&rand="+Math.random()*9999;				
				url = "blackListClient.do";
				var mySearchRequest = new ajaxObject(url);
				mySearchRequest.callback = function(responseText, responseStatus, responseXML) {
					if (responseStatus==200) {							
						var myObject =  eval('(' + responseText + ')');			       	
						if(myObject['error']){
							alert(myObject['error']);
						}else{
							$('isBlackListed').value = myObject['isBlackListed'];
							var isBl = myObject['isBlackListed'];
							if(isBl==1){
								$('Check').value = 'Active';	
							}else{
								$('Check').value = 'Black List';
							}
						}
					}else {
			    	    // 
				    }
				}
				mySearchRequest.update(data,'POST');
			}
	    }
		
		/*Get Grid Data*/
        var dataArray=new Array();
		function getGridData(){
			var clientId = $('stakeholderId').value;
			if(clientId!=''){
				grid.loading();
				var data="dispatch=getGridData&clientId="+clientId+"&rand="+Math.random()*19851227;
				var ajax=new ajaxObject('blackListClient.do');
					ajax.callback=function(responseText, responseStatus, responseXML) {
	             		if (responseStatus == 200) {
	                  		myArray = eval("("+responseText+")");							
	             			setGridData(grid,myArray);
	             			grid.loadingCompleted();
							dataArray = myArray;	
	       	         	}
					}
				ajax.update(data,'POST');
			}
		}
		
		function callFunction(){
			getIsBlackListed();
			getGridData();
		}
		
		function getCreateDate(){
			var status = $('isBlackListed').value;
			var clientId = $('stakeholderId').value
			var reason = $('reason').value
			
			return "&status="+status+"&clientId="+clientId +"&reason="+reason;
		}
		
		function update(){
			if(!(confirm("Confirm to update a record?"))){
				return;														
			}
			var url="dispatch=update&rand="+Math.random()*19851227 + getCreateDate();
			var ajax=new ajaxObject("blackListClient.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(myData['updateSuccess']){
	            		clearAll();
	            		alert(myData['updateSuccess']);
	            	}else if(myData['error']){
	            		alert(myData['error']);
	            	}else{
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function clearAll(){
			$('stakeholderId').value = '';
			$('stakeholderCode').value = '';
			$('stakeholderName').value = '';
			$('reason').value = ''
			$('isBlackListed').value = ''
			
			$('stakeholderDiv').innerHTML = '';
			$('reasonDiv').innerHTML = '';
			
			setGridData(grid,[]);
			
			$('Check').value = 'Black List';
		}
		
		function displayErrors(myData){
			for(i = 0; i<myData.length;i++){
				if(myData[i]["clientId"]){
					$("stakeholderDiv").innerHTML = myData[i]["clientId"];
				}if(myData[i]["reason"]){
					$("reasonDiv").innerHTML = myData[i]["reason"];
				}
			}
		}
	</script>
	
	<style>
		.browserBorder {
		    border-top: #91a7b4 1px solid;
		    border-right: #91a7b4 1px solid;
		    border-left: #91a7b4 1px solid;
		    border-bottom: #91a7b4 1px solid;
		    background-color:#eeeeee;
		}
		
		#grid {height: 330px;width:100%;}
		#grid .aw-row-selector {text-align: center}
		#grid .aw-column-0 {width: 100px;}
		#grid .aw-column-1 {width: 110px;text-align: right}
		#grid .aw-column-2 {width: 110px;text-align: right}
		#grid .aw-column-3 {width: 110px;text-align: right}
		#grid .aw-column-4 {width: 140px;text-align: right}
		#grid .aw-column-5 {width: 100px;text-align: right}
		#grid .aw-column-6 {width: 80px;}
		#grid .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#grid .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
	</style>

  </head>
  
  <body onload="hideTabFrame()" onunload="visibleTabFrame()">
    <table width="100%" align="center">
    	<tr>
    		<td>
    			<table width="100%" align="center" class="browserBorder">
    				<tr>
    					<td colspan="2" height="10px"></td>
    				</tr>
    				<tr>
   						<td align="right" width="15%">
   							<bean:message bundle="lable" key="lable.stakeholder" />
   						</td>
   						<td>
   							<input type="hidden" id="isBlackListed">
  							<input type="hidden" id="stakeholderId" name="stakeholderId" value=""/>
  							<input type="text" name="stakeholderCode" id="stakeholderCode" maxlength="6" size="10"
								onfocus="$('stakeholderDiv').innerHTML=''" onKeyUp=""
								onblur="" readonly="readonly"/>
							<input type="button" name="btnStakeholder" id="btnStakeholder" value="..." class="dot3buttonNormal"
								onclick="showSkyBrowser(3,'stakeholderId','stakeholderCode',0,'stakeholderName',1,0,0,'','','callFunction()');$('stakeholderDiv').innerHTML='';"/>
  							<input name="stakeholderName" type="text" id="stakeholderName" size="58" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
  							<br><div id="stakeholderDiv" class="validate"></div>
  						</td>
   					</tr>
   					<tr>
    					<td colspan="2" height="10px"></td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	<tr>
    		<td>
    			<table width="100%" align="center" class="browserBorder">
    				<tr>
    					<td>
    						<script type="text/javascript">
								var Columns = ["Pawn Ticket No","Pawning Advance","Capital in Arrears","Interest in Arrears","Other Charges in Arrears","Total Arrears","Status"];
								var str=new AW.Formats.String;
								var num=new AW.Formats.Number;
								var grid=createBrowser('grid',Columns,[str,num,num,num,num,num,str]);
								document.write(grid);
								grid.onSelectedRowsChanged = function(row){
									if(row!=undefined && row!='' && parseInt(row)>=0){
										
									}
								}
							</script>
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	<tr>
    		<td>
    			<table width="100%" align="center" class="browserBorder">
    				<tr>
    					<td colspan="2" height="10px"></td>
    				</tr>
    				<tr>
    					<td align="right" width="15%">
    						Reason
    					</td>
    					<td>
    						<input type="text" id="reason" name="reason" size="75" maxlength="100" onfocus="clearScreen('reasonDiv')">
    						<font color="red">*</font><br><div id="reasonDiv" class="validate"></div>
    					</td>
    				</tr>
    				<tr>
    					<td colspan="2" height="10px"></td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	<tr>
			<td>
				<table align="center" width="100%" class="browserBorder">
					<tr>
			  			<td align="right">
			  				<input type="button" value="Black List" onclick="update()" id="Check" class="buttonNormal"/>
			  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
			  			</td>
  					</tr>
				</table>
			</td>
		</tr>
    </table>
  </body>
</html>
