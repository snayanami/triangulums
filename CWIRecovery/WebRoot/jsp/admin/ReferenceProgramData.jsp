<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>Reference Program Data</title>
    
    <script type="text/javascript" src="js/ajax.js"></script>
	<script type="text/javascript" src="js/aw.js"></script>
	<script type="text/javascript" src="js/SkyBrowser.js"></script>
	
	<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
	<link rel="stylesheet" href="css/common/common.css" type="text/css" />
	
	<script type="text/javascript">
		function clearScreen(id){
			$(id).innerHTML = "";              	
		}
		
		function upperCase(id){
			$(id).value = $(id).value.toUpperCase();	
		}
		
		function $(name){return document.getElementById(name);}
		
		function createBrowser(gridName,columns,cellFormat){
			var obj = new AW.UI.Grid;
			obj.setId(gridName);
			var str = new AW.Formats.String;
			var num = new AW.Formats.Number;
			obj.setCellText([]);
			obj.setCellFormat(cellFormat);
			obj.setHeaderText(columns);
			obj.setSelectorVisible(true);
			obj.setRowCount(0);
			obj.setColumnCount(columns.length);
			obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
			obj.setSelectorWidth(30);
			obj.setHeaderHeight(20);
			obj.setSelectionMode("single-row");
			obj.onSelectedRowsChanged= function(row){};
			obj.loadingCompleted= function() {
			    if (obj._maskLoading) { 
			       obj._maskLoading.style.display = 'none'; 
			    } 
			}
			obj.loading = function() { 
			    if (! obj._maskLoading) { 
			        var maskLoading = document.createElement('div'); 
			        document.body.appendChild(maskLoading); 
			        maskLoading.className = 'aw-grid-loading'; 
			        var gridEl = obj.element(); 
			        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
			        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
			        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
			        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
			        obj._maskLoading = maskLoading; 
			   	}
		        obj.clearCellModel(); 
			    obj.clearRowModel(); 
			    obj.clearScrollModel(); 
			    obj.clearSelectionModel(); 
			    obj.clearSortModel(); 
			    obj.refresh(); 
		    	obj._maskLoading.style.display = 'block'; 
			}
			return obj;
		}
		
		function setGridData(grid,data){
			grid.setCellText(data);
			grid.setRowCount(data.length);
			grid.refresh();
			grid.setSelectedRows([]);
		}
		
		function create(){
			if(!(confirm("Confirm to create a record?"))){
				return;														
			}
			var url="dispatch=create&rand="+Math.random()*19851227 + getCreateDate();
			//alert(url);
			var ajax=new ajaxObject("refProgramData.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(myData['createSuccess']){
	            		clearAll();
	            		alert(myData['createSuccess']);
	            	}else if(myData['error']){
	            		alert(myData['error']);
	            	}else{
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function update(){
			if(!checkedRecordSelected()){
				alert('Please select a record.');
				return false;
			}
			if(!(confirm("Confirm to update a record?"))){
				return;														
			}
			var url="dispatch=update&rand="+Math.random()*19851227 + getChangeData();
			//alert(url);
			var ajax=new ajaxObject("refProgramData.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(myData['updateSuccess']){
	            		clearAll();
	            		//getGridData();
	            		alert(myData['updateSuccess']);
	            	}else if(myData['error']){
	            		alert(myData['error']);
	            	}else{
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function remove(){
			if(!checkedRecordSelected()){
				alert('Please select a record.');
				return false;
			}
			if(!(confirm("Confirm to delete a record?"))){
				return;														
			}
			var refProgramDataId = $('refProgramDataId').value;
			var url="dispatch=delete&refProgramDataId="+refProgramDataId+"&rand="+Math.random()*19851227;
			//alert(url);
			var ajax=new ajaxObject("refProgramData.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(['deleteSuccess']){
	            		clearFilling();
	            		//getGridData();
	            		alert(myData['deleteSuccess']);
	            	}else{
	            		//displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function getCreateDate(){
			var refProgramId = $('refProgramId').value;
			var code = $('code').value;
			var description = $('description').value;

			return "&refProgramId="+refProgramId+"&code="+code+"&description="+description;
		}
		
		function getChangeData(){
			var refProgramDataId = $('refProgramDataId').value;
			
			return "&refProgramDataId="+refProgramDataId + getCreateDate();
		}
		
		function displayErrors(myData){
			for(i = 0; i<myData.length;i++){
				if(myData[i]["code"]){
					$("codeDiv").innerHTML = myData[i]["code"];
				}if(myData[i]["description"]){
					$("descriptionDiv").innerHTML = myData[i]["description"];
				}if(myData[i]["refProgramId"]){
					$("refProgramDiv").innerHTML = myData[i]["refProgramId"];
				}
			}
		}
		
		function clearAll(){
			clearFilling();
			clearDivision();
		}
		
		function clearFilling(){
			$('refProgramId').value = '';
			$('refProgramCode').value = '';
			$('refProgramDescription').value = '';
			$('code').value = '';
			$('description').value = '';
			try{
				setGridData(grid,[])
				grid.setSelectedRows([]);	
			}catch(e){
				//alert(e);
			}
		}
		
		function clearDivision(){
			$("codeDiv").innerHTML = '';
			$("descriptionDiv").innerHTML = '';
			$("refProgramDiv").innerHTML = '';
		}
		
		function checkedRecordSelected(){
			var refProgramDataId = $('refProgramDataId').value ;
    		if(refProgramDataId ==null || refProgramDataId ==''){
    			return false;
    		}else
    			return true;
		}
		
		/*Get Grid Data*/
        var dataArray=new Array();
		function getGridData(){
			var refProgramId = $('refProgramId').value;
			if(refProgramId!=''){
				grid.loading();
				var data="dispatch=getAllRefProgramData&refProgramId="+refProgramId+"&rand="+Math.random()*19851227;
				var ajax=new ajaxObject('refProgramData.do');
					ajax.callback=function(responseText, responseStatus, responseXML) {
	             		if (responseStatus == 200) {
	                  		myArray = eval("("+responseText+")");							
	             			setGridData(grid,myArray);
	             			grid.loadingCompleted();
							dataArray = myArray;	
	       	         	}
					}
				ajax.update(data,'POST');
			}
		}
		
		/*Get Grid Data*/
        var dataArray2=new Array();
		function getGridDataView(){
			gridView.loading();
			var data="dispatch=getAllRefProgramDataForView&rand="+Math.random()*19851227;
			var ajax=new ajaxObject('refProgramData.do');
				ajax.callback=function(responseText, responseStatus, responseXML) {
             		if (responseStatus == 200) {
                  		myArray = eval("("+responseText+")");							
             			setGridData(gridView,myArray);
             			gridView.loadingCompleted();
						dataArray2 = myArray;	
       	         	}
				}
			ajax.update(data,'POST');
		}
		
		function callFunction(){
			if($('pg').value=='m' || $('pg').value=='d'){
				getGridData();
			}else if($('pg').value=='v'){
				getGridDataView();
			}
		}
		
	</script>
	
	<style>
		.browserBorder {
		    border-top: #91a7b4 1px solid;
		    border-right: #91a7b4 1px solid;
		    border-left: #91a7b4 1px solid;
		    border-bottom: #91a7b4 1px solid;
		    background-color:#eeeeee;
		}
		
		#refPrgData {height: 260px;width:100%;}
		#refPrgData .aw-row-selector {text-align: center}
		#refPrgData .aw-column-0 {width: 200px;}
		#refPrgData .aw-column-1 {width: 500px;}
		#refPrgData .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#refPrgData .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
		
		#refPrgDataView {height: 410px;width:100%;}
		#refPrgDataView .aw-row-selector {text-align: center}
		#refPrgDataView .aw-column-0 {width: 250px;}
		#refPrgDataView .aw-column-1 {width: 100px;}
		#refPrgDataView .aw-column-2 {width: 350px;}
		#refPrgDataView .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#refPrgDataView .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
	</style>

  </head>
  
  <body onload="callFunction()">
  	<table width="100%">
  		<tr>
  			<td>
  				<logic:equal name="refProgramData" property="action" value="add">
  					<input type="hidden" id="pg" name="pg" value="a">
  					<table width="100%" align="center" class="">
  						<tr>
			    			<td>
			    				<table align="center" width="100%" class="browserBorder">
			    					<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.referenceprogram" />
			  							</td>
			  							<td>
			  								<input type="hidden" id="refProgramId" name="refProgramId" value=""/>
			  								<input type="text" name="refProgramCode" id="refProgramCode" maxlength="3" size="5"
												onfocus="$('refProgramDiv').innerHTML=''" onKeyUp=""
												onblur="" readonly="readonly" />
												<input type="button" name="btnRefProgramCode" id="btnRefProgramCode" value="..." class="dot3buttonNormal"
												onclick="showSkyBrowser(1,'refProgramId','refProgramCode',0,'refProgramDescription',1,0,0,'','');$('refProgramDiv').innerHTML='';"/>
			  								<input name="refProgramDescription" type="text" id="refProgramDescription" maxlength="60" size="76" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
			  								<br><div id="refProgramDiv" class="validate"></div>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.code" />
			  							</td>
			  							<td>
			  								<input type="text" id="code" name="code" size="5" maxlength="3" onfocus="clearScreen('codeDiv')" onblur="upperCase('code')" onkeyup="upperCase('code')"/>
			  								<font color="red">*</font><br><div id="codeDiv" class="validate"></div>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.description" />
			  							</td>
			  							<td>
			  								<input type="text" id="description" name="description" size="90" maxlength="60" onfocus="clearScreen('descriptionDiv')"/>
			  								<font color="red">*</font><br><div id="descriptionDiv" class="validate" ></div>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			    				</table>
			    			</td>
			    		</tr>
  						<tr>
			    			<td>
			    				<table align="center" width="100%" class="browserBorder">
			    					<tr>
							  			<td></td>
							  			<td align="right">
							  				<input type="button" value="Submit" onclick="create()" id="Save" class="buttonNormal"/>
							  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
							  			</td>
							  		</tr>
			    				</table>
			    			</td>
			    		</tr>
  					</table>
  				</logic:equal>	
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<logic:equal name="refProgramData" property="action" value="modify">
  					<input type="hidden" id="pg" name="pg" value="m">
  					<input id="refProgramDataId" type="hidden" name="refProgramDataId">
  					<table width="100%" align="center" class="browserBorder">
  						<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.referenceprogram" />
			  							</td>
			  							<td>
			  								<input type="hidden" id="refProgramId" name="refProgramId" value=""/>
			  								<input type="text" name="refProgramCode" id="refProgramCode" maxlength="3" size="5"
												onfocus="$('refProgramDiv').innerHTML=''" onKeyUp=""
												onblur="" readonly="readonly"/>
												<input type="button" name="btnRefProgramCode" id="btnRefProgramCode" value="..." class="dot3buttonNormal"
												onclick="showSkyBrowser(1,'refProgramId','refProgramCode',0,'refProgramDescription',1,0,0,'','','getGridData()');$('refProgramDiv').innerHTML='';"/>
			  								<input name="refProgramDescription" type="text" id="refProgramDescription" maxlength="60" size="75" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
			  								<br><div id="refProgramDiv" class="validate"></div>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" align="center">
			  								<script type="text/javascript">
												var Columns = ["<bean:message bundle='lable' key='lable.code' />","<bean:message bundle='lable' key='lable.description' />"];
												var str=new AW.Formats.String;
												var num=new AW.Formats.Number;
												var grid=createBrowser('refPrgData',Columns,[str,str,str]);
												document.write(grid);
												grid.onSelectedRowsChanged = function(row){
													$('code').value = this.getCellText(0,row);
													$('description').value = this.getCellText(1,row);
													$('refProgramDataId').value = this.getCellText(2,row);
					                           	};
											</script>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.code" />
			  							</td>
			  							<td>
			  								<input type="text" id="code" name="code" readonly="readonly" size="5"/>
			  								<br><div id="codeDiv" class="validate"></div>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.description" />
			  							</td>
			  							<td>
			  								<input type="text" id="description" name="description" onfocus="clearScreen('descriptionDiv')" maxlength="60" size="91"/>
			  								<font color="red">*</font><br><div id="descriptionDiv" class="validate"></div>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			    				</table>
			    			</td>
			    		</tr>
  						<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
							  			<td></td>
							  			<td align="right">
							  				<input type="button" value="Submit" onclick="update()" id="Check" class="buttonNormal"/>
							  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
							  			</td>
							  		</tr>
			    				</table>
			    			</td>
			    		</tr>
  					</table>
  				</logic:equal>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<logic:equal name="refProgramData" property="action" value="delete">
  					<input type="hidden" id="pg" name="pg" value="d">
  					<input id="refProgramDataId" type="hidden" name="refProgramDataId">
  					<table width="100%" align="center" class="browserBorder">
  						<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.referenceprogram" />
			  							</td>
			  							<td>
			  								<input type="hidden" id="refProgramId" name="refProgramId" value=""/>
			  								<input type="text" name="refProgramCode" id="refProgramCode" maxlength="3" size="5"
												onfocus="$('refProgramDiv').innerHTML=''" onKeyUp=""
												onblur="" readonly="readonly"/>
												<input type="button" name="btnRefProgramCode" id="btnRefProgramCode" value="..." class="dot3buttonNormal"
												onclick="showSkyBrowser(1,'refProgramId','refProgramCode',0,'refProgramDescription',1,0,0,'','','getGridData()');$('refProgramDiv').innerHTML='';"/>
			  								<input name="refProgramDescription" type="text" id="refProgramDescription" maxlength="60" size="75" readonly="readonly" tabindex="-1"/>&nbsp;<font color="red">* </font>
			  								<br><div id="refProgramDiv"></div>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" align="center">
			  								<script type="text/javascript">
												var Columns = ["<bean:message bundle='lable' key='lable.code' />","<bean:message bundle='lable' key='lable.description' />"];
												var str=new AW.Formats.String;
												var num=new AW.Formats.Number;
												var grid=createBrowser('refPrgData',Columns,[str,str,str]);
												document.write(grid);
												grid.onSelectedRowsChanged = function(row){
													$('code').value = this.getCellText(0,row);
													$('description').value = this.getCellText(1,row);
													$('refProgramDataId').value = this.getCellText(2,row);
					                           	};
											</script>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.code" />
			  							</td>
			  							<td>
			  								<input type="text" id="code" name="code" readonly="readonly" size="5"/>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			  						<tr>
			  							<td align="right">
			  								<bean:message bundle="lable" key="lable.description" />
			  							</td>
			  							<td>
			  								<input type="text" id="description" name="description" readonly="readonly" size="91"/>
			  							</td>
			  						</tr>
			  						<tr>
			  							<td colspan="2" height="5px"></td>
			  						</tr>
			    				</table>
			    			</td>
			    		</tr>
  						<tr>
			    			<td>
			    				<table width="100%" class="browserBorder" align="center">
			    					<tr>
							  			<td></td>
							  			<td align="right">
							  				<input type="button" value="Submit" onclick="remove()"id="Check" class="buttonNormal"/>
							  				<input type="button" value="Clear" onclick="clearFilling()" id="Clear" class="buttonNormal"/>
							  			</td>
							  		</tr>
			    				</table>
			    			</td>
			    		</tr>
  					</table>
  				</logic:equal>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<logic:equal name="refProgramData" property="action" value="view">
  					<input type="hidden" id="pg" name="pg" value="v">
  					<table width="100%" align="center" class="browserBorder">
  						<tr>
  							<td colspan="2" height="5px"></td>
  						</tr>
  						<tr>
  							<td colspan="2" align="center">
  								<script type="text/javascript">
									var Columns = ["<bean:message bundle='lable' key='lable.referenceprogram' />","<bean:message bundle='lable' key='lable.code' />","<bean:message bundle='lable' key='lable.description' />"];
									var str=new AW.Formats.String;
									var num=new AW.Formats.Number;
									var gridView=createBrowser('refPrgDataView',Columns,[str,str,str]);
									document.write(gridView);
								</script>
  							</td>
  						</tr>
  						<tr>
  							<td colspan="2" height="5px"></td>
  						</tr>
  					</table>
  				</logic:equal>
  			</td>
  		</tr>
  	</table>
  </body>
</html>
