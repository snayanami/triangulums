<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  	<head>
	    <title>My JSP 'GridBroser.jsp' starting page</title>
	    
		<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
		
		<script type="text/javascript" src="js/GridBrowser.js"></script>
		<script type="text/javascript" src="js/aw.js"></script>
		<script type="text/javascript" src="js/ajax.js"></script>
	</head>
  
  	<body>
	  	<%
	  		out.println("<input type='hidden' id='bean' value='"+request.getParameter("bean")+"'/>");
	  		out.println("<input type='hidden' id='method' value='"+request.getParameter("method")+"'/>");
	  		out.println("<input type='hidden' id='headers' value='"+request.getParameter("headers")+"'/>");
	  		out.println("<input type='hidden' id='elements' value='"+request.getParameter("elements")+"'/>");
	  		out.println("<input type='hidden' id='param1' value='"+request.getParameter("param1")+"'/>");
	  	%>
	  	<table style="width: 100%;">
	  		<tr>
	  			<td id="gridSpace"></td>
	  		</tr>
	  	</table>
	  	<script type="text/javascript">
	  		var method = document.getElementById("method").value;
			var headers = (document.getElementById("headers").value).split(",");
			var bean = document.getElementById("bean").value;
			var elements = (document.getElementById("elements").value).split(",");
			
	  		var browserGrid = new AW.UI.Grid;
			browserGrid.setId("browserGrid");
		
			//	define data formats
			var str = new AW.Formats.String;
			var num = new AW.Formats.Number;
		
			browserGrid.setCellFormat([str, str]);
			var myColumns = headers;
			
			//	set click action handler
			browserGrid.onCellClicked = function(event, col, row){window.status = this.getCellText(col, row)};
			if(param1!=null)
				var url="dispatch="+method+"&param1="+param1+"&rand="+Math.random()*9999;
			else
				var url="dispatch="+method+"&rand="+Math.random()*9999;
			var ajax=new ajaxObject(bean);
			ajax.callback=function(responseText, responseStatus, responseXML) {
		       	if (responseStatus == 200) {
		       		myData=eval('(' + responseText + ')');
		           	if(myData["error"]){
		            		alert(myData["error"]);
		            }else{
		           		//	provide cells and headers text
						browserGrid.setCellText(myData);
						browserGrid.setHeaderText(myColumns);
						
						//	set number of rows/columns
						browserGrid.setRowCount(myData.length);
						browserGrid.setColumnCount(5);
		
						//	enable row selectors
						browserGrid.setSelectorVisible(true);
						browserGrid.setSelectorText(function(i){return this.getRowPosition(i)+1});
					
						//	set headers width/height
						browserGrid.setSelectorWidth(28);
						browserGrid.setHeaderHeight(20);
					
						//	set row selection
						browserGrid.setSelectionMode("single-row");
						document.getElementById("gridSpace").innerHTML = browserGrid;
		            }          
				}
			}
			ajax.update(url,'POST');
			browserGrid.onSelectedRowsChanged = function(row){
				for(i=0;i<elements.length;i++){
					if((window.opener.document.getElementById(elements[i]).id)!='blank')
						window.opener.document.getElementById(elements[i]).value = this.getCellText(i,row)
				}
			}
		</script>  	
  	</body>
</html>