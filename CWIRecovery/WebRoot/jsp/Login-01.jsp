<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>.:: RuSavings ::.</title>
    
    <script type="text/javascript" src="js/ajax.js"></script>
    <script type="text/javascript">
		function $(name){return document.getElementById(name);}
		
		function loadPopup(url,width,height){
			var theTop=(screen.height/2)-(height/2);
			var theLeft=(screen.width/2)-(width/2);
			var features='height='+height+',width='+width+',top='+theTop+',left='+theLeft+",dialog=yes,modal=yes,status=yes,scrollbars=no";
			var win=window.open('Main.jsp?url='+escape(url)+"&height="+escape(height)+"&width="+escape(width),"home",features);
		}
		
		//loadPopup('login.do?dispatch=loadHomePage&officerId='+officerId,1024,768);
		
	
		function getSysPrograms(){
			var uname = document.getElementById('uname').value;
			var pwd = cry(document.getElementById('pwd').value);
			var url="dispatch=login&uname="+uname+"&pwd="+pwd+"&rand="+Math.random()*9999;
			var ajax=new ajaxObject("login.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	//alert(myData);
	            	if(myData[0]=='error'){
	            		alert(myData[1]);
	            	}else if(myData['success']!=null){
	            		//$('enter').disabled = false;
	            		$('loginUserNameTD').innerHTML = myData['loginUserName'];
	            		$('officerId').value = myData['success'];
	            		$('logoutTR').style.display = "" ;
						$('loginTR').style.display = "none" ;
	            		enter();
	            		
	            		//window.open("login.do?dispatch=loadHomePage&officerId="+myData["success"]+"&rand="+Math.random()*9999,"_parent");
	            		//var data = "login.do?dispatch=loadHomePage&officerId="+myData["success"]+"&rand="+Math.random()*9999,"_parent";
	            		//loadPopup(data,800,600);
	            	}else{
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function enter(){
			var officerId = $('officerId').value;
			if(officerId!=''){
				//window.open("login.do?dispatch=loadHomePage&officerId="+officerId+"&rand="+Math.random()*9999,"_new");
				loadPopup('login.do?dispatch=loadHomePage&officerId='+officerId,1024,768);
				return;	
			}else{
				return false;
			}
		}
		
		function displayErrors(myData){
			for(i = 0; i<myData.length;i++){
				if(myData[i]["code"]){
					document.getElementById("codeDiv").innerHTML = myData[i]["code"];
				}if(myData[i]["des"]){
					document.getElementById("desDiv").innerHTML = myData[i]["des"];
				}
			}
		}
		
		function cry(v){var str="";for(var i=0;i<v.length;i++){str+=v.charCodeAt(i++).toString(16);}for(var i=1;i<v.length;i++){str+=v.charCodeAt(i++).toString(16);}return str;}
		
		function logout(){
			var url="dispatch=logout&&rand="+Math.random()*19851227;
			var ajax=new ajaxObject("login.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	         		myData=eval('(' + responseText + ')');
	            	if(myData["logout"]){
		            	$('logoutTR').style.display = "none" ;
						$('loginTR').style.display = "" ;
						clearData();
		            }else{
		            	alert(myData["logout"]);
		            }          
	            }
			}
			ajax.update(url,'POST');
		}
		
		function clearData(){
			$('officerId').value = '';
       		$('uname').value = '';
       		$('pwd').value = '';
		}
		
		/*function logout(id){
			if(document.getElementById('logout')){
				var logout=CreateXHR();
				var uri="login.do";
				logout.onreadystatechange = function() {
					if (logout.readyState == 4) {
						if (logout.status == 200) {
							var mes=eval("("+logout.responseText+")");
							if(mes['logout']){
								try{
									logouted();
								}catch(e){
								
								}
							}else if("error"){
								//document.getElementById('secondRow').innerHTML="<font style=\"font-size:8pt\" color=\"red\" >"+mes['error']+"</font>";
								alert(mes["error"]);
							}
						}
					}
				}
				var data="dispatch=logout";
				if(id!=undefined)
					data+="&userId="+id;
				logout.open("POST", uri, true);
				logout.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
				logout.send(data);
			}
		}*/	
	</script>
  </head>
  
  <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<!-- ImageReady Slices (sri.psd) -->
	<table id="Table_01" width="990" height="500" border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<td colspan="6">
				<img src="images/sri_01.gif" width="990" height="37" alt=""></td>
		</tr>
		<tr>
			<td colspan="6">
				<img src="images/sri_02.gif" width="990" height="74" alt=""></td>
		</tr>
		<tr>
			<td colspan="6">
				<img src="images/sri_03.gif" width="990" height="30" alt=""></td>
		</tr>
		<tr>
			<td rowspan="2">
				<img src="images/sri_04.gif" width="17" height="359" alt=""></td>
			<td onclick="enter();" style="cursor: pointer">
				<img src="images/sri_05.gif" width="290" height="156" alt="">
			</td>
			<td colspan="2">
				<img src="images/sri_06.gif" width="288" height="156" alt=""></td>
			<td colspan="2" width="395" height="156" style="background-image:url(images/sri_07.gif);background-position:top;" valign="top">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr height="40px">
					<td colspan="5"><input type="hidden" id="officerId" name="officerId"></td>
				</tr>
				<tr id="loginTR">
					<td>
						User Name
					</td>
					<td>
						<input type="text" id="uname" name="uname" size="10"/>
						<br><div id="unameDiv"></div>
					</td>
					<td>
						Password
					</td>
					<td>
						<input type="password" id="pwd" name="pwd" size="10"/>
						<br><div id="pwdDiv"></div>
					</td>
					<td>
						<input type="button" value="Login" onclick="getSysPrograms()"/>
					</td>
				</tr>
				<tr id="logoutTR" style="display: none;">
					<td id="loginUserNameTD" colspan="4" style="font-style: italic;font-weight: bold;font-family: monospace;font-size: large" align="center">
					</td>
					<td>
						<input type="button" value="Logout" onclick="logout()"/>
					</td>
				</tr>
			</table>
			</td>

		<tr>
			<td colspan="2">
				<img src="images/sri_08.gif" width="329" height="203" alt=""></td>
			<td colspan="2">
				<img src="images/sri_09.gif" width="316" height="203" alt=""></td>
			<td>
				<img src="images/sri_10.gif" width="328" height="203" alt=""></td>
		</tr>
		<tr>
			<td>
				<img src="images/spacer.gif" width="17" height="1" alt=""></td>
			<td>
				<img src="images/spacer.gif" width="290" height="1" alt=""></td>
			<td>
				<img src="images/spacer.gif" width="39" height="1" alt=""></td>
			<td>
				<img src="images/spacer.gif" width="249" height="1" alt=""></td>
			<td>
				<img src="images/spacer.gif" width="67" height="1" alt=""></td>
			<td>
				<img src="images/spacer.gif" width="328" height="1" alt=""></td>
		</tr>
	</table>
	<!-- End ImageReady Slices -->
	</body>
</html>
