<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'Tab.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" href="css/common/tab.css" type="text/css" />
	<script src="js/tab.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		
		
		function loadEventPage(obj,eve){
			nodelist=document.getElementById("tabTR").childNodes.length;
			obj.className="hint";
			for (var i=0; i<nodelist; i++) {
				if(document.getElementById("tabTR").childNodes[i].id != obj.id ){
					document.getElementById("tabTR").childNodes[i].className="def";
				}
			}
			var p = window.parent.document.getElementById('page').value;
			var a = p.split("?");
			window.parent.document.getElementById('bodyFrame').src = a[0]+"?dispatch=loadTabPage&action="+eve+"&page="+a[0].split(".")[0];
		}
		function loadTab(val){//alert('a');
			var event = val.split(",");
			var index= 1;
			for(i=0;i<event.length;i++){
				if(event[i]==1){
					//document.getElementById('tabTR').innerHTML += "<td class='hint' id='l1' onMouseOver='Tabonmouseover(this)' onClick='TabImageOnclick(this);loadEventPage(\"add\")' onMouseOut='Tabonmouseout(this)' >ADD</td>";
					if(index==1){
						window.parent.frames[0].document.getElementById('tabTR').innerHTML += "<td class='hint' id='l1' onMouseOver='Tabonmouseover(this)' onClick='loadEventPage(this,\"add\")' onMouseOut='Tabonmouseout(this)' ><b>Add</b></td>";
						index++;
					}else{
						window.parent.frames[0].document.getElementById('tabTR').innerHTML += "<td class='def' id='l1' onMouseOver='Tabonmouseover(this)' onClick='loadEventPage(this,\"add\")' onMouseOut='Tabonmouseout(this)' ><b>Add</b></td>";
					}
				}else if(event[i]==2){
					//document.getElementById('tabTR').innerHTML += "<td class='def' id='l2' onMouseOver='Tabonmouseover(this)' onClick='TabImageOnclick(this);loadEventPage(\"delete\")' onMouseOut='Tabonmouseout(this)' >Delete</td>";
					if(index==1){
						window.parent.frames[0].document.getElementById('tabTR').innerHTML += "<td class='hint' id='l2' onMouseOver='Tabonmouseover(this)' onClick='loadEventPage(this,\"delete\")' onMouseOut='Tabonmouseout(this)' ><b>Delete</b></td>";
						index++;
					}else{
						window.parent.frames[0].document.getElementById('tabTR').innerHTML += "<td class='def' id='l2' onMouseOver='Tabonmouseover(this)' onClick='loadEventPage(this,\"delete\")' onMouseOut='Tabonmouseout(this)' ><b>Delete</b></td>";
					}
				}else if(event[i]==3){
					//document.getElementById('tabTR').innerHTML += "<td class='def' id='l3' onMouseOver='Tabonmouseover(this)' onClick='TabImageOnclick(this);loadEventPage(\"modify\")' onMouseOut='Tabonmouseout(this)' >Modify</td>";
					if(index==1){
						window.parent.frames[0].document.getElementById('tabTR').innerHTML += "<td class='hint' id='l3' onMouseOver='Tabonmouseover(this)' onClick='loadEventPage(this,\"modify\")' onMouseOut='Tabonmouseout(this)' ><b>Modify</b></td>";
						index++;
					}else{
						window.parent.frames[0].document.getElementById('tabTR').innerHTML += "<td class='def' id='l3' onMouseOver='Tabonmouseover(this)' onClick='loadEventPage(this,\"modify\")' onMouseOut='Tabonmouseout(this)' ><b>Modify</b></td>";
					}
				}else if(event[i]==4){
					//document.getElementById('tabTR').innerHTML += "<td class='def' id='l4' onMouseOver='Tabonmouseover(this)' onClick='TabImageOnclick(this);loadEventPage(\"view\")' onMouseOut='Tabonmouseout(this)' >View</td>";
					if(index==1){
						window.parent.frames[0].document.getElementById('tabTR').innerHTML += "<td class='hint' id='l4' onMouseOver='Tabonmouseover(this)' onClick='loadEventPage(this,\"view\")' onMouseOut='Tabonmouseout(this)' ><b>View</b></td>";
						index++;
					}else{
						window.parent.frames[0].document.getElementById('tabTR').innerHTML += "<td class='def' id='l4' onMouseOver='Tabonmouseover(this)' onClick='loadEventPage(this,\"view\")' onMouseOut='Tabonmouseout(this)' ><b>View</b></td>";
					}
				}
			}
			loadFirstPage();
		}
		function loadFirstPage(){//alert('b');
			var e ;
			var events = window.parent.document.getElementById('link').value;
			//alert(events);
			var page = window.parent.document.getElementById('page').value;
			var a = page.split("?");
			
			if(events.split(',')[0]==1){
				e = 'add';
			}else if(events.split(',')[0]==2){
				e = 'delete';
			}else if(events.split(',')[0]==3){
				e = 'modify'
			}else if(events.split(',')[0]==4){
				e = 'view';
			}			
			window.parent.document.getElementById('bodyFrame').src = a[0]+"?dispatch=loadTabPage&action="+e+"&page="+a[0].split(".")[0];
		}
	</script>

  </head>
  
  <body>
    <table style="position: relative;top: 0px;left: 0px;background-image:url('');" border="0">
    	<tr>
    		<td>
    			<table id="tab" cellSpacing="0" cellPadding="1" width="100%" border="0">
					<tr id="tabTR" name='tabTR'>
						<!-- td class="hint" id="l1" onMouseOver="Tabonmouseover(this)" onClick="TabImageOnclick(this);loadLyr('lyr1');" onMouseOut="Tabonmouseout(this)" >xxxxxz</td> 
						<td class="def" id="l2" onMouseOver="Tabonmouseover(this)" onClick="TabImageOnclick(this);loadLyr('lyr2');" onMouseOut="Tabonmouseout(this)">zzzzzzzzz</td>
						<td class="def" id="l3" onMouseOver="Tabonmouseover(this)" onClick="TabImageOnclick(this);loadLyr('lyr3');" onMouseOut="Tabonmouseout(this)">cccccccc</td>
						<td class="def" id="l4" onMouseOver="Tabonmouseover(this)" onClick="TabImageOnclick(this);loadLyr('lyr4');" onMouseOut="Tabonmouseout(this)">vbbbbbbbbb</td>
						<td class="def" id="l5" onMouseOver="Tabonmouseover(this)" onClick="TabImageOnclick(this);loadLyr('lyr5');" onMouseOut="Tabonmouseout(this)">zzzzzzzzzzz</td>
						<td style=""></td>-->
					</tr>
				</table>
    		</td>
    	</tr>
    </table>
    <script type="text/javascript">
    	loadTab(window.parent.document.getElementById('link').value);
    </script>
  </body>
</html>
