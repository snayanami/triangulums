<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>My JSP 'EndOfDayProcess.jsp' starting page</title>
    
	<script type="text/javascript" src="js/ajax.js"></script>
	<script type="text/javascript" src="js/aw.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	
	<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
	<link rel="stylesheet" href="css/common/common.css" type="text/css" />
	
	<script type="text/javascript">
		var sysDate="<f:formatDate value="${sessionScope.LOGIN_KEY.loginDate}" pattern="dd/MM/yyyy"/>";
		
		function clearScreen(id){
			$(id).innerHTML = "";              	
		}
		
		function upperCase(id){
			$(id).value = $(id).value.toUpperCase();	
		}
		
		function $(name){return document.getElementById(name);}
		
		function createBrowser(gridName,columns,cellFormat){
			var obj = new AW.UI.Grid;
			obj.setId(gridName);
			var str = new AW.Formats.String;
			var num = new AW.Formats.Number;
			obj.setCellText([]);
			obj.setCellFormat(cellFormat);
			obj.setHeaderText(columns);
			obj.setSelectorVisible(true);
			obj.setRowCount(0);
			obj.setColumnCount(columns.length);
			obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
			obj.setSelectorWidth(20);
			obj.setHeaderHeight(20);
			obj.onRowClicked;
			obj.setSelectionMode("single-row");
			obj.onSelectedRowsChanged= function(row){};
			obj.loadingCompleted= function() {
			    if (obj._maskLoading) { 
			       obj._maskLoading.style.display = 'none'; 
			    } 
			}
			
			obj.disabled = function () {
				if (!obj._maskEl) {
					var maskEl = document.createElement("div");
					document.body.appendChild(maskEl);
					maskEl.className = "aw-grid-mask";
					var gridEl = obj.element();
					maskEl.style.left = AW.getLeft(gridEl) + "px";
					maskEl.style.top = AW.getTop(gridEl) + "px";
					maskEl.style.width = gridEl.clientWidth + "px";
					maskEl.style.height = gridEl.clientHeight + "px";
					obj._maskEl = maskEl;
				}
				obj._maskEl.style.display = "block";
			};
				
			obj.enabled = function () {
				if (obj._maskEl) {
					obj._maskEl.style.display = "none";
				}
			};
			
			obj.loading = function() { 
			    if (! obj._maskLoading) {
			        var maskLoading = document.createElement('div'); 
			        document.body.appendChild(maskLoading); 
			        maskLoading.className = 'aw-grid-loading'; 
			        var gridEl = obj.element(); 
			        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
			        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
			        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
			        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
			        obj._maskLoading = maskLoading; 
			   	}
		        obj.clearCellModel(); 
			    obj.clearRowModel(); 
			    obj.clearScrollModel(); 
			    obj.clearSelectionModel(); 
			    obj.clearSortModel(); 
			    obj.refresh(); 
		    	obj._maskLoading.style.display = 'block'; 
			}
			return obj;
		}
		
		function setGridData(grid,data){
			grid.setCellText(data);
			grid.setRowCount(data.length);
			grid.refresh();
			grid.setSelectedRows([]);
		}
		
		/*Get Grid Data*/
        var dataArray=new Array();
		function getGridData(){
			grid.loading();
			var data="dispatch=getAllBranch&rand="+Math.random()*19851227;
			var ajax=new ajaxObject('eodProcess.do');
				ajax.callback=function(responseText, responseStatus, responseXML) {
             		if (responseStatus == 200) {
                  		myArray = eval("("+responseText+")");							
             			setGridData(grid,myArray);
             			grid.loadingCompleted();
						dataArray = myArray;	
       	         	}
				}
			ajax.update(data,'POST');
		}
		
		function process(){
			var branchIdArray = "";
			for(var i=0;i<grid.getRowCount();i++){
				if(grid.getCellValue(3,i)==true)
					branchIdArray=branchIdArray+grid.getCellText(4,i)+"<>";
			}
			var startDate = $('startDate').value;
			var endDate = $('endDate').value;
			
			if(!(confirm("Confirm to start the process?"))){
				return;														
			}
			//grid.disabled();
			var url='dispatch=processEOD&branchIdArray='+branchIdArray+"&startDate="+startDate+"&endDate="+endDate+'&rand='+Math.random()*19851227;
			var ajax=new ajaxObject('eodProcess.do');
			ajax.callback=function(responseText, responseStatus, responseXML) {
	            if (responseStatus == 200) {
	            	myArray = eval("("+responseText+")");							
	            	if( myArray['createSuccess']){
						//alert(myArray['createSuccess']);
						alert('Day end process successfully completed');
						//grid.enabled();
						//clearAll();
						getGridData();
					}else if( myArray['error'] ){
						alert( myArray['error'] );
	    	      	}else{
	    	        	//prepareRequestDataValidate(myArray)
	    	   		}									
	        	}	   
			}
			ajax.update(url,'POST');
		}
		
		function clearAll(){
			clearFilling();
			clearDivision();
		}
		
		function clearFilling(){
			/*$('bankCode').value = '';
			$('bankName').value = '';
			try{
				grid.setSelectedRows([]);	
			}catch(e){}*/
			for(var i=0;i<grid.getRowCount();i++){
				grid.setCellValue(false,3,i);
				grid.refresh();
			}
			
			try{
				grid.setSelectedRows([]);	
			}catch(e){}
		}
		
		function clearDivision(){
			/*$("bankCodeDiv").innerHTML = '';
			$("bankNameDiv").innerHTML = '';*/
		}
		
		function hideTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="none";
			//window.parent.document.getElementById('bodyFrame').setAttribute('height','1500');
		}
		
		function visibleTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="";
		}
		
	</script>
	
	<style>
		.browserBorder {
		    border-top: #91a7b4 1px solid;
		    border-right: #91a7b4 1px solid;
		    border-left: #91a7b4 1px solid;
		    border-bottom: #91a7b4 1px solid;
		    background-color:#eeeeee;
		}
		
		#obj {height: 340px;width:100%;}
		#obj .aw-row-selector {text-align: center}
		#obj .aw-column-0 {width: 100px;}
		#obj .aw-column-1 {width: 200px;}
		#obj .aw-column-2 {width: 100px;}
		#obj .aw-column-3 {width: 100px;text-align: center;}
		#obj .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#obj .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
		
	</style>
	
  </head>
  
  <body onload="hideTabFrame();getGridData();" onunload="visibleTabFrame()">
    <table width="100%" align="center">
    	<tr>
    		<td>
    			<table width="100%" align="center" class="browserBorder">
    				<tr>
    					<td>
    						<table width="100%" align="center" class="browserBorder">
    							<tr>
    								<td colspan="4" height="3px"></td>
    							</tr>
    							<tr>
    								<td align="right">
    									Start Date
    								</td>
    								<td>
    									<input type="text" id="startDate" readonly="readonly" size="" value="<c:out value="${processStartDate}"/>">
    								</td>
    								<td align="right">
    									End Date
    								</td>
    								<td>
    									<input type="text" id="endDate" readonly="readonly" size="" value="<c:out value="${processEndDate}"/>">
    								</td>
    							</tr>
    							<tr>
    								<td colspan="4" height="3px"></td>
    							</tr>
    						</table>
    					</td>
    				</tr>
    				<tr>
    					<td>
    						<table width="100%" align="center" class="browserBorder">
    							<tr>
    								<td>
    									<script type="text/javascript">
											var Columns = ["Branch Code","Branch Name","Branch Date","Select"];
											var str=new AW.Formats.String;
											var num=new AW.Formats.Number;
											var grid=createBrowser('obj',Columns,[str,str]);
											var check=new AW.Templates.Checkbox;
											grid.setCellTemplate(check, 3);
											document.write(grid);
											
											grid.onCellValueChanged = function(value, column, row){
												if(value==true){
					                       			
													/*if(this.getCellText(3,row)=='Inactive'){
														grid.setCellValue(false, 4, row);
														return false;
													}*/					                       			
					                       			
					                       			if((sysDate!='') && (this.getCellText(2,row)!=sysDate)){
														grid.setCellValue(false, 3, row);
														return false;
													}
													//$('secTypeId').value = grid2.getCellText(4,row);
													//check();
												}
											}
											
											/*check.setEvent("onclick", function(){
												var brnDate = grid.getCellText(2,grid.getCurrentRow());
												checkBool= !(grid.getCellValue(0,grid.getCurrentRow()));
												
												var rowArr = new Array();
												for(var i=0;i<dataArray.length;i++){
													if(dataArray[i][2]==brnDate){
														rowArr[rowArr.length] = i;
														if(i!=grid.getCurrentRow()){
															if(checkBool)
						                             			grid.setCellValue(false,4,i);
						                               		else						                                		
						                               			grid.setCellValue(true,4,i);
						                            	}
						                        	}
												}
												//rwset=rowArr;
											})*/
											
											
										</script>
    								</td>
    							</tr>
    						</table>
    					</td>
    				</tr>
    				<tr>
    					<td>
    						<table width="100%" align="center" class="browserBorder">
    							<tr>
						  			<td></td>
						  			<td align="right">
						  				<input type="button" value="Process" onclick="process()" id="Check" class="buttonNormal"/>
						  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
						  			</td>
						  		</tr>
    						</table>
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    </table>
  </body>
</html>
