<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>Parameter</title>
    
	<script type="text/javascript" src="js/ajax.js"></script>
	<script type="text/javascript" src="js/aw.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/skyCalendar.js"></script>
	
	<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
	<link rel="stylesheet" href="css/common/common.css" type="text/css" />
	<link rel="stylesheet" href="css/common/calendar-system.css" type="text/css" />
	
	<script type="text/javascript">
		function clearScreen(id){
			$(id).innerHTML = "";              	
		}
		
		function upperCase(id){
			$(id).value = $(id).value.toUpperCase();	
		}
		
		function $(name){return document.getElementById(name);}
		
		function createBrowser(gridName,columns,cellFormat){
			var obj = new AW.UI.Grid;
			obj.setId(gridName);
			var str = new AW.Formats.String;
			var num = new AW.Formats.Number;
			obj.setCellText([]);
			obj.setCellFormat(cellFormat);
			obj.setHeaderText(columns);
			obj.setSelectorVisible(true);
			obj.setRowCount(0);
			obj.setColumnCount(columns.length);
			obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
			obj.setSelectorWidth(20);
			obj.setHeaderHeight(20);
			obj.setSelectionMode("single-row");
			obj.onSelectedRowsChanged= function(row){};
			obj.loadingCompleted= function() {
			    if (obj._maskLoading) { 
			       obj._maskLoading.style.display = 'none'; 
			    } 
			}
			obj.loading = function() { 
			    if (! obj._maskLoading) { 
			        var maskLoading = document.createElement('div'); 
			        document.body.appendChild(maskLoading); 
			        maskLoading.className = 'aw-grid-loading'; 
			        var gridEl = obj.element(); 
			        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
			        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
			        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
			        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
			        obj._maskLoading = maskLoading; 
			   	}
		        obj.clearCellModel(); 
			    obj.clearRowModel(); 
			    obj.clearScrollModel(); 
			    obj.clearSelectionModel(); 
			    obj.clearSortModel(); 
			    obj.refresh(); 
		    	obj._maskLoading.style.display = 'block'; 
			}
			return obj;
		}
		
		function setGridData(grid,data){
			grid.setCellText(data);
			grid.setRowCount(data.length);
			grid.refresh();
			grid.setSelectedRows([]);
		}
	
		function create(){
			if(!checkedRecordSelected()){
				alert('Please select a record.');
				return false;
			}
			
			//*Start - Display Errors*//
			var inpComNo = $("inpComNo").value;
			if(inpComNo==1){
				if($("paraValue1").value=='' || $("paraValue1").value==null){
					$("paraValue1Div").innerHTML = 'Value is required.';
					return false;
				}
			}else if(inpComNo==2){
				if($("paraValue2").value=='' || $("paraValue2").value==null){
					$("paraValue2Div").innerHTML = 'Value is required.';
					return false;
				}
			}else{
				return true;
			}
			//*End - Display Errors*//
			
			if(!(confirm("Confirm to create a record?"))){
				return;														
			}
			var url="dispatch=create&rand="+Math.random()*19851227 + getCreateDate();
			var ajax=new ajaxObject("parameter.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	if(myData['createSuccess']){
	            		clearAll();
	            		alert(myData['createSuccess']);
	            	}else if(myData['error']){
	            		alert(myData['error']);
	            	}else{
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function getCreateDate(){
			var paramId = $("parameterId").value;
			var paramCode = $("parameterCode").value;
			var effDate = $("effDate").value;
			var inpComNo = $("inpComNo").value;
			var paramValue = "";
			if(inpComNo==1){
				paramValue = $("paraValue1").value;
			}else if(inpComNo==2){
				paramValue = $("paraValue2").value;
			}
			
			return "&paramId="+paramId+"&effDate="+effDate+"&paramValue="+paramValue;
		}
		
		/*Get Parameter Grid Data*/
        var dataArray1=new Array();
		function getParameterGridData(){
			grid1.loading();
			var data="dispatch=getAllParameter&rand="+Math.random()*19851227;
			var ajax=new ajaxObject('parameter.do');
				ajax.callback=function(responseText, responseStatus, responseXML) {
             		if (responseStatus == 200) {
                  		myArray = eval("("+responseText+")");							
             			setGridData(grid1,myArray);
             			grid1.loadingCompleted();
						dataArray1 = myArray;	
       	         	}
				}
			ajax.update(data,'POST');
		}
		
		/*Get Parameter Value Grid Data*/
        var dataArray2=new Array();
		function getParameterValueGridData(){
			var parameterId = $('parameterId').value;
			grid2.loading();
			var data="dispatch=getAllParameterValueByParameter&parameterId="+parameterId+"&rand="+Math.random()*19851227;
			var ajax=new ajaxObject('parameter.do');
				ajax.callback=function(responseText, responseStatus, responseXML) {
             		if (responseStatus == 200) {
                  		myArray = eval("("+responseText+")");							
             			setGridData(grid2,myArray);
             			grid2.loadingCompleted();
						dataArray2 = myArray;	
       	         	}
				}
			ajax.update(data,'POST');
		}
		
		function showErrorMsg(){
			var inpComNo = $("inpComNo").value;
			if(inpComNo==1){
				$("paraValue1Div").innerHTML = 'Value is required.';
				return false;
			}else if(inpComNo==2){
				$("paraValue2Div").innerHTML = 'Value is required.';
				return false;
			}else{
				return true;
			}
		}
		
		function displayErrors(myData){
			for(i = 0; i<myData.length;i++){
				if(myData[i]["effDate"]){
					$("effDateDiv").innerHTML = myData[i]["effDate"];
				}
			}
		}
		
		function clearAll(){
			clearFilling();
			clearDivision();
		}
		
		function clearFilling(){
			try{
				$("parameterId").value = '';
				$('effDate').value = '';
				$('paraValue1').value = '';
				$('paraValue2').selectedIndex = 0;
				
				grid1.setSelectedRows([]);
				setGridData(grid2,[]);	
				
				$("parameterCode").value = '';
				$("inpComNo").value = '';
			}catch(e){}
		}
		
		function clearDivision(){
			$("effDateDiv").innerHTML = '';
			$("paraValue1Div").innerHTML = '';
			$("paraValue2Div").innerHTML = '';
		}
		
		function checkedRecordSelected(){
			var parameterId = $('parameterId').value ;
    		if(parameterId ==null || parameterId ==''){
    			return false;
    		}else
    			return true;
		}
		
		function hideTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="none";
			//window.parent.document.getElementById('bodyFrame').setAttribute('height','1500');
		}
		
		function visibleTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="";
		}
		
		function getSystemDate(){
			return '<f:formatDate value="${sessionScope.LOGIN_KEY.loginDate}" pattern="dd/MM/yyyy" />';
		}
		
		function validateEffectiveDate(){
			var systemDate = getSystemDate();
			//alert(systemDate);
			var effDate = $('effDate').value;
			
			if(!checkdate($("effDate"))&& !($("effDate").value=='')){
				$('effDateDiv').innerHTML = "Effective Date is Invalid ";
				$('effDate').value='';
				return false;
			}else if(!compareDate(systemDate,effDate) && $("effDate").value!=''){
				$('effDateDiv').innerHTML = "Effective Date cannot be less than System Date";
				$('effDate').value='';
				return false;
			}else if(checkdate($('effDate')) && (compareDate(systemDate,effDate))){
				$('effDateDiv').innerHTML = "";
				return true;
			}
		}
	</script>
	
	<style>
		.browserBorder {
		    border-top: #91a7b4 1px solid;
		    border-right: #91a7b4 1px solid;
		    border-left: #91a7b4 1px solid;
		    border-bottom: #91a7b4 1px solid;
		    background-color:#eeeeee;
		}
		
		#para {height: 140px;width:100%;}
		#para .aw-row-selector {text-align: center}
		#para .aw-column-0 {width: 150px;}
		#para .aw-column-1 {width: 250px;}
		#para .aw-column-2 {width: 200px;}
		#para .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#para .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
		
		#paraVal {height: 150px;width:100%;}
		#paraVal .aw-row-selector {text-align: center}
		#paraVal .aw-column-0 {width: 200px;}
		#paraVal .aw-column-1 {width: 200px;}
		#paraVal .aw-column-2 {width: 200px;}
		#paraVal .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#paraVal .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
	</style>
	
  </head>
  
  <body onload="hideTabFrame();getParameterGridData();" onunload="visibleTabFrame()">
    <table width="100%" align="center">
    	<tr>
    		<td>
    			<table width="100%" align="center" class="browserBorder">
    				<tr>
    					<td>
    						<table width="100%" align="center">
    							<tr>
			    					<td colspan="2">
			    					<FIELDSET>
			    					<legend>Parameter</legend>
			    						<input type="hidden" id="parameterId" name="parameterId">
			    						<input type="hidden" id="parameterCode" name="parameterCode">
			    						<input type="hidden" id="inpComNo" name="inpComNo">
			    						<script type="text/javascript">
			    							var Columns = ["Parameter Code","Description","Default Value"];
											var str=new AW.Formats.String;
											var num=new AW.Formats.Number;
											var grid1=createBrowser('para',Columns,[str,str,str]);
											document.write(grid1);
											grid1.onSelectedRowsChanged = function(row){
												if(row!=undefined && row!='' && parseInt(row)>=0){
													$('parameterId').value = this.getCellText(3,row);
													$("parameterCode").value = this.getCellText(0,row);
													$("inpComNo").value = this.getCellText(4,row);
													getParameterValueGridData();
													var inpComNo = this.getCellText(4,row);
													if(inpComNo==1){
														$('paraVal1').style.display="";
														$('paraVal2').style.display="none";
														$('effDateRow').style.display="";
													}else if(inpComNo==2){
														$('paraVal1').style.display="none";
														$('paraVal2').style.display="";
														$('effDateRow').style.display="";
													}
												}
				                           	};
										</script>
									</FIELDSET>
			    					</td>
			    				</tr>
			    				<tr>
			    					<td colspan="2">
			    					<FIELDSET>
			    					<legend>Parameter Value</legend>
			    						<script type="text/javascript">
											var Columns = ["Sequence","Effective Date","Value"];
											var str=new AW.Formats.String;
											var num=new AW.Formats.Number;
											var grid2=createBrowser('paraVal',Columns,[str,str,str]);
											document.write(grid2);
											grid2.onSelectedRowsChanged = function(row){
												
				                           	};
										</script>
									</FIELDSET>
			    					</td>
			    				</tr>
			    				<tr>
			    					<td>
			    						<table align="center" width="100%" class="browserBorder">
					    					<tr>
					    						<td colspan="2" height="3px"></td>
					    					</tr>
					    					<tr style="display: none;" id="paraVal1">
						    					<td align="right" width="30%">
						    						Value
						    					</td>
						    					<td width="70%">
						    						<input type="text" id="paraValue1" name="paraValue1" size="20" onfocus="clearScreen('paraValue1Div')">
						    						<font color="red">*</font><br><div id="paraValue1Div" class="validate"></div>
						    					</td>
						    				</tr>
						    				<tr style="display: none;" id="paraVal2">
						    					<td align="right" width="30%">
						    						Value
						    					</td>
						    					<td width="70%">
						    						<select id="paraValue2" onfocus="clearScreen('paraValue2Div')">
						    							<option value="">-- Select --</option>
						    							<option value="0">January</option>
						    							<option value="1">February</option>
						    							<option value="2">March</option>
						    							<option value="3">April</option>
						    							<option value="4">May</option>
						    							<option value="5">June</option>
						    							<option value="6">July</option>
						    							<option value="7">August</option>
						    							<option value="8">September</option>
						    							<option value="9">October</option>
						    							<option value="10">November</option>
						    							<option value="11">December</option>
						    						</select>
						    						<font color="red">*</font><br><div id="paraValue2Div" class="validate"></div>
						    					</td>
						    				</tr>
						    				<tr>
					    						<td colspan="2" height="3px"></td>
					    					</tr>
						    				<tr style="display: none;" id="effDateRow">
						    					<td align="right">
						    						Effective Date
						    					</td>
						    					<td>
						    						<input type="text" name="effDate" id="effDate" size="15" maxlength="10" value=""
														onfocus="clearScreen('effDateDiv')"
														onkeypress="DateFormat(this,event);" 
														onkeyup="DateFormat(this,event);" 
														onblur="DateFormat(this,event);validateEffectiveDate();" /><input type="button" name="effDateBtn" id="effDateBtn" class="dot3buttonNormal" value="..." onclick="return showCalendar('effDate', function(){validateEffectiveDate();clearScreen('effDateDiv');});"/> 																							
														<font color="red">* </font><br /><div id="effDateDiv" class="validate"></div>
						    					</td>
						    				</tr>
						    				<tr>
					    						<td colspan="2" height="3px"></td>
					    					</tr>
					    				</table>
			    					</td>
			    				</tr>
    						</table>
    					</td>
    				</tr>
    				<tr>
    					<td>
    						<table align="center" width="100%" class="browserBorder">
		    					<tr>
						  			<td></td>
						  			<td align="right">
						  				<input type="button" value="Submit" onclick="create()" id="Save" class="buttonNormal"/>
						  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
						  			</td>
						  		</tr>
		    				</table>
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    </table>
  </body>
</html>
