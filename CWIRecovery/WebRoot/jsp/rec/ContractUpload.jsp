<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>Contract Upload</title>
    
	<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
	<link rel="stylesheet" href="css/common/calendar-system.css" type="text/css" />
	<link rel="stylesheet" href="css/common/calenderCSS.css" type="text/css" />
	<link rel="stylesheet" href="css/common/common.css" type="text/css" />
	
	<script type="text/javascript" src="js/GridBrowser.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/aw.js"></script>
	<script type="text/javascript" src="js/ajax.js"></script>
	
	<script type="text/javascript" src="js/SkyBrowser.js"></script>
	<script type="text/javascript" src="js/skyCalendar.js"></script>
	
	<script type="text/javascript">
		function hideTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="none";
		}
		
		function visibleTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="";
		}
	
		function clearScreen(id){$(id).innerHTML = "";}
		
		function $(name){return document.getElementById(name);}
		
		function createBrowser(gridName,columns,cellFormat){
			var obj = new AW.UI.Grid;
			obj.setId(gridName);
			var str = new AW.Formats.String;
			var num = new AW.Formats.Number;
			obj.setCellText([]);
			obj.setCellFormat(cellFormat);
			obj.setHeaderText(columns);
			obj.setSelectorVisible(true);
			obj.setRowCount(0);
			obj.setColumnCount(columns.length);
			obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
			obj.setSelectorWidth(20);
			obj.setHeaderHeight(20);
			obj.setSelectionMode("single-row");
			obj.onSelectedRowsChanged= function(row){};
			obj.loadingCompleted= function() {
			    if (obj._maskLoading) { 
			       obj._maskLoading.style.display = 'none'; 
			    } 
			}
			obj.loading = function() { 
			    if (! obj._maskLoading) { 
			        var maskLoading = document.createElement('div'); 
			        document.body.appendChild(maskLoading); 
			        maskLoading.className = 'aw-grid-loading'; 
			        var gridEl = obj.element(); 
			        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
			        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
			        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
			        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
			        obj._maskLoading = maskLoading; 
			   	}
		        obj.clearCellModel(); 
			    obj.clearRowModel(); 
			    obj.clearScrollModel(); 
			    obj.clearSelectionModel(); 
			    obj.clearSortModel(); 
			    obj.refresh(); 
		    	obj._maskLoading.style.display = 'block'; 
			}
			return obj;
		}
		
		function setGridData(grid,data){
			grid.setCellText(data);
			grid.setRowCount(data.length);
			grid.refresh();
			grid.setSelectedRows([]);
		}
		
		function clearAll(){
			
		}
		
	</script>
	
	<style>
		.browserBorder {
		    border-top: #91a7b4 1px solid;
		    border-right: #91a7b4 1px solid;
		    border-left: #91a7b4 1px solid;
		    border-bottom: #91a7b4 1px solid;
		    background-color:#eeeeee;
		}
		
		#grid {height: 330px;width:100%;}
		#grid .aw-row-selector {text-align: center}
		#grid .aw-column-0 {width: 100px;}
		#grid .aw-column-1 {width: 110px;text-align: right}
		#grid .aw-column-2 {width: 110px;text-align: right}
		#grid .aw-column-3 {width: 110px;text-align: right}
		#grid .aw-column-4 {width: 140px;text-align: right}
		#grid .aw-column-5 {width: 100px;text-align: right}
		#grid .aw-column-6 {width: 80px;}
		#grid .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#grid .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
	</style>

  </head>
  
  <body onload="hideTabFrame()" onunload="visibleTabFrame()">
    <table width="100%" align="center">
    	<tr>
    		<td>
    			<table width="100%" align="center" class="browserBorder">
    				<tr>
    					<td colspan="2" height="10px">Hi</td>
    				</tr>
    				
   					<tr>
    					<td colspan="2" height="10px"></td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	<tr>
    		<td>
    			<table width="100%" align="center" class="browserBorder">
    				<tr>
    					<td>
    						
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	<tr>
    		<td>
    			<table width="100%" align="center" class="browserBorder">
    				<tr>
    					<td colspan="2" height="10px"></td>
    				</tr>
    				
    				<tr>
    					<td colspan="2" height="10px"></td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	<tr>
			<td>
				<table align="center" width="100%" class="browserBorder">
					<tr>
			  			<td align="right">
			  				<input type="button" value="Black List" onclick="clearAll()" id="Check" class="buttonNormal"/>
			  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
			  			</td>
  					</tr>
				</table>
			</td>
		</tr>
    </table>
  </body>
</html>
