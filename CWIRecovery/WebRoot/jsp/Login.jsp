<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>.:: Pawning ::.</title>
    	<link rel="stylesheet" href="css/common/common.css" type="text/css" />
   
	
	
    
    <script type="text/javascript" src="js/ajax.js"></script>
    <script type="text/javascript">
    
    	var goldValueListString = "";
    	function getGoldValueListString(){
	    	var data="dispatch=getGoldValueListString&rand="+Math.random()*19851227;				
			url = "login.do";
			var mySearchRequest = new ajaxObject(url);
			mySearchRequest.callback = function(responseText, responseStatus, responseXML) {
				if (responseStatus==200) {							
					var myObject =  eval('(' + responseText + ')');			       	
					if(myObject['error']){
						alert(myObject['error']);
					}else{
						$('goldValueList').innerHTML = myObject['goldValueListString'];
					}
				}else {
		    	    // 
			    }
			}
			mySearchRequest.update(data,'POST');
	    }
    	
		function $(name){return document.getElementById(name);}
		
		function loadPopup(url,width,height){
			var theTop=(screen.height/2)-(height/2);
			var theLeft=(screen.width/2)-(width/2);
			var features='height='+height+',width='+width+',top='+theTop+',left='+theLeft+",dialog=yes,modal=yes,status=yes,scrollbars=no";
			var win=window.open('Main.jsp?url='+escape(url)+"&height="+escape(height)+"&width="+escape(width),"home",features);
		}
		
		//loadPopup('login.do?dispatch=loadHomePage&officerId='+officerId,1024,768);
		
	
		function getSysPrograms(){
			var uname = document.getElementById('uname').value;
			var pwd = cry(document.getElementById('pwd').value);
			var url="dispatch=login&uname="+uname+"&pwd="+pwd+"&rand="+Math.random()*9999;
			var ajax=new ajaxObject("login.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	            	myData=eval('(' + responseText + ')');
	            	//alert(myData);
	            	if(myData[0]=='error'){
	            		alert(myData[1]);
	            	}else if(myData['success']!=null){
	            		//$('enter').disabled = false;
	            		$('loginUserNameTD').innerHTML = myData['loginUserName'];
	            		$('officerId').value = myData['success'];
	            		$('logoutTR').style.display = "" ;
						$('loginTR').style.display = "none" ;
	            		enter();
	            		
	            		//window.open("login.do?dispatch=loadHomePage&officerId="+myData["success"]+"&rand="+Math.random()*9999,"_parent");
	            		//var data = "login.do?dispatch=loadHomePage&officerId="+myData["success"]+"&rand="+Math.random()*9999,"_parent";
	            		//loadPopup(data,800,600);
	            	}else if(myData['invalidLogin']!=null){
	            		$('loginErrorDiv').innerHTML='User Name or Password is incorrect.';
	            		return false;
	            	}else{
	            		displayErrors(myData);
	            	}
	            }	   
			}
			ajax.update(url,'POST');
		}
		
		function enter(){
			var officerId = $('officerId').value;
			if(officerId!=''){
				//window.open("login.do?dispatch=loadHomePage&officerId="+officerId+"&rand="+Math.random()*9999,"_new");
				loadPopup('login.do?dispatch=loadHomePage&officerId='+officerId,1024,768);
				return;	
			}else{
				return false;
			}
		}
		
		function displayErrors(myData){
			for(i = 0; i<myData.length;i++){
				if(myData[i]["uname"]){
					document.getElementById("unameDiv").innerHTML = myData[i]["uname"];
				}if(myData[i]["pwd"]){
					document.getElementById("passwordDiv").innerHTML = myData[i]["pwd"];
				}
			}
		}
		
		function cry(v){var str="";for(var i=0;i<v.length;i++){str+=v.charCodeAt(i++).toString(16);}for(var i=1;i<v.length;i++){str+=v.charCodeAt(i++).toString(16);}return str;}
		
		function logout(){
			var url="dispatch=logout&&rand="+Math.random()*19851227;
			var ajax=new ajaxObject("login.do");
			ajax.callback=function(responseText, responseStatus, responseXML) {
	         	if (responseStatus == 200) {
	         		myData=eval('(' + responseText + ')');
	            	if(myData["logout"]){
		            	$('logoutTR').style.display = "none" ;
						$('loginTR').style.display = "" ;
						clearData();
		            }else{
		            	alert(myData["logout"]);
		            }          
	            }
			}
			ajax.update(url,'POST');
		}
		
		function clearData(){
			$('officerId').value = '';
       		$('uname').value = '';
       		$('pwd').value = '';
		}
		
		/*function logout(id){
			if(document.getElementById('logout')){
				var logout=CreateXHR();
				var uri="login.do";
				logout.onreadystatechange = function() {
					if (logout.readyState == 4) {
						if (logout.status == 200) {
							var mes=eval("("+logout.responseText+")");
							if(mes['logout']){
								try{
									logouted();
								}catch(e){
								
								}
							}else if("error"){
								//document.getElementById('secondRow').innerHTML="<font style=\"font-size:8pt\" color=\"red\" >"+mes['error']+"</font>";
								alert(mes["error"]);
							}
						}
					}
				}
				var data="dispatch=logout";
				if(id!=undefined)
					data+="&userId="+id;
				logout.open("POST", uri, true);
				logout.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
				logout.send(data);
			}
		}*/	
	</script>
  </head>
  
  <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="getGoldValueListString()">
	<table id="Table_01" width="800"  border="0" cellpadding="0" cellspacing="0" align="center" style="border-bottom: solid 1px #333333;border-left: solid 1px #333333;border-right: solid 1px #333333">
		<tr>
			<td colspan="3">
				<img src="images/home_01.gif" width="800" height="80" alt="">
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<img src="images/home_02.gif" width="800" height="24" alt="">
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<img src="images/home_03.gif" width="800" height="70" alt="">
			</td>
		</tr>
		<tr>
			<td>
				<img src="images/home_04.gif" width="210" height="218" alt="">
			</td>
			<td background="images/home_05.gif" align="center">
				<!--<img src="images/home_05.gif" width="355" height="218" alt="">-->
				<table align="center" width="100%">
					<tr id="loginTR">
						<td>
							<table width="100%" align="center">
								<tr>
									<td align="right">
										User Name
									</td>
									<td>
										<input type="hidden" id="officerId" name="officerId"><input type="hidden" id="aaaaaaa" name="aaaaaaa">
										<input type="text" id="uname" name="uname" size="20" onfocus="$('unameDiv').innerHTML='';$('loginErrorDiv').innerHTML='';" value="system">
										<br><font color="red"><div id="unameDiv" style="position: absolute;vertical-align: top;"></div></font>
									</td>
								</tr>
								<tr>
									<td colspan="2" height="10px"></td>
								</tr>
								<tr>
									<td align="right">
										Password
									</td>
									<td>
										<input type="password" id="pwd" name="pwd" size="20" onfocus="$('passwordDiv').innerHTML='';$('loginErrorDiv').innerHTML='';" value="123456">
										<br><font color="red"><div id="passwordDiv" style="position: absolute;"></div></font>
									</td>
								</tr>
								<tr>
			       					<td>&nbsp;</td>
			                        <td><font color="red"><div id="loginErrorDiv" style="position: absolute;"></div></font></td>                        
			       				</tr>
			       				<tr>
									<td colspan="2" height="7px"></td>
								</tr>
								<tr>
									<td colspan="2" align="center">
										<input type="button" style="background-image: url(images/login.jpg); background-repeat:no-repeat; height:25px; width:90px; border:0; cursor: pointer;"  border="0" onclick="getSysPrograms()">
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr id="logoutTR" style="display: none;">
						<td>
							<table width="100%" align="center">
								<tr>
									<td id="loginUserNameTD" colspan="4" style="font-style: italic;font-weight: bold;font-family: monospace;font-size: large" align="center"></td>
								</tr>
								<tr>
									<td height="5px"></td>
								</tr>
								<tr>
									<td align="center">
										<input type="button" style="background-image: url(images/logout.jpg); background-repeat:no-repeat; height:25px; width:90px; border:0; cursor: pointer;"  border="0" onclick="logout()">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<img src="images/home_06.gif" width="235" height="218" alt="">
			</td>
		</tr>
		<tr>
			<td valign="top">
				<img src="images/home_07.gif" width="210" height="150" alt="">
			</td>
			<td valign="top">
				<img src="images/home_08.gif" width="355" height="150" alt="">
			</td>
			<td rowspan="2" valign="top">
				<img src="images/home_09.gif" width="235" height="151" alt="">
			</td>
	  	</tr>
		<tr>
			<td colspan="2" valign="top">
				<img src="images/home_10.gif" width="565" height="1" alt="">
			</td>
		</tr>
    	<tr>
    		<td colspan="3" bgcolor="#831313" height="20px" style="padding:5px; color:#e1db2f ">
    			<!--<marquee WIDTH=100% BEHAVIOR=ALTERNATE SCROLLAMOUNT=1> <font><b>24K - Rs.46 000.00&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;22K - Rs.42 000.00&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;21K - Rs.39 000.00&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;20K - Rs.38 000.00&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;19K - Rs.36 000.00&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;18K - Rs.33 000.00 </b></font></marquee>-->
    			<marquee WIDTH=100% BEHAVIOR=ALTERNATE SCROLLAMOUNT=1> <font><b>
    					<div id="goldValueList"></div>
    			</b></font></marquee>
    		</td>
    	</tr>
	</table>
  </body>
</html>
