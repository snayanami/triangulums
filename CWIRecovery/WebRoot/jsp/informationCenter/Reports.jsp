<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>.::Reports::.</title>
    
    <script type="text/javascript" src="js/ajax.js"></script>
	<script type="text/javascript" src="js/aw.js"></script>
	<script type="text/javascript" src="js/SkyBrowser.js"></script>
	<script type="text/javascript" src="js/skyCalendar.js"></script>
	<script type="text/javascript" src="js/com-all.js"></script>
	
	<link rel="stylesheet" href="css/common/system/aw.css" type="text/css" />
	<link rel="stylesheet" href="css/common/common.css" type="text/css" />
	<link rel="stylesheet" href="css/common/calendar-system.css" type="text/css" />
	<link rel="stylesheet" href="css/common/com-all.css" type="text/css" />
	
	<script type="text/javascript">
		function hideTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="none";
		}
		
		function visibleTabFrame(){
			window.parent.document.getElementById('tabFrame').style.display="";
		}
		
		function $(name){return document.getElementById(name);}
		
		function createBrowser(gridName,columns,cellFormat){
			var obj = new AW.UI.Grid;
			obj.setId(gridName);
			var str = new AW.Formats.String;
			var num = new AW.Formats.Number;
			obj.setCellText([]);
			obj.setCellFormat(cellFormat);
			obj.setHeaderText(columns);
			obj.setSelectorVisible(true);
			obj.setRowCount(0);
			obj.setColumnCount(columns.length);
			obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
			obj.setSelectorWidth(20);
			obj.setHeaderHeight(20);
			obj.setSelectionMode("single-row");
			obj.onSelectedRowsChanged= function(row){};
			obj.loadingCompleted= function() {
			    if (obj._maskLoading) { 
			       obj._maskLoading.style.display = 'none'; 
			    } 
			}
			obj.loading = function() { 
			    if (! obj._maskLoading) { 
			        var maskLoading = document.createElement('div'); 
			        document.body.appendChild(maskLoading); 
			        maskLoading.className = 'aw-grid-loading'; 
			        var gridEl = obj.element(); 
			        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
			        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
			        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
			        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
			        obj._maskLoading = maskLoading; 
			   	}
		        obj.clearCellModel(); 
			    obj.clearRowModel(); 
			    obj.clearScrollModel(); 
			    obj.clearSelectionModel(); 
			    obj.clearSortModel(); 
			    obj.refresh(); 
		    	obj._maskLoading.style.display = 'block'; 
			}
			return obj;
		}
		
		function setGridData(grid,data){
			grid.setCellText(data);
			grid.setRowCount(data.length);
			grid.refresh();
			grid.setSelectedRows([]);
		}
		
		function loadPopup(url,width,height){
			var theTop=(screen.height/2)-(height/2);
			var theLeft=(screen.width/2)-(width/2);
			var features='height='+height+',width='+width+',top='+theTop+',left='+theLeft+",dialog=yes,modal=yes,status=yes,scrollbars=no";
			var ptk=window.open('DocumentViewer.jsp?url='+escape(url)+"&height="+escape(height)+"&width="+escape(width),"reports",features);
		}
		
		var checkBranch = false;
		var checkOfficer = false;
		var checkDateRange = false;
		
		function openReport(no){
			if(no==''){
				alert('Please select a report.');
				return false;
			}
			
			if(!(confirm("Confirm to open the report?"))){
				return;														
			}
			
			checkBranch = false;
			checkOfficer = false;
			checkDateRange = false;
			
			switch(parseInt(no)){
				case 0:
				break;
				case 1:	
					checkBranch = false;				
					url = "getPendingTicketDetails";
					showBranchDialog();
				break;
				case 2:
					checkBranch = false;
					checkDateRange = false;
					url = "getPaymentDetails";
					showBranchDateRangeDialog();
				break;
				case 3:
					checkOfficer = false;
					checkBranch = false;
					checkDateRange = false;
					url = "getCollectionSummary";
					showOfficerBranchDateRangeDialog();
				break;
				case 4:
					checkOfficer = false;
					checkBranch = false;
					checkDateRange = false;
					url = "getCollectionDetails";
					showOfficerBranchDateRangeDialog();
				break;
				case 5:
					checkBranch = false;
					checkDateRange = true;
					url = "getCashFlowStatement";
					showBranchDateRangeDialog();
				break;
				case 6:
					checkBranch = false;
					checkDateRange = true;
					url = "getTransactionSummary";
					showBranchDateRangeDialog();
				break;
				case 7:
					checkBranch = false;
					checkDateRange = true;
					url = "getTransactionDetails";
					showBranchDateRangeDialog();
				break;			
				case 8:
					checkBranch = false;
					url = "getPawningAnalysisDetails";
					showBranchDialog();
				break;
				case 9:
					checkDateRange = false;
					url = "getGoldValueHistory";
					showDateRangeDialog();
				break;
				case 10:
					checkBranch = false;
					checkDateRange = false;
					url = "getPendingReminders";
					showBranchDateRangeDialog();
				break;
				case 11:
					checkBranch = false;
					checkDateRange = false;
					url = "getRemindersSentDetails";
					showBranchDateRangeDialog();
				break;		
				case 12:
					checkDateRange = false;
					url = "getBlackListedClientTicketDetails";
					showDateRangeDialog();
				break;
				case 13:
					/*checkBranch = false;
					checkDateRange = false;
					url = "getAuctionDetails";
					showBranchDateRangeDialog();*/
				break;
				case 14:
					checkBranch = false;
					checkDateRange = false;
					url = "getRedemptionDetails";
					showBranchDateRangeDialog();
				break;
				case 15:
					checkBranch = false;
					checkDateRange = false;
					url = "getExceptionalTransactionDetails";
					showBranchDateRangeDialog();
				break;
				case 16:
					checkOfficer = true;
					checkBranch = false;
					checkDateRange = true;
					url = "getUserTransactionSummary";
					showOfficerBranchDateRangeDialog();
				break;
				case 17:
					checkOfficer = true;
					checkBranch = false;
					checkDateRange = true;
					url = "getUserTransactionDetails";
					showOfficerBranchDateRangeDialog();
				break;
				case 18:
					checkBranch = false;
					url = "getPawningTicketDetails";
					showBranchDialog();
				break;			
				case 19:
					checkBranch = false;
					url = "getUserAccessRights";
					showBranchDialog();
				break;		
				case 20:
					/*checkOfficer = true;
					url = "getUserAccessRightsOfficerWise";
					showOfficerDialog();*/
				break;
				case 21:
					checkDateRange = false;
					url = "getHolidays";
					showDateRangeDialog();
				break;
				default :
					alert("Report is not available at this moment");
			}
		}
		
		/*Branch*/
		var wbrnx;
		function showBranchDialog(){
			if(!wbrnx){
				wbrnx = new Ext.Window({
	                el:'BranchXDialogBox',
	                layout:'fit',
	                width:550,
	                height:150,
	                closable:false, 
	                closeAction:'hide',
	                plain: true,
	                modal : true,
	                resizable : false,
					draggable : false,
	                items: new Ext.TabPanel({
	                    el: 'brnx-tab',
	                    autoTabs:true,
	                    activeTab:0,
	                    deferredRender:false,
	                    border:false
	                }),
							
	                buttons: [{
	                    text: 'Print',
	                    handler: function(){
	                    	var branchId = $('branchId1').value;
	                    	if(checkBranch){
	                    		if(branchId==0 || branchId==''){
	                    			alert("Branch is required.");
	                    			//$('branchDiv1').innerHTML = "Branch is required.";
	                    			return false;
	                    		}else{
	                    			//open("reports.do?dispatch="+url+"&branchId="+branchId,"hiddenFrame");
	                    			loadPopup("reports.do?dispatch="+url+"&branchId="+branchId,800,600);
	                    		}
	                    	}else{
								loadPopup("reports.do?dispatch="+url+"&branchId="+branchId,800,600);
	                    	}
	                    	wbrnx.hide();
	                   	}
		            },{
	                    text: 'Close',
	                    handler: function(){
	                   		wbrnx.hide();
	                   		clearBranchDialog();
	                    }
		        	}]
				});
			}
			wbrnx.show(this);
		}
		
		/*Date Range*/
		var wdtrngx;
		function showDateRangeDialog(){
			if(!wdtrngx){
				wdtrngx = new Ext.Window({
	                el:'DateRangeXDialogBox',
	                layout:'fit',
	                width:550,
	                height:165,
	                closable:false, 
	                closeAction:'hide',
	                plain: true,
	                modal : true,
	                resizable : false,
					draggable : false,
	                items: new Ext.TabPanel({
	                    el: 'dtrngx-tab',
	                    autoTabs:true,
	                    activeTab:0,
	                    deferredRender:false,
	                    border:false
	                }),
							
	                buttons: [{
	                    text: 'Print',
	                    handler: function(){
	                    	var dateFrom = $('dateFrom2').value;
	                    	var dateTo = $('dateTo2').value;
	                    	if(checkDateRange && (dateFrom=='' || dateTo=='')){
	                    		alert("Date Range is required.");
								return false;
							} else{
		                    	//open("reports.do?dispatch="+url+"&dateFrom="+dateFrom+"&dateTo="+dateTo,"hiddenFrame");
		                    	loadPopup("reports.do?dispatch="+url+"&dateFrom="+dateFrom+"&dateTo="+dateTo,800,600);
		                    	wdtrngx.hide();
	                    	}
	                   	}
		            },{
	                    text: 'Close',
	                    handler: function(){
	                    	$('reportNo').value='';
	                   		wdtrngx.hide();
	                   		clearDateRangeDialog();
	                    }
		        	}]
				});
			}
			wdtrngx.show(this);
		}
		
		/*Officer*/
		var woffx;
		function showOfficerDialog(){
			if(!woffx){
				woffx = new Ext.Window({
	                el:'OfficerXDialogBox',
	                layout:'fit',
	                width:550,
	                height:150,
	                closable:false, 
	                closeAction:'hide',
	                plain: true,
	                modal : true,
	                resizable : false,
					draggable : false,
	                items: new Ext.TabPanel({
	                    el: 'offx-tab',
	                    autoTabs:true,
	                    activeTab:0,
	                    deferredRender:false,
	                    border:false
	                }),
							
	                buttons: [{
	                    text: 'Print',
	                    handler: function(){
	                    	var officerId = $('officerId3').value;
	                    	if(checkOfficer){
	                    		if(officerId==0 || officerId==''){
	                    			alert("Officer is required.");
	                    			return false;
	                    		}else{
	                    			//open("reports.do?dispatch="+url+"&officerId="+officerId,"hiddenFrame");
	                    			loadPopup("reports.do?dispatch="+url+"&officerId="+officerId,800,600);
	                    		}
	                    	}else{
								loadPopup("reports.do?dispatch="+url+"&officerId="+officerId,800,600);
	                    	}
	                    	woffx.hide();
	                   	}
		            },{
	                    text: 'Close',
	                    handler: function(){
	                   		woffx.hide();
	                   		clearOfficerDialog();
	                    }
		        	}]
				});
			}
			woffx.show(this);
		}
		
		/*Branch & Date Range*/
		var wbrndtrngx;
		function showBranchDateRangeDialog(){
			if(!wbrndtrngx){
				wbrndtrngx = new Ext.Window({
	                el:'BranchDateRangeXDialogBox',
	                layout:'fit',
	                width:550,
	                height:195,
	                closable:false, 
	                closeAction:'hide',
	                plain: true,
	                modal : true,
	                resizable : false,
					draggable : false,
	                items: new Ext.TabPanel({
	                    el: 'brndtrngx-tab',
	                    autoTabs:true,
	                    activeTab:0,
	                    deferredRender:false,
	                    border:false
	                }),
							
	                buttons: [{
	                    text: 'Print',
	                    handler: function(){
	                    	var branchId = $('branchId4').value;
	                    	var dateFrom = $('dateFrom4').value;
	                    	var dateTo = $('dateTo4').value;
	                    	if(checkBranch && (branchId==0 || branchId=='')){
	                    		alert("Branch is required.");
								return false;
	                    	}else if(checkDateRange && (dateFrom=='' || dateTo=='')){
	                    		alert("Date Range is required.");
								return false;
	                    	}else{                    		
								//open("reports.do?dispatch="+url+"&branchId="+branchId+"&dateFrom="+dateFrom+"&dateTo="+dateTo,"hiddenFrame");
								loadPopup("reports.do?dispatch="+url+"&branchId="+branchId+"&dateFrom="+dateFrom+"&dateTo="+dateTo,800,600);
	                    		wbrndtrngx.hide();
	                    	}
	                   	}
		            },{
	                    text: 'Close',
	                    handler: function(){
	                   		wbrndtrngx.hide();
	                   		clearBranchDateRangeDialog();
	                    }
		        	}]
				});
			}
			wbrndtrngx.show(this);
		}
		
		/*Officer & Date Range*/
		var woffdtrngx;
		function showOfficerDateRangeDialog(){
			if(!woffdtrngx){
				woffdtrngx = new Ext.Window({
	                el:'OfficerDateRangeXDialogBox',
	                layout:'fit',
	                width:550,
	                height:195,
	                closable:false, 
	                closeAction:'hide',
	                plain: true,
	                modal : true,
	                resizable : false,
					draggable : false,
	                items: new Ext.TabPanel({
	                    el: 'offdtrngx-tab',
	                    autoTabs:true,
	                    activeTab:0,
	                    deferredRender:false,
	                    border:false
	                }),
							
	                buttons: [{
	                    text: 'Print',
	                    handler: function(){
	                    	var officerId = $('officerId5').value;
	                    	var dateFrom = $('dateFrom5').value;
	                    	var dateTo = $('dateTo5').value;
	                    	
	                    	if(checkOfficer && (officerId==0 || officerId=='')){
	                    		alert("Officer is required.");
								return false;
	                    	}else if(checkDateRange && (dateFrom=='' || dateTo=='')){
	                    		alert("Date Range is required.");
								return false;
							}else{
								//open("reports.do?dispatch="+url+"&officerId="+officerId+"&dateFrom="+dateFrom+"&dateTo="+dateTo,"hiddenFrame");
								loadPopup("reports.do?dispatch="+url+"&officerId="+officerId+"&dateFrom="+dateFrom+"&dateTo="+dateTo,800,600);
		                    	woffdtrngx.hide();
		                    }
	                   	}
		            },{
	                    text: 'Close',
	                    handler: function(){
	                   		woffdtrngx.hide();
	                   		clearOfficerDateRangeDialog();
	                    }
		        	}]
				});
			}
			woffdtrngx.show(this);
		}
		
		
		/*Officer & Branch*/
		var woffbrnx;
		function showOfficerBranchDialog(){
			if(!woffbrnx){
				woffbrnx = new Ext.Window({
	                el:'OfficerBranchXDialogBox',
	                layout:'fit',
	                width:550,
	                height:175,
	                closable:false, 
	                closeAction:'hide',
	                plain: true,
	                modal : true,
	                resizable : false,
					draggable : false,
	                items: new Ext.TabPanel({
	                    el: 'offbrnx-tab',
	                    autoTabs:true,
	                    activeTab:0,
	                    deferredRender:false,
	                    border:false
	                }),
							
	                buttons: [{
	                    text: 'Print',
	                    handler: function(){
	                    	var branchId = $('branchId6').value;
	                    	var officerId = $('officerId6').value;
	                    	
	                    	if(checkOfficer && (officerId==0 || officerId=='')){
	                    		alert("Officer is required.");
								return false;
	                    	}else if(checkBranch && (branchId=='' || branchId=='')){
	                    		alert("Branch is required.");
								return false;
							}else{
								//open("reports.do?dispatch="+url+"&branchId="+branchId+"&officerId="+officerId,"hiddenFrame");
								loadPopup("reports.do?dispatch="+url+"&branchId="+branchId+"&officerId="+officerId,800,600);
		                    	woffbrnx.hide();
		                    }
	                   	}
		            },{
	                    text: 'Close',
	                    handler: function(){
	                   		woffbrnx.hide();
	                   		clearOfficerBranchDialog();
	                    }
		        	}]
				});
			}
			woffbrnx.show(this);
		}
		
		
		/*Officer & Branch & Date Range*/
		var woffbrndtrngx;
		function showOfficerBranchDateRangeDialog(){
			if(!woffbrndtrngx){
				woffbrndtrngx = new Ext.Window({
	                el:'OfficerBranchDateRangeXDialogBox',
	                layout:'fit',
	                width:560,
	                height:225,
	                closable:false, 
	                closeAction:'hide',
	                plain: true,
	                modal : true,
	                resizable : false,
					draggable : false,
	                items: new Ext.TabPanel({
	                    el: 'offbrndtrngx-tab',
	                    autoTabs:true,
	                    activeTab:0,
	                    deferredRender:false,
	                    border:false
	                }),
							
	                buttons: [{
	                    text: 'Print',
	                    handler: function(){
	                    	var branchId = $('branchId7').value;
	                    	var officerId = $('officerId7').value;
	                    	var dateFrom = $('dateFrom7').value;
	                    	var dateTo = $('dateTo7').value;
	                    	
	                    	if(checkOfficer && (officerId==0 || officerId=='')){
	                    		alert("Officer is required.");
								return false;
	                    	}else if(checkBranch && (branchId=='' || branchId=='')){
	                    		alert("Branch is required.");
								return false;
							}else if(checkDateRange && (dateFrom=='' || dateTo=='')){
	                    		alert("Date Range is required.");
								return false;
							}else{
								//open("reports.do?dispatch="+url+"&branchId="+branchId+"&officerId="+officerId+"&dateFrom="+dateFrom+"&dateTo="+dateTo,"hiddenFrame");
								loadPopup("reports.do?dispatch="+url+"&branchId="+branchId+"&officerId="+officerId+"&dateFrom="+dateFrom+"&dateTo="+dateTo,800,600);
		                    	woffbrndtrngx.hide();
		                    }
	                   	}
		            },{
	                    text: 'Close',
	                    handler: function(){
	                   		woffbrndtrngx.hide();
	                   		clearOfficerBranchDateRangeDialog();
	                    }
		        	}]
				});
			}
			woffbrndtrngx.show(this);
		}
		
		function clearAll(){
			$("reportNo").value = '';
			grid.setSelectedRows([]);
		}
		
		function clearBranchDialog(){
			$('branchDiv1').innerHTML ='';
			$('branchId1').value='';
			$('branchCode1').value='';
			$('branchName1').value='';
		}
		
		function clearDateRangeDialog(){
			$('dateFrom2').value='';
			$('dateTo2').value='';
			$('dateFromDiv2').innerHTML='';
			$('dateToDiv2').innerHTML='';
		}
		
		function clearOfficerDialog(){
			$('officerDiv3').innerHTML ='';
			$('officerId3').value='';
			$('userName3').value='';
			$('description3').value='';
		}
		
		function clearBranchDateRangeDialog(){
			$('dateFrom4').value='';
			$('dateTo4').value='';
			$('dateFromDiv4').innerHTML='';
			$('dateToDiv4').innerHTML='';
			$('branchDiv4').innerHTML='';
			$('branchId4').value='';
			$('branchCode4').value='';
			$('branchName4').value='';
		}
		
		function clearOfficerDateRangeDialog(){
			$('dateFrom5').value='';
			$('dateTo5').value='';
			$('dateFromDiv5').innerHTML='';
			$('dateToDiv5').innerHTML='';
			$('officerDiv5').innerHTML='';
			$('officerId5').value='';
			$('userName5').value='';
			$('description5').value='';
		}
		
		function clearOfficerBranchDialog(){
			$('branchDiv6').innerHTML ='';
			$('branchId6').value='';
			$('branchCode6').value='';
			$('branchName6').value='';
			$('officerDiv6').innerHTML ='';
			$('officerId6').value='';
			$('userName6').value='';
			$('description6').value='';
		}
		
		function clearOfficerBranchDateRangeDialog(){
			$('dateFrom7').value='';
			$('dateTo7').value='';
			$('dateFromDiv7').innerHTML='';
			$('dateToDiv7').innerHTML='';
			$('officerDiv7').innerHTML='';
			$('officerId7').value='';
			$('userName7').value='';
			$('description7').value='';
			$('branchDiv7').innerHTML ='';
			$('branchId7').value='';
			$('branchCode7').value='';
			$('branchName7').value='';
		}
		
	</script>
	
	<style>
		.browserBorder {
		    border-top: #91a7b4 1px solid;
		    border-right: #91a7b4 1px solid;
		    border-left: #91a7b4 1px solid;
		    border-bottom: #91a7b4 1px solid;
		    background-color:#eeeeee;
		}
		
		#reportObj {height: 450px;width:100%;}
		#reportObj .aw-row-selector {text-align: center}
		#reportObj .aw-column-0 {width: 500px;}
		#reportObj .aw-column-1 {width: 180px;}
		#reportObj .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#reportObj .aw-grid-row {border-bottom: 1px solid threedlightshadow;}
	</style>
    
  </head>
  
  <body onload="hideTabFrame();" onunload="visibleTabFrame()"> 
    <table width="100%" align="center">
    	<tr>
    		<td>
    			<table width="100%" align="center" class="browserBorder">
    				<tr>
    					<td>
    						<input type="hidden" id="reportNo">
    						<script>
    							var myColumns = ["Report Name","Criteria",""]
    							var str=new AW.Formats.String;
    							var grid=createBrowser('reportObj',myColumns,[str,str]);
    							grid.setColumnIndices([0,1])
    							var data = [
    										["Management Reports"],
    										["Pending Tickets for Approval","Branch",1],
    										["Payment Report","Branch & Date Range",2],
    										["Collection Summary","Officer & Branch & Date Range",3],
    										["Collection Details","Officer & Branch & Date Range",4],
    										["Cash Flow Statement","Branch & Date Range",5],
    										["Transaction Summary","Branch & Date Range",6],
    										["Transaction Report","Branch & Date Range",7],
    										["Pawning Analysis Report","Branch",8],
    										["Gold Value History","Date Range",9],
    										["Pending Reminders / Termination Letters","Branch & Date Range",10],
    										["Reminders / Termination Letters Sent Details","Branch & Date Range",11],
    										["Black Listed Client / Ticket Details","Date Range",12],
    										["Redemption Detail Report","Branch & Date Range",14],
    										["Exceptional Transactions Report","Branch & Date Range",15],
    										
    										["Officer Reports"],
    										["User Transaction Summary","Officer & Branch & Date Range",16],
    										["User Transaction Details Report","Officer & Branch & Date Range",17],
    										["Pawning Ticket Details Report","Branch",18],
    										
    										["Admin Reports"],
    										["User Access Rights","Branch",19],    										
    										["Defined Holidays Report","Date Range",21]
    										
    									   ];
    							document.write(grid);
    							grid.onSelectedRowsChanged = function(row){
                  					$("reportNo").value=this.getCellText(2,row);
                          		};
                          		
                          		grid.onRowMouseDown = function(event, index){ 
									var value = this.getCellValue(2, index);										 										
									return (value == '' )? ture : "";
								};
                          		
                          		grid.defineRowProperty("background", function(row){										
									var value = this.getCellValue(2, row);										 										
									return (value == '' )? "#ddd" : false;
								});	
								
								grid.getRowTemplate().setStyle("background", function(){										
									return this.getRowProperty("background");
								});
								
								grid.defineRowProperty("font-weight", function(row){										
									var value = this.getCellValue(2, row);										 										
									return (value == '' )? "bold" : "normal";
								});								
								
								grid.getRowTemplate().setStyle("font-weight", function(){										
									return this.getRowProperty("font-weight");
								});
								
                          		grid.onRowDoubleClicked = function(event,row){
		                        	var no = this.getCellText(2,row);
		                        	if(no!=''){
										openReport($("reportNo").value);		                        		
		                        	}else{
		                        		return false;
		                        	}
		                        }
                          		setGridData(grid,data)
                          		//["Auction Details","Branch & Date Range",13],
    						</script>
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	<tr>
			<td>
				<table width="100%" align="center" class="browserBorder">
					<tr>
  						<td></td>
			  			<td align="right">
			  				<input type="button" value="Open" onclick="openReport($('reportNo').value);" id="Print" class="buttonNormal"/>
			  				<input type="button" value="Clear" onclick="clearAll()" id="Clear" class="buttonNormal"/>
			  			</td>
  					</tr>
				</table>
			</td>
		</tr>
		<!-- Criteria Divisions -->
		<tr>
			<td>
				<!-- Branch criteria -->
				<div id="BranchXDialogBox" class="x-hidden">
					<div class="x-window-header">
						Select Branch
					</div>
					<div id="brnx-tab">
						<div class="x-tab" title="Criteria">
							<table width="100%">
								<tr>
									<td colspan="2" height="7px"></td>
								</tr>
								<tr>
									<td align="right">
					    				<bean:message bundle="lable" key="lable.branch" />
					    			</td>
					    			<td>
		  								<input type="hidden" id="branchId1" name="branchId1" value=""/>
		  								<input type="text" name="branchCode1" id="branchCode1" maxlength="3" size="5"
											onfocus="$('branchDiv1').innerHTML=''" onKeyUp="upperCase('branchCode1')"
											onblur="upperCase('branchCode1')" readonly="readonly"/>
											<input type="button" name="btnBranch1" id="btnBranch1" value="..." class="dot3buttonNormal"
											onclick="showSkyBrowser(4,'branchId1','branchCode1',0,'branchName1',1,0,0,'','');$('branchDiv1').innerHTML='';"/>
		  								<input name="branchName1" type="text" id="branchName1" maxlength="60" size="50" readonly="readonly" tabindex="-1"/>
		  								<br><div id="branchDiv1" class="validate"></div>
		  							</td>
									<td><input type="button" name="clear" id="" value="Clear" onclick="$('branchDiv1').innerHTML ='';$('branchId1').value='';$('branchCode1').value='';$('branchName1').value=''" class="" /></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<!-- Date Range Criteria -->
				<div id="DateRangeXDialogBox" class="x-hidden">
					<div class="x-window-header">
						Select Date Range
					</div>
					<div id="dtrngx-tab">
						<div class="x-tab" title="Criteria">
							<table width="100%">
								<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
								<tr>
			    					<td align="right" width="15%">
			    						From
			    					</td>
			    					<td width="48%">
										<input type="text" name="dateFrom2" id="dateFrom2" size="12" maxlength="10" value=""
											onfocus="clearScreen('dateFromDiv2')"
											onkeypress="" 
											onkeyup="" 
											onblur="" /><input type="button" name="dateFromBtn2" id="dateFromBtn2" class="dot3buttonNormal" value="..." onclick="return showCalendar('dateFrom2', function(){});"/> 																							
											<br /><div id="dateFromDiv2" class="validate"></div>
									</td>
									<td align="right" width="10%">
			    						To
			    					</td>
			    					<td width="27%">
										<input type="text" name="dateTo2" id="dateTo2" size="12" maxlength="10" value=""
											onfocus="clearScreen('dateToDiv2')"
											onkeypress="" 
											onkeyup="" 
											onblur="" /><input type="button" name="dateToBtn2" id="dateToBtn2" class="dot3buttonNormal" value="..." onclick="return showCalendar('dateTo2', function(){});"/> 																							
											<br /><div id="dateToDiv2" class="validate"></div>
									</td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" align="right">
			    						<input type="button" value="Clear" onclick="$('dateFrom2').value='';$('dateTo2').value='';$('dateFromDiv2').innerHTML='';$('dateToDiv2').innerHTML='';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    					</td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
							</table>
						</div>
					</div>
				</div>
				<!-- Officer Criteria -->
				<div id="OfficerXDialogBox" class="x-hidden">
					<div class="x-window-header">
						Select Officer
					</div>
					<div id="offx-tab">
						<div class="x-tab" title="Criteria">
							<table width="100%">
								<tr>
									<td colspan="2" height="7px"></td>
								</tr>
								<tr>
									<td align="right">
					    				<bean:message bundle="lable" key="lable.officer" />
					    			</td>
					    			<td>
		  								<input type="hidden" id="officerId3" name="officerId3" value=""/>
		  								<input type="text" name="userName3" id="userName3" size="15"
											onfocus="$('officerDiv3').innerHTML=''" onKeyUp=""
											onblur="" readonly="readonly"/>
											<input type="button" name="btnOfficer3" id="btnOfficer3" value="..." class="dot3buttonNormal"
											onclick="showSkyBrowser(5,'officerId3','userName3',0,'description3',1,0,0,'','','');$('officerDiv3').innerHTML='';"/>
		  								<input name="description3" type="hidden" id="description3" readonly="readonly" tabindex="-1"/>
		  								<br><div id="officerDiv3" class="validate"></div>
		  							</td>
									<td align="right"><input type="button" name="clear" id="" value="Clear" onclick="$('officerDiv3').innerHTML ='';$('officerId3').value='';$('userName3').value='';$('description3').value=''" class="" />&nbsp;&nbsp;</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<!-- Branch & Date Range Criteria -->
				<div id="BranchDateRangeXDialogBox" class="x-hidden">
					<div class="x-window-header">
						Select Branch / Date Range
					</div>
					<div id="brndtrngx-tab">
						<div class="x-tab" title="Criteria">
							<table width="100%">
								<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
			    				<tr>
			    					<td align="right">
					    				<bean:message bundle="lable" key="lable.branch" />
					    			</td>
					    			<td colspan="3">
		  								<input type="hidden" id="branchId4" name="branchId4" value=""/>
		  								<input type="text" name="branchCode4" id="branchCode4" maxlength="3" size="5"
											onfocus="$('branchDiv4').innerHTML=''" onKeyUp="upperCase('branchCode4')"
											onblur="upperCase('branchCode4')" readonly="readonly"/>
											<input type="button" name="btnBranch4" id="btnBranch4" value="..." class="dot3buttonNormal"
											onclick="showSkyBrowser(4,'branchId4','branchCode4',0,'branchName4',1,0,0,'','');$('branchDiv4').innerHTML='';"/>
		  								<input name="branchName4" type="text" id="branchName4" maxlength="60" size="54" readonly="readonly" tabindex="-1"/>
		  								<br><div id="branchDiv4" class="validate"></div>
		  							</td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
								<tr>
			    					<td align="right" width="15%">
			    						From
			    					</td>
			    					<td width="48%">
										<input type="text" name="dateFrom4" id="dateFrom4" size="12" maxlength="10" value=""
											onfocus="clearScreen('dateFromDiv4')"
											onkeypress="" 
											onkeyup="" 
											onblur="" /><input type="button" name="dateFromBtn4" id="dateFromBtn4" class="dot3buttonNormal" value="..." onclick="return showCalendar('dateFrom4', function(){});"/> 																							
											<br /><div id="dateFromDiv4" class="validate"></div>
									</td>
									<td align="right" width="10%">
			    						To
			    					</td>
			    					<td width="27%">
										<input type="text" name="dateTo4" id="dateTo4" size="12" maxlength="10" value=""
											onfocus="clearScreen('dateToDiv4')"
											onkeypress="" 
											onkeyup="" 
											onblur="" /><input type="button" name="dateToBtn4" id="dateToBtn4" class="dot3buttonNormal" value="..." onclick="return showCalendar('dateTo4', function(){});"/> 																							
											<br /><div id="dateToDiv4" class="validate"></div>
									</td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" align="right">
			    						<input type="button" value="Clear" onclick="$('dateFrom4').value='';$('dateTo4').value='';$('dateFromDiv4').innerHTML='';$('dateToDiv4').innerHTML='';$('branchDiv4').innerHTML='';$('branchId4').value='';$('branchCode4').value='';$('branchName4').value='';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    					</td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
							</table>
						</div>
					</div>
				</div>
				<!-- Officer & Date Range Criteria -->
				<div id="OfficerDateRangeXDialogBox" class="x-hidden">
					<div class="x-window-header">
						Select Officer / Date Range
					</div>
					<div id="offdtrngx-tab">
						<div class="x-tab" title="Criteria">
							<table width="100%">
								<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
			    				<tr>
			    					<td align="right">
					    				<bean:message bundle="lable" key="lable.officer" />
					    			</td>
					    			<td colspan="3">
		  								<input type="hidden" id="officerId5" name="officerId5" value=""/>
		  								<input type="text" name="userName5" id="userName5" size="15"
											onfocus="$('officerDiv5').innerHTML=''" onKeyUp=""
											onblur="" readonly="readonly"/>
											<input type="button" name="btnOfficer5" id="btnOfficer5" value="..." class="dot3buttonNormal"
											onclick="showSkyBrowser(5,'officerId5','userName5',0,'description5',1,0,0,'','','');$('officerDiv5').innerHTML='';"/>
		  								<input name="description5" type="hidden" id="description5" readonly="readonly" tabindex="-1"/>
		  								<br><div id="officerDiv5" class="validate"></div>
		  							</td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
								<tr>
			    					<td align="right" width="15%">
			    						From
			    					</td>
			    					<td width="48%">
										<input type="text" name="dateFrom5" id="dateFrom5" size="12" maxlength="10" value=""
											onfocus="clearScreen('dateFromDiv5')"
											onkeypress="" 
											onkeyup="" 
											onblur="" /><input type="button" name="dateFromBtn5" id="dateFromBtn5" class="dot3buttonNormal" value="..." onclick="return showCalendar('dateFrom5', function(){});"/> 																							
											<br /><div id="dateFromDiv5" class="validate"></div>
									</td>
									<td align="right" width="10%">
			    						To
			    					</td>
			    					<td width="27%">
										<input type="text" name="dateTo5" id="dateTo5" size="12" maxlength="10" value=""
											onfocus="clearScreen('dateToDiv5')"
											onkeypress="" 
											onkeyup="" 
											onblur="" /><input type="button" name="dateToBtn5" id="dateToBtn5" class="dot3buttonNormal" value="..." onclick="return showCalendar('dateTo5', function(){});"/> 																							
											<br /><div id="dateToDiv5" class="validate"></div>
									</td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" align="right">
			    						<input type="button" value="Clear" onclick="$('dateFrom5').value='';$('dateTo5').value='';$('dateFromDiv5').innerHTML='';$('dateToDiv5').innerHTML='';$('officerDiv5').innerHTML='';$('officerId5').value='';$('userName5').value='';$('description5').value='';">&nbsp;&nbsp;&nbsp;&nbsp;
			    					</td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
							</table>
						</div>
					</div>
				</div>
				<!-- Officer & Branch Criteria -->
				<div id="OfficerBranchXDialogBox" class="x-hidden">
					<div class="x-window-header">
						Select Officer / Branch
					</div>
					<div id="offbrnx-tab">
						<div class="x-tab" title="Criteria">
							<table width="100%">
								<tr>
									<td colspan="2" height="7px"></td>
								</tr>
								<tr>
									<td align="right">
					    				<bean:message bundle="lable" key="lable.officer" />
					    			</td>
					    			<td colspan="2">
		  								<input type="hidden" id="officerId6" name="officerId6" value=""/>
		  								<input type="text" name="userName6" id="userName6" size="15"
											onfocus="$('officerDiv6').innerHTML=''" onKeyUp=""
											onblur="" readonly="readonly"/>
											<input type="button" name="btnOfficer6" id="btnOfficer6" value="..." class="dot3buttonNormal"
											onclick="showSkyBrowser(5,'officerId6','userName6',0,'description6',1,0,0,'','','');$('officerDiv6').innerHTML='';"/>
		  								<input name="description6" type="hidden" id="description6" readonly="readonly" tabindex="-1"/>
		  								<br><div id="officerDiv6" class="validate"></div>
		  							</td>
								</tr>
								<tr>
									<td colspan="2" height="3px"></td>
								</tr>
								<tr>
									<td align="right">
					    				<bean:message bundle="lable" key="lable.branch" />
					    			</td>
					    			<td>
		  								<input type="hidden" id="branchId6" name="branchId6" value=""/>
		  								<input type="text" name="branchCode6" id="branchCode6" maxlength="3" size="5"
											onfocus="$('branchDiv6').innerHTML=''" onKeyUp="upperCase('branchCode6')"
											onblur="upperCase('branchCode6')" readonly="readonly"/>
											<input type="button" name="btnBranch6" id="btnBranch6" value="..." class="dot3buttonNormal"
											onclick="showSkyBrowser(4,'branchId6','branchCode6',0,'branchName6',1,0,0,'','');$('branchDiv6').innerHTML='';"/>
		  								<input name="branchName6" type="text" id="branchName6" maxlength="60" size="50" readonly="readonly" tabindex="-1"/>
		  								<br><div id="branchDiv6" class="validate"></div>
		  							</td>
									<td><input type="button" name="clear" id="" value="Clear" onclick="$('branchDiv6').innerHTML ='';$('branchId6').value='';$('branchCode6').value='';$('branchName6').value='';$('officerDiv6').innerHTML ='';$('officerId6').value='';$('userName6').value='';$('description6').value='';" class="" /></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<!-- Officer & Branch & Date Range Criteria--> 
				<div id="OfficerBranchDateRangeXDialogBox" class="x-hidden">
					<div class="x-window-header">
						Select Officer / Branch / Date Range
					</div>
					<div id="offbrndtrngx-tab">
						<div class="x-tab" title="Criteria">
							<table width="100%">
								<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
			    				<tr>
			    					<td align="right">
					    				<bean:message bundle="lable" key="lable.officer" />
					    			</td>
					    			<td colspan="3">
		  								<input type="hidden" id="officerId7" name="officerId7" value=""/>
		  								<input type="text" name="userName7" id="userName7" size="15"
											onfocus="$('officerDiv7').innerHTML=''" onKeyUp=""
											onblur="" readonly="readonly"/>
											<input type="button" name="btnOfficer7" id="btnOfficer7" value="..." class="dot3buttonNormal"
											onclick="showSkyBrowser(5,'officerId7','userName7',0,'description7',1,0,0,'','','');$('officerDiv7').innerHTML='';"/>
		  								<input name="description7" type="hidden" id="description7" readonly="readonly" tabindex="-1"/>
		  								<br><div id="officerDiv7" class="validate"></div>
		  							</td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
			    				<tr>
									<td align="right">
					    				<bean:message bundle="lable" key="lable.branch" />
					    			</td>
					    			<td colspan="3">
		  								<input type="hidden" id="branchId7" name="branchId7" value=""/>
		  								<input type="text" name="branchCode7" id="branchCode7" maxlength="3" size="5"
											onfocus="$('branchDiv7').innerHTML=''" onKeyUp="upperCase('branchCode7')"
											onblur="upperCase('branchCode7')" readonly="readonly"/>
											<input type="button" name="btnBranch7" id="btnBranch7" value="..." class="dot3buttonNormal"
											onclick="showSkyBrowser(4,'branchId7','branchCode7',0,'branchName7',1,0,0,'','');$('branchDiv7').innerHTML='';"/>
		  								<input name="branchName7" type="text" id="branchName7" maxlength="60" size="55" readonly="readonly" tabindex="-1"/>
		  								<br><div id="branchDiv7" class="validate"></div>
		  							</td>
								</tr>
								<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
								<tr>
			    					<td align="right" width="15%">
			    						From
			    					</td>
			    					<td width="48%">
										<input type="text" name="dateFrom7" id="dateFrom7" size="12" maxlength="10" value=""
											onfocus="clearScreen('dateFromDiv7')"
											onkeypress="" 
											onkeyup="" 
											onblur="" /><input type="button" name="dateFromBtn7" id="dateFromBtn7" class="dot3buttonNormal" value="..." onclick="return showCalendar('dateFrom7', function(){});"/> 																							
											<br /><div id="dateFromDiv7" class="validate"></div>
									</td>
									<td align="right" width="10%">
			    						To
			    					</td>
			    					<td width="27%">
										<input type="text" name="dateTo7" id="dateTo7" size="12" maxlength="10" value=""
											onfocus="clearScreen('dateToDiv7')"
											onkeypress="" 
											onkeyup="" 
											onblur="" /><input type="button" name="dateToBtn7" id="dateToBtn7" class="dot3buttonNormal" value="..." onclick="return showCalendar('dateTo7', function(){});"/> 																							
											<br /><div id="dateToDiv7" class="validate"></div>
									</td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" align="right">
			    						<input type="button" value="Clear" onclick="$('dateFrom7').value='';$('dateTo7').value='';$('dateFromDiv7').innerHTML='';$('dateToDiv7').innerHTML='';$('officerDiv7').innerHTML='';$('officerId7').value='';$('userName7').value='';$('description7').value='';$('branchDiv7').innerHTML ='';$('branchId7').value='';$('branchCode7').value='';$('branchName7').value='';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    					</td>
			    				</tr>
			    				<tr>
			    					<td colspan="4" height="3px"></td>
			    				</tr>
							</table>
						</div>
					</div>
				</div>
			</td>
		</tr>
    	
    </table>
    <iframe width="0" height="0" id="hiddenFrame" name="hiddenFrame"
		style="visibility: hidden;">
	</iframe>
  </body>
</html>
