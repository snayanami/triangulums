
/* $Id: zpdate-en.js,v 1.1.2.2 2009/09/18 10:50:43 thushanka Exp $ */

/*
 * Populate dictionary.
 */
zapatecDict.set({
	date: {
		day: {
			sun: "Sunday",
			mon: "Monday",
			tue: "Tuesday",
			wed: "Wednesday",
			thu: "Thursday",
			fri: "Friday",
			sat: "Saturday"
		},
		shortDay: {
			sun: "Sun",
			mon: "Mon",
			tue: "Tue",
			wed: "Wed",
			thu: "Thu",
			fri: "Fri",
			sat: "Sat"
		},
		month: {
			jan: "January",
			feb: "February",
			mar: "March",
			apr: "April",
			may: "May",
			jun: "June",
			jul: "July",
			aug: "August",
			sep: "September",
			oct: "October",
			nov: "November",
			dec: "December"
		},
		shortMonth: {
			jan: "Jan",
			feb: "Feb",
			mar: "Mar",
			apr: "Apr",
			may: "May",
			jun: "Jun",
			jul: "Jul",
			aug: "Aug",
			sep: "Sep",
			oct: "Oct",
			nov: "Nov",
			dec: "Dec"
		},
		misc: {
			am: 'am',
			AM: 'AM',
			pm: 'pm',
			PM: 'PM'
		}
	}
});

// Translate constants if this file was loaded after zpdate.js
if (typeof zapatecDate == 'function') {
	zapatecDate.translate();
}
