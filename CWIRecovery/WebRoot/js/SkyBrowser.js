function showSkyBrowser(tableId,IdElementId,CodeElementId,CodeFieldNo,DescElementId,DescFieldNo,checkCompany,checkRecordStatus,additionalParams,additionalParamValues,Function,additionalOparators){
	var url="CommonBrowserAction.do?dispatch=createBrowser&tableId="+tableId+"&IdElementId="+IdElementId+"&CodeElementId="+CodeElementId+"&CodeFieldNo="+CodeFieldNo+"&DescElementId="+DescElementId;
	url+="&DescFieldNo="+DescFieldNo+"&checkCompany="+checkCompany+"&checkRecordStatus="+checkRecordStatus+"&additionalParams="+additionalParams+"&additionalOparators="+additionalOparators+"&additionalParamValues="+additionalParamValues+"&Function="+Function;
	
	theHeight=325;
	theWidth=595;
	var theTop=(screen.height/2)-(theHeight/2);
	var theLeft=(screen.width/2)-(theWidth/2);
	var features='height='+theHeight+',width='+theWidth+',top='+theTop+',left='+theLeft+",dialog=yes,modal=yes,status=yes,scrollbars=no";
	window.open(url,"commonBrowser",features);
}

function skyBrowser(tableId,IdElementId,CodeElementId,CodeFieldNo,DescElementId,DescFieldNo,checkCompany,checkRecordStatus,additionalParams,additionalOparators,additionalParamValues,Function){
	showSkyBrowser(tableId,IdElementId,CodeElementId,CodeFieldNo,DescElementId,DescFieldNo,checkCompany,checkRecordStatus,additionalParams,additionalParamValues,Function,additionalOparators);
}

function createBrowser(myColumns){
	var obj = new AW.UI.Grid;
	obj.setId("myGrid");
	var str = new AW.Formats.String;
	var num = new AW.Formats.Number;
	obj.setCellText([]);
	obj.setCellFormat([str,str]);
	obj.setHeaderText(myColumns);
	obj.setSelectorVisible(true);
	obj.setRowCount(0);
	obj.setColumnCount(myColumns.length);
	obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
	obj.setSelectorWidth(30);
	obj.setHeaderHeight(20);
	obj.setSelectionMode("single-row");
	var defaultEventHandler = obj.getEvent("onkeydown"); 
	obj.setEvent("onkeydown", function(e){
		var row = obj.getSelectedRows();
		var event=e;
		if(event.keyCode==13){
			setAndExit(row);
			event.returnValue = false; 
		}else{
			defaultEventHandler.call(this, event); 
 			event.returnValue = false; 
		}
	});
	
	obj.onRowDoubleClicked = function(event, row){setAndExit(row);};
	obj.onKeyEnter = function(event, row){setAndExit(row);};
	obj.onSelectedRowsChanged= function(row){setData(row);};
	obj.loadingCompleted= function() { 
	    if (obj._maskLoading) { 
	       obj._maskLoading.style.display = 'none'; 
	    } 
	} 
	obj.loading = function() { 
	    if (! obj._maskLoading) { 
	        var maskLoading = document.createElement('div'); 
	        document.body.appendChild(maskLoading); 
	        maskLoading.className = 'aw-grid-loading'; 
	        var gridEl = obj.element(); 
	        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
	        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
	        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
	        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
	        obj._maskLoading = maskLoading; 
	   	}
        obj.clearCellModel(); 
	    obj.clearRowModel(); 
	    obj.clearScrollModel(); 
	    obj.clearSelectionModel(); 
	    obj.clearSortModel(); 
	    obj.refresh(); 
    	obj._maskLoading.style.display = 'block'; 
	}
	
	return obj;
}


function unformatInteger(num){
	return num.replace(/([^0-9])/g,'');
}

function validate(){
	var nums='0123456789';
	var value=document.getElementById('searchValue').value;
	var serachField=document.getElementById('searchField').value;
	var oparator=document.getElementById('oparator').value;
	var dataType='';
	
	var fields=getAttributeNames();
	var types=getDataTypes();
	for (var a=0;a<fields.length;a++){
		if(fields[a]==serachField){
			dataType=types[a];
		}
	}
	if(dataType!='String'){
		if(serachField=='select' && oparator=='select'){
			return true;
		}else{
			if(oparator=='like'){
				document.getElementById('divSearchValue').innerHTML='Cannot Use Oparator "Like" With Numaric Field. Select Another Oparator.';
				return false;
			}else if(value==''){
				document.getElementById('divSearchValue').innerHTML='Search Value Cannot be Blank';
				return false;
			}else{
				//for(var p=0;p<value.length;p++){
					//if(nums.indexOf(value.charAt(p))>-1){
					//	document.getElementById('divSearchValue').innerHTML='Search Value Must Be a Number';
					//	return false;
					//}else{
					//	document.getElementById('divSearchValue').innerHTML='';
					//	return true;
					//}
				//}
				document.getElementById('divSearchValue').innerHTML='';
				document.getElementById('searchValue').value=unformatInteger(document.getElementById('searchValue').value);
				return true;
			}
		}
	}else{
		return true;
	}
}

function validateOnSubmit(){
	var serachField=document.getElementById('searchField').value;
	var oparator=document.getElementById('oparator').value;
	if(serachField!='select' && oparator=='select'){
		alert('Please Select Operator');
		return false;
	}else if(oparator!='select' && serachField=='select'){
		alert('Please Select Search Field');
		return false;
	}else{
		return validate();
	}
}

function setAndExit(row){
	setData(row);
	setDataAndClose();
}

function setDataAndClose(){
	setValues();
	closeWindow();
}

var isClose=false; 
function closeWindow(){
	if(isClose!=true)
		executeFunction();
	setTimeout('window.close()',200);
}
function commonTree(treeID,codeField,descField,hiddenField,isCompany,parentCode,fireFunction){
	var URL = 'BrowserTreeIframe.jsp?';             
    var params1='&treeID='+treeID;  
    var params2='&codeField='+codeField;
    var params3='&descField='+descField;
    var params4='&hiddenField='+hiddenField;
    var params5='&isCompany='+ isCompany; 
    var params6='&parentCode='+ parentCode;
    var params7='&fireFunction='+ fireFunction;      
    var openLink = URL + params1+params2+params3+params4+params5+params6+params7;
    
	if (window.showModalDialog) {
		var returner=window.showModalDialog(openLink,this,"dialogWidth:300px;dialogHeight:450px;center;help:no;Yes;resizable:no;status:no;scroll:no");
					
		if(returner!="" && returner!=undefined){   
			returner=returner.split("|")
			if(hiddenField!="" && hiddenField!=undefined){
				document.getElementById(hiddenField).value=returner[0]; 
			}
			if(codeField!="" && codeField!=undefined){
				document.getElementById(codeField).value=returner[1];
			}
			if(descField!="" && descField!=undefined){
				document.getElementById(descField).value=returner[2];
			}
		}
	}else{
		theHeight=400;
		theWidth=300;
		var theTop=(screen.height/2)-(theHeight/2);
		var theLeft=(screen.width/2)-(theWidth/2);
		var features='height='+theHeight+',width='+theWidth+',top='+theTop+',left='+theLeft+",dialog=yes,modal=yes,status=yes,scrollbars=no";
		window.open(openLink,"commonTree",features);
	}           
} 