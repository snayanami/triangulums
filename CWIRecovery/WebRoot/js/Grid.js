function createBrowser(gridName,columns,cellFormat){
	var obj = new AW.UI.Grid;
	obj.setId(gridName);
	var str = new AW.Formats.String;
	var num = new AW.Formats.Number;
	obj.setCellText([]);
	obj.setCellFormat(cellFormat);
	obj.setHeaderText(columns);
	obj.setSelectorVisible(true);
	obj.setRowCount(0);
	obj.setColumnCount(columns.length);
	obj.setSelectorText(function(i){return this.getRowPosition(i)+ 1});
	obj.setSelectorWidth(20);
	obj.setHeaderHeight(20);
	obj.setSelectionMode("single-row");
	obj.onSelectedRowsChanged= function(row){};
	obj.loadingCompleted= function() {
	    if (obj._maskLoading) { 
	       obj._maskLoading.style.display = 'none'; 
	    } 
	}
	obj.loading = function() { 
	    if (! obj._maskLoading) { 
	        var maskLoading = document.createElement('div'); 
	        document.body.appendChild(maskLoading); 
	        maskLoading.className = 'aw-grid-loading'; 
	        var gridEl = obj.element(); 
	        maskLoading.style.left = AW.getLeft(gridEl) + 'px'; 
	        maskLoading.style.top = AW.getTop(gridEl) + 'px'; 
	        maskLoading.style.width =  gridEl.clientWidth + 'px'; 
	        maskLoading.style.height =  gridEl.clientHeight + 'px'; 
	        obj._maskLoading = maskLoading; 
	   	}
        obj.clearCellModel(); 
	    obj.clearRowModel(); 
	    obj.clearScrollModel(); 
	    obj.clearSelectionModel(); 
	    obj.clearSortModel(); 
	    obj.refresh(); 
    	obj._maskLoading.style.display = 'block'; 
	}
	return obj;
}

function setGridData(grid,data){
	grid.setCellText(data);
	grid.setRowCount(data.length);
	grid.refresh();
	grid.setSelectedRows([]);
}