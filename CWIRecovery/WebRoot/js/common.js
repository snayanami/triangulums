function clearScreen(id){
	document.getElementById(id).innerHTML = "";              	
}

function upperCase(id){
	document.getElementById(id).value = document.getElementById(id).value.toUpperCase();	
}



function setGridData(obj,data,length){
	//	provide cells and headers text
	obj.setCellText(data);
	obj.setHeaderText(myColumns);
	
	//	set number of rows/columns
	obj.setRowCount(length);
	obj.setColumnCount(5);

	//	enable row selectors
	obj.setSelectorVisible(true);
	obj.setSelectorText(function(i){return this.getRowPosition(i)+1});

	//	set headers width/height
	obj.setSelectorWidth(28);
	obj.setHeaderHeight(20);

	//	set row selection
	obj.setSelectionMode("single-row");
	document.getElementById('grid1').innerHTML = obj;
}