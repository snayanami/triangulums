<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>.::Pawning::.</title>
		<link rel="stylesheet" href="/css/common/common.css" type="text/css" />
		<script type="text/javascript">
			function getParameter( name ){
				name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
				var regexS = "[\\?&]"+name+"=([^&#]*)";
				var regex = new RegExp( regexS );
				var results = regex.exec( window.location.href );
				if( results == null )
				    return "";
				else
				    return results[1];
			}
			
			function getUrlPatameters( name ){
				name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
				var regexS = "[\\?&]"+name+"=([^&#]*)";
				var regex = new RegExp( regexS );
				var results = regex.exec( unescape(getParameter('url')) );
				if( results == null )
				    return "";
				else
				    return results[1];
			}
			
			function setupWindow(){
				document.getElementById('mainTable').width=getParameter('width')*1;
				document.getElementById('toolbar').width=getParameter('width')*1;
				document.getElementById('documentIframe').height=getParameter('height')*1-40;
				document.getElementById('documentIframe').width=getParameter('width')*1;
				document.getElementById('documentIframe').src=unescape(getParameter('url'));
			}
			
			/*window.onunload=function(){
			    try{
				    if(getUrlPatameters('func')!=undefined && getUrlPatameters('func')!='')
				        window.opener.parent.parent.frames['mainbody'].changeWorkFlowStatus();
				}catch(e){}
			}*/
		</script>
		
		<style>
			.toolBarBorder { 
			    border-top: #91a7b4 1px solid;
			    border-right: #91a7b4 1px solid;
			    border-left: #91a7b4 1px solid;
			    border-bottom: #91a7b4 1px solid;
			    background-color:#f9f9ef;
			    height: 40px;
			    vertical-align: middle;
			}
			
			.closeButton{
				background-image: url('images/button/EN/delete.gif');
				padding-right:20px;
				font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;
				font-size:13px;
				color: #000000;
				background-color: #e4e4f5;
				border: medium none;
				cursor: hand;
				width:90px;
				height:22px;
			}
		</style>
	</head>
	<body onload="setupWindow();">
		<table id="mainTable" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td align="center">
						<iframe id="documentIframe" frameborder="0" align="top" scrolling="auto" name="documentIframe"></iframe>
					</td>
				</tr>
				<tr>
					<td align="center" style="display: none;">
						<table class="toolBarBorder" id="toolbar">
							<tbody>
								<tr>
									<td align="center">
										<input type="button" value="Close" class="closeButton" onclick="window.close()">				
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>
