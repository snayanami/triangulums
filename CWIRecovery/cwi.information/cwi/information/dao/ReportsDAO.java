package cwi.information.dao;

import java.util.Date;
import java.util.List;

import cwi.basic.resourse.UserSessionConfig;
import cwi.information.dto.HolidayDTO;
import cwi.information.dto.UserAccessRightsBranchWiseDTO;
import cwi.information.dto.UserAccessRightsOfficerWiseDTO;


public interface ReportsDAO {
	public List<UserAccessRightsBranchWiseDTO> getUserAccessRights(UserSessionConfig userSessionConfig,int branchId);
	public List<UserAccessRightsOfficerWiseDTO> getUserAccessRightsOfficerWise(UserSessionConfig userSessionConfig,int officerId);
	public List<HolidayDTO> getHolidays(UserSessionConfig userSessionConfig,Date dateFrom,Date dateTo);
	public String getOfficerName(int cashierId);
}