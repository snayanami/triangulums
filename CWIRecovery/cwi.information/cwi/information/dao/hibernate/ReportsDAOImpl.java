package cwi.information.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import cwi.admin.domain.Branch;
import cwi.admin.domain.BranchOfficer;
import cwi.admin.domain.GroupProgram;
import cwi.admin.domain.Holiday;
import cwi.admin.domain.Officer;
import cwi.admin.domain.RefProgramData;
import cwi.basic.resourse.CommonDaoSupport;
import cwi.basic.resourse.UserSessionConfig;
import cwi.information.dao.ReportsDAO;
import cwi.information.dto.HolidayDTO;
import cwi.information.dto.UserAccessRightsBranchWiseDTO;
import cwi.information.dto.UserAccessRightsOfficerWiseDTO;

public class ReportsDAOImpl extends CommonDaoSupport implements ReportsDAO {	
	
	public List<UserAccessRightsBranchWiseDTO> getUserAccessRights(UserSessionConfig userSessionConfig,int branchId){
		List<UserAccessRightsBranchWiseDTO> returnList = new ArrayList<UserAccessRightsBranchWiseDTO>();
		
		Criteria branchCriteria=getSession().createCriteria(Branch.class);
		if(branchId>0)
			branchCriteria.add(Restrictions.eq("branchId", branchId));
		List<Branch> branchList=branchCriteria.list();
		if(branchList!=null){
			for(Branch branch:branchList){
				boolean branchAdded=false;
				Criteria brnOffCriteria=getSession().createCriteria(BranchOfficer.class);
				brnOffCriteria.add(Restrictions.eq("branchId", branch.getBranchId()));
				brnOffCriteria.setProjection(Projections.distinct(Projections.property("referenceProgramDataId")));
				List<Integer> accessGroupIdList=brnOffCriteria.list();
				if(accessGroupIdList!=null){
					for(Integer refProDataId:accessGroupIdList){
						List<UserAccessRightsBranchWiseDTO> branchWiseDataList = new ArrayList<UserAccessRightsBranchWiseDTO>();
						RefProgramData refProgramData=(RefProgramData)getHibernateTemplate().get(RefProgramData.class, refProDataId.intValue());
						if(refProgramData!=null){
							// Add Officers
							Criteria officerCriteria=getSession().createCriteria(BranchOfficer.class);
							officerCriteria.add(Restrictions.eq("branchId", branch.getBranchId()));
							officerCriteria.add(Restrictions.eq("referenceProgramDataId", refProgramData.getRefProgDataId()));
							officerCriteria.setProjection(Projections.distinct(Projections.property("officerId")));
							List<Integer> officerIdList=officerCriteria.list();
							if(officerIdList!=null){
								for(int i=0;i<officerIdList.size();i++){
									Query query=getSession().createQuery("SELECT off.userName FROM Officer off WHERE off.officerId=:offId");
									query.setInteger("offId", officerIdList.get(i));
									String name=(String)query.uniqueResult();
									if(branchWiseDataList.size()>i){
										branchWiseDataList.get(i).setOfficerName(name);
									}else{
										UserAccessRightsBranchWiseDTO dto=new UserAccessRightsBranchWiseDTO();
										dto.setOfficerName(name);
										if(i==0){
											if(branchAdded==false){
												dto.setBranchName(branch.getBranchName());
												branchAdded=true;
											}
											dto.setAccessGroup(refProgramData.getRefProgDataDescription());
										}
										branchWiseDataList.add(dto);
									}
								}
							}
							// Add Accessible Screens
							Criteria proCriteria=getSession().createCriteria(GroupProgram.class);
							proCriteria.add(Restrictions.eq("referenceProgramDataId", refProgramData.getRefProgDataId()));
							proCriteria.setProjection(Projections.distinct(Projections.property("systemProgramId")));
							List<Integer> programIdList=proCriteria.list();
							if(programIdList!=null){
								for(int i=0;i<programIdList.size();i++){
									Query query=getSession().createQuery("SELECT sp.systemProgramName FROM SystemProgram sp WHERE sp.systemProgramId=:systemProgramId");
									query.setInteger("systemProgramId", programIdList.get(i));
									String name=(String)query.uniqueResult();
									if(branchWiseDataList.size()>i){
										branchWiseDataList.get(i).setAccessibleScreens(name);
									}else{
										UserAccessRightsBranchWiseDTO dto=new UserAccessRightsBranchWiseDTO();
										dto.setAccessibleScreens(name);
										if(i==0){
											if(branchAdded==false){
												dto.setBranchName(branch.getBranchName());
												branchAdded=true;
											}
											dto.setAccessGroup(refProgramData.getRefProgDataDescription());
										}
										branchWiseDataList.add(dto);
									}
								}
							}
							
						}
						returnList.addAll(branchWiseDataList);
					}
				}
			}
		}
		/*Branch, Access Group, Officers in the Group, Assessible Screens*/
		return returnList;	
	}
	
	public List<UserAccessRightsOfficerWiseDTO> getUserAccessRightsOfficerWise(UserSessionConfig userSessionConfig,int officerId){
		List<UserAccessRightsOfficerWiseDTO> returnList = new ArrayList<UserAccessRightsOfficerWiseDTO>();
		return returnList;	
	}
	
	public List<HolidayDTO> getHolidays(UserSessionConfig userSessionConfig,Date dateFrom,Date dateTo){
		List<HolidayDTO> returnList = new ArrayList<HolidayDTO>();
		Criteria criteria = getSession().createCriteria(Holiday.class);
		if(dateFrom!=null)
			criteria.add(Restrictions.ge("holiday", dateFrom));
		if(dateTo!=null)
			criteria.add(Restrictions.le("holiday", dateTo));
		List<Holiday> list = criteria.list();
		
		for(Holiday holiday : list){
			
			Officer officer = (Officer)getHibernateTemplate().get(Officer.class,holiday.getUserId());
			
			HolidayDTO holidayDTO = new HolidayDTO();
			holidayDTO.setHoliday(holiday.getHoliday());
			holidayDTO.setReason(holiday.getReason());
			holidayDTO.setUserName(officer!=null?officer.getUserName():"");
			holidayDTO.setEnteredDate(holiday.getLastUpdatedDate());
			returnList.add(holidayDTO);
		}

		return returnList;
		/*Holiday, Reason, Created By, Entered Date */
	}

	public String getOfficerName(int cashierId){
		String cashier = null;
		
		String strOfficer = "SELECT off.userName FROM Officer off WHERE off.officerId="+cashierId;
		Query qryOfficer= getSession().createQuery(strOfficer);
		//qryOfficer.setInteger("officerId", cashierId);
		cashier=(String)qryOfficer.uniqueResult();
		
		return cashier;
	}
}