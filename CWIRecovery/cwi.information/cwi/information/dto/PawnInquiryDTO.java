package cwi.information.dto;

import java.util.Date;

public class PawnInquiryDTO {
	private int pawningTicketId;
	private String pawningTicketNo;
	private String status;
	private double totalAssessedValue;
	private double totalAmount;
	private double pawningAdvance;
	private double interestAmount;
	private double interestRate;
	private Date redemptionDate;
	private String description;
	private String branchName;
	
	private String articleType;
	private String goldType;
	private double grossWeight;
	private double netWeight;
	private double assesedValue;
	
	private double capitalArrears;
	private double interestArrears;
	private double otherChargesArrears;
	
	
	public int getPawningTicketId() {
		return pawningTicketId;
	}
	public void setPawningTicketId(int pawningTicketId) {
		this.pawningTicketId = pawningTicketId;
	}
	public String getPawningTicketNo() {
		return pawningTicketNo;
	}
	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getTotalAssessedValue() {
		return totalAssessedValue;
	}
	public void setTotalAssessedValue(double totalAssessedValue) {
		this.totalAssessedValue = totalAssessedValue;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getPawningAdvance() {
		return pawningAdvance;
	}
	public void setPawningAdvance(double pawningAdvance) {
		this.pawningAdvance = pawningAdvance;
	}
	public double getInterestAmount() {
		return interestAmount;
	}
	public void setInterestAmount(double interestAmount) {
		this.interestAmount = interestAmount;
	}
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	public Date getRedemptionDate() {
		return redemptionDate;
	}
	public void setRedemptionDate(Date redemptionDate) {
		this.redemptionDate = redemptionDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getArticleType() {
		return articleType;
	}
	public void setArticleType(String articleType) {
		this.articleType = articleType;
	}
	public String getGoldType() {
		return goldType;
	}
	public void setGoldType(String goldType) {
		this.goldType = goldType;
	}
	public double getGrossWeight() {
		return grossWeight;
	}
	public void setGrossWeight(double grossWeight) {
		this.grossWeight = grossWeight;
	}
	public double getNetWeight() {
		return netWeight;
	}
	public void setNetWeight(double netWeight) {
		this.netWeight = netWeight;
	}
	public double getAssesedValue() {
		return assesedValue;
	}
	public void setAssesedValue(double assesedValue) {
		this.assesedValue = assesedValue;
	}
	public double getCapitalArrears() {
		return capitalArrears;
	}
	public void setCapitalArrears(double capitalArrears) {
		this.capitalArrears = capitalArrears;
	}
	public double getInterestArrears() {
		return interestArrears;
	}
	public void setInterestArrears(double interestArrears) {
		this.interestArrears = interestArrears;
	}
	public double getOtherChargesArrears() {
		return otherChargesArrears;
	}
	public void setOtherChargesArrears(double otherChargesArrears) {
		this.otherChargesArrears = otherChargesArrears;
	}
	
	
	
	
}
