package cwi.information.dto;

import java.util.Date;

public class PendingTicketDetailsDTO {
	
	private String branchName;
	private String pawningTicketNo;
	private String description;
	private String stakeholderName;
	private double totalAssessedValue;
	private double pawningAdvance;
	private double totalInterest;
	private double redemptionAmount;
	private Date redemptionDate;
	private double interestRate;
	private String createdBy;
	private Date createdDate;
	private String articleType;
	private String goldType;
	private double netWeight;
	private double grossWeight;
	private double assessedValue;	
	
	public PendingTicketDetailsDTO(){}	
	
	public String getBranchName() {
		return branchName;
	}
	
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getPawningTicketNo() {
		return pawningTicketNo;
	}

	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String getStakeholderName() {
		return stakeholderName;
	}

	public void setStakeholderName(String stakeholderName) {
		this.stakeholderName = stakeholderName;
	}

	public double getTotalAssessedValue() {
		return totalAssessedValue;
	}
	
	public void setTotalAssessedValue(double totalAssessedValue) {
		this.totalAssessedValue = totalAssessedValue;
	}

	public double getPawningAdvance() {
		return pawningAdvance;
	}
	
	public void setPawningAdvance(double pawningAdvance) {
		this.pawningAdvance = pawningAdvance;
	}

	public double getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(double totalInterest) {
		this.totalInterest = totalInterest;
	}

	public double getRedemptionAmount() {
		return redemptionAmount;
	}

	public void setRedemptionAmount(double redemptionAmount) {
		this.redemptionAmount = redemptionAmount;
	}

	public Date getRedemptionDate() {
		return redemptionDate;
	}

	public void setRedemptionDate(Date redemptionDate) {
		this.redemptionDate = redemptionDate;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getArticleType() {
		return articleType;
	}

	public void setArticleType(String articleType) {
		this.articleType = articleType;
	}

	public String getGoldType() {
		return goldType;
	}

	public void setGoldType(String goldType) {
		this.goldType = goldType;
	}

	public double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(double netWeight) {
		this.netWeight = netWeight;
	}

	public double getGrossWeight() {
		return grossWeight;
	}
	
	public void setGrossWeight(double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public double getAssessedValue() {
		return assessedValue;
	}

	public void setAssessedValue(double assessedValue) {
		this.assessedValue = assessedValue;
	}	
}