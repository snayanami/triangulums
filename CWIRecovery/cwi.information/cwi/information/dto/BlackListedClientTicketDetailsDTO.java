package cwi.information.dto;

import java.util.Date;

public class BlackListedClientTicketDetailsDTO {
	
	private String clientName;
	private String userId;
	private Date blackListedDate;
	private String reason;
	private Date removedDateFromBlackList;
	private String removedUserId;
	private String removedReason;
	private double totalNoOfPawnGranted;
	private double totalAmountOfPawnGranted;
	private double totalOutstandingAmount;


	public BlackListedClientTicketDetailsDTO(){}


	public String getClientName() {
		return clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public Date getBlackListedDate() {
		return blackListedDate;
	}


	public void setBlackListedDate(Date blackListedDate) {
		this.blackListedDate = blackListedDate;
	}


	public String getReason() {
		return reason;
	}


	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getRemovedDateFromBlackList() {
		return removedDateFromBlackList;
	}


	public void setRemovedDateFromBlackList(Date removedDateFromBlackList) {
		this.removedDateFromBlackList = removedDateFromBlackList;
	}


	public String getRemovedUserId() {
		return removedUserId;
	}


	public void setRemovedUserId(String removedUserId) {
		this.removedUserId = removedUserId;
	}


	public String getRemovedReason() {
		return removedReason;
	}


	public void setRemovedReason(String removedReason) {
		this.removedReason = removedReason;
	};	
	

	public double getTotalNoOfPawnGranted() {
		return totalNoOfPawnGranted;
	}


	public void setTotalNoOfPawnGranted(double totalNoOfPawnGranted) {
		this.totalNoOfPawnGranted = totalNoOfPawnGranted;
	}


	public double getTotalAmountOfPawnGranted() {
		return totalAmountOfPawnGranted;
	}


	public void setTotalAmountOfPawnGranted(double totalAmountOfPawnGranted) {
		this.totalAmountOfPawnGranted = totalAmountOfPawnGranted;
	}


	public double getTotalOutstandingAmount() {
		return totalOutstandingAmount;
	}


	public void setTotalOutstandingAmount(double totalOutstandingAmount) {
		this.totalOutstandingAmount = totalOutstandingAmount;
	}	
}