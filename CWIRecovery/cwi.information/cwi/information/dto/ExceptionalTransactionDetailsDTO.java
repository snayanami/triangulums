package cwi.information.dto;

import java.util.Date;

public class ExceptionalTransactionDetailsDTO {
	
	private String branchName;	
	private String pawningTicketNo;
	private Date pawningDate;
	private String description;
	private String stakeholderName;
	private double totalAssessedValue;
	private double pawningAdvance;
	private double totalInterest;	
	private String userId;
	private double applicableInterestRate;
	private double givenInterestRate;
	private String interestRateOverwriteBy;
	
	public ExceptionalTransactionDetailsDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getPawningTicketNo() {
		return pawningTicketNo;
	}

	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStakeholderName() {
		return stakeholderName;
	}

	public void setStakeholderName(String stakeholderName) {
		this.stakeholderName = stakeholderName;
	};

	public double getTotalAssessedValue() {
		return totalAssessedValue;
	}

	public void setTotalAssessedValue(double totalAssessedValue) {
		this.totalAssessedValue = totalAssessedValue;
	}

	public double getPawningAdvance() {
		return pawningAdvance;
	}

	public void setPawningAdvance(double pawningAdvance) {
		this.pawningAdvance = pawningAdvance;
	}

	public double getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(double totalInterest) {
		this.totalInterest = totalInterest;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public double getApplicableInterestRate() {
		return applicableInterestRate;
	}

	public void setApplicableInterestRate(double applicableInterestRate) {
		this.applicableInterestRate = applicableInterestRate;
	}

	public double getGivenInterestRate() {
		return givenInterestRate;
	}

	public void setGivenInterestRate(double givenInterestRate) {
		this.givenInterestRate = givenInterestRate;
	}

	public String getInterestRateOverwriteBy() {
		return interestRateOverwriteBy;
	}

	public void setInterestRateOverwriteBy(String interestRateOverwriteBy) {
		this.interestRateOverwriteBy = interestRateOverwriteBy;
	}
	
	public Date getPawningDate() {
		return pawningDate;
	}

	public void setPawningDate(Date pawningDate) {
		this.pawningDate = pawningDate;
	}
}