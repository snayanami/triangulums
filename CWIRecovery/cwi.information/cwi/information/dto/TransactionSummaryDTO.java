package cwi.information.dto;

import java.util.Date;

public class TransactionSummaryDTO {
	
	private String branchName;
	private Date transactionDate;
	private double totalPawnAdvApproved;
	private double totalPayment;
	private double totalCollectionOnReceipts;
	private double totalCollectionOnAuctions;
	
	public TransactionSummaryDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public double getTotalPawnAdvApproved() {
		return totalPawnAdvApproved;
	}

	public void setTotalPawnAdvApproved(double totalPawnAdvApproved) {
		this.totalPawnAdvApproved = totalPawnAdvApproved;
	}

	public double getTotalPayment() {
		return totalPayment;
	}

	public void setTotalPayment(double totalPayment) {
		this.totalPayment = totalPayment;
	}

	public double getTotalCollectionOnReceipts() {
		return totalCollectionOnReceipts;
	}

	public void setTotalCollectionOnReceipts(double totalCollectionOnReceipts) {
		this.totalCollectionOnReceipts = totalCollectionOnReceipts;
	}

	public double getTotalCollectionOnAuctions() {
		return totalCollectionOnAuctions;
	}

	public void setTotalCollectionOnAuctions(double totalCollectionOnAuctions) {
		this.totalCollectionOnAuctions = totalCollectionOnAuctions;
	};
}