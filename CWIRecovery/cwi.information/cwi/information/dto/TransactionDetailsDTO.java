package cwi.information.dto;

import java.util.Date;

public class TransactionDetailsDTO {
	
	private String branchName;
	private String pawningTicketNo;
	private String stakeholderName;
	private double pawnAdvApprovedAmount;
	private Date pawnAdvApprovedDate;
	private double paymentAmount;
	private Date paymentDate;
	private double collectionAmountOnReciepts;
	private double collectionAmountOnAuction;

	public TransactionDetailsDTO(){}
	
	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getPawningTicketNo() {
		return pawningTicketNo;
	}

	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}

	public String getStakeholderName() {
		return stakeholderName;
	}

	public void setStakeholderName(String stakeholderName) {
		this.stakeholderName = stakeholderName;
	}

	public double getPawnAdvApprovedAmount() {
		return pawnAdvApprovedAmount;
	}

	public void setPawnAdvApprovedAmount(double pawnAdvApprovedAmount) {
		this.pawnAdvApprovedAmount = pawnAdvApprovedAmount;
	}

	public Date getPawnAdvApprovedDate() {
		return pawnAdvApprovedDate;
	}

	public void setPawnAdvApprovedDate(Date pawnAdvApprovedDate) {
		this.pawnAdvApprovedDate = pawnAdvApprovedDate;
	}

	public double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getCollectionAmountOnReciepts() {
		return collectionAmountOnReciepts;
	}

	public void setCollectionAmountOnReciepts(double collectionAmountOnReciepts) {
		this.collectionAmountOnReciepts = collectionAmountOnReciepts;
	}

	public double getCollectionAmountOnAuction() {
		return collectionAmountOnAuction;
	}

	public void setCollectionAmountOnAuction(double collectionAmountOnAuction) {
		this.collectionAmountOnAuction = collectionAmountOnAuction;
	}	
}