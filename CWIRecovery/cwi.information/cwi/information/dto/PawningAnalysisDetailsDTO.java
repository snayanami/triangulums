package cwi.information.dto;

public class PawningAnalysisDetailsDTO {
	
	private String branchName;	
	private int noOfPawnTickets;
	private double amountOfPawnTickets;
	private int noOfPendingApproval;
	private double amountOfPendingApproval;	
	private int noOfPendingPayments;
	private double amountOfPendingPayments;	
	private int noOfActivePawnings;
	private double amountOfActivePawnings;	
	private int noOfTicketsForAuction;
	private double amountOfTicketsForAuction;
	private int noOfRedemptionTickets;	
	private double amountOfRedemptionTickets;
	private double totalCapitalOutstanding;
	private double totalInterestOutstanding;
	private double totalOtherChargesOutstanding;
	private double totalOutstanding;
	
	public PawningAnalysisDetailsDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	
	public int getNoOfPawnTickets() {
		return noOfPawnTickets;
	}

	public void setNoOfPawnTickets(int noOfPawnTickets) {
		this.noOfPawnTickets = noOfPawnTickets;
	}

	public double getAmountOfPawnTickets() {
		return amountOfPawnTickets;
	}

	public void setAmountOfPawnTickets(double amountOfPawnTickets) {
		this.amountOfPawnTickets = amountOfPawnTickets;
	}

	public int getNoOfPendingApproval() {
		return noOfPendingApproval;
	}

	public void setNoOfPendingApproval(int noOfPendingApproval) {
		this.noOfPendingApproval = noOfPendingApproval;
	}

	public double getAmountOfPendingApproval() {
		return amountOfPendingApproval;
	}

	public void setAmountOfPendingApproval(double amountOfPendingApproval) {
		this.amountOfPendingApproval = amountOfPendingApproval;
	}

	public int getNoOfPendingPayments() {
		return noOfPendingPayments;
	}

	public void setNoOfPendingPayments(int noOfPendingPayments) {
		this.noOfPendingPayments = noOfPendingPayments;
	}

	public double getAmountOfPendingPayments() {
		return amountOfPendingPayments;
	}

	public void setAmountOfPendingPayments(double amountOfPendingPayments) {
		this.amountOfPendingPayments = amountOfPendingPayments;
	}

	public int getNoOfActivePawnings() {
		return noOfActivePawnings;
	}

	public void setNoOfActivePawnings(int noOfActivePawnings) {
		this.noOfActivePawnings = noOfActivePawnings;
	}

	public double getAmountOfActivePawnings() {
		return amountOfActivePawnings;
	}

	public void setAmountOfActivePawnings(double amountOfActivePawnings) {
		this.amountOfActivePawnings = amountOfActivePawnings;
	}

	public int getNoOfTicketsForAuction() {
		return noOfTicketsForAuction;
	}

	public void setNoOfTicketsForAuction(int noOfTicketsForAuction) {
		this.noOfTicketsForAuction = noOfTicketsForAuction;
	}

	public double getAmountOfTicketsForAuction() {
		return amountOfTicketsForAuction;
	}

	public void setAmountOfTicketsForAuction(double amountOfTicketsForAuction) {
		this.amountOfTicketsForAuction = amountOfTicketsForAuction;
	}
	
	public int getNoOfRedemptionTickets() {
		return noOfRedemptionTickets;
	}

	public void setNoOfRedemptionTickets(int noOfRedemptionTickets) {
		this.noOfRedemptionTickets = noOfRedemptionTickets;
	}

	public double getAmountOfRedemptionTickets() {
		return amountOfRedemptionTickets;
	}

	public void setAmountOfRedemptionTickets(double amountOfRedemptionTickets) {
		this.amountOfRedemptionTickets = amountOfRedemptionTickets;
	}

	public double getTotalCapitalOutstanding() {
		return totalCapitalOutstanding;
	}

	public void setTotalCapitalOutstanding(double totalCapitalOutstanding) {
		this.totalCapitalOutstanding = totalCapitalOutstanding;
	}

	public double getTotalInterestOutstanding() {
		return totalInterestOutstanding;
	}

	public void setTotalInterestOutstanding(double totalInterestOutstanding) {
		this.totalInterestOutstanding = totalInterestOutstanding;
	}

	public double getTotalOtherChargesOutstanding() {
		return totalOtherChargesOutstanding;
	}

	public void setTotalOtherChargesOutstanding(double totalOtherChargesOutstanding) {
		this.totalOtherChargesOutstanding = totalOtherChargesOutstanding;
	}

	public double getTotalOutstanding() {
		return totalOutstanding;
	}

	public void setTotalOutstanding(double totalOutstanding) {
		this.totalOutstanding = totalOutstanding;
	}
}