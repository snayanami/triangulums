package cwi.information.dto;

import java.util.Date;

public class PawningTicketDetailsDTO {
	
	private String branchName;
	private Date date;
	private String pawningTicketNo;
	private String status;
	private String stakehoderName;	
	private double totalAssessedValue;
	private double pawningAdvanceAmount;
	private double totalInterest;
	private double interestRate;
	private double totalOtherCharges;
	private double capitalOutstandingAmount;
	private double interestOutstandingAmount;
	private double otherChargesOutstandingAmount;	
	private double totalOutstandingAmount;

	public PawningTicketDetailsDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPawningTicketNo() {
		return pawningTicketNo;
	}

	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStakehoderName() {
		return stakehoderName;
	}

	public void setStakehoderName(String stakehoderName) {
		this.stakehoderName = stakehoderName;
	};

	public double getTotalAssessedValue() {
		return totalAssessedValue;
	}

	public void setTotalAssessedValue(double totalAssessedValue) {
		this.totalAssessedValue = totalAssessedValue;
	}

	public double getPawningAdvanceAmount() {
		return pawningAdvanceAmount;
	}

	public void setPawningAdvanceAmount(double pawningAdvanceAmount) {
		this.pawningAdvanceAmount = pawningAdvanceAmount;
	}

	public double getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(double totalInterest) {
		this.totalInterest = totalInterest;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public double getTotalOtherCharges() {
		return totalOtherCharges;
	}

	public void setTotalOtherCharges(double totalOtherCharges) {
		this.totalOtherCharges = totalOtherCharges;
	}

	public double getCapitalOutstandingAmount() {
		return capitalOutstandingAmount;
	}

	public void setCapitalOutstandingAmount(double capitalOutstandingAmount) {
		this.capitalOutstandingAmount = capitalOutstandingAmount;
	}

	public double getInterestOutstandingAmount() {
		return interestOutstandingAmount;
	}

	public void setInterestOutstandingAmount(double interestOutstandingAmount) {
		this.interestOutstandingAmount = interestOutstandingAmount;
	}

	public double getOtherChargesOutstandingAmount() {
		return otherChargesOutstandingAmount;
	}

	public void setOtherChargesOutstandingAmount(
			double otherChargesOutstandingAmount) {
		this.otherChargesOutstandingAmount = otherChargesOutstandingAmount;
	}

	public double getTotalOutstandingAmount() {
		return totalOutstandingAmount;
	}

	public void setTotalOutstandingAmount(double totalOutstandingAmount) {
		this.totalOutstandingAmount = totalOutstandingAmount;
	}
}
