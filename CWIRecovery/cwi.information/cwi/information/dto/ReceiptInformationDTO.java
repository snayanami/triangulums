package cwi.information.dto;

import java.util.Date;

public class ReceiptInformationDTO {
	
	private String branchName;
	private String receiptNo;
	private Date receiptDate;
	private double receiptAmount;
	private String description;
	private String pawningTicketNo;
	private String stakeholderName;
	private String cashierName;	
	private String receiptStatus;
	private String isRedemptionreceipt;
	
	public ReceiptInformationDTO(){};
	
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public Date getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}
	public double getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPawningTicketNo() {
		return pawningTicketNo;
	}
	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}
	public String getStakeholderName() {
		return stakeholderName;
	}
	public void setStakeholderName(String stakeholderName) {
		this.stakeholderName = stakeholderName;
	}
	public String getCashierName() {
		return cashierName;
	}
	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}
	public String getReceiptStatus() {
		return receiptStatus;
	}
	public void setReceiptStatus(String receiptStatus) {
		this.receiptStatus = receiptStatus;
	}
	public String getIsRedemptionreceipt() {
		return isRedemptionreceipt;
	}
	public void setIsRedemptionreceipt(String isRedemptionreceipt) {
		this.isRedemptionreceipt = isRedemptionreceipt;
	}
}
