package cwi.information.dto;

public class ReportsDTO {
	private String pawningTicketNo;
	private String clientNo;
	private String branch;
	private double redemptionValue;
	
	public String getPawningTicketNo() {
		return pawningTicketNo;
	}
	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}
	public String getClientNo() {
		return clientNo;
	}
	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public double getRedemptionValue() {
		return redemptionValue;
	}
	public void setRedemptionValue(double redemptionValue) {
		this.redemptionValue = redemptionValue;
	}
	
	
}
