package cwi.information.dto;

import java.util.Date;

public class PaymentDetailsDTO {
	
	private String branchName;
	private String pawningTicketNo;
	private String paymentVoucherNo;
	private Date date;
	private String description;
	private String stakeholderName;	
	private double totalAssessedValue;
	private double pawningAdvance;	
	private String approvedBy;
	private Date approvedDate;

	public PaymentDetailsDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getPawningTicketNo() {
		return pawningTicketNo;
	}

	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}

	public String getPaymentVoucherNo() {
		return paymentVoucherNo;
	}

	public void setPaymentVoucherNo(String paymentVoucherNo) {
		this.paymentVoucherNo = paymentVoucherNo;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStakeholderName() {
		return stakeholderName;
	}

	public void setStakeholderName(String stakeholderName) {
		this.stakeholderName = stakeholderName;
	}

	public double getTotalAssessedValue() {
		return totalAssessedValue;
	}

	public void setTotalAssessedValue(double totalAssessedValue) {
		this.totalAssessedValue = totalAssessedValue;
	}

	public double getPawningAdvance() {
		return pawningAdvance;
	}

	public void setPawningAdvance(double pawningAdvance) {
		this.pawningAdvance = pawningAdvance;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	};
}