package cwi.information.dto;

import java.util.List;

public class UserAccessRightsBranchWiseDTO {
	
	private String branchName;
	private String accessGroup;
	private String officerName;
	private String accessibleScreens;

	public UserAccessRightsBranchWiseDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getAccessGroup() {
		return accessGroup;
	}

	public void setAccessGroup(String accessGroup) {
		this.accessGroup = accessGroup;
	}

	public String getOfficerName() {
		return officerName;
	}

	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}

	public String getAccessibleScreens() {
		return accessibleScreens;
	}

	public void setAccessibleScreens(String accessibleScreens) {
		this.accessibleScreens = accessibleScreens;
	};
}