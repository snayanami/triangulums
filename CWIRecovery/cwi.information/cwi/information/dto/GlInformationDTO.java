package cwi.information.dto;

import java.util.Date;

public class GlInformationDTO {
	private String branchName;
	private String pawningTicketNo;
	private String ledgerAccountCode;
	private String ledgerAccountDescription;
	private String debit;
	private String credit;
	private double amount;
	private String description;
	private Date transactionDate;
	private String transactionNo;
	private String stakeholderName;
	private double debitAmount;
	private double creditAmount;
	
	//*For The Trial Balance*//
	private double debitTotal;
	private double creditTotal;
	
	public GlInformationDTO(){}

	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	
	public String getPawningTicketNo() {
		return pawningTicketNo;
	}
	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}

	public String getLedgerAccountCode() {
		return ledgerAccountCode;
	}
	public void setLedgerAccountCode(String ledgerAccountCode) {
		this.ledgerAccountCode = ledgerAccountCode;
	}

	public String getLedgerAccountDescription() {
		return ledgerAccountDescription;
	}
	public void setLedgerAccountDescription(String ledgerAccountDescription) {
		this.ledgerAccountDescription = ledgerAccountDescription;
	}

	public String getDebit() {
		return debit;
	}
	public void setDebit(String debit) {
		this.debit = debit;
	}

	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}

	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getStakeholderName() {
		return stakeholderName;
	}
	public void setStakeholderName(String stakeholderName) {
		this.stakeholderName = stakeholderName;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public double getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public double getDebitTotal() {
		return debitTotal;
	}
	public void setDebitTotal(double debitTotal) {
		this.debitTotal = debitTotal;
	}

	public double getCreditTotal() {
		return creditTotal;
	}
	public void setCreditTotal(double creditTotal) {
		this.creditTotal = creditTotal;
	}

}
