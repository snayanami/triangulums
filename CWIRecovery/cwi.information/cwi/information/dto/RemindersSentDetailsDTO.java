package cwi.information.dto;

import java.util.Date;

public class RemindersSentDetailsDTO {
	
	private String branchName;
	private Date remiderGeneratedDate;
	private String description;
	private String pawningTicketNo;
	private String stakeholderName;
	private double capitalOutstanding;
	private double interestOutstanding;
	private double otherChargesOutstanding;
	private double totalOutstanding;
	private String reminderSentBy;
	private Date remiderSentDate;
	
	public RemindersSentDetailsDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getRemiderGeneratedDate() {
		return remiderGeneratedDate;
	}

	public void setRemiderGeneratedDate(Date remiderGeneratedDate) {
		this.remiderGeneratedDate = remiderGeneratedDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPawningTicketNo() {
		return pawningTicketNo;
	}

	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}

	public String getStakeholderName() {
		return stakeholderName;
	}

	public void setStakeholderName(String stakeholderName) {
		this.stakeholderName = stakeholderName;
	}

	public double getCapitalOutstanding() {
		return capitalOutstanding;
	}

	public void setCapitalOutstanding(double capitalOutstanding) {
		this.capitalOutstanding = capitalOutstanding;
	}

	public double getInterestOutstanding() {
		return interestOutstanding;
	}

	public void setInterestOutstanding(double interestOutstanding) {
		this.interestOutstanding = interestOutstanding;
	}
	
	public double getOtherChargesOutstanding() {
		return otherChargesOutstanding;
	}

	public void setOtherChargesOutstanding(double otherChargesOutstanding) {
		this.otherChargesOutstanding = otherChargesOutstanding;
	};

	public double getTotalOutstanding() {
		return totalOutstanding;
	}

	public void setTotalOutstanding(double totalOutstanding) {
		this.totalOutstanding = totalOutstanding;
	}

	public String getReminderSentBy() {
		return reminderSentBy;
	}

	public void setReminderSentBy(String reminderSentBy) {
		this.reminderSentBy = reminderSentBy;
	}

	public Date getRemiderSentDate() {
		return remiderSentDate;
	}

	public void setRemiderSentDate(Date remiderSentDate) {
		this.remiderSentDate = remiderSentDate;
	}	
}