package cwi.information.dto;

import java.util.Date;

public class AuctionDetailsDTO {
	
	private String branchName;
	private Date auctionDate;	
	private String pawningTicketNo;
	private String articleDescription;
	private double assessedValue;
	private double outstandingAmount;
	private double salePrice;
	private double profitLoss;
	private String buyerName;
	private String address;
	private String nicNo;
	private String remarks;


	public AuctionDetailsDTO(){}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public Date getAuctionDate() {
		return auctionDate;
	}


	public void setAuctionDate(Date auctionDate) {
		this.auctionDate = auctionDate;
	}


	public String getPawningTicketNo() {
		return pawningTicketNo;
	}


	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}


	public String getArticleDescription() {
		return articleDescription;
	}


	public void setArticleDescription(String articleDescription) {
		this.articleDescription = articleDescription;
	}


	public double getAssessedValue() {
		return assessedValue;
	}


	public void setAssessedValue(double assessedValue) {
		this.assessedValue = assessedValue;
	}


	public double getOutstandingAmount() {
		return outstandingAmount;
	}


	public void setOutstandingAmount(double outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}
	
	
	public double getSalePrice() {
		return salePrice;
	}


	public void setSalePrice(double salePrice) {
		this.salePrice = salePrice;
	};


	public double getProfitLoss() {
		return profitLoss;
	}


	public void setProfitLoss(double profitLoss) {
		this.profitLoss = profitLoss;
	}


	public String getBuyerName() {
		return buyerName;
	}


	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getNicNo() {
		return nicNo;
	}


	public void setNicNo(String nicNo) {
		this.nicNo = nicNo;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}	
}
