package cwi.information.dto;

import java.util.Date;

public class PendingRemindersDTO {
	
	private String branchName;
	private Date reminderGeneratedDate;
	private String description;
	private String pawningTicketNo;
	private String stakeholderName;	
	private double capitalOutstanding;
	private double interestOutstanding;
	private double otherChargesOutstandingAmount;	
	private double totalOutstanding;
	
	public PendingRemindersDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getReminderGeneratedDate() {
		return reminderGeneratedDate;
	}

	public void setReminderGeneratedDate(Date reminderGeneratedDate) {
		this.reminderGeneratedDate = reminderGeneratedDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPawningTicketNo() {
		return pawningTicketNo;
	}

	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}

	public String getStakeholderName() {
		return stakeholderName;
	}

	public void setStakeholderName(String stakeholderName) {
		this.stakeholderName = stakeholderName;
	}

	public double getCapitalOutstanding() {
		return capitalOutstanding;
	}

	public void setCapitalOutstanding(double capitalOutstanding) {
		this.capitalOutstanding = capitalOutstanding;
	}

	public double getInterestOutstanding() {
		return interestOutstanding;
	}

	public void setInterestOutstanding(double interestOutstanding) {
		this.interestOutstanding = interestOutstanding;
	}
	
	public double getOtherChargesOutstandingAmount() {
		return otherChargesOutstandingAmount;
	}

	public void setOtherChargesOutstandingAmount(
			double otherChargesOutstandingAmount) {
		this.otherChargesOutstandingAmount = otherChargesOutstandingAmount;
	};

	public double getTotalOutstanding() {
		return totalOutstanding;
	}

	public void setTotalOutstanding(double totalOutstanding) {
		this.totalOutstanding = totalOutstanding;
	}	
}