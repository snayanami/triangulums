package cwi.information.dto;

public class UserAccessRightsOfficerWiseDTO {
	
	private String branchName;
	private String accessGroup;
	private String accessibleScreens;
	
	public UserAccessRightsOfficerWiseDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getAccessGroup() {
		return accessGroup;
	}

	public void setAccessGroup(String accessGroup) {
		this.accessGroup = accessGroup;
	}

	public String getAccessibleScreens() {
		return accessibleScreens;
	}

	public void setAccessibleScreens(String accessibleScreens) {
		this.accessibleScreens = accessibleScreens;
	};
}