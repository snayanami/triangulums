package cwi.information.dto;

import java.util.Date;

public class UserTransactionSummaryDTO {
	
	private String branchName;
	private Date transactionDate;
	private String userId;
	private double totalPawnAdvApproved;
	private double totalPayment;
	private double totalCollectionOnReceipts;
	private double totalCollectionOnAuction;

	public UserTransactionSummaryDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public double getTotalPawnAdvApproved() {
		return totalPawnAdvApproved;
	}

	public void setTotalPawnAdvApproved(double totalPawnAdvApproved) {
		this.totalPawnAdvApproved = totalPawnAdvApproved;
	}

	public double getTotalPayment() {
		return totalPayment;
	}

	public void setTotalPayment(double totalPayment) {
		this.totalPayment = totalPayment;
	}

	public double getTotalCollectionOnReceipts() {
		return totalCollectionOnReceipts;
	}

	public void setTotalCollectionOnReceipts(double totalCollectionOnReceipts) {
		this.totalCollectionOnReceipts = totalCollectionOnReceipts;
	}

	public double getTotalCollectionOnAuction() {
		return totalCollectionOnAuction;
	}

	public void setTotalCollectionOnAuction(double totalCollectionOnAuction) {
		this.totalCollectionOnAuction = totalCollectionOnAuction;
	};
}