package cwi.information.dto;

import java.util.Date;

public class GoldValueHistoryDTO {
	
	private Date effectiveDate;
	private String goldType;
	private double value;
	private String userName;
	
	private Date EnteredDate;
	
	public GoldValueHistoryDTO(){};
	
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getGoldType() {
		return goldType;
	}
	public void setGoldType(String goldType) {
		this.goldType = goldType;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public Date getEnteredDate() {
		return EnteredDate;
	}
	public void setEnteredDate(Date enteredDate) {
		EnteredDate = enteredDate;
	}
}