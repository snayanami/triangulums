package cwi.information.dto;

import java.util.Date;

public class RedemptionDetailsDTO {	
	
	private String branchName;
	private Date redemptionDate;	
	private String pawningTicketNo;
	private String stakeholderName;
	private double totalAssessedValue;
	private double pawningAdvance;
	private double totalInterest;
	private double totalOtherCharges;
	private double redemptionAmount;
	private String redemptionReceiptNo;
	private String userId;

	public RedemptionDetailsDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getRedemptionDate() {
		return redemptionDate;
	}

	public void setRedemptionDate(Date redemptionDate) {
		this.redemptionDate = redemptionDate;
	}

	public String getPawningTicketNo() {
		return pawningTicketNo;
	}

	public void setPawningTicketNo(String pawningTicketNo) {
		this.pawningTicketNo = pawningTicketNo;
	}

	public String getStakeholderName() {
		return stakeholderName;
	}

	public void setStakeholderName(String stakeholderName) {
		this.stakeholderName = stakeholderName;
	}

	public double getTotalAssessedValue() {
		return totalAssessedValue;
	}

	public void setTotalAssessedValue(double totalAssessedValue) {
		this.totalAssessedValue = totalAssessedValue;
	}

	public double getPawningAdvance() {
		return pawningAdvance;
	}

	public void setPawningAdvance(double pawningAdvance) {
		this.pawningAdvance = pawningAdvance;
	}

	public double getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(double totalInterest) {
		this.totalInterest = totalInterest;
	}

	public double getTotalOtherCharges() {
		return totalOtherCharges;
	}

	public void setTotalOtherCharges(double totalOtherCharges) {
		this.totalOtherCharges = totalOtherCharges;
	}

	public double getRedemptionAmount() {
		return redemptionAmount;
	}

	public void setRedemptionAmount(double redemptionAmount) {
		this.redemptionAmount = redemptionAmount;
	}

	public String getRedemptionReceiptNo() {
		return redemptionReceiptNo;
	}

	public void setRedemptionReceiptNo(String redemptionReceiptNo) {
		this.redemptionReceiptNo = redemptionReceiptNo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	};
}