package cwi.information.dto;

import java.util.Date;

public class CollectionSummaryDTO {
	
	private String branchName;
	private Date receiptDate;
	private double totalReceiptAmount;
	
	public CollectionSummaryDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public double getTotalReceiptAmount() {
		return totalReceiptAmount;
	}

	public void setTotalReceiptAmount(double totalReceiptAmount) {
		this.totalReceiptAmount = totalReceiptAmount;
	};
}