package cwi.information.dto;

import java.util.Date;

public class CashFlowStatementDTO {
	private String branchName;
	private Date transactionDate;
	private double totalPayment;
	private double totalCollection;
	private double cashFlowDifference;
	
	public CashFlowStatementDTO(){}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public double getTotalPayment() {
		return totalPayment;
	}

	public void setTotalPayment(double totalPayment) {
		this.totalPayment = totalPayment;
	}

	public double getTotalCollection() {
		return totalCollection;
	}

	public void setTotalCollection(double totalCollection) {
		this.totalCollection = totalCollection;
	}

	public double getCashFlowDifference() {
		return cashFlowDifference;
	}

	public void setCashFlowDifference(double cashFlowDifference) {
		this.cashFlowDifference = cashFlowDifference;
	};
}