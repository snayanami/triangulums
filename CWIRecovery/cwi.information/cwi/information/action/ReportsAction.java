package cwi.information.action;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import jxl.SheetSettings;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;
import cwi.basic.resourse.UserSessionConfig;
import cwi.information.dto.HolidayDTO;
import cwi.information.dto.UserAccessRightsBranchWiseDTO;
import cwi.information.spring.ReportsBD;

public class ReportsAction extends CommonDispatch {
	private ByteArrayOutputStream pdfStream;
	
	public static final int ALIGNMENT_LEFT = 0;
	public static final int ALIGNMENT_CENTER = 1;
	public static final int ALIGNMENT_RIGHT = 2;
	public static final int ORIENTATION_PORTRAIT = 3;
	public static final int ORIENTATION_LANDSCAPE = 4;	
	
	
	static DecimalFormat points2decimalFormat = new DecimalFormat();
	static {
		points2decimalFormat.setMinimumFractionDigits(2);
		points2decimalFormat.setMaximumFractionDigits(2);
		points2decimalFormat.setGroupingSize(3);
	}
	
	private ReportsBD reportsBD;
	
	public ReportsBD getReportsBD() {
		return reportsBD;
	}

	public void setReportsBD(ReportsBD reportsBD) {
		this.reportsBD = reportsBD;
	}
	
	public ActionForward getUserAccessRights(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		int branchId = 0;
		
		if(request.getParameter("branchId")!=null && !request.getParameter("branchId").equals(""))
			branchId=Integer.parseInt(request.getParameter("branchId"));
		
		List<UserAccessRightsBranchWiseDTO> list  = null;
		try {
			list = getReportsBD().getUserAccessRights(SessionUtil.getUserSession(request), branchId);
			/*JSONArray mainArray = new JSONArray();
			for(UserAccessRightsBranchWiseDTO userAccessRightsBranchWiseDTO : list){				
				JSONArray array = new JSONArray();
				array.put(userAccessRightsBranchWiseDTO.getBranchName());
				array.put(userAccessRightsBranchWiseDTO.getAccessGroup());
				array.put(userAccessRightsBranchWiseDTO.getOfficerName());
				array.put(userAccessRightsBranchWiseDTO.getAccessibleScreens());
				mainArray.put(array);	
			}
			response.getWriter().write(mainArray.toString());*/
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
		
		String headerArray[] = {"Branch Name","Access Group","Officer Name","Accessible Screens"};
		
		String dataField[] = {"BranchName","AccessGroup","OfficerName","AccessibleScreens"};
		
		float columnSize[] = {1,1,1,1.5f};
		
		int headerAlign[] = {Element.ALIGN_LEFT,Element.ALIGN_LEFT,Element.ALIGN_LEFT,Element.ALIGN_LEFT};
	
		printReport(request, response, list, dataField, columnSize, UserAccessRightsBranchWiseDTO.class, headerArray, headerAlign, 0, PageSize.A3, "User Access Rights", SessionUtil.getUserSession(request).getLoginDate(), "User Access Rights");
	
        return null;
	}
	
	public ActionForward getUserAccessRightsOfficerWise(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		/*MessageResources messageResources = getResources(request,"message");
		int officerId = 0;
		
		if(request.getParameter("officerId")!=null && !request.getParameter("officerId").equals(""))
			officerId=Integer.parseInt(request.getParameter("officerId"));
		
		List<UserAccessRightsOfficerWiseDTO> list  = null;
		try {
			list = getReportsBD().getUserAccessRightsOfficerWise(SessionUtil.getUserSession(request), officerId);
			/*JSONArray mainArray = new JSONArray();
			for(UserAccessRightsOfficerWiseDTO userAccessRightsOfficerWiseDTO : list){				
				JSONArray array = new JSONArray();
				array.put(userAccessRightsOfficerWiseDTO.getBranchName());
				array.put(userAccessRightsOfficerWiseDTO.getAccessGroup());
				array.put(userAccessRightsOfficerWiseDTO.getAccessibleScreens());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());*/
		/*} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
		
		String headerArray[] = {"Branch Name","Access Group","Accessible Screens"};
		
		String dataField[] = {"BranchName","AccessGroup","AccessibleScreens"};
		
		printReport(request, response, list, dataField, UserAccessRightsOfficerWiseDTO.class, headerArray, "User Access Rights(Officer wise)", SessionUtil.getUserSession(request).getLoginDate(), "User Access Rights Officer wise");
	*/
        return null;
	}
	
	/************ OK*******************************/
	public ActionForward getHolidays(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		Date dateFrom = null;
		Date dateTo	= null;
		
		if(request.getParameter("dateFrom")!=null && !request.getParameter("dateFrom").equals(""))
			dateFrom=(StrutsFormValidateUtil.parseDate(request.getParameter("dateFrom")));
		
		if(request.getParameter("dateTo")!=null && !request.getParameter("dateTo").equals(""))
			dateTo=(StrutsFormValidateUtil.parseDate(request.getParameter("dateTo")));
		
		List<HolidayDTO> list  = null;
		try {
			list = getReportsBD().getHolidays(SessionUtil.getUserSession(request), dateFrom, dateTo);
			/*JSONArray mainArray = new JSONArray();
			for(Holiday holiday : list){				
				JSONArray array = new JSONArray();
				array.put(StrutsFormValidateUtil.parseString(holiday.getHoliday()));
				array.put(holiday.getReason());
				array.put(holiday.getUserId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());*/
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
		
		String headerArray[] = {"Holiday","Reason","User Name","Created Date"};
		
		String dataField[] = {"Holiday","Reason","UserName","EnteredDate"};
		
		float columnSize[] = {1.1f,2.5f,1.5f,1.1f};
		
		int headerAlign[] = {Element.ALIGN_LEFT,Element.ALIGN_LEFT,Element.ALIGN_LEFT,Element.ALIGN_LEFT};
		
		printReport(request, response, list, dataField, columnSize, HolidayDTO.class, headerArray, headerAlign, 0, PageSize.A3, "Holidays", SessionUtil.getUserSession(request).getLoginDate(), "Holidays");
		//printExcel(mapping, form, request, response, list, dataField, HolidayDTO.class, headerArray, "Holidays", SessionUtil.getUserSession(request).getLoginDate(), "Holidays");
		
        return null;
	}
	
	public void printReport(HttpServletRequest request, HttpServletResponse response, List dataList, String[] dataField, float[] columnSize, Class clazz, String[] headerArray, int[] headerAlign, int isPrintTot,  Rectangle PageSze, String reportName, Date reportDate, String fileName) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		
		//*Font*//
		Font normalFont = FontFactory.getFont("VERDANA" , 10, Font.NORMAL,Color.black);
		Font hadingFont = FontFactory.getFont("VERDANA", 13, Font.BOLD, Color.black);
		Font subHedFont = FontFactory.getFont("VERDANA", 12, Font.BOLD, Color.black);
		
		PdfPCell cell = null;
        PdfPCell subCell = null;
        PdfPCell subDataCell = null;       
        Rectangle pageSize = PageSze;
		Document document = new Document(pageSize);
		PdfWriter writer = PdfWriter.getInstance(document,response.getOutputStream());
		response.setContentType("application/pdf");
        response.setHeader("Content-disposition","inline; filename="+fileName+".pdf" );
		document.open();
		
		PdfPTable headTable = new PdfPTable(1);        
        headTable.setWidthPercentage(100);
        
        cell = new PdfPCell(new Paragraph(reportName,hadingFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setGrayFill(0.9f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        headTable.addCell(cell);
		
        cell = new PdfPCell(new Paragraph("REPORT DATE : "+StrutsFormValidateUtil.parseString(reportDate),normalFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        headTable.addCell(cell);
		
        cell = new PdfPCell(new Paragraph("USER : "+SessionUtil.getUserSession(request).getLoginUserName(),normalFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        headTable.addCell(cell);
        
        cell = new PdfPCell(new Paragraph("",normalFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        headTable.addCell(cell);
        
        PdfPTable dataColTable = new PdfPTable(columnSize);        
        dataColTable.setWidthPercentage(100);
        
        int arrIndex=0;
        for(String lable:headerArray){
        	subCell = new PdfPCell(new Paragraph(lable,subHedFont));
        	subCell.setGrayFill(0.9f);
        	//subCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	subCell.setHorizontalAlignment(headerAlign[arrIndex]);
        	dataColTable.addCell(subCell);
        	arrIndex++;
        }
        
        PdfPTable dataTable = new PdfPTable(columnSize);        
        dataTable.setWidthPercentage(100);
        
        
        int count = 0;
    	int prntSubTot=0;
        for (Object object : dataList) {
        	for (int i = 0; i < dataField.length; i++) {
        		int alignment = Element.ALIGN_LEFT;
				Object returnValue = clazz.getMethod("get"+dataField[i]).invoke(object);
				if(returnValue!=null && Date.class.isInstance(returnValue))
        			returnValue = StrutsFormValidateUtil.parseString((Date)returnValue);
        		if(returnValue!=null && Double.class.isInstance(returnValue)){
        			returnValue = points2decimalFormat.format((Double)returnValue);
        			alignment = Element.ALIGN_RIGHT;
        		}
				
				subDataCell = new PdfPCell(new Paragraph(returnValue!=null?returnValue.toString():"",normalFont));
	        	subDataCell.setHorizontalAlignment(alignment);
	        	
	        	if(returnValue!=null && "Sub Total".equals(returnValue.toString()))
	        		prntSubTot = 1;
	        	
	        	if(prntSubTot==1)
	        		subDataCell.setGrayFill(0.9f);
	        	
	        		
	        	if((dataList.size()-1)==count && isPrintTot==1){
	        		subDataCell.setGrayFill(0.9f);
	        	}
	        	dataTable.addCell(subDataCell);
			}
        	count++;
        	prntSubTot=0;
		}
        
        cell = new PdfPCell(dataColTable);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        headTable.addCell(cell);
        
        cell = new PdfPCell(dataTable);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        headTable.addCell(cell);
        
        document.add(headTable);
       
        document.close();
        
		response.setContentType("application/pdf");
		
		pdfStream.writeTo(response.getOutputStream());
		pdfStream.flush();
	}
	
	
	public ActionForward printExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, List dataList, String[] dataField, Class clazz, String[] headerArray, String reportName, Date reportDate, String fileName) throws Exception {
		WritableCellFormat companyHeaderFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 12));
		WritableCellFormat reportNameCellFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));
		WritableCellFormat companyAddressFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));

		WritableCellFormat headerCellFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));

		WritableCellFormat headerCellFormatR = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));
		WritableCellFormat stringCellFormatt = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));
		WritableCellFormat stringCellFormattR = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));
		WritableCellFormat stringTotCellFormatt = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));

		WritableCellFormat intValueCellFormat = new WritableCellFormat(new NumberFormat("#0"));
		WritableCellFormat intTotValueCellFormat = new WritableCellFormat(new NumberFormat("#0"));

		WritableCellFormat decimalValueCellFormat = new WritableCellFormat(new NumberFormat("#,##0.00"));

		WritableCellFormat decimalAllTotalCellFormat = new WritableCellFormat(new NumberFormat("#,##0.00"));
		companyAddressFormat.setAlignment(Alignment.CENTRE);
		companyHeaderFormat.setAlignment(Alignment.CENTRE);

		decimalAllTotalCellFormat.setFont(new WritableFont(WritableFont.ARIAL, 10));
		decimalAllTotalCellFormat.setBackground(Colour.GREY_25_PERCENT);
		decimalAllTotalCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		decimalAllTotalCellFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
		decimalAllTotalCellFormat.setAlignment(Alignment.RIGHT);

		decimalValueCellFormat.setFont(new WritableFont(WritableFont.ARIAL, 10));
		decimalValueCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		decimalValueCellFormat.setAlignment(Alignment.RIGHT);

		intValueCellFormat.setFont(new WritableFont(WritableFont.ARIAL, 10));
		intValueCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		intValueCellFormat.setAlignment(Alignment.RIGHT);

		intTotValueCellFormat.setFont(new WritableFont(WritableFont.ARIAL, 10));
		intTotValueCellFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		intTotValueCellFormat.setBackground(Colour.GREY_25_PERCENT);
		intTotValueCellFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		intTotValueCellFormat.setBorder(Border.TOP, BorderLineStyle.THIN);
		intTotValueCellFormat.setBorder(Border.BOTTOM, BorderLineStyle.DOUBLE);
		intTotValueCellFormat.setAlignment(Alignment.RIGHT);

		stringCellFormatt.setAlignment(Alignment.LEFT);
		stringCellFormatt.setBorder(Border.ALL, BorderLineStyle.THIN);

		stringCellFormattR.setAlignment(Alignment.CENTRE);
		stringCellFormattR.setBorder(Border.ALL, BorderLineStyle.THIN);
		stringCellFormattR.setBackground(Colour.GREY_25_PERCENT);

		stringTotCellFormatt.setAlignment(Alignment.LEFT);
		stringTotCellFormatt.setBorder(Border.LEFT, BorderLineStyle.THIN);
		stringTotCellFormatt.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		stringTotCellFormatt.setBorder(Border.TOP, BorderLineStyle.THIN);
		stringTotCellFormatt.setBackground(Colour.GREY_25_PERCENT);
		stringTotCellFormatt.setBorder(Border.BOTTOM, BorderLineStyle.DOUBLE);

		headerCellFormatR.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerCellFormatR.setAlignment(Alignment.RIGHT);

		headerCellFormat.setBackground(Colour.GREY_25_PERCENT);
		headerCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

		reportNameCellFormat.setBackground(Colour.GREY_25_PERCENT);
		reportNameCellFormat.setAlignment(Alignment.CENTRE);
		WritableFont boldRedFont = new WritableFont(WritableFont.ARIAL, 10);
		boldRedFont.setColour(Colour.RED);
		WritableCellFormat boldRed = new WritableCellFormat(boldRedFont);
		boldRed.setBorder(Border.LEFT, BorderLineStyle.THIN);
		boldRed.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		boldRed.setBorder(Border.TOP, BorderLineStyle.THIN);
		boldRed.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PrintWriter writer = new PrintWriter(outputStream);
		WorkbookSettings settings = new WorkbookSettings();
		settings.setInitialFileSize(1);
		WritableWorkbook workBook = Workbook.createWorkbook(outputStream, settings);
		WritableSheet sheet = workBook.createSheet("Sheet1", 0);
		
		UserSessionConfig sessionConfig = (UserSessionConfig)SessionUtil.getUserSession(request);
		
		int headerWidth = headerArray.length;
		int sheetNumber = 1;
		
		sheet.addCell(new Label(0, 1, reportName, reportNameCellFormat));
		sheet.mergeCells(0, 1, headerWidth - 1, 1);
		
		sheet.addCell(new Label(0, 3, "REPORT DATE : " + StrutsFormValidateUtil.parseString(sessionConfig.getLoginDate()), headerCellFormat));
		sheet.mergeCells(0, 3, headerWidth - 1, 3);
		sheet.addCell(new Label(0, 4, "USER : " + sessionConfig.getLoginUserName(), headerCellFormat));
		sheet.mergeCells(0, 4, headerWidth - 1, 4);
		
		for (int j = 0; j < headerArray.length; j++) {
			sheet.addCell(new Label(j, 6, headerArray[j], headerCellFormat));
		}
		
		SheetSettings sheetSettings = sheet.getSettings();
		sheetSettings.setVerticalFreeze(7);
		
		int line = 7;
		
		int count = 0;
		WritableCellFormat cellValueFormat = null;
        for (Object object : dataList) {
        	for (int i = 0; i < dataField.length; i++) {
        		cellValueFormat = stringCellFormatt;
				Object returnValue = clazz.getMethod("get"+dataField[i]).invoke(object);
				if(returnValue!=null && Date.class.isInstance(returnValue))
        			returnValue = StrutsFormValidateUtil.parseString((Date)returnValue);
        		if(returnValue!=null && Double.class.isInstance(returnValue)){
        			returnValue = points2decimalFormat.format((Double)returnValue);
        			cellValueFormat = decimalValueCellFormat;
        		}
        		
	        	if((dataList.size()-1)==count){
	        		cellValueFormat = decimalAllTotalCellFormat;
	        	}
	        	
	        	sheet.addCell(new Label(i, line, returnValue!=null?returnValue.toString():"", cellValueFormat));
			}
        	count++;
        	line++;
		}
		
        workBook.write();
		workBook.close();
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-disposition", "inline; filename="+fileName+"_"+ StrutsFormValidateUtil.parseString(sessionConfig.getLoginDate()) + ".xls");
		response.setContentLength(outputStream.size());
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		ServletOutputStream stream = response.getOutputStream();
		outputStream.writeTo(stream);
		stream.flush();
		
		return null;
	}	
	
	public ActionForward getReportExcel1(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		/*MessageResources messageResources = getResources(request,"message");
		int branchId = 0;
		if(request.getParameter("branchId")!=null && !request.getParameter("branchId").equals(""))
			branchId=Integer.parseInt(request.getParameter("branchId"));
		
		List<ReportsDTO> dataList = null;
		try {
			dataList = getReportsBD().getReport1(SessionUtil.getUserSession(request), branchId);
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e, messageResources, getLocale(request)).toString());
		}
		
		/*getting total*/
		/*ReportsDTO total=new ReportsDTO();
		for(ReportsDTO reportsDTO:dataList){
			total.setRedemptionValue(total.getRedemptionValue()+reportsDTO.getRedemptionValue());
		}
		dataList.add(total);
		
		
		/*String headerArray[] = {"Branch","Client No","Pawning Ticket No","Redemption Value"};
		
		String dataField[] = {"Branch","ClientNo","PawningTicketNo","RedemptionValue"};
		
		printExcel(mapping, form, request, response, dataList, dataField, ReportsDTO.class, headerArray, "Report 1", SessionUtil.getUserSession(request).getLoginDate(), "Report 1");
		*/
		return null;
	}
	
	
	
	
	
	
	
	
	
}
