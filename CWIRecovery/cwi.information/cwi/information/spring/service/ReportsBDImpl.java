package cwi.information.spring.service;

import java.util.List;
import java.util.Date;

import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;
import cwi.information.dao.ReportsDAO;
import cwi.information.dto.HolidayDTO;
import cwi.information.dto.UserAccessRightsBranchWiseDTO;
import cwi.information.dto.UserAccessRightsOfficerWiseDTO;
import cwi.information.spring.ReportsBD;


public class ReportsBDImpl implements ReportsBD {	
	
	private ReportsDAO reportsDAO;

	public ReportsDAO getReportsDAO() {
		return reportsDAO;
	}

	public void setReportsDAO(ReportsDAO reportsDAO) {
		this.reportsDAO = reportsDAO;
	}
	
	public List<UserAccessRightsBranchWiseDTO> getUserAccessRights(UserSessionConfig userSessionConfig,int branchId)throws BasicException{
		return getReportsDAO().getUserAccessRights(userSessionConfig, branchId);
	}
	
	public List<UserAccessRightsOfficerWiseDTO> getUserAccessRightsOfficerWise(UserSessionConfig userSessionConfig,int officerId)throws BasicException{
		return getReportsDAO().getUserAccessRightsOfficerWise(userSessionConfig, officerId);
	}
	
	public List<HolidayDTO> getHolidays(UserSessionConfig userSessionConfig,Date dateFrom,Date dateTo)throws BasicException{
		return getReportsDAO().getHolidays(userSessionConfig, dateFrom, dateTo);
	}
	
	public String getOfficerName(int cashierId)throws BasicException{
		return getReportsDAO().getOfficerName(cashierId);
	}
}
