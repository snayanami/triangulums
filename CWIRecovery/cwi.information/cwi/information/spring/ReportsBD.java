package cwi.information.spring;

import java.util.List;
import java.util.Date;

import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;
import cwi.information.dto.HolidayDTO;
import cwi.information.dto.UserAccessRightsBranchWiseDTO;
import cwi.information.dto.UserAccessRightsOfficerWiseDTO;

public interface ReportsBD {
	public List<UserAccessRightsBranchWiseDTO> getUserAccessRights(UserSessionConfig userSessionConfig,int branchId)throws BasicException;
	public List<UserAccessRightsOfficerWiseDTO> getUserAccessRightsOfficerWise(UserSessionConfig userSessionConfig,int officerId)throws BasicException;
	public List<HolidayDTO> getHolidays(UserSessionConfig userSessionConfig,Date dateFrom,Date dateTo)throws BasicException;
	public String getOfficerName(int cashierId)throws BasicException;
}
