package cwi.admin.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SYSTEM_PROGRAM")
public class SystemProgram {
	
	private int systemProgramId;
	private String systemProgramCode;
	private String systemProgramName;
	private int parentProgramId;
	private String link;
	
	public SystemProgram(){}
	public SystemProgram(int systemProgramId){
		this.systemProgramId = systemProgramId;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="SYSTEM_PROGRAM_ID")
	public int getSystemProgramId() {
		return systemProgramId;
	}
	public void setSystemProgramId(int systemProgramId) {
		this.systemProgramId = systemProgramId;
	}
	
	@Column(name="SYSTEM_PROGRAM_CODE")
	public String getSystemProgramCode() {
		return systemProgramCode;
	}
	public void setSystemProgramCode(String systemProgramCode) {
		this.systemProgramCode = systemProgramCode;
	}
	
	@Column(name="SYSTEM_PROGRAM_NAME")
	public String getSystemProgramName() {
		return systemProgramName;
	}
	public void setSystemProgramName(String systemProgramName) {
		this.systemProgramName = systemProgramName;
	}
	
	@Column(name="PARENT_ID")
	public int getParentProgramId() {
		return parentProgramId;
	}
	public void setParentProgramId(int parentProgramId) {
		this.parentProgramId = parentProgramId;
	}
	
	@Column(name="LINK")
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
}