package cwi.admin.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GRP_PRG_EVENT")
public class GroupProgramEvent {

	private int groupProgramEventId;
	private int groupProgramId;
	private int eventId;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="GRP_PRG_EVENT_ID")
	public int getGroupProgramEventId() {
		return groupProgramEventId;
	}
	public void setGroupProgramEventId(int groupProgramEventId) {
		this.groupProgramEventId = groupProgramEventId;
	}
	
	@Column(name="GRP_PRG_ID")
	public int getGroupProgramId() {
		return groupProgramId;
	}
	public void setGroupProgramId(int groupProgramId) {
		this.groupProgramId = groupProgramId;
	}
	
	@Column(name="EVENT_ID")
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
}
