package cwi.admin.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="STAKEHOLDER")
public class Stakeholder {
	
	private int stakeholderId;
	private String stakeholderCode;
	private String initials;
	private String lastName;
	private String fullName;	//*Company Name*//
	private String gender;
	private String civilStatus;
	private String idBrNo;	//*NIC or BR*//
	private String corpIndividual;	//*I & C*//
	private Date dateOfBirth;
	private Date dateOfRegistration;	//C
	private String contactPersonName;	//C
	private String contactPersonDesignation;	//C
	private String contactPersonPhoneNo;	//C
	private String phoneNo;
	private String mobileNo;
	private String faxNo;
	private String emailAddress;
	private String address;
	private RefProgramData title;
	private int isMember;
	private int isBlackListed;
	private byte[] profileImage;
	
	public Stakeholder(){}
	public Stakeholder(int stakeholderId){
		this.stakeholderId = stakeholderId;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="STK_ID")
	public int getStakeholderId() {
		return stakeholderId;
	}
	public void setStakeholderId(int stakeholderId) {
		this.stakeholderId = stakeholderId;
	}
	
	@Column(name="INITIALS")
	public String getInitials() {
		return initials;
	}
	public void setInitials(String initials) {
		this.initials = initials;
	}
	
	@Column(name="LNAME")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name="FNAME")
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	@Column(name="GENDER")
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Column(name="CIVIL_STATUS")
	public String getCivilStatus() {
		return civilStatus;
	}
	public void setCivilStatus(String civilStatus) {
		this.civilStatus = civilStatus;
	}
	
	@Column(name="NIC_OR_BR")
	public String getIdBrNo() {
		return idBrNo;
	}
	public void setIdBrNo(String idBrNo) {
		this.idBrNo = idBrNo;
	}
	
	@Column(name="IND_OR_COR")
	public String getCorpIndividual() {
		return corpIndividual;
	}
	public void setCorpIndividual(String corpIndividual) {
		this.corpIndividual = corpIndividual;
	}
	
	@Column(name="DOB")
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	@Column(name="DOR")
	public Date getDateOfRegistration() {
		return dateOfRegistration;
	}
	public void setDateOfRegistration(Date dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}
	
	@Column(name="CONTACT_NAME")
	public String getContactPersonName() {
		return contactPersonName;
	}
	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}
	
	@Column(name="CONTACT_DESIGNATION")
	public String getContactPersonDesignation() {
		return contactPersonDesignation;
	}
	public void setContactPersonDesignation(String contactPersonDesignation) {
		this.contactPersonDesignation = contactPersonDesignation;
	}
	
	@Column(name="CONTACT_TPNO")
	public String getContactPersonPhoneNo() {
		return contactPersonPhoneNo;
	}
	public void setContactPersonPhoneNo(String contactPersonPhoneNo) {
		this.contactPersonPhoneNo = contactPersonPhoneNo;
	}
	
	@Column(name="TPNO")
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	@Column(name="MOBNO")
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	@Column(name="FAXNO")
	public String getFaxNo() {
		return faxNo;
	}
	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}
	
	@Column(name="EMAIL")
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	@Column(name="ADDRESS")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@ManyToOne(optional=true)
	@JoinColumn(name="TITLE")
	public RefProgramData getTitle() {
		return title;
	}
	public void setTitle(RefProgramData title) {
		this.title = title;
	}
	
	@Column(name="STK_CODE")
	public String getStakeholderCode() {
		return stakeholderCode;
	}
	public void setStakeholderCode(String stakeholderCode) {
		this.stakeholderCode = stakeholderCode;
	}
	
	@Column(name="IS_MEMBER")
	public int getIsMember() {
		return isMember;
	}
	public void setIsMember(int isMember) {
		this.isMember = isMember;
	}
	
	@Column(name="IS_BLACKLISTED")
	public int getIsBlackListed() {
		return isBlackListed;
	}
	public void setIsBlackListed(int isBlackListed) {
		this.isBlackListed = isBlackListed;
	}
	
	@Column(name="PROFILE_IMAGE")
	public byte[] getProfileImage() {
		return profileImage;
	}
	public void setProfileImage(byte[] profileImage) {
		this.profileImage = profileImage;
	}
}
