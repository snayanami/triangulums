package cwi.admin.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="REF_PROG_DATA")
public class RefProgramData {

	
	private int refProgDataId;
	private int refPrgId;
	private String refProgDataCode;
	private String refProgDataDescription;
	
	public RefProgramData(){}
	public RefProgramData(int refProgDataId){
		this.refProgDataId = refProgDataId;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="REF_PORG_DATA_ID")
	public int getRefProgDataId() {
		return refProgDataId;
	}
	public void setRefProgDataId(int refProgDataId) {
		this.refProgDataId = refProgDataId;
	}
	
	@Column(name="REF_PROG_DATA_CODE")
	public String getRefProgDataCode() {
		return refProgDataCode;
	}
	public void setRefProgDataCode(String refProgDataCode) {
		this.refProgDataCode = refProgDataCode;
	}
	
	@Column(name="REF_PROG_DATA_DES")
	public String getRefProgDataDescription() {
		return refProgDataDescription;
	}
	public void setRefProgDataDescription(String refProgDataDescription) {
		this.refProgDataDescription = refProgDataDescription;
	}
	
	@Column(name="REF_PROG_ID")
	public int getRefPrgId() {
		return refPrgId;
	}
	public void setRefPrgId(int refPrgId) {
		this.refPrgId = refPrgId;
	}
		
}