package cwi.admin.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BRN_OFF_GRP_MAP")
public class BranchOfficerGroupMapping {
	
	private int branchOfficerGroupMappingId;
	private int referenceProgramDataId; //*Access Group Id*//
	private int branchOfficerId;
	private int isDefault;
	
	@Column(name="IS_DEFAULT")
	public int getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}
	public BranchOfficerGroupMapping(){}
	public BranchOfficerGroupMapping(int branchOfficerGroupMappingId){
		this.branchOfficerGroupMappingId = branchOfficerGroupMappingId;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="BRANCH_OFF_GRP_MAP_ID")
	public int getBranchOfficerGroupMappingId() {
		return branchOfficerGroupMappingId;
	}
	public void setBranchOfficerGroupMappingId(int branchOfficerGroupMappingId) {
		this.branchOfficerGroupMappingId = branchOfficerGroupMappingId;
	}
	
	@Column(name="ACCESS_GRP_ID")
	public int getReferenceProgramDataId() {
		return referenceProgramDataId;
	}
	public void setReferenceProgramDataId(int referenceProgramDataId) {
		this.referenceProgramDataId = referenceProgramDataId;
	}
	
	@Column(name="BRANCH_OFFICER_ID")
	public int getBranchOfficerId() {
		return branchOfficerId;
	}
	public void setBranchOfficerId(int branchOfficerId) {
		this.branchOfficerId = branchOfficerId;
	}
}
