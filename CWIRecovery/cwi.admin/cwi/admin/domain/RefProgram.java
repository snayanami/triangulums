package cwi.admin.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="REF_PROG")
public class RefProgram {

	private int refProgId;
	private String refProgCode;
	private String refProgDescription;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="REF_PROG_ID")
	public int getRefProgId() {
		return refProgId;
	}
	public void setRefProgId(int refProgId) {
		this.refProgId = refProgId;
	}
	
	@Column(name="REF_PROG_CODE")
	public String getRefProgCode() {
		return refProgCode;
	}
	public void setRefProgCode(String refProgCode) {
		this.refProgCode = refProgCode;
	}
	
	@Column(name="REF_PROG_DES")
	public String getRefProgDescription() {
		return refProgDescription;
	}
	public void setRefProgDescription(String refProgDescription) {
		this.refProgDescription = refProgDescription;
	}	
}