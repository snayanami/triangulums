package cwi.admin.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BLACKLIST_CLIENT_HISTORY")
public class BlackListClientHistory extends SuperCommon {
	private int blackListClientHistoryId;
	private int clientId;
	private String reason;
	private int blackListStatus;	//1 = Black Listed, 0 = Active
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="BLACKLIST_CLIENT_HISTORY_ID")
	public int getBlackListClientHistoryId() {
		return blackListClientHistoryId;
	}
	public void setBlackListClientHistoryId(int blackListClientHistoryId) {
		this.blackListClientHistoryId = blackListClientHistoryId;
	}
	
	@Column(name="CLIENTID")
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	
	@Column(name="REASON")
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@Column(name="BLACKLIST_STS")
	public int getBlackListStatus() {
		return blackListStatus;
	}
	public void setBlackListStatus(int blackListStatus) {
		this.blackListStatus = blackListStatus;
	}
}
