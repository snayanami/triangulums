package cwi.admin.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BRANCH_OFFICER")
public class BranchOfficer extends SuperCommon {
	
	private int branchOfficerId;
	private int branchId;
	private int officerId;
	private int isDefault;
	private int referenceProgramDataId; //*Access Group Id*//
	
	
//	public int getAccessGroupId() {
//		return accessGroupId;
//	}
//	public void setAccessGroupId(int accessGroupId) {
//		this.accessGroupId = accessGroupId;
//	}
	public BranchOfficer(){}
	public BranchOfficer(int branchOfficerId){
		this.branchOfficerId = branchOfficerId;
	}
	
	@Column(name="ACC_GRP_ID")
	public int getReferenceProgramDataId() {
		return referenceProgramDataId;
	}
	public void setReferenceProgramDataId(int referenceProgramDataId) {
		this.referenceProgramDataId = referenceProgramDataId;
	}
		
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="BRANCH_OFFICER_ID")
	public int getBranchOfficerId() {
		return branchOfficerId;
	}
	public void setBranchOfficerId(int branchOfficerId) {
		this.branchOfficerId = branchOfficerId;
	}
	
	@Column(name="BRANCH_ID")
	public int getBranchId() {
		return branchId;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	
	@Column(name="OFFICER_ID")
	public int getOfficerId() {
		return officerId;
	}
	public void setOfficerId(int officerId) {
		this.officerId = officerId;
	}
	
	@Column(name="IS_DEFAULT")
	public int getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}	
}
