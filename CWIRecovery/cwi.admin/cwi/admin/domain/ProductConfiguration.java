package cwi.admin.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PRODUCT_CONFIG")
public class ProductConfiguration extends SuperCommon {
	private int productConfigurationId;
	private String productCode;
	private String productName;
	private Date effectiveDate;
	private String frequency; //D - Daily, M - Monthly
	private int period;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="PRODUCT_CONFIG_ID")
	public int getProductConfigurationId() {
		return productConfigurationId;
	}
	public void setProductConfigurationId(int productConfigurationId) {
		this.productConfigurationId = productConfigurationId;
	}
	
	@Column(name="PRODUCT_CODE")
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	@Column(name="PRODUCT_NAME")
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	@Column(name="EFF_DATE")
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	@Column(name="FREQ")
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
	@Column(name="PERIOD")
	public int getPeriod() {
		return period;
	}
	public void setPeriod(int period) {
		this.period = period;
	}
}
