package cwi.admin.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="STK_TYPE_MAP")
public class StakeholderTypeMapping {
	
	private int stakeholderTypeMappingId;
	private int stakeholderId;
	private int referenceProgramDataId; //*Stakeholder Type Id*//
	
	public StakeholderTypeMapping(){}
	public StakeholderTypeMapping(int stakeholderTypeMappingId){
		this.stakeholderTypeMappingId = stakeholderTypeMappingId;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="STK_TYPE_MAP_ID")
	public int getStakeholderTypeMappingId() {
		return stakeholderTypeMappingId;
	}
	public void setStakeholderTypeMappingId(int stakeholderTypeMappingId) {
		this.stakeholderTypeMappingId = stakeholderTypeMappingId;
	}
	
	@Column(name="STK_ID")
	public int getStakeholderId() {
		return stakeholderId;
	}
	public void setStakeholderId(int stakeholderId) {
		this.stakeholderId = stakeholderId;
	}
	
	@Column(name="STK_TYPE_ID")
	public int getReferenceProgramDataId() {
		return referenceProgramDataId;
	}
	public void setReferenceProgramDataId(int referenceProgramDataId) {
		this.referenceProgramDataId = referenceProgramDataId;
	}
}
