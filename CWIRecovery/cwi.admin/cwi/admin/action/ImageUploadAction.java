package cwi.admin.action;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.FileRenamePolicy;

import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.ImageUploadRenameUtil;


public class ImageUploadAction extends CommonDispatch{
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println(request.getContentType());
		String webTempPath=request.getSession().getServletContext().getRealPath("/userImages/");
		FileRenamePolicy frp=new ImageUploadRenameUtil(request.getSession().getId());
		MultipartRequest mpr = new MultipartRequest(request,webTempPath,5*1024*1024, "multipart/form-data", frp);
		Enumeration enumr = mpr.getFileNames();
		while(enumr.hasMoreElements()) {
			System.out.println(enumr.nextElement());
		}
		System.out.println(enumr.toString());
		return super.execute(mapping, form, request, response);
	}
}