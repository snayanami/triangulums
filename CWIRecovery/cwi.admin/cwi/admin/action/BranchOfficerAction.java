package cwi.admin.action;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;
import org.json.JSONObject;

import cwi.admin.domain.Branch;
import cwi.admin.domain.BranchOfficer;
import cwi.admin.domain.RefProgramData;
import cwi.admin.domain.Stakeholder;
import cwi.admin.dto.BranchOfficerDTO;
import cwi.admin.spring.BranchBD;
import cwi.admin.spring.BranchOfficerBD;
import cwi.admin.spring.RefProgramDataBD;
import cwi.admin.spring.StakeholderBD;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;


public class BranchOfficerAction extends CommonDispatch {

	private BranchOfficerBD branchOfficerBD;
	private StakeholderBD stakeholderBD;
	private BranchBD branchBD;
	private RefProgramDataBD refProgramDataBD;
	public StakeholderBD getStakeholderBD() {
		return stakeholderBD;
	}
	public void setStakeholderBD(StakeholderBD stakeholderBD) {
		this.stakeholderBD = stakeholderBD;
	}
	public BranchOfficerBD getBranchOfficerBD() {
		return branchOfficerBD;
	}
	public void setBranchOfficerBD(BranchOfficerBD branchOfficerBD) {
		this.branchOfficerBD = branchOfficerBD;
	}
	public BranchBD getBranchBD() {
		return branchBD;
	}
	public void setBranchBD(BranchBD branchBD) {
		this.branchBD = branchBD;
	}
	public RefProgramDataBD getRefProgramDataBD() {
		return refProgramDataBD;
	}
	public void setRefProgramDataBD(RefProgramDataBD refProgramDataBD) {
		this.refProgramDataBD = refProgramDataBD;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int officerId = 0;
    		if(request.getParameter("officerId")!=null && !request.getParameter("officerId").equals(""))
    			officerId=Integer.parseInt(request.getParameter("officerId"));
    		int branchId = 0;
    		if(request.getParameter("branchId")!=null && !request.getParameter("branchId").equals(""))
    			branchId=Integer.parseInt(request.getParameter("branchId"));
    		int accessGroupId = 0;
    		if(request.getParameter("accessGroupId")!=null && !request.getParameter("accessGroupId").equals(""))
    			accessGroupId=Integer.parseInt(request.getParameter("accessGroupId"));
    		int isDefault = 0;
    		if(request.getParameter("isDefault")!=null && !request.getParameter("isDefault").equals(""))
    			isDefault=Integer.parseInt(request.getParameter("isDefault"));
        	
    		try {
    			BranchOfficer branchOfficer = new BranchOfficer();
    			branchOfficer.setBranchId(branchId);
    			branchOfficer.setOfficerId(officerId);
    			branchOfficer.setReferenceProgramDataId(accessGroupId);
    			branchOfficer.setIsDefault(isDefault);
    			
    			getBranchOfficerBD().createBranchOfficer(SessionUtil.getUserSession(request), branchOfficer);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int branchOfficerId = 0;
    		if(request.getParameter("branchOfficerId")!=null && !request.getParameter("branchOfficerId").equals(""))
    			branchOfficerId=Integer.parseInt(request.getParameter("branchOfficerId"));
    		int officerId = 0;
    		if(request.getParameter("officerId")!=null && !request.getParameter("officerId").equals(""))
    			officerId=Integer.parseInt(request.getParameter("officerId"));
    		int branchId = 0;
    		if(request.getParameter("branchId")!=null && !request.getParameter("branchId").equals(""))
    			branchId=Integer.parseInt(request.getParameter("branchId"));
    		int accessGroupId = 0;
    		if(request.getParameter("accessGroupId")!=null && !request.getParameter("accessGroupId").equals(""))
    			accessGroupId=Integer.parseInt(request.getParameter("accessGroupId"));
    		int isDefault = 0;
    		if(request.getParameter("isDefault")!=null && !request.getParameter("isDefault").equals(""))
    			isDefault=Integer.parseInt(request.getParameter("isDefault"));
    		try {
    			BranchOfficer branchOfficer = getBranchOfficerBD().getBranchOfficerById(SessionUtil.getUserSession(request), branchOfficerId);

    			branchOfficer.setBranchId(branchId);
    			branchOfficer.setOfficerId(officerId);
    			branchOfficer.setReferenceProgramDataId(accessGroupId);
    			branchOfficer.setIsDefault(isDefault);
    			
    			getBranchOfficerBD().updateBranchOfficer(SessionUtil.getUserSession(request), branchOfficer);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageUpdateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
        MessageResources messageResources = getResources(request,"message");
		
        int branchOfficerId = 0;
		if(request.getParameter("branchOfficerId")!=null && !request.getParameter("branchOfficerId").equals(""))
			branchOfficerId=Integer.parseInt(request.getParameter("branchOfficerId"));
		try {
			getBranchOfficerBD().deleteBranchOfficer(SessionUtil.getUserSession(request), branchOfficerId);
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
			return null;
		}
		response.getWriter().write(StrutsFormValidateUtil.getMessageDeleteSuccess(messageResources).toString());
		return null;
	}
	
	public ActionForward getAllBranchOfficerByOfficerId(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		int officerId = 0;
		if(request.getParameter("officerId")!=null && !request.getParameter("officerId").equals(""))
			officerId=Integer.parseInt(request.getParameter("officerId"));
		try {
			List<BranchOfficer> list = getBranchOfficerBD().getAllBranchOfficerByOfficerId(SessionUtil.getUserSession(request), officerId);
			JSONArray mainArray = new JSONArray();
			for(BranchOfficer branchOfficer : list){
				
				Branch branch = getBranchBD().getBranchById(SessionUtil.getUserSession(request), branchOfficer.getBranchId());
				RefProgramData accessGroup = getRefProgramDataBD().getRefProgramDataById(SessionUtil.getUserSession(request), branchOfficer.getReferenceProgramDataId());
				
				JSONArray array = new JSONArray();
				
				/*00*/array.put(branch.getBranchName());
				/*01*/array.put(accessGroup.getRefProgDataDescription());
				/*02*/array.put(branchOfficer.getIsDefault()==1?"Yes":branchOfficer.getIsDefault()==0?"No":"");
				/*03*/array.put(branchOfficer.getBranchId());
				/*04*/array.put(branch.getBranchCode());
				/*05*/array.put(branchOfficer.getReferenceProgramDataId());
				/*06*/array.put(accessGroup.getRefProgDataCode());
				/*07*/array.put(branchOfficer.getBranchOfficerId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public ActionForward addBranchOfficer(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages actionMessages = form.validate(mapping, request);
		MessageResources messageResources = getResources(request,"message");
		
		if(!actionMessages.isEmpty())
			response.getWriter().write(StrutsFormValidateUtil.getMessages(request, actionMessages,messageResources,getLocale(request),null).toString());
		else{
			int branchId = Integer.parseInt(request.getParameter("branchId"));
			int officerId = Integer.parseInt(request.getParameter("officerId"));
			int isDefault = Integer.parseInt(request.getParameter("isDefault"));
			try {
				BranchOfficer branchOfficer = new BranchOfficer();
				branchOfficer.setBranchId(Integer.parseInt(request.getParameter("branchId")));
				branchOfficer.setOfficerId(Integer.parseInt(request.getParameter("officerId")));
				branchOfficer.setIsDefault(Integer.parseInt(request.getParameter("isDefault")));
				getBranchOfficerBD().addBranchOfficer(branchOfficer);
				JSONObject object = new JSONObject();
				response.getWriter().write(object.put("success", "Branch Officer added successfuly.").toString());
								
			} catch (BasicDataAccessException e) {
				JSONObject object = new JSONObject();
				response.getWriter().write(object.put("error", e.getErrorCode()).toString());
			}
		}			
		return null;
	}
	public ActionForward getAllBranchOfficer(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		Collection<BranchOfficerDTO> collection = getBranchOfficerBD().getAllBranchOfficer();
		JSONArray array = new JSONArray();
		for(BranchOfficerDTO branchOfficerDTO : collection){
			JSONArray subArr = new JSONArray();
			
			subArr.put(branchOfficerDTO.getOfficerName());
			subArr.put(branchOfficerDTO.getBranchName());
			subArr.put(branchOfficerDTO.getAccessGruopName());
			subArr.put((branchOfficerDTO.getIsDefault()==1)?"Yes":"No");
			
			subArr.put(branchOfficerDTO.getBranchOfficerId());
			subArr.put(branchOfficerDTO.getOffiderId());
			subArr.put(branchOfficerDTO.getBranchId());
			subArr.put(branchOfficerDTO.getBranchCode());
			subArr.put(branchOfficerDTO.getAccessGruopId());
			subArr.put(branchOfficerDTO.getAccessGruopCode());
			
			array.put(subArr);
		}			
		try {
			JSONObject object = new JSONObject();
			response.getWriter().write(array.toString());
		} catch (BasicDataAccessException e) {
			JSONObject object = new JSONObject();
			response.getWriter().write(object.put("error", e.getErrorCode()).toString());
		}
		return null;
	}	
	public ActionForward getBranchOfficerByBranchId(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		try {
			int a = Integer.parseInt(request.getParameter("branchId"));
			Collection<BranchOfficer> collection = getBranchOfficerBD().getAllBranchOfficerByBranchId(Integer.parseInt(request.getParameter("branchId")));
			JSONArray array = new JSONArray();
			for(BranchOfficer branchOfficer : collection){
				JSONArray subArr = new JSONArray();
				Stakeholder stakeholder  = getBranchOfficerBD().getBranchOfficerDetailsById(branchOfficer.getBranchOfficerId());
				subArr.put(stakeholder.getInitials()+" "+stakeholder.getLastName());
				array.put(subArr);
			}
			JSONObject object = new JSONObject();
			response.getWriter().write(array.toString());
		} catch (BasicDataAccessException e) {
			JSONObject object = new JSONObject();
			response.getWriter().write(object.put("error", e.getErrorCode()).toString());
		}
		return null;
	}
}