package cwi.admin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONObject;

import cwi.admin.domain.SystemProgram;
import cwi.admin.spring.SystemProgramBD;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.StrutsFormValidateUtil;


public class SystemProgramAction extends CommonDispatch {

	private SystemProgramBD systemProgramBD;

	public SystemProgramBD getSystemProgramBD() {
		return systemProgramBD;
	}
	public void setSystemProgramBD(SystemProgramBD systemProgramBD) {
		this.systemProgramBD = systemProgramBD;
	}
	
	public ActionForward addSystemProgram(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages actionMessages = form.validate(mapping, request);
		MessageResources messageResources = getResources(request,"message");
		
		if(!actionMessages.isEmpty())
			response.getWriter().write(StrutsFormValidateUtil.getMessages(request, actionMessages,messageResources,getLocale(request),null).toString());
		else{
			SystemProgram systemProgram = new SystemProgram();
			systemProgram.setSystemProgramCode(request.getParameter("systemProgramCode"));
			systemProgram.setSystemProgramName(request.getParameter("systemProgramName"));
			systemProgram.setParentProgramId(Integer.parseInt(request.getParameter("parentProgramId")));
			try {
				getSystemProgramBD().addSystemProgram(systemProgram);
			} catch (BasicDataAccessException e) {
				JSONObject object = new JSONObject();
				response.getWriter().write(object.put("error", e.getErrorCode()).toString());
			}
		}			
		return null;
	}	
}