package cwi.admin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONObject;

import cwi.admin.domain.GroupProgramEvent;
import cwi.admin.spring.GroupProgramEventBD;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.StrutsFormValidateUtil;


public class GroupProgramEventAction extends CommonDispatch{

	private GroupProgramEventBD groupProgramEventBD;

	public GroupProgramEventBD getGroupProgramEventBD() {
		return groupProgramEventBD;
	}
	public void setGroupProgramEventBD(GroupProgramEventBD groupProgramEventBD) {
		this.groupProgramEventBD = groupProgramEventBD;
	}
	
	public ActionForward addBranch(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages actionMessages = form.validate(mapping, request);
		MessageResources messageResources = getResources(request,"message");
		
		if(!actionMessages.isEmpty())
			response.getWriter().write(StrutsFormValidateUtil.getMessages(request, actionMessages,messageResources,getLocale(request),null).toString());
		else{
			GroupProgramEvent groupProgramEvent = new GroupProgramEvent();
			groupProgramEvent.setGroupProgramEventId(Integer.parseInt(request.getParameter("groupProgramEventId")));
			groupProgramEvent.setGroupProgramId(Integer.parseInt(request.getParameter("groupProgramId")));
			try {
				getGroupProgramEventBD().addGruopProgramEvent(groupProgramEvent);
				//JSONObject object = new JSONObject();
				//response.getWriter().write("");
			} catch (BasicDataAccessException e) {
				JSONObject object = new JSONObject();
				response.getWriter().write(object.put("error", e.getErrorCode()).toString());
			}
		}			
		return null;
	}
}