package cwi.admin.action;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONArray;
import org.json.JSONObject;

import cwi.admin.domain.BranchOfficer;
import cwi.admin.domain.RefProgramData;
import cwi.admin.domain.Stakeholder;
import cwi.admin.spring.RefProgramDataBD;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.ReferenceProgramEnum;


public class AccessGroupAction extends CommonDispatch{

	private RefProgramDataBD refProgramDataBD;
	
	public RefProgramDataBD getRefProgramDataBD() {
		return refProgramDataBD;
	}
	public void setRefProgramDataBD(RefProgramDataBD refProgramDataBD) {
		this.refProgramDataBD = refProgramDataBD;
	}


	public ActionForward getAllAccessGroup(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		//int id = ReferenceProgramEnum.ACCESS_GROUP;
		
//		Collection<RefProgramData> collection = getRefProgramDataBD().getAllByrefProgId(ReferenceProgramEnum.ACCESS_GROUP);
//		JSONArray array = new JSONArray();
//		for(RefProgramData refProgramData : collection){
//			JSONArray subArr = new JSONArray();
//			subArr.put(refProgramData.getRefProgDataCode());
//			subArr.put(refProgramData.getRefProgDataDescription());
//			subArr.put(refProgramData.getRefProgDataId());
//			subArr.put(refProgramData.getRefProgId());
//			array.put(subArr);
//		}
//		JSONObject object = new JSONObject();
//		response.getWriter().write(array.toString());
		return null;
	}
}
