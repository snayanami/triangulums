package cwi.admin.action;

import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;
import org.json.JSONObject;

import cwi.admin.domain.BlackListClientHistory;
import cwi.admin.domain.Branch;
import cwi.admin.domain.Stakeholder;
import cwi.admin.spring.BlackListClientBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;

public class BlackListClientAction extends CommonDispatch {
	static DecimalFormat points2decimalFormat = new DecimalFormat();
	static {
		points2decimalFormat.setMinimumFractionDigits(2);
		points2decimalFormat.setMaximumFractionDigits(2);
		points2decimalFormat.setGroupingSize(3);
	}
	
	private BlackListClientBD blackListClientBD;
	public BlackListClientBD getBlackListClientBD() {
		return blackListClientBD;
	}
	public void setBlackListClientBD(BlackListClientBD blackListClientBD) {
		this.blackListClientBD = blackListClientBD;
	}
	
	public ActionForward getIsBlackListed(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		int clientId = 0;
		if(request.getParameter("clientId")!=null && !request.getParameter("clientId").equals(""))
			clientId=Integer.parseInt(request.getParameter("clientId"));
		try{
			int isBlackLIsted = getBlackListClientBD().getIsBlackListed(SessionUtil.getUserSession(request), clientId);
			JSONObject object = new JSONObject();
			object.put("isBlackListed", isBlackLIsted);
			response.getWriter().write(object.toString());
		}catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
			return null;
		}
		return null;
	}
	
	public ActionForward getGridData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
//		MessageResources messageResources = getResources(request,"message");
//		int clientId = 0;
//		if(request.getParameter("clientId")!=null && !request.getParameter("clientId").equals(""))
//			clientId=Integer.parseInt(request.getParameter("clientId"));
//		try {
//			List<PawnDetailsDTO> list = getBlackListClientBD().getPawnDetailsByClientId(SessionUtil.getUserSession(request), clientId);
//			JSONArray mainArray = new JSONArray();
//			for (PawnDetailsDTO pawnDetailsDTO : list) {
//				JSONArray array = new JSONArray();
//				/*00*/array.put(pawnDetailsDTO.getPawnTicketNo());
//				/*01*/array.put(points2decimalFormat.format(pawnDetailsDTO.getPawningAdvance()));
//				/*02*/array.put(points2decimalFormat.format(pawnDetailsDTO.getBalanceCapital()));
//				/*03*/array.put(points2decimalFormat.format(pawnDetailsDTO.getInterestInArrears()));
//				/*04*/array.put(points2decimalFormat.format(pawnDetailsDTO.getOtherChargesArrears()));
//				/*05*/array.put(points2decimalFormat.format(pawnDetailsDTO.getTotalOutstanding()));
//				/*06*/array.put(pawnDetailsDTO.getPawningTicketStatus());
//				mainArray.put(array);
//			}
//			response.getWriter().write(mainArray.toString());
//		} catch (BasicException e) {
//			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
//		}
        return null;
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int status = 0;
    		if(request.getParameter("status")!=null && !request.getParameter("status").equals(""))
    			status=Integer.parseInt(request.getParameter("status"));
        	int clientId = 0;
    		if(request.getParameter("clientId")!=null && !request.getParameter("clientId").equals(""))
    			clientId=Integer.parseInt(request.getParameter("clientId"));
        	String reason = request.getParameter("reason");
    		
    		try {
    			BlackListClientHistory blackListClientHistory = new BlackListClientHistory();
    			
    			blackListClientHistory.setClientId(clientId);
    			blackListClientHistory.setReason(reason);
    			blackListClientHistory.setBlackListStatus(status==0?1:0);
    			
    			getBlackListClientBD().updateBlackListClient(SessionUtil.getUserSession(request), blackListClientHistory);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageUpdateSuccess(messageResources).toString());
        }
		return null;
	}
}
