package cwi.admin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONObject;

import cwi.admin.domain.GroupProgram;
import cwi.admin.spring.GroupProgramBD;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.StrutsFormValidateUtil;


public class GroupProgramAction extends CommonDispatch{

	private GroupProgramBD groupProgramBD;

	public GroupProgramBD getGroupProgramBD() {
		return groupProgramBD;
	}
	public void setGroupProgramBD(GroupProgramBD groupProgramBD) {
		this.groupProgramBD = groupProgramBD;
	}
	
	public ActionForward addRefProgramData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages actionMessages = form.validate(mapping, request);
		MessageResources messageResources = getResources(request,"message");
		
		if(!actionMessages.isEmpty())
			response.getWriter().write(StrutsFormValidateUtil.getMessages(request, actionMessages,messageResources,getLocale(request),null).toString());
		else{
			GroupProgram groupProgram = new GroupProgram();
			groupProgram.setReferenceProgramDataId(Integer.parseInt(request.getParameter("referenceProgramDataId")));
			groupProgram.setSystemProgramId(Integer.parseInt(request.getParameter("systemProgramId")));
			try {
				getGroupProgramBD().addGroupProgram(groupProgram);
				//JSONObject object = new JSONObject();
				//response.getWriter().write("");
			} catch (BasicDataAccessException e) {
				JSONObject object = new JSONObject();
				response.getWriter().write(object.put("error", e.getErrorCode()).toString());
			}
		}			
		return null;
	}
}