package cwi.admin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.util.MessageResources;

import cwi.admin.domain.Officer;
import cwi.admin.domain.RefProgramData;
import cwi.admin.spring.ChangePasswordBD;
import cwi.admin.spring.OfficerBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.PasswordService;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;

public class ChangePasswordAction extends CommonDispatch {
	private ChangePasswordBD changePasswordBD;
	private OfficerBD officerBD;
	public ChangePasswordBD getChangePasswordBD() {
		return changePasswordBD;
	}
	public void setChangePasswordBD(ChangePasswordBD changePasswordBD) {
		this.changePasswordBD = changePasswordBD;
	}
	public OfficerBD getOfficerBD() {
		return officerBD;
	}
	public void setOfficerBD(OfficerBD officerBD) {
		this.officerBD = officerBD;
	}
	
	public ActionForward loadTabPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		if (!SessionUtil.isValidSession(request)) {
			return mapping.findForward("sessionError");
		}
		Officer officer = null;
		try{
			officer = getOfficerBD().getOfficerById(SessionUtil.getUserSession(request), SessionUtil.getUserSession(request).getOfficerId());
		}catch (Exception e) {
			System.out.println(e.toString());
		}
		
		if(officer!=null){
			request.setAttribute("userId", officer.getOfficerId());
			request.setAttribute("userName", officer.getUserName());
			request.setAttribute("currentPassword", officer.getPassword());
		}
		
		String action = request.getParameter("action");
		DynaActionForm frm = (DynaActionForm) form;
		frm.initialize(mapping);
        frm.set("action",action);
        String page = request.getParameter("page");
		return mapping.findForward(page);
	}
	
	public ActionForward changePassword(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int officerId = 0;
    		if(request.getParameter("officerId")!=null && !request.getParameter("officerId").equals(""))
    			officerId=Integer.parseInt(request.getParameter("officerId"));
    		String newPassword = PasswordService.encrypt(PasswordService.decrypt(request.getParameter("newPassword")).toUpperCase());
    		String currentPassword = PasswordService.encrypt(PasswordService.decrypt(request.getParameter("currentPassword")).toUpperCase());
    		try {
    			getChangePasswordBD().changePassword(SessionUtil.getUserSession(request), officerId, currentPassword, newPassword);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageUpdateSuccess(messageResources).toString());
        }
		return null;
	}
}
