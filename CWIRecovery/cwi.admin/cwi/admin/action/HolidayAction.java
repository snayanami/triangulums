package cwi.admin.action;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;
import org.json.JSONObject;

import cwi.admin.domain.Holiday;
import cwi.admin.spring.HolidayBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;


public class HolidayAction extends CommonDispatch {
	private HolidayBD holidayBD;
	public HolidayBD getHolidayBD() {
		return holidayBD;
	}
	public void setHolidayBD(HolidayBD holidayBD) {
		this.holidayBD = holidayBD;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
			
			String Dates = (request.getParameter("strDate"));
			String arr[] = Dates.split("<sri>");
			Date dateArr[] =  new Date[arr.length];
			String reason = request.getParameter("reason");
			
			for (int i = 0; i < arr.length; i++) {
				dateArr[i] = StrutsFormValidateUtil.parseDate(arr[i]);
			}
			
			try {
				List<Holiday> holidayList = getHolidayBD().createHoliday(SessionUtil.getUserSession(request),dateArr,reason);
				JSONObject messageObject = new JSONObject();
				if (holidayList==null || holidayList.isEmpty()) {
					messageObject.put("createSuccess",messageResources.getMessage("msg.createsuccess"));
				}else{
					String dateStr="These Dates are Already Exist ";
					for (Holiday holiday : holidayList) {
						if(holidayList.size()-1==holidayList.indexOf(holiday)){
							dateStr+=StrutsFormValidateUtil.parseString(holiday.getHoliday());
						}else{
							dateStr+=StrutsFormValidateUtil.parseString(holiday.getHoliday())+" - ";	
						}
					}
					messageObject.put("error",dateStr);
				}
				response.getWriter().write(messageObject.toString());
			} catch (BasicException ex) {
				response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(ex,messageResources, getLocale(request)).toString());
				return null;
			}
		}
		return null;
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int holidayId = 0;
    		if(request.getParameter("holidayId")!=null && !request.getParameter("holidayId").equals(""))
    			holidayId=Integer.parseInt(request.getParameter("holidayId"));
        	Date holiday = StrutsFormValidateUtil.parseDate(request.getParameter("holiday"));
    		String reason = request.getParameter("reason");
    		try {
    			Holiday holidayObj = getHolidayBD().getHolidayById(SessionUtil.getUserSession(request), holidayId);

    			holidayObj.setHoliday(holiday);
    			holidayObj.setReason(reason);
    			
    			getHolidayBD().updateHoliday(SessionUtil.getUserSession(request), holidayObj);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageUpdateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
        MessageResources messageResources = getResources(request,"message");
		
    	int holidayId = 0;
		if(request.getParameter("holidayId")!=null && !request.getParameter("holidayId").equals(""))
			holidayId=Integer.parseInt(request.getParameter("holidayId"));
		try {
			getHolidayBD().deleteHoliday(SessionUtil.getUserSession(request), holidayId);
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
			return null;
		}
		response.getWriter().write(StrutsFormValidateUtil.getMessageDeleteSuccess(messageResources).toString());
		return null;
	}
	
	public ActionForward getAllHoliday(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<Holiday> list = getHolidayBD().getAllHoliday(SessionUtil.getUserSession(request));
			JSONArray mainArray = new JSONArray();
			for(Holiday holiday : list){
				JSONArray array = new JSONArray();
				array.put(StrutsFormValidateUtil.parseString(holiday.getHoliday()));
				array.put(holiday.getReason());
				array.put(holiday.getHolidayId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
}
