package cwi.admin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONObject;

import cwi.admin.domain.ProgramEvent;
import cwi.admin.spring.ProgramEventBD;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.StrutsFormValidateUtil;


public class ProgramEventAction extends CommonDispatch {

	private ProgramEventBD programEventBD;

	public ProgramEventBD getProgramEventBD() {
		return programEventBD;
	}
	public void setProgramEventBD(ProgramEventBD programEventBD) {
		this.programEventBD = programEventBD;
	}
	
	public ActionForward addProgramEvent(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages actionMessages = form.validate(mapping, request);
		MessageResources messageResources = getResources(request,"message");
		
		if(!actionMessages.isEmpty())
			response.getWriter().write(StrutsFormValidateUtil.getMessages(request, actionMessages,messageResources,getLocale(request),null).toString());
		else{
			ProgramEvent programEvent = new ProgramEvent();
			programEvent.setSystemProgramId(Integer.parseInt(request.getParameter("systemProgramId")));
			programEvent.setEvent(Integer.parseInt(request.getParameter("event")));
			try {
				getProgramEventBD().addProgramEvent(programEvent);
				//JSONObject object = new JSONObject();
				//response.getWriter().write("");
			} catch (BasicDataAccessException e) {
				JSONObject object = new JSONObject();
				response.getWriter().write(object.put("error", e.getErrorCode()).toString());
			}
		}			
		return null;
	}
}