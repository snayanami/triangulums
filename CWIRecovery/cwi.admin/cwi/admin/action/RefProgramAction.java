package cwi.admin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;

import cwi.admin.domain.RefProgram;
import cwi.admin.spring.RefProgramBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;




public class RefProgramAction extends CommonDispatch{

	private RefProgramBD refProgramBD;
	public RefProgramBD getRefProgramBD() {
		return refProgramBD;
	}
	public void setRefProgramBD(RefProgramBD refProgramBD) {
		this.refProgramBD = refProgramBD;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	String code = request.getParameter("code");
    		String description = request.getParameter("description");
    		try {
    			RefProgram refProgram = new RefProgram();
    			refProgram.setRefProgCode(code);
    			refProgram.setRefProgDescription(description);
    			
    			getRefProgramBD().createRefProgram(SessionUtil.getUserSession(request), refProgram);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int refProgramId = 0;
    		if(request.getParameter("refProgramId")!=null && !request.getParameter("refProgramId").equals(""))
    			refProgramId=Integer.parseInt(request.getParameter("refProgramId"));
        	String code = request.getParameter("code");
    		String description = request.getParameter("description");
    		try {
    			RefProgram refProgram = getRefProgramBD().getReferenceProgramById(SessionUtil.getUserSession(request), refProgramId);

    			refProgram.setRefProgCode(code);
    			refProgram.setRefProgDescription(description);
    			
    			getRefProgramBD().updateRefProgram(SessionUtil.getUserSession(request), refProgram);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageUpdateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
        MessageResources messageResources = getResources(request,"message");
		
    	int refProgramId = 0;
		if(request.getParameter("refProgramId")!=null && !request.getParameter("refProgramId").equals(""))
			refProgramId=Integer.parseInt(request.getParameter("refProgramId"));
		try {
			getRefProgramBD().deleteRefProgram(SessionUtil.getUserSession(request), refProgramId);
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
			return null;
		}
		response.getWriter().write(StrutsFormValidateUtil.getMessageDeleteSuccess(messageResources).toString());
		return null;
	}
	
	public ActionForward getAllRefProgram(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<RefProgram> list = getRefProgramBD().getAllReferenceProgram(SessionUtil.getUserSession(request));
			JSONArray mainArray = new JSONArray();
			for(RefProgram refProgram : list){
				JSONArray array = new JSONArray();
				array.put(refProgram.getRefProgCode());
				array.put(refProgram.getRefProgDescription());
				array.put(refProgram.getRefProgId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	
}
