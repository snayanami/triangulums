package cwi.admin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;

import cwi.admin.domain.Officer;
import cwi.admin.domain.Stakeholder;
import cwi.admin.spring.OfficerBD;
import cwi.admin.spring.StakeholderBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.PasswordService;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;



public class OfficerAction extends CommonDispatch{

	private OfficerBD officerBD;
	private StakeholderBD stakeholderBD;
	public StakeholderBD getStakeholderBD() {
		return stakeholderBD;
	}
	public void setStakeholderBD(StakeholderBD stakeholderBD) {
		this.stakeholderBD = stakeholderBD;
	}
	public OfficerBD getOfficerBD() {
		return officerBD;
	}
	public void setOfficerBD(OfficerBD officerBD) {
		this.officerBD = officerBD;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int stakeholderId = 0;
    		if(request.getParameter("stakeholderId")!=null && !request.getParameter("stakeholderId").equals(""))
    			stakeholderId=Integer.parseInt(request.getParameter("stakeholderId"));
        	String userName = request.getParameter("userName");
    		String password = request.getParameter("password");
    		try {
    			Officer officer = new Officer();
    			officer.setStakeholderId(stakeholderId);
    			officer.setUserName(userName);
    			officer.setPassword(PasswordService.encrypt(password));
    			
    			getOfficerBD().createOfficer(SessionUtil.getUserSession(request), officer);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int officerId = 0;
    		if(request.getParameter("officerId")!=null && !request.getParameter("officerId").equals(""))
    			officerId=Integer.parseInt(request.getParameter("officerId"));
    		int stakeholderId = 0;
    		if(request.getParameter("stakeholderId")!=null && !request.getParameter("stakeholderId").equals(""))
    			stakeholderId=Integer.parseInt(request.getParameter("stakeholderId"));
        	String userName = request.getParameter("userName");
    		String password = request.getParameter("password");
    		try {
    			Officer officer = getOfficerBD().getOfficerById(SessionUtil.getUserSession(request), officerId);

    			officer.setStakeholderId(stakeholderId);
    			officer.setUserName(userName);
    			officer.setPassword(PasswordService.encrypt(password));
    			
    			getOfficerBD().updateOfficer(SessionUtil.getUserSession(request), officer);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageUpdateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward getAllOfficer(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<Officer> list = getOfficerBD().getAllOfficer(SessionUtil.getUserSession(request));
			JSONArray mainArray = new JSONArray();
			for(Officer officer : list){
				
				Stakeholder stakeholder = getStakeholderBD().getStakeholderById(SessionUtil.getUserSession(request), officer.getStakeholderId());
				
				JSONArray array = new JSONArray();
				array.put(stakeholder.getTitle()!=null?stakeholder.getTitle().getRefProgDataCode()+" "+stakeholder.getFullName():stakeholder.getFullName());
				array.put(officer.getUserName());
				array.put(officer.getStakeholderId());
				array.put(stakeholder.getStakeholderCode());
				array.put(stakeholder.getFullName());
				array.put(officer.getPassword());
				array.put(officer.getPassword());
				array.put(officer.getOfficerId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
}