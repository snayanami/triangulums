package cwi.admin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;

import cwi.admin.dto.SystemTreeDTO;
import cwi.admin.spring.UserAccessRightsBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;

public class UserAccessRightsAction extends CommonDispatch {
	private UserAccessRightsBD userAccessRightsBD;
	public UserAccessRightsBD getUserAccessRightsBD() {
		return userAccessRightsBD;
	}
	public void setUserAccessRightsBD(UserAccessRightsBD userAccessRightsBD) {
		this.userAccessRightsBD = userAccessRightsBD;
	}
	
	public ActionForward getSystemPrograms(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<SystemTreeDTO> list = getUserAccessRightsBD().getSystemPrograms(SessionUtil.getUserSession(request));
			JSONArray mainArray = new JSONArray();
			int previousPrtId=-2;
			int previousPrgId=-2;
			boolean addPrtPrograme=true;
			boolean addPrg=true;
			for(SystemTreeDTO treeDTO : list){
				JSONArray array = new JSONArray();
				if(previousPrtId==treeDTO.getParentProgramId())
					addPrtPrograme=false;
				else{
					addPrtPrograme=true;
					previousPrtId=treeDTO.getParentProgramId();
				}
				array.put((addPrtPrograme)?treeDTO.getParentProgramName():"");
				if(previousPrgId==treeDTO.getProgramId())
					addPrg=false;
				else{
					addPrg=true;
					previousPrgId=treeDTO.getProgramId();
				}
				array.put((addPrg)?treeDTO.getProgramName():"");
				array.put(treeDTO.getEventName());
				
				array.put("");
				
				array.put(treeDTO.getParentProgramId());
				array.put(treeDTO.getProgramId());
				array.put(treeDTO.getEventId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	public ActionForward getGroupSystemPrograms(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		int accessGroupId = 0;
		if(request.getParameter("accessGroupId")!=null && !request.getParameter("accessGroupId").equals(""))
			accessGroupId=Integer.parseInt(request.getParameter("accessGroupId"));
		try {
			List<SystemTreeDTO> list = getUserAccessRightsBD().getGroupSystemPrograms(SessionUtil.getUserSession(request),accessGroupId);
			JSONArray mainArray = new JSONArray();
			int previousPrtId=-2;
			int previousPrgId=-2;
			boolean addPrtPrograme=true;
			boolean addPrg=true;
			for(SystemTreeDTO treeDTO : list){
				JSONArray array = new JSONArray();
				if(previousPrtId==treeDTO.getParentProgramId())
					addPrtPrograme=false;
				else{
					addPrtPrograme=true;
					previousPrtId=treeDTO.getParentProgramId();
				}
		  /*00*/array.put((addPrtPrograme)?treeDTO.getParentProgramName():"");
				if(previousPrgId==treeDTO.getProgramId())
					addPrg=false;
				else{
					addPrg=true;
					previousPrgId=treeDTO.getProgramId();
				}
		  /*01*/array.put((addPrg)?treeDTO.getProgramName():"");
		  /*02*/array.put(treeDTO.getEventName());
		  /*03*/array.put("");
		  /*04*/array.put(treeDTO.getParentProgramId());
		  /*05*/array.put(treeDTO.getProgramId());
		  /*06*/array.put(treeDTO.getEventId());
		  /*07*/array.put(treeDTO.getIsGrant());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int accessGroupId = 0;
        	if(request.getParameter("accessGroupId")!=null && !request.getParameter("accessGroupId").equals(""))
        		accessGroupId=Integer.parseInt(request.getParameter("accessGroupId"));
        	String dataString = request.getParameter("dataString");
        	
        	try {
				getUserAccessRightsBD().createAccessRights(SessionUtil.getUserSession(request), accessGroupId, dataString);
        	} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
        	response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
        }
        return null;
	}
	
}
