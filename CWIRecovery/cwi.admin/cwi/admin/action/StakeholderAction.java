package cwi.admin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;
import org.json.JSONObject;

import cwi.admin.domain.RefProgramData;
import cwi.admin.domain.Stakeholder;
import cwi.admin.spring.RefProgramDataBD;
import cwi.admin.spring.StakeholderBD;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;


public class StakeholderAction extends CommonDispatch{

	private StakeholderBD stakeholderBD;
	private RefProgramDataBD refProgramDataBD;
	public StakeholderBD getStakeholderBD() {
		return stakeholderBD;
	}
	public void setStakeholderBD(StakeholderBD stakeholderBD) {
		this.stakeholderBD = stakeholderBD;
	}
	public RefProgramDataBD getRefProgramDataBD() {
		return refProgramDataBD;
	}
	public void setRefProgramDataBD(RefProgramDataBD refProgramDataBD) {
		this.refProgramDataBD = refProgramDataBD;
	}
	
	public ActionForward loadTabPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		if (!SessionUtil.isValidSession(request)) {
			return mapping.findForward("sessionError");
		}
		List<RefProgramData> titleList = null;
		try{
			titleList = (List<RefProgramData>) getStakeholderBD().getAllTitles(SessionUtil.getUserSession(request));
		}catch (Exception e) {
			System.out.println(e.toString());
		}
		request.setAttribute("titleList", titleList);
		
		String action = request.getParameter("action");
		DynaActionForm frm = (DynaActionForm) form;
		frm.initialize(mapping);
        frm.set("action",action);
        String page = request.getParameter("page");
		return mapping.findForward(page);
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int stakeholderTypeId = 0;
        	Stakeholder stakeholder = null;
        	
        	String indOrCor = request.getParameter("indOrCor");
        	
        	if(indOrCor.equalsIgnoreCase("I")){
        		
        		if(request.getParameter("stakeholderTypeId")!=null && !request.getParameter("stakeholderTypeId").equals(""))
        			stakeholderTypeId=Integer.parseInt(request.getParameter("stakeholderTypeId"));
        		
        		RefProgramData title = null;
        		
        		int titleId = 0;
        		if(request.getParameter("titleId")!=null && !request.getParameter("titleId").equals(""))
        			titleId=Integer.parseInt(request.getParameter("titleId"));
        		try {
        			title = getRefProgramDataBD().getRefProgramDataById(SessionUtil.getUserSession(request), titleId);
				} catch (BasicException e) {
					response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
					return null;
				}
				
				String initials = request.getParameter("initials");
            	String lastName = request.getParameter("lastName");
            	String fullName = request.getParameter("fullName");
            	String civilStatus = request.getParameter("civilStatus");
            	String gender = request.getParameter("gender");
            	String nicNo = request.getParameter("nicNo");
            	String dob = request.getParameter("dob");
            	String tpNo = request.getParameter("tpNo");
            	String mobileNo = request.getParameter("mobileNo");
            	String faxNo = request.getParameter("faxNo");
            	String email = request.getParameter("email");
            	String address = request.getParameter("address");
            	String data = request.getParameter("profileImage");
            	data = data.substring(data.indexOf(",") + 1);
            	
            	stakeholder = new Stakeholder();
            	stakeholder.setCorpIndividual(indOrCor);
            	stakeholder.setTitle(title);
            	stakeholder.setInitials(initials);
            	stakeholder.setLastName(lastName);
            	stakeholder.setFullName(fullName);
            	stakeholder.setCivilStatus(civilStatus);
            	stakeholder.setGender(gender);
            	stakeholder.setIdBrNo(nicNo);
            	stakeholder.setDateOfBirth(StrutsFormValidateUtil.parseDate(dob));
            	stakeholder.setPhoneNo(tpNo);
            	stakeholder.setMobileNo(mobileNo);
            	stakeholder.setFaxNo(faxNo);
            	stakeholder.setEmailAddress(email);
            	stakeholder.setAddress(address);
            	stakeholder.setProfileImage(javax.xml.bind.DatatypeConverter.parseBase64Binary(data));
        	}else if(indOrCor.equalsIgnoreCase("C")){
        		
        		if(request.getParameter("stakeholderTypeIdC")!=null && !request.getParameter("stakeholderTypeIdC").equals(""))
        			stakeholderTypeId=Integer.parseInt(request.getParameter("stakeholderTypeIdC"));
        		
        		String companyName = request.getParameter("companyName");
        		String brNo = request.getParameter("brNo");
        		String dor = request.getParameter("dor");
        		String contactName = request.getParameter("contactName");
        		String contactDesignation = request.getParameter("contactDesignation");
        		String contactTpNo = request.getParameter("contactTpNo");
        		String tpNoC = request.getParameter("tpNoC");
        		String mobileNoC = request.getParameter("mobileNoC");
        		String faxNoC = request.getParameter("faxNoC");
        		String emailC = request.getParameter("emailC");
        		String addressC = request.getParameter("addressC");
        		
        		stakeholder = new Stakeholder();
        		stakeholder.setCorpIndividual(indOrCor);
        		stakeholder.setFullName(companyName);
        		stakeholder.setIdBrNo(brNo);
        		stakeholder.setDateOfRegistration(StrutsFormValidateUtil.parseDate(dor));
        		stakeholder.setContactPersonName(contactName);
        		stakeholder.setContactPersonDesignation(contactDesignation);
        		stakeholder.setContactPersonPhoneNo(contactTpNo);
        		stakeholder.setPhoneNo(tpNoC);
        		stakeholder.setMobileNo(mobileNoC);
        		stakeholder.setFaxNo(faxNoC);
        		stakeholder.setEmailAddress(emailC);
        		stakeholder.setAddress(addressC);
        	}

        	try {
        		getStakeholderBD().createStakeholder(SessionUtil.getUserSession(request), stakeholder, stakeholderTypeId);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int stakeholderId = 0;
    		if(request.getParameter("stakeholderId")!=null && !request.getParameter("stakeholderId").equals(""))
    			stakeholderId=Integer.parseInt(request.getParameter("stakeholderId"));
        	
    		String indOrCor = request.getParameter("indOrCor");
    		try {
    			
    			Stakeholder stakeholder = getStakeholderBD().getStakeholderById(SessionUtil.getUserSession(request), stakeholderId);
    			
	        	if(indOrCor.equalsIgnoreCase("I")){
	        		
	        		RefProgramData title = null;
	        		int titleId = 0;
	        		if(request.getParameter("titleId")!=null && !request.getParameter("titleId").equals(""))
	        			titleId=Integer.parseInt(request.getParameter("titleId"));
	        		title = getRefProgramDataBD().getRefProgramDataById(SessionUtil.getUserSession(request), titleId);
	        		
	        		String initials = request.getParameter("initials");
	            	String lastName = request.getParameter("lastName");
	            	String fullName = request.getParameter("fullName");
	            	String civilStatus = request.getParameter("civilStatus");
	            	String gender = request.getParameter("gender");
	            	String nicNo = request.getParameter("nicNo");
	            	String dob = request.getParameter("dob");
	            	String tpNo = request.getParameter("tpNo");
	            	String mobileNo = request.getParameter("mobileNo");
	            	String faxNo = request.getParameter("faxNo");
	            	String email = request.getParameter("email");
	            	String address = request.getParameter("address");
	            	String data = request.getParameter("profileImage");
	            	data = data.substring(data.indexOf(",") + 1);
	            	
	            	stakeholder.setCorpIndividual(indOrCor);
	            	stakeholder.setTitle(title);
	            	stakeholder.setInitials(initials);
	            	stakeholder.setLastName(lastName);
	            	stakeholder.setFullName(fullName);
	            	stakeholder.setCivilStatus(civilStatus);
	            	stakeholder.setGender(gender);
	            	stakeholder.setIdBrNo(nicNo);
	            	stakeholder.setDateOfBirth(StrutsFormValidateUtil.parseDate(dob));
	            	stakeholder.setPhoneNo(tpNo);
	            	stakeholder.setMobileNo(mobileNo);
	            	stakeholder.setFaxNo(faxNo);
	            	stakeholder.setEmailAddress(email);
	            	stakeholder.setAddress(address);
	            	stakeholder.setProfileImage(javax.xml.bind.DatatypeConverter.parseBase64Binary(data));
	        	}else if(indOrCor.equalsIgnoreCase("C")){
	        		
	        		String companyName = request.getParameter("companyName");
	        		String brNo = request.getParameter("brNo");
	        		String dor = request.getParameter("dor");
	        		String contactName = request.getParameter("contactName");
	        		String contactDesignation = request.getParameter("contactDesignation");
	        		String contactTpNo = request.getParameter("contactTpNo");
	        		String tpNoC = request.getParameter("tpNoC");
	        		String mobileNoC = request.getParameter("mobileNoC");
	        		String faxNoC = request.getParameter("faxNoC");
	        		String emailC = request.getParameter("emailC");
	        		String addressC = request.getParameter("addressC");
	        		
	        		stakeholder.setFullName(companyName);
	        		stakeholder.setIdBrNo(brNo);
	        		stakeholder.setDateOfRegistration(StrutsFormValidateUtil.parseDate(dor));
	        		stakeholder.setContactPersonName(contactName);
	        		stakeholder.setContactPersonDesignation(contactDesignation);
	        		stakeholder.setContactPersonPhoneNo(contactTpNo);
	        		stakeholder.setPhoneNo(tpNoC);
	        		stakeholder.setMobileNo(mobileNoC);
	        		stakeholder.setFaxNo(faxNoC);
	        		stakeholder.setEmailAddress(emailC);
	        		stakeholder.setAddress(addressC);
	        	}
    			
	        	getStakeholderBD().updateStakeholder(SessionUtil.getUserSession(request), stakeholder);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageUpdateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward getStakeholderDetails(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		int stakeholderId = 0;
		if(request.getParameter("stakeholderId")!=null && !request.getParameter("stakeholderId").equals(""))
			stakeholderId=Integer.parseInt(request.getParameter("stakeholderId"));
		try{
			Stakeholder stakeholder = getStakeholderBD().getStakeholderById(SessionUtil.getUserSession(request), stakeholderId);
			JSONObject object = new JSONObject();
			object.put("indOrCor", stakeholder.getCorpIndividual());
			if(stakeholder.getCorpIndividual().equalsIgnoreCase("I")){
				object.put("titleId", stakeholder.getTitle().getRefProgDataId());
				object.put("titleCode", stakeholder.getTitle().getRefProgDataCode());
				object.put("titleDescription", stakeholder.getTitle().getRefProgDataDescription());
				object.put("initials", stakeholder.getInitials());
				object.put("lastName", stakeholder.getLastName());
				object.put("fullName", stakeholder.getFullName());
				object.put("civilStatus", stakeholder.getCivilStatus());
				object.put("gender", stakeholder.getGender());
				object.put("nicNo", stakeholder.getIdBrNo());
				object.put("dob", StrutsFormValidateUtil.parseString(stakeholder.getDateOfBirth()));
				object.put("tpNo", stakeholder.getPhoneNo());
				object.put("mobileNo", stakeholder.getMobileNo());
				object.put("faxNo", stakeholder.getFaxNo());
				object.put("email", stakeholder.getEmailAddress());
				object.put("address", stakeholder.getAddress());
				object.put("profileImage", javax.xml.bind.DatatypeConverter.printBase64Binary(stakeholder.getProfileImage()));
			}else if(stakeholder.getCorpIndividual().equalsIgnoreCase("C")){
				object.put("companyName", stakeholder.getFullName());
				object.put("brNo", stakeholder.getIdBrNo());
				object.put("dor", StrutsFormValidateUtil.parseString(stakeholder.getDateOfRegistration()));
				object.put("contactName", stakeholder.getContactPersonName());
				object.put("contactDesignation", stakeholder.getContactPersonDesignation());
				object.put("contactTpNo", stakeholder.getContactPersonPhoneNo());
				object.put("tpNoC", stakeholder.getPhoneNo());
				object.put("mobileNoC", stakeholder.getMobileNo());
				object.put("faxNoC", stakeholder.getFaxNo());
				object.put("emailC", stakeholder.getEmailAddress());
				object.put("addressC", stakeholder.getAddress());
			}
			request.setAttribute("stk", stakeholder);
			response.getWriter().write(object.toString());
		}catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
			return null;
		}
		return null;
	}
	
	public ActionForward getAllStakeholder(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<Stakeholder> list = getStakeholderBD().getAllStakeholder(SessionUtil.getUserSession(request));
			JSONArray mainArray = new JSONArray();
			for(Stakeholder stakeholder : list){
				JSONArray array = new JSONArray();
				array.put(stakeholder.getStakeholderCode());
				array.put(stakeholder.getTitle()!=null?(stakeholder.getTitle().getRefProgDataCode()+" "+stakeholder.getFullName()):stakeholder.getFullName());
				array.put(stakeholder.getInitials());
				if(stakeholder.getGender()!=null)
					array.put(stakeholder.getGender().equalsIgnoreCase("M")?"Male":stakeholder.getGender().equalsIgnoreCase("F")?"Female":"");
				else
					array.put("");
				array.put(stakeholder.getCivilStatus().equalsIgnoreCase("S")?"Single":stakeholder.getCivilStatus().equalsIgnoreCase("M")?"Married":stakeholder.getCivilStatus().equalsIgnoreCase("D")?"Divorced":"");
				array.put(stakeholder.getIdBrNo());
				array.put(stakeholder.getCorpIndividual().equalsIgnoreCase("I")?StrutsFormValidateUtil.parseString(stakeholder.getDateOfBirth()):StrutsFormValidateUtil.parseString(stakeholder.getDateOfRegistration()));
				array.put(stakeholder.getPhoneNo());
				array.put(stakeholder.getMobileNo()!=null?stakeholder.getMobileNo():"");
				array.put(stakeholder.getFaxNo()!=null?stakeholder.getFaxNo():"");
				array.put(stakeholder.getEmailAddress()!=null?stakeholder.getEmailAddress():"");
				array.put(stakeholder.getAddress());
				array.put(stakeholder.getIsMember()==1?"Yes":"No");
				array.put(stakeholder.getAddress());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
	
	
	
	public ActionForward addStakeholder(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages actionMessages = form.validate(mapping, request);
		MessageResources messageResources = getResources(request,"message");
		if(!actionMessages.isEmpty()){
			response.getWriter().write(StrutsFormValidateUtil.getMessages(request, actionMessages,messageResources,getLocale(request),null).toString());
		}else{
			Stakeholder stakeholder = new Stakeholder();
			stakeholder.setInitials(request.getParameter("initial"));
			stakeholder.setFullName(request.getParameter("fullName"));
			stakeholder.setLastName(request.getParameter("lastName"));
			try {
				getStakeholderBD().addStakeholder(stakeholder);
				JSONObject object = new JSONObject();
				response.getWriter().write(object.put("success", "Stakeholder added successfuly.").toString());
			} catch (BasicDataAccessException e) {
				JSONObject object = new JSONObject();
				response.getWriter().write(object.put("error", e.getErrorCode()).toString());
			}
		}
		return null;
	}
	
//	public ActionForward deleteStakeholder(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
//		ActionMessages actionMessages = form.validate(mapping, request);
//		MessageResources messageResources = getResources(request,"message");
//		if(!actionMessages.isEmpty()){
//			response.getWriter().write(StrutsFormValidateUtil.getMessages(request, actionMessages,messageResources,getLocale(request),null).toString());
//		}else{
//			Stakeholder stakeholder = getStakeholderBD().getStakeholderById(Integer.parseInt(request.getParameter("stakeholderId")));
//			try {
//				getStakeholderBD().deleteStakeholder(stakeholder);
//				JSONObject object = new JSONObject();
//				response.getWriter().write(object.put("success", "Stakeholder deleted successfuly.").toString());
//			} catch (BasicDataAccessException e) {
//				JSONObject object = new JSONObject();
//				response.getWriter().write(object.put("error", e.getErrorCode()).toString());
//			}
//		}
//		return null;
//	}
	
//	public ActionForward modifyStakeholder(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
//		ActionMessages actionMessages = form.validate(mapping, request);
//		MessageResources messageResources = getResources(request,"message");
//		if(!actionMessages.isEmpty()){
//			response.getWriter().write(StrutsFormValidateUtil.getMessages(request, actionMessages,messageResources,getLocale(request),null).toString());
//		}else{
//			Stakeholder stakeholder = getStakeholderBD().getStakeholderById(Integer.parseInt(request.getParameter("stakeholderId")));
//			stakeholder.setInitials(request.getParameter("initial"));
//			stakeholder.setFullName(request.getParameter("fullName"));
//			stakeholder.setLastName(request.getParameter("lastName"));
//			try {
//				getStakeholderBD().modifyStakeholder(stakeholder);
//				JSONObject object = new JSONObject();
//				response.getWriter().write(object.put("success", "Stakeholder modified successfuly.").toString());
//			} catch (BasicDataAccessException e) {
//				JSONObject object = new JSONObject();
//				response.getWriter().write(object.put("error", e.getErrorCode()).toString());
//			}
//		}
//		return null;
//	}
	
//	public ActionForward getAllStakeholder(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
//		try {
//			Collection<Stakeholder> collection = getStakeholderBD().getAllStakeholder();
//			JSONArray array = new JSONArray();
//			for(Stakeholder stakeholder:collection){
//				JSONArray subArr = new JSONArray();
//				subArr.put(stakeholder.getStakeholderId());
//				subArr.put(stakeholder.getInitials());
//				subArr.put(stakeholder.getLastName());
//				subArr.put(stakeholder.getFullName());
//				array.put(subArr);
//			}
//			JSONObject object = new JSONObject();
//			response.getWriter().write(array.toString());
//		} catch (BasicDataAccessException e) {
//			JSONObject object = new JSONObject();
//			response.getWriter().write(object.put("error", e.getErrorCode()).toString());
//		}
//		return null;
//	}
}