package cwi.admin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;

import cwi.admin.domain.RefProgramData;
import cwi.admin.domain.StakeholderTypeMapping;
import cwi.admin.spring.RefProgramDataBD;
import cwi.admin.spring.StakeholderTypeMappingBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;


public class StakeholderTypeMappingAction extends CommonDispatch {

	private StakeholderTypeMappingBD stakeholderTypeMappingBD;
	private RefProgramDataBD refProgramDataBD;
	public StakeholderTypeMappingBD getStakeholderTypeMappingBD() {
		return stakeholderTypeMappingBD;
	}
	public void setStakeholderTypeMappingBD(StakeholderTypeMappingBD stakeholderTypeMappingBD) {
		this.stakeholderTypeMappingBD = stakeholderTypeMappingBD;
	}
	public RefProgramDataBD getRefProgramDataBD() {
		return refProgramDataBD;
	}
	public void setRefProgramDataBD(RefProgramDataBD refProgramDataBD) {
		this.refProgramDataBD = refProgramDataBD;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int stakeholderId = 0;
    		if(request.getParameter("stakeholderId")!=null && !request.getParameter("stakeholderId").equals(""))
    			stakeholderId=Integer.parseInt(request.getParameter("stakeholderId"));
    		int stakeholderTypeId = 0;
    		if(request.getParameter("stakeholderTypeId")!=null && !request.getParameter("stakeholderTypeId").equals(""))
    			stakeholderTypeId=Integer.parseInt(request.getParameter("stakeholderTypeId"));

    		try {
    			StakeholderTypeMapping stakeholderTypeMapping = new StakeholderTypeMapping();
    			stakeholderTypeMapping.setStakeholderId(stakeholderId);
    			stakeholderTypeMapping.setReferenceProgramDataId(stakeholderTypeId);
    			
    			getStakeholderTypeMappingBD().createStakeholderTypeMapping(SessionUtil.getUserSession(request), stakeholderTypeMapping);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
        MessageResources messageResources = getResources(request,"message");
		
    	int stakeholderTypeMappingId = 0;
		if(request.getParameter("stakeholderTypeMappingId")!=null && !request.getParameter("stakeholderTypeMappingId").equals(""))
			stakeholderTypeMappingId=Integer.parseInt(request.getParameter("stakeholderTypeMappingId"));
		try {
			getStakeholderTypeMappingBD().deleteStakeholderTypeMapping(SessionUtil.getUserSession(request), stakeholderTypeMappingId);
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
			return null;
		}
		response.getWriter().write(StrutsFormValidateUtil.getMessageDeleteSuccess(messageResources).toString());
		return null;
	}
	
	public ActionForward getAllStakeholderMapping(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		int stakeholderId = 0;
		if(request.getParameter("stakeholderId")!=null && !request.getParameter("stakeholderId").equals(""))
			stakeholderId=Integer.parseInt(request.getParameter("stakeholderId"));
		try {
			List<StakeholderTypeMapping> list = getStakeholderTypeMappingBD().getAllStakeholderTypeMppingByStakeholderId(SessionUtil.getUserSession(request), stakeholderId);
			JSONArray mainArray = new JSONArray();
			for(StakeholderTypeMapping stakeholderTypeMapping : list){
				JSONArray array = new JSONArray();
				RefProgramData stakeholderType = getRefProgramDataBD().getRefProgramDataById(SessionUtil.getUserSession(request), stakeholderTypeMapping.getReferenceProgramDataId());
				array.put(stakeholderType.getRefProgDataCode());
				array.put(stakeholderType.getRefProgDataDescription());
				array.put(stakeholderTypeMapping.getStakeholderTypeMappingId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
}