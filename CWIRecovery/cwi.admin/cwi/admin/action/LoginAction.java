package cwi.admin.action;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;
import org.json.JSONObject;

import cwi.admin.domain.Branch;
import cwi.admin.domain.Officer;
import cwi.admin.domain.Stakeholder;
import cwi.admin.spring.BranchBD;
import cwi.admin.spring.LoginBD;
import cwi.admin.spring.OfficerBD;
import cwi.admin.spring.StakeholderBD;
import cwi.admin.spring.SystemProgramBD;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.PasswordService;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;
import cwi.basic.resourse.UserSessionConfig;
import cwi.information.dto.PawnInquiryDTO;

public class LoginAction extends CommonDispatch {

	private SystemProgramBD systemProgramBD;
	private OfficerBD officerBD;
	private StakeholderBD stakeholderBD;
	private LoginBD loginBD;
	private BranchBD branchBD;
	
	public OfficerBD getOfficerBD() {
		return officerBD;
	}
	public void setOfficerBD(OfficerBD officerBD) {
		this.officerBD = officerBD;
	}
	
	public SystemProgramBD getSystemProgramBD() {
		return systemProgramBD;
	}
	public void setSystemProgramBD(SystemProgramBD systemProgramBD) {
		this.systemProgramBD = systemProgramBD;
	}
	
	public StakeholderBD getStakeholderBD() {
		return stakeholderBD;
	}
	public void setStakeholderBD(StakeholderBD stakeholderBD) {
		this.stakeholderBD = stakeholderBD;
	}
	
	public LoginBD getLoginBD() {
		return loginBD;
	}
	public void setLoginBD(LoginBD loginBD) {
		this.loginBD = loginBD;
	}
	
	public BranchBD getBranchBD() {
		return branchBD;
	}
	public void setBranchBD(BranchBD branchBD) {
		this.branchBD = branchBD;
	}
	
	public ActionForward getGoldValueListString(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try{
			String goldValueListString = getLoginBD().getGoldValueListString();
			JSONObject object = new JSONObject();
			object.put("goldValueListString", goldValueListString);
			response.getWriter().write(object.toString());
		}catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
			return null;
		}
		return null;
	}
	
	public ActionForward login(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages validateForm = form.validate(mapping, request);
		MessageResources messageResources = getResources(request,"message");
		boolean loginStatus=false;
		JSONObject object = null;
		if(!validateForm.isEmpty())
			response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
		else{
			String userName = request.getParameter("uname");
			String password = request.getParameter("pwd");
			UserSessionConfig user = null;
			try {
				user = getLoginBD().systemLogin(userName, PasswordService.encrypt(PasswordService.decrypt(password).toUpperCase()));
				loginStatus=true;				
			} catch (BasicDataAccessException e) {
				object = new JSONObject();
				response.getWriter().write(object.put("error", e.getErrorCode()).toString());
			}
			
			if(loginStatus) {
				if (user != null) {
					HttpSession sessionHTTP = request.getSession(false);
					if (sessionHTTP != null) {
						sessionHTTP.invalidate();
					}
					sessionHTTP = request.getSession(true);
					user.setClientIp(request.getRemoteAddr());
					
					//int userLogId=getSystemLoginBD().createUserLog(user);
					
					sessionHTTP.setAttribute("LOGIN_KEY", user);
					//sessionHTTP.setAttribute("USERLOG_ID", userLogId);
					
					object = new JSONObject();
					object.put("success", user.getOfficerId()).toString();
					object.put("loginUserName", "Welcome "+user.getLoginUserName());
					response.getWriter().write(object.toString());
				}else{
					object = new JSONObject();
					object.put("invalidLogin", "invalidLogin").toString();
					response.getWriter().write(object.toString());
					System.out.println("User Not found");
				}
			}else{
				System.out.println("Login Fail");
			}
		}			
		return null;
	}
	
	public ActionForward logout(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject object=new JSONObject();
		try {
			request.getSession().removeAttribute("LOGIN_KEY");
			request.getSession(false).invalidate();
			object.put("logout","Successfully logouted");
		} catch (Exception e) {
			object.put("logout","Logout Failed ! Please Click Logout Again.");
		}
		response.getWriter().write(object.toString());
		return null;
	}
	
	
	
	public ActionForward loadHomePage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		int officerId = 0;
		if(request.getParameter("officerId")!=null && !request.getParameter("officerId").equals(""))
			officerId=Integer.parseInt(request.getParameter("officerId"));
		
		Calendar calendar = Calendar.getInstance();
		calendar.getTime();
		
		Officer officer = getOfficerBD().getOfficerById(SessionUtil.getUserSession(request), officerId);
		Stakeholder stakeholder = getStakeholderBD().getStakeholderById(SessionUtil.getUserSession(request), officer.getStakeholderId());
		String systemDate = "2011-JUL-04";
		List<Branch> branchList = getOfficerBD().getAllBranchForOfficerId(SessionUtil.getUserSession(request), officerId);
		//Branch defBranch = getOfficerBD().getDefaultBranchByOfficerId(SessionUtil.getUserSession(request), officerId);
		Branch defBranch=getBranchBD().getBranchById(SessionUtil.getUserSession(request), SessionUtil.getUserSession(request).getBranchId());
		request.setAttribute("officerId", officerId);
		request.setAttribute("systemDate", StrutsFormValidateUtil.parseString(SessionUtil.getUserSession(request).getLoginDate()));
		request.setAttribute("loginName", stakeholder.getTitle()!=null?(stakeholder.getTitle().getRefProgDataCode()+" "+stakeholder.getFullName()):stakeholder.getFullName());
		request.setAttribute("branchList", branchList);
		request.setAttribute("defaultBranch", defBranch);
		
		return mapping.findForward("home");
	}	
	
	
	public ActionForward loadPrograms(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		int officerId = 0;
		if(request.getParameter("officerId")!=null && !request.getParameter("officerId").equals(""))
			officerId=Integer.parseInt(request.getParameter("officerId"));
			//JSONArray array = getSystemProgramBD().getSystemProgramDetailsForLogin(officerId);
			JSONArray array = getLoginBD().getSystemProgramsForLogin(SessionUtil.getUserSession(request), officerId);
		try {
			//JSONObject object = new JSONObject();
			response.getWriter().write(array.toString());
		} catch (BasicDataAccessException e) {
			JSONObject object = new JSONObject();
			response.getWriter().write(object.put("error", e.getErrorCode()).toString());
		}
		return null;
	}
	
	public ActionForward loadProgramsForBranchChange(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		/*int officerId = 0;
		if(request.getParameter("officerId")!=null && !request.getParameter("officerId").equals(""))
			officerId=Integer.parseInt(request.getParameter("officerId"));*/
		int branchId = 0;
		if(request.getParameter("newBranchId")!=null && !request.getParameter("newBranchId").equals(""))
			branchId=Integer.parseInt(request.getParameter("newBranchId"));
		
		UserSessionConfig sessionConfig=SessionUtil.getUserSession(request);
		sessionConfig.setBranchId(branchId);
		
		/*	//JSONArray array = getSystemProgramBD().getSystemProgramDetailsForLogin(officerId);
			JSONArray array = getLoginBD().getSystemProgramsForBranchChange(SessionUtil.getUserSession(request), officerId, branchId);
		try {
			//JSONObject object = new JSONObject();
			response.getWriter().write(array.toString());
		} catch (BasicDataAccessException e) {
			JSONObject object = new JSONObject();
			response.getWriter().write(object.put("error", e.getErrorCode()).toString());
		}*/
		
		return loadHomePage(mapping, form, request, response);
	}
}