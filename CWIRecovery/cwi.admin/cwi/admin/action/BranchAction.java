package cwi.admin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.json.JSONArray;

import cwi.admin.domain.Branch;
import cwi.admin.spring.BranchBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.CommonDispatch;
import cwi.basic.resourse.SessionUtil;
import cwi.basic.resourse.StrutsFormValidateUtil;


public class BranchAction extends CommonDispatch {

	private BranchBD branchBD;
	public BranchBD getBranchBD() {
		return branchBD;
	}
	public void setBranchBD(BranchBD branchBD) {
		this.branchBD = branchBD;
	}
	
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (!SessionUtil.isValidSession(request)){
            return mapping.findForward("sessionError");
        } 
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	String branchCode = request.getParameter("branchCode");
    		String branchName = request.getParameter("branchName");
    		String address = request.getParameter("address");
    		String contactNo = request.getParameter("contactNo");
    		try {
    			Branch branch = new Branch();
    			branch.setBranchCode(branchCode);
    			branch.setBranchName(branchName);
    			branch.setAddress(address);
    			branch.setContactNumber(contactNo);
    			
    			getBranchBD().createBranch(SessionUtil.getUserSession(request), branch);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageCreateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ActionMessages validateForm =form.validate(mapping,request);
        MessageResources messageResources = getResources(request,"message");
		
        if(!validateForm.isEmpty()){
        	response.getWriter().write(StrutsFormValidateUtil.getMessages(request, validateForm,messageResources,getLocale(request),null).toString());
        }else{
        	int branchId = 0;
    		if(request.getParameter("branchId")!=null && !request.getParameter("branchId").equals(""))
    			branchId=Integer.parseInt(request.getParameter("branchId"));
    		String branchCode = request.getParameter("branchCode");
    		String branchName = request.getParameter("branchName");
    		String address = request.getParameter("address");
    		String contactNo = request.getParameter("contactNo");
    		try {
    			Branch branch = getBranchBD().getBranchById(SessionUtil.getUserSession(request), branchId);

    			branch.setBranchCode(branchCode);
    			branch.setBranchName(branchName);
    			branch.setAddress(address);
    			branch.setContactNumber(contactNo);
    			
    			getBranchBD().updateBranch(SessionUtil.getUserSession(request), branch);
    		} catch (BasicException e) {
    			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
    			return null;
    		}
    		response.getWriter().write(StrutsFormValidateUtil.getMessageUpdateSuccess(messageResources).toString());
        }
		return null;
	}
	
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
        MessageResources messageResources = getResources(request,"message");
		
        int branchId = 0;
		if(request.getParameter("branchId")!=null && !request.getParameter("branchId").equals(""))
			branchId=Integer.parseInt(request.getParameter("branchId"));
		try {
			getBranchBD().deleteBranch(SessionUtil.getUserSession(request), branchId);
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
			return null;
		}
		response.getWriter().write(StrutsFormValidateUtil.getMessageDeleteSuccess(messageResources).toString());
		return null;
	}
	
	public ActionForward getAllBranch(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MessageResources messageResources = getResources(request,"message");
		try {
			List<Branch> list = getBranchBD().getAllBranch(SessionUtil.getUserSession(request));
			JSONArray mainArray = new JSONArray();
			for(Branch branch : list){
				JSONArray array = new JSONArray();
				array.put(branch.getBranchCode());
				array.put(branch.getBranchName());
				array.put(branch.getAddress());
				array.put(branch.getContactNumber());
				array.put(branch.getBranchId());
				mainArray.put(array);
			}
			response.getWriter().write(mainArray.toString());
		} catch (BasicException e) {
			response.getWriter().write(StrutsFormValidateUtil.getErrorMessage(e,messageResources, getLocale(request)).toString());
		}
        return null;
	}
}