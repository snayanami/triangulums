package cwi.admin.dto;

public class BranchOfficerDTO {
	
	private int branchOfficerId;
	private int offiderId;
	private String officerName;
	private int branchId;
	private String branchCode;
	private String branchName;
	private int accessGruopId;
	private String accessGruopCode;
	private String accessGruopName;
	private int isDefault;
	
	public BranchOfficerDTO(){}
	
	public BranchOfficerDTO(int branchOfficerId,int offiderId,String officerName,int branchId,String branchCode,String branchName,int accessGruopId,String accessGruopCode,String accessGruopName,int isDefault){
		this.branchOfficerId = branchOfficerId;
		this.offiderId = offiderId;
		this.officerName = officerName;
		this.branchId = branchId;
		this.branchCode = branchCode;
		this.branchName = branchName;
		this.accessGruopId = accessGruopId;
		this.accessGruopCode = accessGruopCode;
		this.accessGruopName = accessGruopName;
		this.isDefault = isDefault;
	}

	public int getOffiderId() {
		return offiderId;
	}

	public void setOffiderId(int offiderId) {
		this.offiderId = offiderId;
	}

	public String getOfficerName() {
		return officerName;
	}

	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public int getAccessGruopId() {
		return accessGruopId;
	}

	public void setAccessGruopId(int accessGruopId) {
		this.accessGruopId = accessGruopId;
	}

	public String getAccessGruopCode() {
		return accessGruopCode;
	}

	public void setAccessGruopCode(String accessGruopCode) {
		this.accessGruopCode = accessGruopCode;
	}

	public String getAccessGruopName() {
		return accessGruopName;
	}

	public void setAccessGruopName(String accessGruopName) {
		this.accessGruopName = accessGruopName;
	}

	public int getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}

	public int getBranchOfficerId() {
		return branchOfficerId;
	}

	public void setBranchOfficerId(int branchOfficerId) {
		this.branchOfficerId = branchOfficerId;
	}
}
