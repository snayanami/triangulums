package cwi.admin.dto;

public class SystemTreeDTO {	
    private int parentProgramId;
    private int programId;
    private int eventId;
    private String parentProgramName;
    private String programName;
    private String eventName;
    private boolean isGrant;
    
    
	public int getParentProgramId() {
		return parentProgramId;
	}
	public void setParentProgramId(int parentProgramId) {
		this.parentProgramId = parentProgramId;
	}
	
	public int getProgramId() {
		return programId;
	}
	public void setProgramId(int programId) {
		this.programId = programId;
	}
	
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	
	public String getParentProgramName() {
		return parentProgramName;
	}
	public void setParentProgramName(String parentProgramName) {
		this.parentProgramName = parentProgramName;
	}
	
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	public boolean getIsGrant() {
		return isGrant;
	}
	public void setIsGrant(boolean isGrant) {
		this.isGrant = isGrant;
	}
    
}
