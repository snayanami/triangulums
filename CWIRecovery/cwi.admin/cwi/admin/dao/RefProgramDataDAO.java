package cwi.admin.dao;

import java.util.List;

import cwi.admin.domain.RefProgramData;
import cwi.basic.resourse.UserSessionConfig;


public interface RefProgramDataDAO {

	public void createRefProgramData(UserSessionConfig userSessionConfig, RefProgramData refProgramData);
	public void updateRefProgramData(UserSessionConfig userSessionConfig, RefProgramData refProgramData);
	public void deleteRefProgramData(UserSessionConfig userSessionConfig, int recordId);
	public List<RefProgramData> getAllRefProgramDataByRefProgramId(UserSessionConfig userSessionConfig, int refProgramId);
	public List<RefProgramData> getAllRefProgramData(UserSessionConfig userSessionConfig);
	public RefProgramData getRefProgramDataById(UserSessionConfig userSessionConfig, int recordId);
}
