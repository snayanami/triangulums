package cwi.admin.dao;

import java.util.Collection;
import java.util.List;

import cwi.admin.domain.Branch;
import cwi.admin.domain.Officer;
import cwi.admin.domain.Stakeholder;
import cwi.basic.resourse.UserSessionConfig;


public interface OfficerDAO {

	public void createOfficer(UserSessionConfig userSessionConfig, Officer officer);
	public void updateOfficer(UserSessionConfig userSessionConfig, Officer officer);
	public Officer getOfficerById(UserSessionConfig userSessionConfig,int recordId);
	public List<Officer> getAllOfficer(UserSessionConfig userSessionConfig);
	public List<Branch> getAllBranchForOfficerId(UserSessionConfig userSessionConfig, int officerId);
	public Branch getDefaultBranchByOfficerId(UserSessionConfig userSessionConfig, int officerId);
}
