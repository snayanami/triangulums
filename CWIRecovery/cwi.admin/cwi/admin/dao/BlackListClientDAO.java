package cwi.admin.dao;


import cwi.admin.domain.BlackListClientHistory;
import cwi.basic.resourse.UserSessionConfig;

public interface BlackListClientDAO {
//	public List<PawnDetailsDTO> getPawnDetailsByClientId(UserSessionConfig userSessionConfig, int clientId);
	public int getIsBlackListed(UserSessionConfig userSessionConfig, int clientId);
	public void updateBlackListClient(UserSessionConfig userSessionConfig, BlackListClientHistory blackListClientHistory);
}
