package cwi.admin.dao;

import java.util.Date;
import java.util.List;

import cwi.admin.domain.Holiday;
import cwi.basic.resourse.UserSessionConfig;


public interface HolidayDAO {
	public List<Holiday> createHoliday(UserSessionConfig userConfig, Date dateArr[], String reason);
	public List<Holiday> getAllHoliday(UserSessionConfig userSessionConfig);
	public void updateHoliday(UserSessionConfig userSessionConfig, Holiday holiday);
	public void deleteHoliday(UserSessionConfig userSessionConfig, int recordId);
	public Holiday getHolidayById(UserSessionConfig userSessionConfig,int recordId);
}
