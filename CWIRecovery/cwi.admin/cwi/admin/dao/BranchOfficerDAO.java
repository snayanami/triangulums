package cwi.admin.dao;

import java.util.Collection;
import java.util.List;

import org.json.JSONArray;

import cwi.admin.domain.BranchOfficer;
import cwi.admin.domain.Stakeholder;
import cwi.admin.dto.BranchOfficerDTO;
import cwi.basic.resourse.UserSessionConfig;


public interface BranchOfficerDAO {
	
	public void createBranchOfficer(UserSessionConfig userSessionConfig, BranchOfficer branchOfficer);
	public void updateBranchOfficer(UserSessionConfig userSessionConfig, BranchOfficer branchOfficer);
	public void deleteBranchOfficer(UserSessionConfig userSessionConfig, int recordId);
	public List<BranchOfficer> getAllBranchOfficerByOfficerId(UserSessionConfig userSessionConfig, int officerId);
	public BranchOfficer getBranchOfficerById(UserSessionConfig userSessionConfig, int recordId);
	
	
	
	
	

	public void addBranchOfficer(BranchOfficer branchOfficer);
	public void deleteBranchOfficer(BranchOfficer branchOfficer);
	public void modifyBranchOfficer(BranchOfficer branchOfficer);
	public Collection<BranchOfficerDTO> getAllBranchOfficer();
	public BranchOfficer getBranchOfficerById(int branchOfficerId);
	public Collection<BranchOfficer> getAllBranchOfficerByBranchId(int branchId);
	public Stakeholder getBranchOfficerDetailsById(int branchOfficerId);
	public boolean isBranchOfficerExists(int branchId,int officerId);
}
