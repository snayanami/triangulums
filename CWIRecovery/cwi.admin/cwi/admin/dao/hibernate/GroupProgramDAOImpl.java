package cwi.admin.dao.hibernate;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cwi.admin.dao.GroupProgramDAO;
import cwi.admin.domain.GroupProgram;
import cwi.basic.resourse.BasicDataAccessException;



public class GroupProgramDAOImpl extends HibernateDaoSupport implements GroupProgramDAO {

	public void addGroupProgram(GroupProgram groupProgram) {
		getHibernateTemplate().save(groupProgram);
	}

	public void deleteGroupProgram(GroupProgram groupProgram) {
		getHibernateTemplate().delete(groupProgram);
	}

	public Collection<GroupProgram> getAllGroupProgram() {
		Criteria criteria = getSession().createCriteria(GroupProgram.class);
		if(criteria.list()!=null)
			return criteria.list();
		else
			throw new BasicDataAccessException("No records found.");
	}

	public Collection<GroupProgram> getAllGroupProgramByGroupId(int groupId) {
		Criteria criteria = getSession().createCriteria(GroupProgram.class);
		criteria.add(Restrictions.eq("referenceProgramDataId", groupId));
		if(criteria.list()!=null)
			return criteria.list();
		else
			throw new BasicDataAccessException("No records found.");
	}

	public GroupProgram getGroupProgramById(int groupProgramId) {
		Criteria criteria = getSession().createCriteria(GroupProgram.class);
		criteria.add(Restrictions.eq("groupProgramId", groupProgramId));
		if(criteria.uniqueResult()!=null)
			return (GroupProgram) criteria.uniqueResult();
		else
			throw new BasicDataAccessException("No such record found.");
	}

	public void modifyGroupProgram(GroupProgram groupProgram) {
		getHibernateTemplate().update(groupProgram);
	}
}