package cwi.admin.dao.hibernate;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cwi.admin.dao.SearchDAO;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.Entity;
import cwi.basic.resourse.EntityAttribute;
import cwi.basic.resourse.SearchAttribute;
import cwi.basic.resourse.SearchEntity;
import cwi.basic.resourse.SearchResult;



public class SearchDAOImpl extends HibernateDaoSupport implements SearchDAO {
    private static Logger logger = Logger.getLogger(SearchDAOImpl.class);

    public Entity getEntityById(int entityId) {
        logger.info("Entered to get entity by id");
        Entity entity = null;
        entity = (Entity) getHibernateTemplate().get(Entity.class, Integer.valueOf(entityId));
        //Remove hidden fields from the search fields list
        Iterator it = entity.getAttributes().iterator();
        while(it.hasNext()){
        	EntityAttribute attr = (EntityAttribute)it.next();
        	if (attr.getIsDisplay()==0)
        		it.remove();
        }
        logger.info("Leaving from get entity by id");
        return entity;
    }
    
    private Entity getEntityAttributesById(int entityId) {
        logger.info("Entered to get entity attributes by id");
        Entity entity = null;
        entity = (Entity) getHibernateTemplate().get(Entity.class, Integer.valueOf(entityId));
        logger.info("Leaving from get entity attributes by id");
        return entity;
    }    

    public SearchResult getSearchData(SearchEntity searchEntity){
        logger.info("Entered to search");
        
        ArrayList<LinkedHashMap> dataMap = new ArrayList<LinkedHashMap>();
        LinkedHashMap<String,Object> map=null;
        SearchResult searchResult = null;

        Entity entity = this.getEntityAttributesById(searchEntity.getEntityId());
        
        Collection attributesCollection = entity.getAttributes();
        Iterator attributesIterator =  attributesCollection.iterator();
        
        String[] displayAttribute = new String[attributesCollection.size()];
        String[] displayAttributeHeading = new String[attributesCollection.size()];
        String[] attributesName = new String[attributesCollection.size()];
        String[] attributesType = new String[attributesCollection.size()];
        
        String[] joins = new String[attributesCollection.size()];
        String[] joinsAlias = new String[attributesCollection.size()];
        
        StringBuilder select=new StringBuilder("SELECT ");        
        int attrCount = 0;
        int allCount = 0;
        int associationCount=0;
        boolean isAssociation=false;
        while(attributesIterator.hasNext()){
            EntityAttribute attribute = (EntityAttribute)attributesIterator.next();
            isAssociation = false;
            if (attribute.getIsAssociation()==1){
            	//Assumption : Only one Association entry available
            	//joins[associationCount]= attribute.getAttributeName().split("\\.")[0];
            	joins[associationCount]= getAttributeName(attribute.getAttributeName(), 0);
            	joinsAlias[associationCount]= Character.toString((char)(65+associationCount));
            	associationCount++;
            	isAssociation = true;
            }
            
            //attributesName[allCount] = isAssociation==true ? attribute.getAttributeName()+ joinsAlias[associationCount-1] :attribute.getAttributeName();
            attributesName[allCount] = attribute.getAttributeName();
            attributesType[allCount] = attribute.getType();            
                        
            //Change to get key field(1 sequence)
            if ((attribute.getIsDisplay()==1)||(attribute.getDisplaySequence()==1)){
                displayAttribute[attrCount] = attribute.getAttributeName();
                displayAttributeHeading[attrCount] = attribute.getResourcesKey();
                attrCount++;
            	select.append(isAssociation==true ? joinsAlias[associationCount-1] :"MainEntity");
            	select.append(".");
            	select.append(isAssociation==true ? getAttributeName(attribute.getAttributeName(),1) : attribute.getAttributeName());
            	select.append(",");                
            }
            allCount++;
        }
        select.replace(select.length()-1,select.length()," ");
        select.append(" FROM " + entity.getEntityName());
        select.append(" MainEntity");
        
        for(int j=0;j<joins.length;j++){
        	if (joins[j]!=null){
        		select.append(" LEFT JOIN MainEntity.");
        		select.append(joins[j]);
        		select.append(" ");
        		select.append(joinsAlias[j]);
        	}
        }
        
        /*
         * Set Query Parameters
        */
        StringBuilder parameters = new StringBuilder();        
        Map<String,Object> prop = new HashMap<String,Object>();
        boolean isJoin=false;
        Iterator it = searchEntity.getAttributes().iterator();
        
        while(it.hasNext()){            
            SearchAttribute tempAtt = (SearchAttribute)it.next();
            isJoin = false;
            for(int a=0;a<joins.length ;a++){
            	if(joins[a]!=null && getAttributeName(tempAtt.getAttributeName(),0).equals(joins[a])){
            		parameters.append(joinsAlias[a]);
            		parameters.append(".");
            		parameters.append(getAttributeName(tempAtt.getAttributeName(),1));
                    parameters.append(" "+tempAtt.getOparator());
                    parameters.append(":");
                    parameters.append(getAttributeName(tempAtt.getAttributeName(),1));
                    parameters.append(" AND ");            		
            		isJoin=true;
            		break;
            	}
            }
            if (isJoin ==false){
            	parameters.append("MainEntity.");
            	parameters.append(tempAtt.getAttributeName());
                parameters.append(" ");
                parameters.append(tempAtt.getOparator());
                parameters.append(":");
                parameters.append(tempAtt.getAttributeName());
                parameters.append(" AND ");            	
            }   
                        
            String exVal =tempAtt.getOparator().equalsIgnoreCase("like")? "%":"";
            for(int temp=0;temp<attributesName.length;temp++){
                logger.info("Looping Attributes " + attributesName[temp]);
                if (tempAtt.getAttributeName().equalsIgnoreCase(attributesName[temp])){
                   	logger.info(attributesType[temp]);
                    logger.info("MatchesAttributes " + attributesName[temp] + " to type " + attributesType[temp]);
                    tempAtt.setType(attributesType[temp]);
                    break;
                }
            }            
            if (tempAtt.getType().equalsIgnoreCase("Integer")){                
                prop.put(getAttributeName(tempAtt.getAttributeName(),1),Integer.valueOf(tempAtt.getValue()));    
            }else if (tempAtt.getType().equalsIgnoreCase("String")){                
                prop.put(getAttributeName(tempAtt.getAttributeName(),1),tempAtt.getValue()+ exVal);
            }else if (tempAtt.getType().equalsIgnoreCase("Decimal")){                
                prop.put(getAttributeName(tempAtt.getAttributeName(),1),Double.valueOf(tempAtt.getValue()));                
            }else if (tempAtt.getType().equalsIgnoreCase("Date")){               
                prop.put(getAttributeName(tempAtt.getAttributeName(),1),new Date(tempAtt.getValue()));            
            }
            else
                throw new BasicDataAccessException("Invalid data type for parameter !");
        }
        if (parameters.length() > 5)
            parameters.replace(parameters.length()-4,parameters.length()," ");
        
        logger.info("Parameter list " + parameters.toString());
        logger.info("Value list " + prop);        
        
        if (prop.isEmpty()==false)
        	select.append(" WHERE ");
        
        String queryEnd = searchEntity.getSortOrder()!= null ? " ORDER BY " + searchEntity.getSortOrder() : "";
        
        select.append(parameters.toString());
        select.append(queryEnd);
        
        /*
        select = new StringBuilder("SELECT 	MainEntity.moduleSubProductId," +
        		"MainEntity.moduleSubProductCode," +
        		"MainEntity.description," +
        		"MainEntity.internalExternal," +
        		"XX.subProductCode " +
        		" FROM ModuleSubProduct MainEntity" +
        		" INNER JOIN MainEntity.clientProductModule XX " +
        		" WHERE MainEntity.recordStatus =:recordStatus " +
        		" AND MainEntity.companyId =:companyId " +
        		" AND XX.clientProductModuleId=:xxx");        
        
        prop = new HashMap<String,Object>();
        prop.put("recordStatus",1);    
        prop.put("companyId",2);        
        prop.put("xxx",2);
        */
        /*
				SELECT MainEntity.moduleSubProductId,
				MainEntity.moduleSubProductCode,
				MainEntity.description,
				MainEntity.internalExternal,
				A.clientProductModule.clientProductModuleId  FROM com.openarc.skybank.skycorpdata.api.domain.ModuleSubProduct MainEntity INNER JOIN MainEntity.clientProductModule A WHERE MainEntity.recordStatus =:recordStatus AND MainEntity.companyId =:companyId"         
         */
        
        /*select = new StringBuilder("SELECT MainEntity.moduleSubProductId," +
    	"MainEntity.moduleSubProductCode, " + 
    	"MainEntity.description, " +
    	"MainEntity.internalExternal " +
    	"A.clientProductModuleId " +
    	"FROM ModuleSubProduct MainEntity " + 
    	"INNER JOIN MainEntity.clientProductModule A " + 
    	"WHERE A.clientProductModuleId =:clientProductModule.clientProductModuleId " + 
    	"AND MainEntity.recordStatus =:recordStatus " +
    	"AND MainEntity.companyId =:companyId ");
         */
        
        logger.info("Query to execute " + select.toString());
       
		List<Object[]> result = null;
		
		Query query = getSession().createQuery(select.toString());
		query.setProperties(prop);
		result = query.list();
		
		for(Object obj[]:result){
			map = new LinkedHashMap<String,Object>(); 
			for(int o=0;o<obj.length;o++){
				map.put(displayAttribute[o],obj[o]);				
			}
	        dataMap.add(map);
		}
		
        String[] displayAttributeHeadingOut = new String[attrCount];        
        for(int c = 0;c<attrCount;c++){
            displayAttributeHeadingOut[c] = displayAttributeHeading[c];
        }               
        searchResult = new SearchResult(dataMap,displayAttributeHeadingOut,result.size());		
		        
        logger.info("Leaving from search");
        return searchResult;        
    }
    
    private String getAttributeName(String attributeName,int position){
    	String []arrAttributeName = attributeName.split("\\."); 
    	if (arrAttributeName.length==1)
    		return arrAttributeName[0];
    	else 
    		return arrAttributeName[position];
    }
    
    public SearchResult getSearchDataOld(SearchEntity searchEntity) {
        
        logger.info("Entered to search");
        
        Collection dataList = null;
        ArrayList<LinkedHashMap> dataMap = new ArrayList<LinkedHashMap>();
        LinkedHashMap map=null;
        SearchResult searchResult = null;
        
        Entity entity = this.getEntityAttributesById(searchEntity.getEntityId());
        
        Collection attributesCollection = entity.getAttributes();
        Iterator attributesIterator =  attributesCollection.iterator();
        
        String[] displayAttribute = new String[attributesCollection.size()];
        String[] displayAttributeHeading = new String[attributesCollection.size()];
        String[] attributesName = new String[attributesCollection.size()];
        String[] attributesType = new String[attributesCollection.size()];        
        
        int attrCount = 0;
        int allCount = 0;
        while(attributesIterator.hasNext()){
            EntityAttribute attribute = (EntityAttribute)attributesIterator.next();
            attributesName[allCount] = attribute.getAttributeName();
            attributesType[allCount] = attribute.getType();
            //Change to get key field(1 sequence)
            if ((attribute.getIsDisplay()==1)||(attribute.getDisplaySequence()==1)){
                displayAttribute[attrCount] = attribute.getAttributeName();
                displayAttributeHeading[attrCount] = attribute.getResourcesKey();
                attrCount++;
            }
            allCount++;
        }        
      
        String queryBegin = "from " + entity.getEntityName();
        String queryEnd = searchEntity.getSortOrder()!= null ? " order by " + searchEntity.getSortOrder() : "";
        
        StringBuilder parameters = new StringBuilder();
        String queryStr = "";
        Map<String,Object> prop = new HashMap<String,Object>();
        
        Iterator it = searchEntity.getAttributes().iterator();
        
        while(it.hasNext()){            
            SearchAttribute tempAtt = (SearchAttribute)it.next();
            parameters.append(tempAtt.getAttributeName() + " " + tempAtt.getOparator() + ":" + tempAtt.getAttributeName() + " AND ");
            
            String exVal =tempAtt.getOparator().equalsIgnoreCase("like")? "%":"";
            for(int temp=0;temp<attributesName.length;temp++){
                logger.info("Looping Attributes " + attributesName[temp]);
                if (tempAtt.getAttributeName().equalsIgnoreCase(attributesName[temp])){
                   	logger.info(attributesType[temp]);
                    logger.info("MatchesAttributes " + attributesName[temp] + " to type " + attributesType[temp]);
                    tempAtt.setType(attributesType[temp]);
                    break;
                }
            }            
            if (tempAtt.getType().equalsIgnoreCase("Integer")){                
                prop.put(tempAtt.getAttributeName(),Integer.valueOf(tempAtt.getValue()));    
            }else if (tempAtt.getType().equalsIgnoreCase("String")){                
                prop.put(tempAtt.getAttributeName(),tempAtt.getValue()+ exVal);
            }else if (tempAtt.getType().equalsIgnoreCase("Decimal")){                
                prop.put(tempAtt.getAttributeName(),Double.valueOf(tempAtt.getValue()));                
            }else if (tempAtt.getType().equalsIgnoreCase("Date")){               
                prop.put(tempAtt.getAttributeName(),new Date(tempAtt.getValue()));            
            }
            else
                throw new BasicDataAccessException("Invalid data type for parameter !");
        }
        if (parameters.length() > 5)
            parameters.replace(parameters.length()-4,parameters.length()," ");
        
        logger.info("Parameter list " + parameters.toString());
        logger.info("Value list " + prop);

        if (prop.isEmpty()==false)
            queryBegin += " where ";
        
        queryStr = queryBegin + parameters.toString() + queryEnd;
        
        logger.info("Query = " + queryStr);
        
        Query query = getSession().createQuery(queryStr);
        
        query.setProperties(prop);        
        query.setReadOnly(true);
        
        dataList = query.list();
        
        logger.info("Query finish, got " + dataList.size() + " records.");
        
        logger.info("Start preparation of output data");
        
        Iterator searchIterator = dataList.iterator();
                
        Class clazz = null;         
       
        boolean isFirst = false;
        Method [] availableMethods = null;
        Method [] requireMethods = null;            
        
        while(searchIterator.hasNext()){                
            Object obj = searchIterator.next();
            clazz = obj.getClass();                
                           
            if (isFirst==false){
                availableMethods = clazz.getMethods();
                requireMethods = new Method[displayAttribute.length];
       
                for(int countArr=0;countArr<displayAttribute.length;countArr++){
                    for(int count=0;count<availableMethods.length;count++){
                        if (availableMethods[count].getName().equalsIgnoreCase("get" + displayAttribute[countArr])){
                            requireMethods[countArr] = availableMethods[count];
                        }                                
                    }
                }                    
                isFirst = true;
            }
            map = new LinkedHashMap(); 
            for(int invokeCount=0;invokeCount<requireMethods.length;invokeCount++){
                try{
                    if (requireMethods[invokeCount] != null)
                        map.put(requireMethods[invokeCount].getName(),requireMethods[invokeCount].invoke(obj));
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            dataMap.add(map);               
        }
        String[] displayAttributeHeadingOut = new String[attrCount];        
        for(int c = 0;c<attrCount;c++){
            displayAttributeHeadingOut[c] = displayAttributeHeading[c];
        }               
        searchResult = new SearchResult(dataMap,displayAttributeHeadingOut,dataList.size());
        
        logger.info("Finish preparation of output data");
        logger.info("*** Leaving from search ***");
        
        return searchResult;
    }
}