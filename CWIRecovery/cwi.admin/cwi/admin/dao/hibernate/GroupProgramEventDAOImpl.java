package cwi.admin.dao.hibernate;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cwi.admin.dao.GroupProgramEventDAO;
import cwi.admin.domain.GroupProgramEvent;
import cwi.basic.resourse.BasicDataAccessException;



public class GroupProgramEventDAOImpl extends HibernateDaoSupport implements GroupProgramEventDAO {

	public void addGruopProgramEvent(GroupProgramEvent groupProgramEvent) {
		getHibernateTemplate().save(groupProgramEvent);
	}

	public void deleteGroupProgramEvent(GroupProgramEvent groupProgramEvent) {
		getHibernateTemplate().delete(groupProgramEvent);
	}

	public Collection<GroupProgramEvent> getAllGroupProgramEvent() {
		Criteria criteria = getSession().createCriteria(GroupProgramEvent.class);
		if(criteria.list()!=null)
			return criteria.list();
		else
			throw new BasicDataAccessException("No records found.");
	}

	public Collection<GroupProgramEvent> getAllGroupProgramEventByGroupProgramId(int groupProgramId) {
		Criteria criteria = getSession().createCriteria(GroupProgramEvent.class);
		criteria.add(Restrictions.eq("groupProgramId", groupProgramId));
		if (criteria.list()!=null) {
			return criteria.list();
		}else
			throw new BasicDataAccessException("No records found.");
	}

	public GroupProgramEvent getGroupProgramEventById(int groupProgramEventId) {
		Criteria criteria = getSession().createCriteria(GroupProgramEvent.class);
		criteria.add(Restrictions.eq("groupProgramEventId", groupProgramEventId));
		if (criteria.uniqueResult()!=null) {
			return (GroupProgramEvent) criteria.uniqueResult();
		}else
			throw new BasicDataAccessException("No records found.");
	}

	public void modifyGroupProgramEvent(GroupProgramEvent groupProgramEvent) {
		getHibernateTemplate().update(groupProgramEvent);
	}
}