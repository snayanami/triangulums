package cwi.admin.dao.hibernate;

import cwi.admin.dao.ChangePasswordDAO;
import cwi.admin.domain.Officer;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.CommonDaoSupport;
import cwi.basic.resourse.UserSessionConfig;

public class ChangePasswordDAOImpl extends CommonDaoSupport implements ChangePasswordDAO {
	
	public void changePassword(UserSessionConfig userSessionConfig, int userId, String currentPassword, String newPassword){
		Officer officer = (Officer)getHibernateTemplate().load(Officer.class, userId);
		if(officer==null)
			throw new BasicDataAccessException("Record not found.");
		if(!officer.getPassword().equals(currentPassword))
			throw new BasicDataAccessException("Current Password is invalid.");
		if(currentPassword.equals(newPassword))
			throw new BasicDataAccessException("Current Password and New Password cannot be same.");
		
		officer.setPassword(newPassword);
		getHibernateTemplate().update(officer);
	}
}
