package cwi.admin.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cwi.admin.dao.BranchOfficerDAO;
import cwi.admin.domain.Branch;
import cwi.admin.domain.BranchOfficer;
import cwi.admin.domain.Officer;
import cwi.admin.domain.RefProgramData;
import cwi.admin.domain.Stakeholder;
import cwi.admin.dto.BranchOfficerDTO;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.CommonDaoSupport;
import cwi.basic.resourse.UserSessionConfig;



public class BranchOfficerDAOImpl extends CommonDaoSupport implements BranchOfficerDAO {
	
	public void createBranchOfficer(UserSessionConfig userSessionConfig, BranchOfficer branchOfficer){
		Criteria criteria1 = getSession().createCriteria(BranchOfficer.class);
		criteria1.add(Restrictions.eq("branchId", branchOfficer.getBranchId()));
		criteria1.add(Restrictions.eq("officerId", branchOfficer.getOfficerId()));
		criteria1.setProjection(Projections.rowCount());          
        int count = ((Integer) criteria1.uniqueResult()).intValue();
        if (count !=0)
            throw new BasicDataAccessException("Record already exist.");
        
        if(branchOfficer.getIsDefault()==1){
        	Criteria criteria2 = getSession().createCriteria(BranchOfficer.class);
    		criteria2.add(Restrictions.eq("officerId", branchOfficer.getOfficerId()));
    		criteria2.add(Restrictions.eq("isDefault", 1));
    		BranchOfficer brnOfficer = (BranchOfficer)criteria2.uniqueResult();
    		if(brnOfficer!=null){
    			brnOfficer.setIsDefault(0);
    			getHibernateTemplate().update(brnOfficer);
    		}
        }
        
        branchOfficer.setUserId(userSessionConfig.getOfficerId());
        branchOfficer.setLastUpdatedDate(userSessionConfig.getLoginDate());
        branchOfficer.setCreatedTime(getCreatedTime());
        
        getHibernateTemplate().save(branchOfficer);
	}
	
	public void updateBranchOfficer(UserSessionConfig userSessionConfig, BranchOfficer branchOfficer){
		if(branchOfficer.getIsDefault()==1){
        	Criteria criteria2 = getSession().createCriteria(BranchOfficer.class);
    		criteria2.add(Restrictions.eq("officerId", branchOfficer.getOfficerId()));
    		criteria2.add(Restrictions.eq("isDefault", 1));
    		BranchOfficer brnOfficer = (BranchOfficer)criteria2.uniqueResult();
    		if(brnOfficer!=null){
    			brnOfficer.setIsDefault(0);
    			getHibernateTemplate().update(brnOfficer);
    		}
        }
		
		branchOfficer.setUserId(userSessionConfig.getOfficerId());
        branchOfficer.setLastUpdatedDate(userSessionConfig.getLoginDate());
        branchOfficer.setCreatedTime(getCreatedTime());
		getHibernateTemplate().update(branchOfficer);
	}
	
	public void deleteBranchOfficer(UserSessionConfig userSessionConfig, int recordId){
		Criteria criteria = getSession().createCriteria(BranchOfficer.class);
		criteria.add(Restrictions.eq("branchOfficerId", recordId));
		BranchOfficer branchOfficer = (BranchOfficer)criteria.uniqueResult();
		if(branchOfficer.getIsDefault()==1)
			throw new BasicDataAccessException("Cannot Delete Default Record");
		
		getHibernateTemplate().delete(branchOfficer);
	}
	
	public List<BranchOfficer> getAllBranchOfficerByOfficerId(UserSessionConfig userSessionConfig, int officerId){
		Criteria criteria = getSession().createCriteria(BranchOfficer.class);
		criteria.add(Restrictions.eq("officerId", officerId));
		List<BranchOfficer> returnList = criteria.list();
		return returnList;
	}
	
	public BranchOfficer getBranchOfficerById(UserSessionConfig userSessionConfig, int recordId){
		Criteria criteria = getSession().createCriteria(BranchOfficer.class);
		criteria.add(Restrictions.eq("branchOfficerId", recordId));
		BranchOfficer branchOfficer = (BranchOfficer)criteria.uniqueResult();
		return branchOfficer;
	}
	
	
	
	
	
	
	
	
	
	
	

	public void addBranchOfficer(BranchOfficer branchOfficer) {
		if(branchOfficer.getIsDefault()==0){
			Criteria criteria = getSession().createCriteria(BranchOfficer.class);
			criteria.add(Restrictions.eq("branchId", branchOfficer.getBranchId()));
			criteria.add(Restrictions.eq("officerId", branchOfficer.getOfficerId()));
			criteria.add(Restrictions.eq("isDefault",branchOfficer.getIsDefault()));
			//criteria.add(Restrictions.eq("accessGroupId",branchOfficer.getAccessGroupId()));
			if(criteria.uniqueResult()!=null)
				throw new BasicDataAccessException("Branch Officer already exists.");
			else{
				getHibernateTemplate().save(branchOfficer);
			}					
		}else{
			Criteria criteria = getSession().createCriteria(BranchOfficer.class);
			criteria.add(Restrictions.eq("branchId", branchOfficer.getBranchId()));
			criteria.add(Restrictions.eq("isDefault",branchOfficer.getIsDefault()));
			if(criteria.uniqueResult()!=null)
				throw new BasicDataAccessException("Branch Officer already exists with a default value.");
			else{
				getHibernateTemplate().save(branchOfficer);
			}
		}		
	}

	public void deleteBranchOfficer(BranchOfficer branchOfficer) {
		getHibernateTemplate().delete(branchOfficer);
	}

	public Collection<BranchOfficerDTO> getAllBranchOfficer() {
		Criteria criteria = getSession().createCriteria(BranchOfficer.class);
		Collection<BranchOfficerDTO> dtoCollection = new ArrayList<BranchOfficerDTO>();
		if(criteria.list()!=null){
			Collection<BranchOfficer> collection = criteria.list();
			for(BranchOfficer branchOfficer : collection){
				BranchOfficerDTO branchOfficerDTO = new BranchOfficerDTO();
				branchOfficerDTO.setOffiderId(branchOfficer.getBranchOfficerId());
				
				Criteria criteria1 = getSession().createCriteria(Officer.class);
				criteria1.add(Restrictions.eq("officerId", branchOfficer.getOfficerId()));
				Officer officer = (Officer) criteria1.uniqueResult();
				Criteria criteria2 = getSession().createCriteria(Stakeholder.class);
				criteria2.add(Restrictions.eq("stakeholderId", officer.getStakeholderId()));
				Stakeholder stakeholder = (Stakeholder) criteria2.uniqueResult();
				
				branchOfficerDTO.setOffiderId(branchOfficer.getOfficerId());
				branchOfficerDTO.setOfficerName(stakeholder.getInitials()+" "+stakeholder.getLastName());
				
				Criteria criteria3 = getSession().createCriteria(Branch.class);
				criteria3.add(Restrictions.eq("branchId", branchOfficer.getBranchId()));
				Branch branch = (Branch) criteria3.uniqueResult();
				
				branchOfficerDTO.setBranchId(branch.getBranchId());
				branchOfficerDTO.setBranchCode(branch.getBranchCode());
				branchOfficerDTO.setBranchName(branch.getBranchName());
				
				Criteria criteria4 = getSession().createCriteria(RefProgramData.class);
				//criteria4.add(Restrictions.eq("refProgDataId", branchOfficer.getAccessGroupId()));
				RefProgramData refProgramData = (RefProgramData) criteria4.uniqueResult();
				
				branchOfficerDTO.setAccessGruopId(refProgramData.getRefProgDataId());
				branchOfficerDTO.setAccessGruopCode(refProgramData.getRefProgDataCode());
				branchOfficerDTO.setAccessGruopName(refProgramData.getRefProgDataDescription());
				dtoCollection.add(branchOfficerDTO);
			}
			return dtoCollection;
		}			
		else
			throw new BasicDataAccessException("No records found.");
	}

	public BranchOfficer getBranchOfficerById(int branchOfficerId) {
		Criteria criteria = getSession().createCriteria(Branch.class);
		criteria.add(Restrictions.eq("branchOfficerId", branchOfficerId));
		if(criteria.uniqueResult()!=null)
			return (BranchOfficer) criteria.uniqueResult();
		else
			throw new BasicDataAccessException("No such record found.");
	}

	public void modifyBranchOfficer(BranchOfficer branchOfficer) {
		getHibernateTemplate().update(branchOfficer);
	}

	public Collection<BranchOfficer> getAllBranchOfficerByBranchId(int branchId) {
		Criteria criteria = getSession().createCriteria(BranchOfficer.class);
		criteria.add(Restrictions.eq("branchId", branchId));
		if(criteria.list()!=null)
			return criteria.list();
		else
			throw new BasicDataAccessException("No records found.");
	}

	public Stakeholder getBranchOfficerDetailsById(int branchOfficerId) {
		Criteria criteria = getSession().createCriteria(BranchOfficer.class);
		criteria.add(Restrictions.eq("branchOfficerId", branchOfficerId));
		BranchOfficer branchOfficer = (BranchOfficer) criteria.uniqueResult();
		Criteria criteria1 = getSession().createCriteria(Officer.class);
		criteria1.add(Restrictions.eq("branchId", branchOfficer.getBranchId()));
		Officer officer = (Officer) criteria1.uniqueResult();
		Criteria criteria2 = getSession().createCriteria(Stakeholder.class);
		criteria2.add(Restrictions.eq("stakeholderId", officer.getStakeholderId()));
		return (Stakeholder) criteria2.uniqueResult();
	}

	public boolean isBranchOfficerExists(int branchId, int officerId) {
		Criteria criteria = getSession().createCriteria(BranchOfficer.class);
		criteria.add(Restrictions.eq("branchId", branchId));
		criteria.add(Restrictions.eq("officerId", officerId));
		criteria.add(Restrictions.eq("isDefault", 1));
		if(criteria.uniqueResult()!=null)
			return true;
		else
			return false;
	}
}