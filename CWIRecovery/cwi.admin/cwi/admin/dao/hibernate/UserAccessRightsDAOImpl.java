package cwi.admin.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import cwi.admin.dao.UserAccessRightsDAO;
import cwi.admin.domain.Event;
import cwi.admin.domain.GroupProgram;
import cwi.admin.domain.GroupProgramEvent;
import cwi.admin.domain.ProgramEvent;
import cwi.admin.domain.SystemProgram;
import cwi.admin.dto.SystemTreeDTO;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.CommonDaoSupport;
import cwi.basic.resourse.ParentProgramEnum;
import cwi.basic.resourse.UserSessionConfig;

public class UserAccessRightsDAOImpl extends CommonDaoSupport implements UserAccessRightsDAO {
	
	public List<SystemTreeDTO> getSystemPrograms(UserSessionConfig userSessionConfig){
		List<SystemTreeDTO> returnList = new ArrayList<SystemTreeDTO>();
		
		Query query = getSession().createQuery("SELECT pe FROM SystemProgram sp, ProgramEvent pe WHERE sp.systemProgramId=pe.systemProgramId ORDER BY sp.parentProgramId, sp.systemProgramId, pe.event");
		List<ProgramEvent> list = query.list();
		for (ProgramEvent programEvent : list) {
			SystemTreeDTO systemTreeDTO = new SystemTreeDTO();
			
			SystemProgram systemProgram = (SystemProgram)getHibernateTemplate().get(SystemProgram.class, programEvent.getSystemProgramId());
			Event event = (Event)getHibernateTemplate().get(Event.class, programEvent.getEvent());
			systemTreeDTO.setParentProgramId(systemProgram.getParentProgramId()==1?ParentProgramEnum.REF.getId():systemProgram.getParentProgramId()==2?ParentProgramEnum.PAWN.getId():systemProgram.getParentProgramId()==3?ParentProgramEnum.GL.getId():systemProgram.getParentProgramId()==4?ParentProgramEnum.INFO.getId():systemProgram.getParentProgramId()==5?ParentProgramEnum.ADMIN.getId():systemProgram.getParentProgramId()==6?ParentProgramEnum.CASHIER.getId():systemProgram.getParentProgramId()==7?ParentProgramEnum.DASHBOARD.getId():systemProgram.getParentProgramId()==8?ParentProgramEnum.MAINTENANCE.getId():0);
			systemTreeDTO.setParentProgramName(systemProgram.getParentProgramId()==1?ParentProgramEnum.REF.getName():systemProgram.getParentProgramId()==2?ParentProgramEnum.PAWN.getName():systemProgram.getParentProgramId()==3?ParentProgramEnum.GL.getName():systemProgram.getParentProgramId()==4?ParentProgramEnum.INFO.getName():systemProgram.getParentProgramId()==5?ParentProgramEnum.ADMIN.getName():systemProgram.getParentProgramId()==6?ParentProgramEnum.CASHIER.getName():systemProgram.getParentProgramId()==7?ParentProgramEnum.DASHBOARD.getName():systemProgram.getParentProgramId()==8?ParentProgramEnum.MAINTENANCE.getName():"");
			systemTreeDTO.setProgramId(systemProgram.getSystemProgramId());
			systemTreeDTO.setProgramName(systemProgram.getSystemProgramName());
			systemTreeDTO.setEventId(programEvent.getEvent());
			systemTreeDTO.setEventName(event.getEventName());
			
			returnList.add(systemTreeDTO);
		}
		return returnList;
	}
	
	public List<SystemTreeDTO> getGroupSystemPrograms(UserSessionConfig userSessionConfig, int accessGroupId){
		List<SystemTreeDTO> returnList = new ArrayList<SystemTreeDTO>();
		
		Query query = getSession().createQuery("SELECT pe FROM SystemProgram sp, ProgramEvent pe WHERE sp.systemProgramId=pe.systemProgramId ORDER BY sp.parentProgramId, sp.systemProgramId, pe.event");
		List<ProgramEvent> list = query.list();
		for (ProgramEvent programEvent : list) {
			SystemTreeDTO systemTreeDTO = new SystemTreeDTO();
			
			SystemProgram systemProgram = (SystemProgram)getHibernateTemplate().get(SystemProgram.class, programEvent.getSystemProgramId());
			Event event = (Event)getHibernateTemplate().get(Event.class, programEvent.getEvent());
			
			Criteria grpPrgCriteria = getSession().createCriteria(GroupProgram.class);
			grpPrgCriteria.add(Restrictions.eq("referenceProgramDataId", accessGroupId));
			grpPrgCriteria.add(Restrictions.eq("systemProgramId", programEvent.getSystemProgramId()));
			GroupProgram groupProgram = (GroupProgram)grpPrgCriteria.uniqueResult();
			
			Criteria grpPrgEvnCriteria = getSession().createCriteria(GroupProgramEvent.class);
			grpPrgEvnCriteria.add(Restrictions.eq("groupProgramId", groupProgram!=null?groupProgram.getGroupProgramId():0));
			grpPrgEvnCriteria.add(Restrictions.eq("eventId", programEvent.getEvent()));
			GroupProgramEvent groupProgramEvent = (GroupProgramEvent)grpPrgEvnCriteria.uniqueResult();
			
			systemTreeDTO.setParentProgramId(systemProgram.getParentProgramId()==1?ParentProgramEnum.REF.getId():systemProgram.getParentProgramId()==2?ParentProgramEnum.PAWN.getId():systemProgram.getParentProgramId()==3?ParentProgramEnum.GL.getId():systemProgram.getParentProgramId()==4?ParentProgramEnum.INFO.getId():systemProgram.getParentProgramId()==5?ParentProgramEnum.ADMIN.getId():systemProgram.getParentProgramId()==6?ParentProgramEnum.CASHIER.getId():systemProgram.getParentProgramId()==7?ParentProgramEnum.DASHBOARD.getId():systemProgram.getParentProgramId()==8?ParentProgramEnum.MAINTENANCE.getId():0);
			systemTreeDTO.setParentProgramName(systemProgram.getParentProgramId()==1?ParentProgramEnum.REF.getName():systemProgram.getParentProgramId()==2?ParentProgramEnum.PAWN.getName():systemProgram.getParentProgramId()==3?ParentProgramEnum.GL.getName():systemProgram.getParentProgramId()==4?ParentProgramEnum.INFO.getName():systemProgram.getParentProgramId()==5?ParentProgramEnum.ADMIN.getName():systemProgram.getParentProgramId()==6?ParentProgramEnum.CASHIER.getName():systemProgram.getParentProgramId()==7?ParentProgramEnum.DASHBOARD.getName():systemProgram.getParentProgramId()==8?ParentProgramEnum.MAINTENANCE.getName():"");
			systemTreeDTO.setProgramId(systemProgram.getSystemProgramId());
			systemTreeDTO.setProgramName(systemProgram.getSystemProgramName());
			systemTreeDTO.setEventId(programEvent.getEvent());
			systemTreeDTO.setEventName(event.getEventName());
			//systemTreeDTO.setIsGrant((groupProgram!=null?groupProgram.getSystemProgramId():0)==programEvent.getSystemProgramId() || (groupProgramEvent!=null?groupProgramEvent.getEventId():0)==programEvent.getEvent());
			systemTreeDTO.setIsGrant((groupProgramEvent!=null?groupProgramEvent.getEventId():0)==programEvent.getEvent());
			
			returnList.add(systemTreeDTO);
		}
		return returnList;
	}
	
	public void createAccessRights(UserSessionConfig userSessionConfig, int accessGroupId, String dataString){
//		Criteria brnOffCriteria = getSession().createCriteria(BranchOfficer.class);
//		brnOffCriteria.add(Restrictions.eq("officerId", userSessionConfig.getOfficerId()));
//		brnOffCriteria.add(Restrictions.eq("referenceProgramDataId", accessGroupId));
//		List<BranchOfficer> brnOffList = brnOffCriteria.list();
//		if(brnOffList.size()>0){
//			throw new BasicDataAccessException("Not authorized.");
//		}
		
		if(dataString.equals("") || dataString==null){
			throw new BasicDataAccessException("Please select the records.");
		}
		
		Criteria grpPrgDelCriteria = getSession().createCriteria(GroupProgram.class);
		grpPrgDelCriteria.add(Restrictions.eq("referenceProgramDataId", accessGroupId));
		List<GroupProgram> grpPrgDelList = grpPrgDelCriteria.list();
		
		grpPrgDelCriteria.setProjection(Projections.property("groupProgramId"));
		List<Integer> grpPrgDelIdList = grpPrgDelCriteria.list();
		
		List<GroupProgramEvent> grpPrgEvnDelList = new ArrayList<GroupProgramEvent>();
		if(!grpPrgDelIdList.isEmpty() && grpPrgDelIdList!=null){
			Criteria grpPrgEvnDelCriteria = getSession().createCriteria(GroupProgramEvent.class);
			grpPrgEvnDelCriteria.add(Restrictions.in("groupProgramId", grpPrgDelIdList));
			grpPrgEvnDelList = grpPrgEvnDelCriteria.list();
		}
		
		
		getHibernateTemplate().deleteAll(grpPrgEvnDelList);
		System.out.println("Deleted Previous Group Program Events");
		getHibernateTemplate().deleteAll(grpPrgDelList);
		System.out.println("Deleted Previous Group Programs");
		
		String[] rows = dataString.split("<row>");
		
		int prePrgId = -1;
		boolean isAdd = true;
		GroupProgram preGroupProgram = null;
		for (int i = 0; i < rows.length; i++) {
			String cols[] = rows[i].split("<col>");
			
			int parentId = Integer.parseInt((cols[0]));
			int programId = Integer.parseInt((cols[1]));
			int eventId = Integer.parseInt((cols[2]));
			
			GroupProgram groupProgram = new GroupProgram();
			groupProgram.setParentProgramId(parentId);
			groupProgram.setSystemProgramId(programId);
			groupProgram.setReferenceProgramDataId(accessGroupId);
			
			if(prePrgId == groupProgram.getSystemProgramId()){
				isAdd=false;
			}else{
				isAdd=true;
				prePrgId=groupProgram.getSystemProgramId();
				preGroupProgram = groupProgram;
			}
			
			if(isAdd)
				getHibernateTemplate().save(preGroupProgram);
			
			
			GroupProgramEvent groupProgramEvent = new GroupProgramEvent();
			groupProgramEvent.setGroupProgramId(preGroupProgram.getGroupProgramId());
			groupProgramEvent.setEventId(eventId);
			
			getHibernateTemplate().save(groupProgramEvent);
			
			
		}
		
	}
	
}
