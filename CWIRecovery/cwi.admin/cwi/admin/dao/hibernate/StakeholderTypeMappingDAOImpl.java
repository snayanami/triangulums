package cwi.admin.dao.hibernate;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cwi.admin.dao.StakeholderTypeMappingDAO;
import cwi.admin.domain.RefProgramData;
import cwi.admin.domain.StakeholderTypeMapping;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.StakeholderTypeEnum;
import cwi.basic.resourse.UserSessionConfig;



public class StakeholderTypeMappingDAOImpl extends HibernateDaoSupport implements StakeholderTypeMappingDAO {
	
	public void createStakeholderTypeMapping(UserSessionConfig userSessionConfig, StakeholderTypeMapping stakeholderTypeMapping){
		Criteria criteria = getSession().createCriteria(StakeholderTypeMapping.class);
		criteria.add(Restrictions.eq("stakeholderId", stakeholderTypeMapping.getStakeholderId()));
		criteria.add(Restrictions.eq("referenceProgramDataId", stakeholderTypeMapping.getReferenceProgramDataId()));
		criteria.setProjection(Projections.rowCount());          
        int count = ((Integer) criteria.uniqueResult()).intValue();
        if (count !=0)
            throw new BasicDataAccessException("Record already exist.");
        
        getHibernateTemplate().save(stakeholderTypeMapping);
	}
	
	public void deleteStakeholderTypeMapping(UserSessionConfig userSessionConfig, int recordId){
		Criteria criteria = getSession().createCriteria(StakeholderTypeMapping.class);
		criteria.add(Restrictions.eq("stakeholderTypeMappingId", recordId));
		StakeholderTypeMapping stakeholderTypeMapping = (StakeholderTypeMapping)criteria.uniqueResult();
		
		RefProgramData stakeholderType = (RefProgramData)getHibernateTemplate().get(RefProgramData.class, stakeholderTypeMapping.getReferenceProgramDataId());
		
		Query mapQuery = getSession().createQuery("SELECT COUNT(map.stakeholderTypeMappingId) FROM StakeholderTypeMapping map WHERE map.stakeholderId=:stakeholderId");
		mapQuery.setInteger("stakeholderId", stakeholderTypeMapping.getStakeholderId());
		Number mapNum = (Number)mapQuery.uniqueResult();
		int mapCount = mapNum!=null?mapNum.intValue():0;
		
		if(mapCount==1){
			throw new BasicDataAccessException("Cannot delete all the Mapping.");
		}
		
		Query query = getSession().createQuery("SELECT COUNT(pt.pawningTicketId) FROM PawningTicket pt WHERE pt.clientId=:clientId");
		query.setInteger("clientId", stakeholderTypeMapping.getStakeholderId());
		Number num = (Number)query.uniqueResult();
		int count = num!=null?num.intValue():0;
		
		if(count>0 && stakeholderType.getRefProgDataCode().equalsIgnoreCase(StakeholderTypeEnum.CLIENT.getCode())){
			throw new BasicDataAccessException("Cannot delete the record. Client has existing Pawn Facilities.");
		}
		
		
		
		getHibernateTemplate().delete(stakeholderTypeMapping);
		
	}
	
	public List<StakeholderTypeMapping> getAllStakeholderTypeMppingByStakeholderId(UserSessionConfig userSessionConfig, int stakeholderId){
		Criteria criteria = getSession().createCriteria(StakeholderTypeMapping.class);
		criteria.add(Restrictions.eq("stakeholderId", stakeholderId));
		List<StakeholderTypeMapping> returnList = criteria.list();
		return returnList;
	}
	
	
}