package cwi.admin.dao.hibernate;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cwi.admin.dao.SystemProgramDAO;
import cwi.admin.domain.BranchOfficer;
import cwi.admin.domain.GroupProgram;
import cwi.admin.domain.GroupProgramEvent;
import cwi.admin.domain.Officer;
import cwi.admin.domain.ProgramEvent;
import cwi.admin.domain.SystemProgram;
import cwi.admin.spring.SystemProgramBD;
import cwi.basic.resourse.BasicDataAccessException;



public class SystemProgramDAOImpl extends HibernateDaoSupport implements SystemProgramDAO, SystemProgramBD {

	public void addSystemProgram(SystemProgram systemProgram) {
		getHibernateTemplate().save(systemProgram);
	}

	public void deleteSystemProgram(SystemProgram systemProgram) {
		getHibernateTemplate().delete(systemProgram);
	}

	public Collection<SystemProgram> getAllSystemProgram() {
		Criteria criteria = getSession().createCriteria(SystemProgram.class);
		if(criteria.list()!=null)
			return criteria.list();
		else
			throw new BasicDataAccessException("No records found.");
	}

	public SystemProgram getSystemProgramById(int systemProgramId) {
		Criteria criteria = getSession().createCriteria(SystemProgram.class);
		criteria.add(Restrictions.eq("systemProgramId", systemProgramId));
		if(criteria.uniqueResult()!=null)
			return (SystemProgram) criteria.uniqueResult();
		else
			throw new BasicDataAccessException("No such record found.");
	}

	public void modifySystemProgram(SystemProgram systemProgram) {
		getHibernateTemplate().update(systemProgram);
	}

	public JSONArray getSystemProgramDetailsForLogin(int officerId) {
		JSONArray array = new JSONArray();
		//Get Officer from Officer
		Criteria criteria = getSession().createCriteria(Officer.class);
		criteria.add(Restrictions.eq("officerId", officerId));
		if(criteria.uniqueResult()==null)
			throw new BasicDataAccessException("No such user found.");
		else{
			//Get Branch Officer according to officer Id
			Officer officer = (Officer) criteria.uniqueResult();
			Criteria branchOffCriteria = getSession().createCriteria(BranchOfficer.class);
			branchOffCriteria.add(Restrictions.eq("officerId", officer.getOfficerId()));
			branchOffCriteria.add(Restrictions.eq("isDefault",1));
			
			if(branchOffCriteria.uniqueResult()==null)
				throw new BasicDataAccessException("No such Branch Officer found.");
			else{
				//Get Group Programs according to Access Group
				BranchOfficer branchOfficer = (BranchOfficer) branchOffCriteria.uniqueResult();
				Criteria accessGrpProgCriteria = getSession().createCriteria(GroupProgram.class);
				//accessGrpProgCriteria.add(Restrictions.eq("referenceProgramDataId", branchOfficer.getAccessGroupId()));
				//accessGrpProgCriteria.add((Criterion) Projections.groupProperty("parentProgramId"));
				
				if (accessGrpProgCriteria.list()==null) {
					throw new BasicDataAccessException("No Programs exist.");
				}else{
					
					Collection<GroupProgram> collection = accessGrpProgCriteria.list();
					for(GroupProgram groupProgram : collection ){
						JSONArray subArray = new JSONArray();
						subArray.put(groupProgram.getSystemProgramId());
						subArray.put(groupProgram.getParentProgramId());
						
						
						//Get System Program Details
						Criteria systemProgcriteria = getSession().createCriteria(SystemProgram.class);
						systemProgcriteria.add(Restrictions.eq("systemProgramId", groupProgram.getSystemProgramId()));
						SystemProgram systemProgram = (SystemProgram) systemProgcriteria.uniqueResult();
						
						subArray.put(systemProgram.getSystemProgramCode());
						subArray.put(systemProgram.getSystemProgramName());
						subArray.put(systemProgram.getLink());
						
						//Get Group Program Event Details.
						Criteria eventCriteria = getSession().createCriteria(GroupProgramEvent.class);
						eventCriteria.add(Restrictions.eq("groupProgramId", groupProgram.getGroupProgramId()));
						Collection<GroupProgramEvent> groupProgramEventCollection= eventCriteria.list();
						
						for(GroupProgramEvent groupProgramEvent: groupProgramEventCollection){
							Criteria programEventCriteria = getSession().createCriteria(ProgramEvent.class);
							programEventCriteria.add(Restrictions.eq("programEventId", groupProgramEvent.getEventId()));
							ProgramEvent programEvent = (ProgramEvent) programEventCriteria.uniqueResult();
							subArray.put(programEvent.getEvent());
							
						}
						array.put(subArray);
					}
					return array;
				}
			}
		}
	}
}