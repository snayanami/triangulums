package cwi.admin.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cwi.admin.dao.OfficerDAO;
import cwi.admin.domain.Branch;
import cwi.admin.domain.BranchOfficer;
import cwi.admin.domain.Officer;
import cwi.admin.domain.Stakeholder;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.UserSessionConfig;



public class OfficerDAOImpl extends HibernateDaoSupport implements OfficerDAO {

	public void createOfficer(UserSessionConfig userSessionConfig, Officer officer){
		Criteria criteria1 = getSession().createCriteria(Officer.class);
		criteria1.add(Restrictions.eq("userName", officer.getUserName()));
		criteria1.setProjection(Projections.rowCount());          
        int count1 = ((Integer) criteria1.uniqueResult()).intValue();
        if (count1 !=0)
            throw new BasicDataAccessException("Already exist with given User Name.");
        
        Criteria  criteria2 = getSession().createCriteria(Officer.class);
        criteria2.add(Restrictions.eq("stakeholderId", officer.getStakeholderId()));
        criteria2.setProjection(Projections.rowCount());
        int count2 = ((Integer) criteria2.uniqueResult()).intValue();
        if (count2 !=0)
            throw new BasicDataAccessException("Profile already created for this Stakeholder");
        
        getHibernateTemplate().save(officer);
	}
	
	public void updateOfficer(UserSessionConfig userSessionConfig, Officer officer){
		getHibernateTemplate().update(officer);
	}
	
	public Officer getOfficerById(UserSessionConfig userSessionConfig,int recordId){
		Criteria criteria = getSession().createCriteria(Officer.class);
		criteria.add(Restrictions.eq("officerId", recordId));
		Officer officer = (Officer)criteria.uniqueResult();
		return officer;
	}
	
	public List<Officer> getAllOfficer(UserSessionConfig userSessionConfig){
		Criteria criteria = getSession().createCriteria(Officer.class);
		List<Officer> returnList = criteria.list();
		return returnList;
	}
	
	public List<Branch> getAllBranchForOfficerId(UserSessionConfig userSessionConfig, int officerId){
		List<Branch> branchList = new ArrayList<Branch>();
		Criteria criteria = getSession().createCriteria(BranchOfficer.class);
		criteria.add(Restrictions.eq("officerId", officerId));
		//criteria.addOrder(Order.desc("isDefault"));
		List<BranchOfficer> branchOfficerList = criteria.list();
		for (BranchOfficer branchOfficer : branchOfficerList) {
			Branch branch = (Branch)getHibernateTemplate().get(Branch.class, branchOfficer.getBranchId());
			branchList.add(branch);
		}
		return branchList;
	}
	
	public Branch getDefaultBranchByOfficerId(UserSessionConfig userSessionConfig, int officerId){
		
		Criteria criteria = getSession().createCriteria(BranchOfficer.class);
		criteria.add(Restrictions.eq("officerId", officerId));
		criteria.add(Restrictions.eq("isDefault", 1));
		BranchOfficer branchOfficer = (BranchOfficer)criteria.uniqueResult();
		
		Branch branch = (Branch)getHibernateTemplate().get(Branch.class, branchOfficer.getBranchId());
		return branch;
	}
		
	
}