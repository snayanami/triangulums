package cwi.admin.dao.hibernate;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cwi.admin.dao.ProgramEventDAO;
import cwi.admin.domain.ProgramEvent;
import cwi.basic.resourse.BasicDataAccessException;



public class ProgramEventDAOImpl extends HibernateDaoSupport implements ProgramEventDAO {

	public void addProgramEvent(ProgramEvent programEvent) {
		getHibernateTemplate().save(programEvent);
	}

	public void deleteProgramEvent(ProgramEvent programEvent) {
		getHibernateTemplate().delete(programEvent);
	}

	public Collection<ProgramEvent> getAllProgramEvent() {
		Criteria criteria = getSession().createCriteria(ProgramEvent.class);
		if(criteria.list()!=null)
			return criteria.list();
		else
			throw new BasicDataAccessException("No records found.");
	}

	public Collection<ProgramEvent> getAllProgramEventByProgramId(int programId) {
		Criteria criteria = getSession().createCriteria(ProgramEvent.class);
		criteria.add(Restrictions.eq("programId", programId));
		if(criteria.list()!=null)
			return criteria.list();
		else
			throw new BasicDataAccessException("No records found.");
	}

	public ProgramEvent getProgramEventById(int programEventId) {
		Criteria criteria = getSession().createCriteria(ProgramEvent.class);
		criteria.add(Restrictions.eq("programEventId", programEventId));
		if(criteria.uniqueResult()!=null)
			return (ProgramEvent) criteria.uniqueResult();
		else
			throw new BasicDataAccessException("No such record found.");
	}

	public void modifyProgramEvent(ProgramEvent programEvent) {
		getHibernateTemplate().update(programEvent);
	}
}