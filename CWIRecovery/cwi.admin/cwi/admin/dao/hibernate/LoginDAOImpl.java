package cwi.admin.dao.hibernate;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;

import cwi.admin.dao.LoginDAO;
import cwi.admin.domain.BranchOfficer;
import cwi.admin.domain.GroupProgram;
import cwi.admin.domain.GroupProgramEvent;
import cwi.admin.domain.Officer;
import cwi.admin.domain.ProgramEvent;
import cwi.admin.domain.RefProgramData;
import cwi.admin.domain.Stakeholder;
import cwi.admin.domain.SystemProgram;
import cwi.basic.resourse.CommonDaoSupport;
import cwi.basic.resourse.ReferenceProgramEnum;
import cwi.basic.resourse.UserSessionConfig;
import cwi.system.domain.BranchDayStatus;


public class LoginDAOImpl extends CommonDaoSupport implements LoginDAO {
	static DecimalFormat points2decimalFormat = new DecimalFormat();
	static {
		points2decimalFormat.setMinimumFractionDigits(2);
		points2decimalFormat.setMaximumFractionDigits(2);
		points2decimalFormat.setGroupingSize(3);
	}
	
	public UserSessionConfig systemLogin(String userName, String password) {
		UserSessionConfig userSessionConfig = null;
		
		Criteria criteria = getSession().createCriteria(Officer.class);
		criteria.add(Restrictions.eq("userName", userName));
		criteria.add(Restrictions.eq("password", password));
		Officer officer = (Officer)criteria.uniqueResult();
		
		if(officer!=null){
			userSessionConfig = new UserSessionConfig();
			Stakeholder stakeholder = (Stakeholder)getHibernateTemplate().get(Stakeholder.class, officer.getStakeholderId());
			
			Criteria brnOffCriteria = getSession().createCriteria(BranchOfficer.class);
			brnOffCriteria.add(Restrictions.eq("officerId", officer.getOfficerId()));
			brnOffCriteria.add(Restrictions.eq("isDefault", 1));
			BranchOfficer branchOfficer = (BranchOfficer)brnOffCriteria.uniqueResult();
			
			BranchDayStatus branchDayStatus  = getBranchDayStatusByBranchId(userSessionConfig, branchOfficer.getBranchId());
			
			userSessionConfig.setLoginUserName(stakeholder.getTitle().getRefProgDataCode()+" "+stakeholder.getFullName());
			userSessionConfig.setOfficerId(officer.getOfficerId());
			userSessionConfig.setUserName(officer.getUserName());
			
//			Calendar systemDate = Calendar.getInstance();
//			systemDate.getTime();
			
			userSessionConfig.setLoginDate(branchDayStatus.getBranchDate());
			userSessionConfig.setBranchId(branchOfficer.getBranchId());
		}
		return userSessionConfig;
	}
	
	public JSONArray getSystemProgramsForLogin(UserSessionConfig sessionConfig, int officerId){
		JSONArray mainArray = new JSONArray();
		BranchDayStatus branchDayStatus = getBranchDayStatusByBranchId(sessionConfig, sessionConfig.getBranchId());
		Criteria brnOffCriteria = getSession().createCriteria(BranchOfficer.class);
		brnOffCriteria.add(Restrictions.eq("officerId", officerId));
		brnOffCriteria.add(Restrictions.eq("branchId", sessionConfig.getBranchId()));
		BranchOfficer branchOfficer = (BranchOfficer)brnOffCriteria.uniqueResult();
		if(branchOfficer!=null){
			Criteria grpPrgCriteria = getSession().createCriteria(GroupProgram.class);
			grpPrgCriteria.add(Restrictions.eq("referenceProgramDataId", branchOfficer.getReferenceProgramDataId()));
			List<GroupProgram> grpPrgList = grpPrgCriteria.list();
			if(grpPrgList!=null && !grpPrgList.isEmpty()){
				for (GroupProgram groupProgram : grpPrgList) {
					SystemProgram systemProgram = (SystemProgram)getHibernateTemplate().get(SystemProgram.class, groupProgram.getSystemProgramId());
					JSONArray subArray = new JSONArray();
					if(systemProgram!=null){
						subArray.put(systemProgram.getSystemProgramId());
						subArray.put(systemProgram.getParentProgramId());
						subArray.put(systemProgram.getSystemProgramCode());
						subArray.put(systemProgram.getSystemProgramName());
						subArray.put(systemProgram.getLink());
						
						Criteria grpPrgEventCriteria = getSession().createCriteria(GroupProgramEvent.class);
						grpPrgEventCriteria.add(Restrictions.eq("groupProgramId", groupProgram.getGroupProgramId()));
						List<GroupProgramEvent> grpPrgEventList = grpPrgEventCriteria.list();
						if(grpPrgEventList!=null && !grpPrgEventList.isEmpty()){
							for (GroupProgramEvent groupProgramEvent : grpPrgEventList) {
								if(groupProgramEvent!=null){
									ProgramEvent programEvent = (ProgramEvent)getHibernateTemplate().get(ProgramEvent.class, groupProgramEvent.getEventId());
									subArray.put(programEvent.getEvent());
								}
							}
						}
						mainArray.put(subArray);
					}
				}
			}
		}
		sessionConfig.setLoginDate(branchDayStatus.getBranchDate());
		return mainArray;
	}
	
	public JSONArray getSystemProgramsForBranchChange(UserSessionConfig sessionConfig, int officerId, int branchId){
		JSONArray mainArray = new JSONArray();
		
		BranchDayStatus branchDayStatus = getBranchDayStatusByBranchId(sessionConfig, branchId);
		
		Criteria brnOffCriteria = getSession().createCriteria(BranchOfficer.class);
		brnOffCriteria.add(Restrictions.eq("officerId", officerId));
		brnOffCriteria.add(Restrictions.eq("branchId", branchId));
		BranchOfficer branchOfficer = (BranchOfficer)brnOffCriteria.uniqueResult();
		if(branchOfficer!=null){
			Criteria grpPrgCriteria = getSession().createCriteria(GroupProgram.class);
			grpPrgCriteria.add(Restrictions.eq("referenceProgramDataId", branchOfficer.getReferenceProgramDataId()));
			List<GroupProgram> grpPrgList = grpPrgCriteria.list();
			if(grpPrgList!=null && !grpPrgList.isEmpty()){
				for (GroupProgram groupProgram : grpPrgList) {
					SystemProgram systemProgram = (SystemProgram)getHibernateTemplate().get(SystemProgram.class, groupProgram.getSystemProgramId());
					JSONArray subArray = new JSONArray();
					if(systemProgram!=null){
						subArray.put(systemProgram.getSystemProgramId());
						subArray.put(systemProgram.getParentProgramId());
						subArray.put(systemProgram.getSystemProgramCode());
						subArray.put(systemProgram.getSystemProgramName());
						subArray.put(systemProgram.getLink());
						
						Criteria grpPrgEventCriteria = getSession().createCriteria(GroupProgramEvent.class);
						grpPrgEventCriteria.add(Restrictions.eq("groupProgramId", groupProgram.getGroupProgramId()));
						List<GroupProgramEvent> grpPrgEventList = grpPrgEventCriteria.list();
						if(grpPrgEventList!=null && !grpPrgEventList.isEmpty()){
							for (GroupProgramEvent groupProgramEvent : grpPrgEventList) {
								if(groupProgramEvent!=null){
									ProgramEvent programEvent = (ProgramEvent)getHibernateTemplate().get(ProgramEvent.class, groupProgramEvent.getEventId());
									subArray.put(programEvent.getEvent());
								}
							}
						}
						mainArray.put(subArray);
					}
				}
			}
		}
		sessionConfig.setBranchId(branchId);
		sessionConfig.setLoginDate(branchDayStatus.getBranchDate());
		return mainArray;
	}
	
	public String getGoldValueListString(){
		String goldValueListString = "";
		
		Criteria goldTypeCriteria = getSession().createCriteria(RefProgramData.class);
		goldTypeCriteria.add(Restrictions.eq("refPrgId", ReferenceProgramEnum.GOLD_TYPE));
		goldTypeCriteria.addOrder(Order.asc("refProgDataCode"));
		goldTypeCriteria.setProjection(Projections.property("refProgDataId"));
		List<Integer> goldTypeIdList = goldTypeCriteria.list();
		for (Integer goldTypeId : goldTypeIdList) {
			RefProgramData goldType = (RefProgramData)getHibernateTemplate().get(RefProgramData.class, goldTypeId);
			
			Query query = getSession().createQuery("select k.value FROM GoldValue k WHERE k.goldValueId = (SELECT MAX(kv.goldValueId) FROM GoldValue kv WHERE kv.goldType.refProgDataId=:goldTypeId AND k.effectiveDate<=:systemDate)");
			query.setInteger("goldTypeId", goldTypeId);
			query.setDate("systemDate", new Date());
			Number curVal = (Number)query.uniqueResult();
			double currentValue = curVal!=null?curVal.doubleValue():0;
			
			String value = "|  "+goldType.getRefProgDataCode()+" - Rs. "+points2decimalFormat.format(currentValue);
			
			goldValueListString += value;
		}
		return goldValueListString+"  |";
	}
	
}
