package cwi.admin.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import cwi.admin.dao.BlackListClientDAO;
import cwi.admin.domain.BlackListClientHistory;
import cwi.admin.domain.Stakeholder;
import cwi.basic.resourse.CommonDaoSupport;
import cwi.basic.resourse.UserSessionConfig;

public class BlackListClientDAOImpl extends CommonDaoSupport implements BlackListClientDAO {
	
//	public List<PawnDetailsDTO> getPawnDetailsByClientId(UserSessionConfig userSessionConfig, int clientId){
//		List<PawnDetailsDTO> returnList = new ArrayList<PawnDetailsDTO>();
//		Criteria criteria = getSession().createCriteria(PawningTicket.class);
//		criteria.add(Restrictions.eq("clientId", clientId));
//		List<PawningTicket> list = criteria.list();
//		for (PawningTicket pawningTicket : list) {
//			PawnDetailsDTO detailsDTO = new PawnDetailsDTO();
//			
//			Criteria dueCriteria = getSession().createCriteria(DueDetails.class);
//			dueCriteria.add(Restrictions.eq("pawningTicketId", pawningTicket.getPawningTicketId()));
//			DueDetails dueDetails = (DueDetails)dueCriteria.uniqueResult();
//			
//			detailsDTO.setPawnTicketNo(pawningTicket.getPawningTicketNo());
//			detailsDTO.setPawningAdvance(pawningTicket.getCapitalAmount());
//			detailsDTO.setBalanceCapital(dueDetails.getTotalCapitalAmount()-dueDetails.getSettledCapitalAmount());
//			detailsDTO.setInterestInArrears(dueDetails.getTotalInterestAmount()-dueDetails.getSettledWaivedInterestAmount());
//			detailsDTO.setOtherChargesArrears(dueDetails.getTotalOtherChargesAmount()-dueDetails.getSettledOtherChargesAmount());
//			detailsDTO.setTotalOutstanding(detailsDTO.getBalanceCapital()+detailsDTO.getInterestInArrears()+detailsDTO.getOtherChargesArrears());
//			detailsDTO.setPawningTicketStatus(pawningTicket.getStatus());
//			
//			returnList.add(detailsDTO);
//		}
//		return returnList;
//	}
	
	public int getIsBlackListed(UserSessionConfig userSessionConfig, int clientId){
		Query query = getSession().createQuery("SELECT s.isBlackListed FROM Stakeholder s WHERE s.stakeholderId=:stakeholderId");
		query.setInteger("stakeholderId", clientId);
		Number number = (Number)query.uniqueResult();
		int isBlackListed = number!=null?number.intValue():0;
		return isBlackListed;
	}
	
	public void updateBlackListClient(UserSessionConfig userSessionConfig, BlackListClientHistory blackListClientHistory){
		blackListClientHistory.setUserId(userSessionConfig.getOfficerId());
		blackListClientHistory.setLastUpdatedDate(userSessionConfig.getLoginDate());
		blackListClientHistory.setCreatedTime(getCreatedTime());
		
		getHibernateTemplate().save(blackListClientHistory);
		
		Stakeholder stakeholder = (Stakeholder)getHibernateTemplate().load(Stakeholder.class, blackListClientHistory.getClientId());
		if(blackListClientHistory.getBlackListStatus()==1)
			stakeholder.setIsBlackListed(1);
		else
			stakeholder.setIsBlackListed(0);
		
		getHibernateTemplate().update(stakeholder);
	}
}
