package cwi.admin.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cwi.admin.dao.RefProgramDataDAO;
import cwi.admin.domain.RefProgram;
import cwi.admin.domain.RefProgramData;
import cwi.basic.resourse.BasicDataAccessException;
import cwi.basic.resourse.UserSessionConfig;


public class RefProgramDataDAOImpl extends HibernateDaoSupport implements RefProgramDataDAO {

	public void createRefProgramData(UserSessionConfig userSessionConfig, RefProgramData refProgramData) {
		Criteria criteria = getSession().createCriteria(RefProgramData.class);
		criteria.add(Restrictions.eq("refProgDataCode", refProgramData.getRefProgDataCode()));
		criteria.setProjection(Projections.rowCount());          
        int count = ((Integer) criteria.uniqueResult()).intValue();
        if (count !=0)
            throw new BasicDataAccessException("Record already exist.");
		
		getHibernateTemplate().save(refProgramData);
	}
	
	public void updateRefProgramData(UserSessionConfig userSessionConfig, RefProgramData refProgramData) {
		getHibernateTemplate().update(refProgramData);
	}

	public void deleteRefProgramData(UserSessionConfig userSessionConfig, int recordId) {
		Criteria criteria = getSession().createCriteria(RefProgramData.class);
		criteria.add(Restrictions.eq("refProgDataId", recordId));
		RefProgramData refProgramData = (RefProgramData)criteria.uniqueResult();
		if(refProgramData!=null)
			getHibernateTemplate().delete(refProgramData);
		else
			throw new BasicDataAccessException("Record not found.");
	}
	
	public List<RefProgramData> getAllRefProgramDataByRefProgramId(UserSessionConfig userSessionConfig, int refProgramId){
		Criteria criteria = getSession().createCriteria(RefProgramData.class);
		criteria.add(Restrictions.eq("refPrgId", refProgramId));
		List<RefProgramData> returnList = criteria.list();
		return returnList;
	}
	
	public List<RefProgramData> getAllRefProgramData(UserSessionConfig userSessionConfig){
		Criteria criteria = getSession().createCriteria(RefProgramData.class);
		List<RefProgramData> returnList = criteria.list();
		return returnList;
	}
	
	public RefProgramData getRefProgramDataById(UserSessionConfig userSessionConfig, int recordId) {
		Criteria criteria = getSession().createCriteria(RefProgramData.class);
		criteria.add(Restrictions.eq("refProgDataId", recordId));
		RefProgramData refProgramData = (RefProgramData)criteria.uniqueResult();
		if(refProgramData!=null)
			return refProgramData;
		else
			throw new BasicDataAccessException("Record not found.");	
	}
	
}
