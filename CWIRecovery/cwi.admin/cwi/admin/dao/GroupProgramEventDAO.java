package cwi.admin.dao;

import java.util.Collection;

import cwi.admin.domain.GroupProgramEvent;


public interface GroupProgramEventDAO {

	public void addGruopProgramEvent(GroupProgramEvent groupProgramEvent);
	public void deleteGroupProgramEvent(GroupProgramEvent groupProgramEvent);
	public void modifyGroupProgramEvent(GroupProgramEvent groupProgramEvent);
	public GroupProgramEvent getGroupProgramEventById(int groupProgramEventId);
	public Collection<GroupProgramEvent> getAllGroupProgramEventByGroupProgramId(int groupProgramId);
	public Collection<GroupProgramEvent> getAllGroupProgramEvent();
}
