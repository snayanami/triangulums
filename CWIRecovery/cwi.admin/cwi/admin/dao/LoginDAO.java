package cwi.admin.dao;

import org.json.JSONArray;

import cwi.basic.resourse.UserSessionConfig;


public interface LoginDAO {
	
	public UserSessionConfig systemLogin(String userName, String password);
	public JSONArray getSystemProgramsForLogin(UserSessionConfig sessionConfig, int officerId);
	public JSONArray getSystemProgramsForBranchChange(UserSessionConfig sessionConfig, int officerId, int branchId);
	public String getGoldValueListString();
}
