package cwi.admin.dao;

import java.util.List;

import cwi.admin.dto.SystemTreeDTO;
import cwi.basic.resourse.UserSessionConfig;

public interface UserAccessRightsDAO {
	public List<SystemTreeDTO> getSystemPrograms(UserSessionConfig userSessionConfig);
	public List<SystemTreeDTO> getGroupSystemPrograms(UserSessionConfig userSessionConfig, int accessGroupId);
	public void createAccessRights(UserSessionConfig userSessionConfig, int accessGroupId, String dataString);
}
