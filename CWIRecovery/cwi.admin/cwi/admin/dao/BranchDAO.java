package cwi.admin.dao;

import java.util.Collection;
import java.util.List;

import cwi.admin.domain.Branch;
import cwi.admin.domain.RefProgram;
import cwi.basic.resourse.UserSessionConfig;


public interface BranchDAO {
	public void createBranch(UserSessionConfig userSessionConfig, Branch branch);
	public void updateBranch(UserSessionConfig userSessionConfig, Branch branch);
	public void deleteBranch(UserSessionConfig userSessionConfig, int recordId);
	public Branch getBranchById(UserSessionConfig userSessionConfig, int recordId);
	public List<Branch> getAllBranch(UserSessionConfig userSessionConfig);	
}
