package cwi.admin.dao;

import java.util.Collection;

import cwi.admin.domain.ProgramEvent;


public interface ProgramEventDAO {

	public void addProgramEvent(ProgramEvent programEvent);
	public void deleteProgramEvent(ProgramEvent programEvent);
	public void modifyProgramEvent(ProgramEvent programEvent);
	public ProgramEvent getProgramEventById(int programEventId);
	public Collection<ProgramEvent> getAllProgramEvent();
	public Collection<ProgramEvent> getAllProgramEventByProgramId(int programId);
}
