package cwi.admin.dao;

import java.util.Collection;
import java.util.List;

import cwi.admin.domain.RefProgramData;
import cwi.admin.domain.Stakeholder;
import cwi.basic.resourse.UserSessionConfig;


public interface StakeholderDAO {
	
	public void createStakeholder(UserSessionConfig userSessionConfig, Stakeholder stakeholder, int stakeholderTypeId);
	public void updateStakeholder(UserSessionConfig userSessionConfig, Stakeholder stakeholder);
	public Stakeholder getStakeholderById(UserSessionConfig userSessionConfig, int stakeholderId);
	public List<Stakeholder> getAllStakeholder(UserSessionConfig userSessionConfig);
	
	public List<RefProgramData> getAllTitles(UserSessionConfig userSessionConfig);

	public void addStakeholder(Stakeholder stakeholder);
	public void deleteStakeholder(Stakeholder stakeholder);
	public void modifyStakeholder(Stakeholder stakeholder);
	public Collection<Stakeholder> getAllStakeholder();
	
}
