package cwi.admin.dao;

import java.util.Collection;
import java.util.List;

import cwi.admin.domain.StakeholderTypeMapping;
import cwi.basic.resourse.UserSessionConfig;


public interface StakeholderTypeMappingDAO {

	public void createStakeholderTypeMapping(UserSessionConfig userSessionConfig, StakeholderTypeMapping stakeholderTypeMapping);
	public void deleteStakeholderTypeMapping(UserSessionConfig userSessionConfig, int recordId);
	public List<StakeholderTypeMapping> getAllStakeholderTypeMppingByStakeholderId(UserSessionConfig userSessionConfig, int stakeholderId);
}
