package cwi.admin.dao;

import cwi.basic.resourse.UserSessionConfig;

public interface ChangePasswordDAO {
	public void changePassword(UserSessionConfig userSessionConfig, int userId, String currentPassword, String newPassword);
}
