package cwi.admin.dao;

import java.util.Collection;

import org.json.JSONArray;

import cwi.admin.domain.SystemProgram;


public interface SystemProgramDAO {

	public void addSystemProgram(SystemProgram systemProgram);
	public void deleteSystemProgram(SystemProgram systemProgram);
	public void modifySystemProgram(SystemProgram systemProgram);
	public Collection<SystemProgram> getAllSystemProgram();
	public SystemProgram getSystemProgramById(int systemProgramId);
	public JSONArray getSystemProgramDetailsForLogin(int officerId);
}
