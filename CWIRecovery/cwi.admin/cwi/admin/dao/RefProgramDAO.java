package cwi.admin.dao;

import java.util.List;

import cwi.admin.domain.RefProgram;
import cwi.basic.resourse.UserSessionConfig;


public interface RefProgramDAO {

	public void createRefProgram(UserSessionConfig userSessionConfig, RefProgram refProgram);
	public void deleteRefProgram(UserSessionConfig userSessionConfig, int refProgramId);
	public void updateRefProgram(UserSessionConfig userSessionConfig, RefProgram refProgram);
	public List<RefProgram> getAllReferenceProgram(UserSessionConfig userSessionConfig);
	public RefProgram getReferenceProgramById(UserSessionConfig userSessionConfig, int recordId);
}