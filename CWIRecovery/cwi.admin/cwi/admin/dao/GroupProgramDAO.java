package cwi.admin.dao;

import java.util.Collection;

import cwi.admin.domain.GroupProgram;


public interface GroupProgramDAO {

	public void addGroupProgram(GroupProgram groupProgram);
	public void deleteGroupProgram(GroupProgram groupProgram);
	public void modifyGroupProgram(GroupProgram groupProgram);
	public GroupProgram getGroupProgramById(int groupProgramId);
	public Collection<GroupProgram> getAllGroupProgram();
	public Collection<GroupProgram> getAllGroupProgramByGroupId(int groupId);
}