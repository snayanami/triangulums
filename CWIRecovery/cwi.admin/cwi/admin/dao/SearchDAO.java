package cwi.admin.dao;

import cwi.basic.resourse.Entity;
import cwi.basic.resourse.SearchEntity;
import cwi.basic.resourse.SearchResult;


public interface SearchDAO {
    public Entity getEntityById(int entityId);
    public SearchResult getSearchData(SearchEntity searchEntity);
}
