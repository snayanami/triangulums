package cwi.admin.spring;

import cwi.admin.domain.BlackListClientHistory;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;

public interface BlackListClientBD {
//	public List<PawnDetailsDTO> getPawnDetailsByClientId(UserSessionConfig userSessionConfig, int clientId)throws BasicException;
	public int getIsBlackListed(UserSessionConfig userSessionConfig, int clientId)throws BasicException;
	public void updateBlackListClient(UserSessionConfig userSessionConfig, BlackListClientHistory blackListClientHistory)throws BasicException;
}
