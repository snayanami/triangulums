package cwi.admin.spring.service;

import java.util.List;

import cwi.admin.dao.RefProgramDataDAO;
import cwi.admin.domain.RefProgramData;
import cwi.admin.spring.RefProgramDataBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;


public class RefProgramDataBDImpl implements RefProgramDataBD {

	private RefProgramDataDAO refProgramDataDAO;
	public RefProgramDataDAO getRefProgramDataDAO() {
		return refProgramDataDAO;
	}
	public void setRefProgramDataDAO(RefProgramDataDAO refProgramDataDAO) {
		this.refProgramDataDAO = refProgramDataDAO;
	}
	
	public void createRefProgramData(UserSessionConfig userSessionConfig, RefProgramData refProgramData) throws BasicException {
		getRefProgramDataDAO().createRefProgramData(userSessionConfig, refProgramData);
	}
	
	public void deleteRefProgramData(UserSessionConfig userSessionConfig, int recordId) throws BasicException {
		getRefProgramDataDAO().deleteRefProgramData(userSessionConfig, recordId);
	}
	
	public List<RefProgramData> getAllRefProgramData(UserSessionConfig userSessionConfig) throws BasicException {
		return getRefProgramDataDAO().getAllRefProgramData(userSessionConfig);
	}
	
	public List<RefProgramData> getAllRefProgramDataByRefProgramId(UserSessionConfig userSessionConfig, int refProgramId) throws BasicException {
		return getRefProgramDataDAO().getAllRefProgramDataByRefProgramId(userSessionConfig, refProgramId);
	}
	
	public RefProgramData getRefProgramDataById(UserSessionConfig userSessionConfig, int recordId) throws BasicException {
		return getRefProgramDataDAO().getRefProgramDataById(userSessionConfig, recordId);
	}
	
	public void updateRefProgramData(UserSessionConfig userSessionConfig, RefProgramData refProgramData) throws BasicException {
		getRefProgramDataDAO().updateRefProgramData(userSessionConfig, refProgramData);
	}
	
}