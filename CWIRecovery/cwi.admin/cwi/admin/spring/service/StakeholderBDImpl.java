package cwi.admin.spring.service;

import java.util.Collection;
import java.util.List;

import cwi.admin.dao.StakeholderDAO;
import cwi.admin.domain.RefProgramData;
import cwi.admin.domain.Stakeholder;
import cwi.admin.spring.StakeholderBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;


public class StakeholderBDImpl implements StakeholderBD {

	private StakeholderDAO stakeholderDAO;
	public StakeholderDAO getStakeholderDAO() {
		return stakeholderDAO;
	}
	public void setStakeholderDAO(StakeholderDAO stakeholderDAO) {
		this.stakeholderDAO = stakeholderDAO;
	}
	
	public void createStakeholder(UserSessionConfig userSessionConfig, Stakeholder stakeholder, int stakeholderTypeId) throws BasicException {
		getStakeholderDAO().createStakeholder(userSessionConfig, stakeholder, stakeholderTypeId);
	}
	
	public void updateStakeholder(UserSessionConfig userSessionConfig, Stakeholder stakeholder) throws BasicException {
		getStakeholderDAO().updateStakeholder(userSessionConfig, stakeholder);
	}
	
	public Stakeholder getStakeholderById(UserSessionConfig userSessionConfig, int stakeholderId)throws BasicException {
		return getStakeholderDAO().getStakeholderById(userSessionConfig, stakeholderId);
	}
	
	public List<Stakeholder> getAllStakeholder(UserSessionConfig userSessionConfig) throws BasicException {
		return getStakeholderDAO().getAllStakeholder(userSessionConfig);
	}
	
	
	
	
	
	
	public List<RefProgramData> getAllTitles(UserSessionConfig userSessionConfig) throws BasicException {
		return getStakeholderDAO().getAllTitles(userSessionConfig);
	}

	public void addStakeholder(Stakeholder stakeholder) throws BasicException {
		getStakeholderDAO().addStakeholder(stakeholder);
	}

	public void deleteStakeholder(Stakeholder stakeholder)throws BasicException {
		getStakeholderDAO().deleteStakeholder(stakeholder);
	}

	public Collection<Stakeholder> getAllStakeholder() throws BasicException{
		return getStakeholderDAO().getAllStakeholder();
	}

	

	public void modifyStakeholder(Stakeholder stakeholder)throws BasicException {
		getStakeholderDAO().modifyStakeholder(stakeholder);
	}
	
	
	
	
	
}