package cwi.admin.spring.service;

import org.json.JSONArray;

import cwi.admin.dao.LoginDAO;
import cwi.admin.spring.LoginBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;


public class LoginBDImpl implements LoginBD {
	private LoginDAO loginDAO;
	public LoginDAO getLoginDAO() {
		return loginDAO;
	}
	public void setLoginDAO(LoginDAO loginDAO) {
		this.loginDAO = loginDAO;
	}

	public UserSessionConfig systemLogin(String userName, String password) throws BasicException {
		return getLoginDAO().systemLogin(userName, password);
	}
	
	public JSONArray getSystemProgramsForLogin(UserSessionConfig sessionConfig, int officerId) throws BasicException {
		return getLoginDAO().getSystemProgramsForLogin(sessionConfig, officerId);
	}
	
	public JSONArray getSystemProgramsForBranchChange(UserSessionConfig sessionConfig, int officerId, int branchId) throws BasicException {
		return getLoginDAO().getSystemProgramsForBranchChange(sessionConfig, officerId, branchId);
	}
	
	public String getGoldValueListString() throws BasicException {
		return getLoginDAO().getGoldValueListString();
	}

}
