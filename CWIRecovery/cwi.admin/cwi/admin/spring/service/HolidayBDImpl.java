package cwi.admin.spring.service;

import java.util.Date;
import java.util.List;

import cwi.admin.dao.HolidayDAO;
import cwi.admin.domain.Holiday;
import cwi.admin.spring.HolidayBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;


public class HolidayBDImpl implements HolidayBD {
	private HolidayDAO holidayDAO;
	public HolidayDAO getHolidayDAO() {
		return holidayDAO;
	}
	public void setHolidayDAO(HolidayDAO holidayDAO) {
		this.holidayDAO = holidayDAO;
	}
	
	public List<Holiday> createHoliday(UserSessionConfig userConfig, Date[] dateArr, String reason) throws BasicException {
		return getHolidayDAO().createHoliday(userConfig, dateArr, reason);
	}
	
	public List<Holiday> getAllHoliday(UserSessionConfig userSessionConfig) throws BasicException {
		return getHolidayDAO().getAllHoliday(userSessionConfig);
	}

	public void deleteHoliday(UserSessionConfig userSessionConfig, int recordId) throws BasicException {
		getHolidayDAO().deleteHoliday(userSessionConfig, recordId);
	}
	
	public Holiday getHolidayById(UserSessionConfig userSessionConfig, int recordId) throws BasicException {
		return getHolidayDAO().getHolidayById(userSessionConfig, recordId);
	}
	
	public void updateHoliday(UserSessionConfig userSessionConfig, Holiday holiday) throws BasicException {
		getHolidayDAO().updateHoliday(userSessionConfig, holiday);
	}
	
}
