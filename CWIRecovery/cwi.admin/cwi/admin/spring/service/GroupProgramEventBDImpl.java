package cwi.admin.spring.service;

import java.util.Collection;

import cwi.admin.dao.GroupProgramEventDAO;
import cwi.admin.domain.GroupProgramEvent;
import cwi.admin.spring.GroupProgramEventBD;


public class GroupProgramEventBDImpl implements GroupProgramEventBD {

	private GroupProgramEventDAO groupProgramEventDAO;
	
	
	public GroupProgramEventDAO getGroupProgramEventDAO() {
		return groupProgramEventDAO;
	}
	public void setGroupProgramEventDAO(GroupProgramEventDAO groupProgramEventDAO) {
		this.groupProgramEventDAO = groupProgramEventDAO;
	}

	public void addGruopProgramEvent(GroupProgramEvent groupProgramEvent) {
		getGroupProgramEventDAO().addGruopProgramEvent(groupProgramEvent);
	}

	public void deleteGroupProgramEvent(GroupProgramEvent groupProgramEvent) {
		getGroupProgramEventDAO().deleteGroupProgramEvent(groupProgramEvent);
	}

	public Collection<GroupProgramEvent> getAllGroupProgramEvent() {
		return getGroupProgramEventDAO().getAllGroupProgramEvent();
	}

	public Collection<GroupProgramEvent> getAllGroupProgramEventByGroupProgramId(int groupProgramId) {
		return getGroupProgramEventDAO().getAllGroupProgramEventByGroupProgramId(groupProgramId);
	}

	public GroupProgramEvent getGroupProgramEventById(int groupProgramEventId) {
		return getGroupProgramEventDAO().getGroupProgramEventById(groupProgramEventId);
	}

	public void modifyGroupProgramEvent(GroupProgramEvent groupProgramEvent) {
		getGroupProgramEventDAO().modifyGroupProgramEvent(groupProgramEvent);
	}
}