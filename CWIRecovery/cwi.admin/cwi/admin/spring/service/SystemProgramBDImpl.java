package cwi.admin.spring.service;

import java.util.Collection;

import org.json.JSONArray;

import cwi.admin.dao.SystemProgramDAO;
import cwi.admin.domain.SystemProgram;
import cwi.admin.spring.SystemProgramBD;
import cwi.basic.resourse.BasicException;


public class SystemProgramBDImpl implements SystemProgramBD {

	private SystemProgramDAO systemProgramDAO;
	
	
	public SystemProgramDAO getSystemProgramDAO() {
		return systemProgramDAO;
	}
	public void setSystemProgramDAO(SystemProgramDAO systemProgramDAO) {
		this.systemProgramDAO = systemProgramDAO;
	}

	public void addSystemProgram(SystemProgram systemProgram)throws BasicException {
		getSystemProgramDAO().addSystemProgram(systemProgram);
	}

	public void deleteSystemProgram(SystemProgram systemProgram)throws BasicException {
		getSystemProgramDAO().deleteSystemProgram(systemProgram);
	}

	public Collection<SystemProgram> getAllSystemProgram()throws BasicException {
		return getSystemProgramDAO().getAllSystemProgram();
	}

	public SystemProgram getSystemProgramById(int systemProgramId)throws BasicException {
		return getSystemProgramDAO().getSystemProgramById(systemProgramId);
	}

	public void modifySystemProgram(SystemProgram systemProgram)throws BasicException {
		getSystemProgramDAO().modifySystemProgram(systemProgram);
	}
	public JSONArray getSystemProgramDetailsForLogin(int officerId)throws BasicException {
		return getSystemProgramDAO().getSystemProgramDetailsForLogin(officerId);
	}
}