package cwi.admin.spring.service;

import cwi.admin.dao.BlackListClientDAO;
import cwi.admin.domain.BlackListClientHistory;
import cwi.admin.spring.BlackListClientBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;

public class BlackListClientBDImpl implements BlackListClientBD {
	private BlackListClientDAO blackListClientDAO;
	public BlackListClientDAO getBlackListClientDAO() {
		return blackListClientDAO;
	}
	public void setBlackListClientDAO(BlackListClientDAO blackListClientDAO) {
		this.blackListClientDAO = blackListClientDAO;
	}
	
//	public List<PawnDetailsDTO> getPawnDetailsByClientId(UserSessionConfig userSessionConfig, int clientId) throws BasicException {
//		return getBlackListClientDAO().getPawnDetailsByClientId(userSessionConfig, clientId);
//	}
	
	public int getIsBlackListed(UserSessionConfig userSessionConfig, int clientId) throws BasicException {
		return getBlackListClientDAO().getIsBlackListed(userSessionConfig, clientId);
	}
	
	public void updateBlackListClient(UserSessionConfig userSessionConfig, BlackListClientHistory blackListClientHistory) throws BasicException {
		getBlackListClientDAO().updateBlackListClient(userSessionConfig, blackListClientHistory);
	}
	
}
