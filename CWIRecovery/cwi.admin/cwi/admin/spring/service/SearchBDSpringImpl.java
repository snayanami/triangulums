package cwi.admin.spring.service;

import cwi.admin.dao.SearchDAO;
import cwi.admin.spring.SearchBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.Entity;
import cwi.basic.resourse.SearchEntity;
import cwi.basic.resourse.SearchResult;


public class SearchBDSpringImpl implements SearchBD {

	private SearchDAO searchDAO;
	
	public SearchDAO getSearchDAO() {
		return searchDAO;
	}

	public void setSearchDAO(SearchDAO searchDAO) {
		this.searchDAO = searchDAO;
	}

	public Entity getEntityById(int entityId) throws BasicException {
		return getSearchDAO().getEntityById(entityId);
	}

	public SearchResult getSearchData(SearchEntity searchEntity)throws BasicException {
		return getSearchDAO().getSearchData(searchEntity);
	}

}
