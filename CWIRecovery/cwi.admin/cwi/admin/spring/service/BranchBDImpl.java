package cwi.admin.spring.service;

import java.util.List;

import cwi.admin.dao.BranchDAO;
import cwi.admin.domain.Branch;
import cwi.admin.spring.BranchBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;


public class BranchBDImpl implements BranchBD {

	private BranchDAO branchDAO;
	public BranchDAO getBranchDAO() {
		return branchDAO;
	}
	public void setBranchDAO(BranchDAO branchDAO) {
		this.branchDAO = branchDAO;
	}
	
	public void createBranch(UserSessionConfig userSessionConfig, Branch branch) throws BasicException {
		getBranchDAO().createBranch(userSessionConfig, branch);
	}
	
	public void deleteBranch(UserSessionConfig userSessionConfig, int recordId) throws BasicException {
		getBranchDAO().deleteBranch(userSessionConfig, recordId);
	}
	
	public List<Branch> getAllBranch(UserSessionConfig userSessionConfig) throws BasicException {
		return getBranchDAO().getAllBranch(userSessionConfig);
	}
	
	public Branch getBranchById(UserSessionConfig userSessionConfig, int recordId) throws BasicException {
		return getBranchDAO().getBranchById(userSessionConfig, recordId);
	}
	
	public void updateBranch(UserSessionConfig userSessionConfig, Branch branch) throws BasicException {
		getBranchDAO().updateBranch(userSessionConfig, branch);
	}
	
}