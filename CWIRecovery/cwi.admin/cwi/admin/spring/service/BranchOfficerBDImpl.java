package cwi.admin.spring.service;

import java.util.Collection;
import java.util.List;

import org.json.JSONArray;

import cwi.admin.dao.BranchOfficerDAO;
import cwi.admin.domain.BranchOfficer;
import cwi.admin.domain.Stakeholder;
import cwi.admin.dto.BranchOfficerDTO;
import cwi.admin.spring.BranchOfficerBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;


public class BranchOfficerBDImpl implements BranchOfficerBD {

	private BranchOfficerDAO branchOfficerDAO;
	public BranchOfficerDAO getBranchOfficerDAO() {
		return branchOfficerDAO;
	}
	public void setBranchOfficerDAO(BranchOfficerDAO branchOfficerDAO) {
		this.branchOfficerDAO = branchOfficerDAO;
	}
	
	public void createBranchOfficer(UserSessionConfig userSessionConfig, BranchOfficer branchOfficer) throws BasicException {
		getBranchOfficerDAO().createBranchOfficer(userSessionConfig, branchOfficer);
	}
	
	public void updateBranchOfficer(UserSessionConfig userSessionConfig, BranchOfficer branchOfficer) throws BasicException {
		getBranchOfficerDAO().updateBranchOfficer(userSessionConfig, branchOfficer);
	}
	
	public void deleteBranchOfficer(UserSessionConfig userSessionConfig, int recordId) throws BasicException {
		getBranchOfficerDAO().deleteBranchOfficer(userSessionConfig, recordId);
	}
	
	public List<BranchOfficer> getAllBranchOfficerByOfficerId(UserSessionConfig userSessionConfig, int officerId) throws BasicException {
		return getBranchOfficerDAO().getAllBranchOfficerByOfficerId(userSessionConfig, officerId);
	}
	
	public BranchOfficer getBranchOfficerById(UserSessionConfig userSessionConfig, int recordId) throws BasicException {
		return getBranchOfficerDAO().getBranchOfficerById(userSessionConfig, recordId);
	}
	
	
	

	public void addBranchOfficer(BranchOfficer branchOfficer)throws BasicException {
		getBranchOfficerDAO().addBranchOfficer(branchOfficer);
	}

	public void deleteBranchOfficer(BranchOfficer branchOfficer)throws BasicException {
		getBranchOfficerDAO().deleteBranchOfficer(branchOfficer);
	}

	public Collection<BranchOfficerDTO> getAllBranchOfficer()throws BasicException {
		return getBranchOfficerDAO().getAllBranchOfficer();
	}

	public BranchOfficer getBranchOfficerById(int branchOfficerId)throws BasicException {
		return getBranchOfficerDAO().getBranchOfficerById(branchOfficerId);
	}

	public void modifyBranchOfficer(BranchOfficer branchOfficer)throws BasicException {
		getBranchOfficerDAO().modifyBranchOfficer(branchOfficer);
	}
	
	public Collection<BranchOfficer> getAllBranchOfficerByBranchId(int branchId)throws BasicException {
		return getBranchOfficerDAO().getAllBranchOfficerByBranchId(branchId);
	}
	
	public Stakeholder getBranchOfficerDetailsById(int branchOfficerId) throws BasicException {
		return getBranchOfficerDAO().getBranchOfficerDetailsById(branchOfficerId);
	}
	
	public boolean isBranchOfficerExists(int branchId, int officerId)throws BasicException {
		return getBranchOfficerDAO().isBranchOfficerExists(branchId, officerId);
	}
	
	
	
	
}