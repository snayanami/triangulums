package cwi.admin.spring.service;

import java.util.List;

import cwi.admin.dao.RefProgramDAO;
import cwi.admin.domain.RefProgram;
import cwi.admin.spring.RefProgramBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;



public class RefProgramBDImpl implements RefProgramBD {

	private RefProgramDAO refProgramDAO;
	
	public RefProgramDAO getRefProgramDAO() {
		return refProgramDAO;
	}
	public void setRefProgramDAO(RefProgramDAO refProgramDAO) {
		this.refProgramDAO = refProgramDAO;
	}

	public void createRefProgram(UserSessionConfig userSessionConfig, RefProgram refProgram) throws BasicException {
		getRefProgramDAO().createRefProgram(userSessionConfig, refProgram);
	}

	public void deleteRefProgram(UserSessionConfig userSessionConfig, int refProgramId) throws BasicException {
		getRefProgramDAO().deleteRefProgram(userSessionConfig, refProgramId);
	}

	public List<RefProgram> getAllReferenceProgram(UserSessionConfig userSessionConfig) throws BasicException {
		return getRefProgramDAO().getAllReferenceProgram(userSessionConfig);
	}

	public RefProgram getReferenceProgramById(UserSessionConfig userSessionConfig, int recordId) throws BasicException {
		return getRefProgramDAO().getReferenceProgramById(userSessionConfig, recordId);
	}

	public void updateRefProgram(UserSessionConfig userSessionConfig, RefProgram refProgram) throws BasicException {
		getRefProgramDAO().updateRefProgram(userSessionConfig, refProgram);
	}

}
