package cwi.admin.spring.service;

import java.util.Collection;
import java.util.List;

import cwi.admin.dao.StakeholderTypeMappingDAO;
import cwi.admin.domain.StakeholderTypeMapping;
import cwi.admin.spring.StakeholderTypeMappingBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;


public class StakeholderTypeMappingBDImpl implements StakeholderTypeMappingBD {

	private StakeholderTypeMappingDAO stakeholderTypeMappingDAO;
	public StakeholderTypeMappingDAO getStakeholderTypeMappingDAO() {
		return stakeholderTypeMappingDAO;
	}
	public void setStakeholderTypeMappingDAO(StakeholderTypeMappingDAO stakeholderTypeMappingDAO) {
		this.stakeholderTypeMappingDAO = stakeholderTypeMappingDAO;
	}
	
	public void createStakeholderTypeMapping(UserSessionConfig userSessionConfig, StakeholderTypeMapping stakeholderTypeMapping) throws BasicException {
		getStakeholderTypeMappingDAO().createStakeholderTypeMapping(userSessionConfig, stakeholderTypeMapping);
	}
	
	public void deleteStakeholderTypeMapping(UserSessionConfig userSessionConfig, int recordId) throws BasicException {
		getStakeholderTypeMappingDAO().deleteStakeholderTypeMapping(userSessionConfig, recordId);
	}
	
	public List<StakeholderTypeMapping> getAllStakeholderTypeMppingByStakeholderId(UserSessionConfig userSessionConfig, int stakeholderId) throws BasicException {
		return getStakeholderTypeMappingDAO().getAllStakeholderTypeMppingByStakeholderId(userSessionConfig, stakeholderId);
	}

	
}