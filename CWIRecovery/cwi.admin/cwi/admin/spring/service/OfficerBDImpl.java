package cwi.admin.spring.service;

import java.util.Collection;
import java.util.List;

import cwi.admin.dao.OfficerDAO;
import cwi.admin.domain.Branch;
import cwi.admin.domain.Officer;
import cwi.admin.domain.Stakeholder;
import cwi.admin.spring.OfficerBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;


public class OfficerBDImpl implements OfficerBD {

	private OfficerDAO officerDAO;
	public OfficerDAO getOfficerDAO() {
		return officerDAO;
	}
	public void setOfficerDAO(OfficerDAO officerDAO) {
		this.officerDAO = officerDAO;
	}
	
	public void createOfficer(UserSessionConfig userSessionConfig, Officer officer) throws BasicException {
		getOfficerDAO().createOfficer(userSessionConfig, officer);
	}
	
	public List<Officer> getAllOfficer(UserSessionConfig userSessionConfig) throws BasicException {
		return getOfficerDAO().getAllOfficer(userSessionConfig);
	}
	
	public Officer getOfficerById(UserSessionConfig userSessionConfig, int recordId) throws BasicException {
		return getOfficerDAO().getOfficerById(userSessionConfig, recordId);
	}
	
	public void updateOfficer(UserSessionConfig userSessionConfig, Officer officer) throws BasicException {
		getOfficerDAO().updateOfficer(userSessionConfig, officer);
	}
	
	public List<Branch> getAllBranchForOfficerId(UserSessionConfig userSessionConfig, int officerId) throws BasicException {
		return getOfficerDAO().getAllBranchForOfficerId(userSessionConfig, officerId);
	}
	public Branch getDefaultBranchByOfficerId(UserSessionConfig userSessionConfig, int officerId) throws BasicException {
		return getOfficerDAO().getDefaultBranchByOfficerId(userSessionConfig, officerId);
	}

	
}