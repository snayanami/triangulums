package cwi.admin.spring.service;

import java.util.List;

import cwi.admin.dao.UserAccessRightsDAO;
import cwi.admin.dto.SystemTreeDTO;
import cwi.admin.spring.UserAccessRightsBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;

public class UserAccessRightsBDImpl implements UserAccessRightsBD {
	private UserAccessRightsDAO userAccessRightsDAO;
	public UserAccessRightsDAO getUserAccessRightsDAO() {
		return userAccessRightsDAO;
	}
	public void setUserAccessRightsDAO(UserAccessRightsDAO userAccessRightsDAO) {
		this.userAccessRightsDAO = userAccessRightsDAO;
	}
	
	public List<SystemTreeDTO> getSystemPrograms(UserSessionConfig userSessionConfig) throws BasicException {
		return getUserAccessRightsDAO().getSystemPrograms(userSessionConfig);
	}
	
	public List<SystemTreeDTO> getGroupSystemPrograms(UserSessionConfig userSessionConfig, int accessGroupId) throws BasicException {
		return getUserAccessRightsDAO().getGroupSystemPrograms(userSessionConfig, accessGroupId);
	}
	
	public void createAccessRights(UserSessionConfig userSessionConfig, int accessGroupId, String dataString) throws BasicException {
		getUserAccessRightsDAO().createAccessRights(userSessionConfig, accessGroupId, dataString);
	}
	
}
