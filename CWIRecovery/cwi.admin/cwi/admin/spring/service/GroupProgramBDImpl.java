package cwi.admin.spring.service;

import java.util.Collection;

import cwi.admin.dao.GroupProgramDAO;
import cwi.admin.domain.GroupProgram;
import cwi.admin.spring.GroupProgramBD;
import cwi.basic.resourse.BasicException;


public class GroupProgramBDImpl implements GroupProgramBD {

	private GroupProgramDAO groupProgramDAO;
	
	
	public GroupProgramDAO getGroupProgramDAO() {
		return groupProgramDAO;
	}
	public void setGroupProgramDAO(GroupProgramDAO groupProgramDAO) {
		this.groupProgramDAO = groupProgramDAO;
	}

	public void addGroupProgram(GroupProgram groupProgram)throws BasicException {
		getGroupProgramDAO().addGroupProgram(groupProgram);
	}

	public void deleteGroupProgram(GroupProgram groupProgram)throws BasicException {
		getGroupProgramDAO().deleteGroupProgram(groupProgram);
	}

	public Collection<GroupProgram> getAllGroupProgram() throws BasicException {
		return getGroupProgramDAO().getAllGroupProgram();
	}

	public Collection<GroupProgram> getAllGroupProgramByGroupId(int groupId)throws BasicException {
		return getGroupProgramDAO().getAllGroupProgramByGroupId(groupId);
	}

	public GroupProgram getGroupProgramById(int groupProgramId)throws BasicException {
		return getGroupProgramDAO().getGroupProgramById(groupProgramId);
	}

	public void modifyGroupProgram(GroupProgram groupProgram)throws BasicException {
		getGroupProgramDAO().modifyGroupProgram(groupProgram);
	}
}