package cwi.admin.spring.service;

import cwi.admin.dao.ChangePasswordDAO;
import cwi.admin.domain.Officer;
import cwi.admin.spring.ChangePasswordBD;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;

public class ChangePasswordBDImpl implements ChangePasswordBD {
	private ChangePasswordDAO changePasswordDAO;
	public ChangePasswordDAO getChangePasswordDAO() {
		return changePasswordDAO;
	}
	public void setChangePasswordDAO(ChangePasswordDAO changePasswordDAO) {
		this.changePasswordDAO = changePasswordDAO;
	}
	
	public void changePassword(UserSessionConfig userSessionConfig, int userId, String currentPassword, String newPassword) throws BasicException {
		getChangePasswordDAO().changePassword(userSessionConfig, userId, currentPassword, newPassword);
	}
}
