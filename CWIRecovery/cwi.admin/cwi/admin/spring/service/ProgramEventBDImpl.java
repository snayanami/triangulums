package cwi.admin.spring.service;

import java.util.Collection;

import cwi.admin.dao.ProgramEventDAO;
import cwi.admin.domain.ProgramEvent;
import cwi.admin.spring.ProgramEventBD;
import cwi.basic.resourse.BasicException;


public class ProgramEventBDImpl implements ProgramEventBD {

	private ProgramEventDAO programEventDAO;
		
	public ProgramEventDAO getProgramEventDAO() {
		return programEventDAO;
	}
	public void setProgramEventDAO(ProgramEventDAO programEventDAO) {
		this.programEventDAO = programEventDAO;
	}

	public void addProgramEvent(ProgramEvent programEvent)throws BasicException {
		getProgramEventDAO().addProgramEvent(programEvent);
	}

	public void deleteProgramEvent(ProgramEvent programEvent)throws BasicException {
		getProgramEventDAO().deleteProgramEvent(programEvent);
	}

	public Collection<ProgramEvent> getAllProgramEvent() throws BasicException {
		return getProgramEventDAO().getAllProgramEvent();
	}

	public Collection<ProgramEvent> getAllProgramEventByProgramId(int programId)throws BasicException {
		return getProgramEventDAO().getAllProgramEventByProgramId(programId);
	}

	public ProgramEvent getProgramEventById(int programEventId)throws BasicException {
		return getProgramEventDAO().getProgramEventById(programEventId);
	}

	public void modifyProgramEvent(ProgramEvent programEvent)throws BasicException {
		getProgramEventDAO().modifyProgramEvent(programEvent);
	}
}