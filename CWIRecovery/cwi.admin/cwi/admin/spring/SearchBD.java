package cwi.admin.spring;

import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.Entity;
import cwi.basic.resourse.SearchEntity;
import cwi.basic.resourse.SearchResult;



public interface SearchBD {
    public Entity getEntityById(int entityId)throws BasicException;
    public SearchResult getSearchData(SearchEntity searchEntity)throws BasicException;	
}
