package cwi.admin.spring;

import java.util.Collection;
import java.util.List;

import cwi.admin.domain.RefProgramData;
import cwi.admin.domain.Stakeholder;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;



public interface StakeholderBD {
	
	public void createStakeholder(UserSessionConfig userSessionConfig, Stakeholder stakeholder, int stakeholderTypeId) throws BasicException;
	public void updateStakeholder(UserSessionConfig userSessionConfig, Stakeholder stakeholder) throws BasicException;
	public Stakeholder getStakeholderById(UserSessionConfig userSessionConfig, int stakeholderId) throws BasicException;
	public List<Stakeholder> getAllStakeholder(UserSessionConfig userSessionConfig) throws BasicException;
	
	
	public List<RefProgramData> getAllTitles(UserSessionConfig userSessionConfig) throws BasicException;

	public void addStakeholder(Stakeholder stakeholder) throws BasicException;
	public void deleteStakeholder(Stakeholder stakeholder) throws BasicException;
	public void modifyStakeholder(Stakeholder stakeholder) throws BasicException;
	public Collection<Stakeholder> getAllStakeholder() throws BasicException;
	
}
