package cwi.admin.spring;

import org.json.JSONArray;

import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;


public interface LoginBD {
	
	public UserSessionConfig systemLogin(String userName, String password) throws BasicException;
	public JSONArray getSystemProgramsForLogin(UserSessionConfig sessionConfig, int officerId) throws BasicException;
	public JSONArray getSystemProgramsForBranchChange(UserSessionConfig sessionConfig, int officerId, int branchId) throws BasicException;
	public String getGoldValueListString() throws BasicException;;
}
