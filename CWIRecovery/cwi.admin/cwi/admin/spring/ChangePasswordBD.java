package cwi.admin.spring;

import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;

public interface ChangePasswordBD {
	public void changePassword(UserSessionConfig userSessionConfig, int userId, String currentPassword, String newPassword) throws BasicException;
}
