package cwi.admin.spring;

import java.util.Collection;
import java.util.List;

import cwi.admin.domain.Branch;
import cwi.admin.domain.Officer;
import cwi.admin.domain.Stakeholder;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;



public interface OfficerBD {
	public void createOfficer(UserSessionConfig userSessionConfig, Officer officer) throws BasicException;
	public void updateOfficer(UserSessionConfig userSessionConfig, Officer officer) throws BasicException;
	public Officer getOfficerById(UserSessionConfig userSessionConfig,int recordId) throws BasicException;
	public List<Officer> getAllOfficer(UserSessionConfig userSessionConfig) throws BasicException;
	public List<Branch> getAllBranchForOfficerId(UserSessionConfig userSessionConfig, int officerId) throws BasicException;
	public Branch getDefaultBranchByOfficerId(UserSessionConfig userSessionConfig, int officerId) throws BasicException;
}
