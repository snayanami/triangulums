package cwi.admin.spring;

import java.util.List;

import cwi.admin.domain.RefProgramData;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;



public interface RefProgramDataBD {

	public void createRefProgramData(UserSessionConfig userSessionConfig, RefProgramData refProgramData) throws BasicException;
	public void updateRefProgramData(UserSessionConfig userSessionConfig, RefProgramData refProgramData) throws BasicException;
	public void deleteRefProgramData(UserSessionConfig userSessionConfig, int recordId) throws BasicException;
	public List<RefProgramData> getAllRefProgramDataByRefProgramId(UserSessionConfig userSessionConfig, int refProgramId) throws BasicException;
	public List<RefProgramData> getAllRefProgramData(UserSessionConfig userSessionConfig) throws BasicException;
	public RefProgramData getRefProgramDataById(UserSessionConfig userSessionConfig, int recordId) throws BasicException;
}
