package cwi.admin.spring;

import java.util.Date;
import java.util.List;

import cwi.admin.domain.Holiday;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;


public interface HolidayBD {
	public List<Holiday> createHoliday(UserSessionConfig userConfig, Date dateArr[], String reason)throws BasicException;
	public List<Holiday> getAllHoliday(UserSessionConfig userSessionConfig)throws BasicException;
	public void updateHoliday(UserSessionConfig userSessionConfig, Holiday holiday)throws BasicException;
	public void deleteHoliday(UserSessionConfig userSessionConfig, int recordId)throws BasicException;
	public Holiday getHolidayById(UserSessionConfig userSessionConfig,int recordId)throws BasicException;
}
