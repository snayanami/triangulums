package cwi.admin.spring;

import java.util.Collection;
import java.util.List;

import org.json.JSONArray;

import cwi.admin.domain.BranchOfficer;
import cwi.admin.domain.Stakeholder;
import cwi.admin.dto.BranchOfficerDTO;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;



public interface BranchOfficerBD {
	
	public void createBranchOfficer(UserSessionConfig userSessionConfig, BranchOfficer branchOfficer) throws BasicException;
	public void updateBranchOfficer(UserSessionConfig userSessionConfig, BranchOfficer branchOfficer) throws BasicException;
	public void deleteBranchOfficer(UserSessionConfig userSessionConfig, int recordId) throws BasicException;
	public List<BranchOfficer> getAllBranchOfficerByOfficerId(UserSessionConfig userSessionConfig, int officerId) throws BasicException;
	public BranchOfficer getBranchOfficerById(UserSessionConfig userSessionConfig, int recordId) throws BasicException;
	
	

	public void addBranchOfficer(BranchOfficer branchOfficer) throws BasicException;
	public void deleteBranchOfficer(BranchOfficer branchOfficer) throws BasicException;
	public void modifyBranchOfficer(BranchOfficer branchOfficer) throws BasicException;
	public Collection<BranchOfficerDTO> getAllBranchOfficer() throws BasicException;
	public BranchOfficer getBranchOfficerById(int branchOfficerId) throws BasicException;
	public Collection<BranchOfficer> getAllBranchOfficerByBranchId(int branchId)throws BasicException;
	public Stakeholder getBranchOfficerDetailsById(int branchOfficerId)throws BasicException;
	public boolean isBranchOfficerExists(int branchId,int officerId)throws BasicException;
}

