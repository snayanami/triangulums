package cwi.admin.spring;

import java.util.Collection;
import java.util.List;

import cwi.admin.domain.Branch;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;



public interface BranchBD {
	public void createBranch(UserSessionConfig userSessionConfig, Branch branch) throws BasicException;
	public void updateBranch(UserSessionConfig userSessionConfig, Branch branch) throws BasicException;
	public void deleteBranch(UserSessionConfig userSessionConfig, int recordId) throws BasicException;
	public Branch getBranchById(UserSessionConfig userSessionConfig, int recordId) throws BasicException;
	public List<Branch> getAllBranch(UserSessionConfig userSessionConfig) throws BasicException;
}
