package cwi.admin.spring;

import java.util.Collection;

import cwi.admin.dao.GroupProgramDAO;
import cwi.admin.domain.GroupProgram;
import cwi.basic.resourse.BasicException;



public interface GroupProgramBD {

	public void addGroupProgram(GroupProgram groupProgram) throws BasicException;
	public void deleteGroupProgram(GroupProgram groupProgram) throws BasicException;
	public void modifyGroupProgram(GroupProgram groupProgram) throws BasicException;
	public GroupProgram getGroupProgramById(int groupProgramId) throws BasicException;
	public Collection<GroupProgram> getAllGroupProgram() throws BasicException;
	public Collection<GroupProgram> getAllGroupProgramByGroupId(int groupId) throws BasicException;
}
