package cwi.admin.spring;

import java.util.List;

import cwi.admin.dto.SystemTreeDTO;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;

public interface UserAccessRightsBD {
	public List<SystemTreeDTO> getSystemPrograms(UserSessionConfig userSessionConfig)throws BasicException;
	public List<SystemTreeDTO> getGroupSystemPrograms(UserSessionConfig userSessionConfig, int accessGroupId)throws BasicException;
	public void createAccessRights(UserSessionConfig userSessionConfig, int accessGroupId, String dataString)throws BasicException;
}
