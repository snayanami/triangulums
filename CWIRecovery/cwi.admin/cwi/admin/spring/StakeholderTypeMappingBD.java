package cwi.admin.spring;

import java.util.Collection;
import java.util.List;

import cwi.admin.domain.StakeholderTypeMapping;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;



public interface StakeholderTypeMappingBD {

	public void createStakeholderTypeMapping(UserSessionConfig userSessionConfig, StakeholderTypeMapping stakeholderTypeMapping) throws BasicException;
	public void deleteStakeholderTypeMapping(UserSessionConfig userSessionConfig, int recordId) throws BasicException;
	public List<StakeholderTypeMapping> getAllStakeholderTypeMppingByStakeholderId(UserSessionConfig userSessionConfig, int stakeholderId) throws BasicException;
}
