package cwi.admin.spring;

import java.util.Collection;

import org.json.JSONArray;

import cwi.admin.domain.SystemProgram;
import cwi.basic.resourse.BasicException;



public interface SystemProgramBD {

	public void addSystemProgram(SystemProgram systemProgram) throws BasicException;
	public void deleteSystemProgram(SystemProgram systemProgram) throws BasicException;
	public void modifySystemProgram(SystemProgram systemProgram) throws BasicException;
	public Collection<SystemProgram> getAllSystemProgram() throws BasicException;
	public SystemProgram getSystemProgramById(int systemProgramId) throws BasicException;
	public JSONArray getSystemProgramDetailsForLogin(int officerId) throws BasicException;
}
