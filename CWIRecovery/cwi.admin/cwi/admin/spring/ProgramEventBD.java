package cwi.admin.spring;

import java.util.Collection;

import cwi.admin.domain.ProgramEvent;
import cwi.basic.resourse.BasicException;



public interface ProgramEventBD {

	public void addProgramEvent(ProgramEvent programEvent)throws BasicException;
	public void deleteProgramEvent(ProgramEvent programEvent)throws BasicException;
	public void modifyProgramEvent(ProgramEvent programEvent)throws BasicException;
	public ProgramEvent getProgramEventById(int programEventId)throws BasicException;
	public Collection<ProgramEvent> getAllProgramEvent()throws BasicException;
	public Collection<ProgramEvent> getAllProgramEventByProgramId(int programId)throws BasicException;
}
