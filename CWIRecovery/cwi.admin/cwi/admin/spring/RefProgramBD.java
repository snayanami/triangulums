package cwi.admin.spring;

import java.util.Collection;
import java.util.List;

import cwi.admin.domain.RefProgram;
import cwi.basic.resourse.BasicException;
import cwi.basic.resourse.UserSessionConfig;



public interface RefProgramBD {

	public void createRefProgram(UserSessionConfig userSessionConfig, RefProgram refProgram) throws BasicException;
	public void deleteRefProgram(UserSessionConfig userSessionConfig, int refProgramId) throws BasicException;
	public void updateRefProgram(UserSessionConfig userSessionConfig, RefProgram refProgram) throws BasicException;
	public List<RefProgram> getAllReferenceProgram(UserSessionConfig userSessionConfig) throws BasicException;
	public RefProgram getReferenceProgramById(UserSessionConfig userSessionConfig, int recordId) throws BasicException;
}
